/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessor.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Linker/LinkerTool.h"
#include "VPDet/DeVP.h"

/** @class VPTrackEff
 *
 * Class for Velo track monitoring Ntuple
 *  @author T. Bird
 *  @date   2013-10-02
 */

class VPTrackEff
    : public Gaudi::Functional::Consumer<void( const LHCb::MCParticles&, const LHCb::MCHits&, const DeVP& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeVP>> {

public:
  /// Standard constructor
  VPTrackEff( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm execution
  void operator()( const LHCb::MCParticles&, const LHCb::MCHits&, const DeVP& ) const override;
  /// Algorithm initialization
  StatusCode initialize() override;

private:
  const LHCb::MCVertex* findMCOriginVertex( const LHCb::MCParticle& particle, const double decaylengthtolerance );

  Gaudi::Property<std::string> m_trackLocation{this, "TrackLocation", LHCb::TrackLocation::Velo};
};

DECLARE_COMPONENT( VPTrackEff )

namespace {
  //=============================================================================
  // Walk up the decay tree of a particle and determine its origin vertex.
  //=============================================================================
  const LHCb::MCVertex* findMCOriginVertex( const LHCb::MCParticle& particle,
                                            const double            decaylengthtolerance = 1.e-3 ) {

    const LHCb::MCVertex* ov = particle.originVertex();
    if ( !ov ) return ov;
    const LHCb::MCParticle* mother = ov->mother();
    if ( mother && mother != &particle ) {
      const LHCb::MCVertex* mov = mother->originVertex();
      if ( !mov ) return ov;
      const double d = ( mov->position() - ov->position() ).R();
      if ( mov == ov || d < decaylengthtolerance ) { ov = findMCOriginVertex( *mother, decaylengthtolerance ); }
    }
    return ov;
  }

  //=============================================================================
  // Return the origin vertex type of the mother particle.
  //=============================================================================
  int mcMotherType( const LHCb::MCParticle& particle ) {

    const LHCb::MCVertex& vertex = findMCOriginVertex( particle );

    int rc( -1 );
    if ( vertex.isPrimary() )
      rc = 0;
    else if ( const LHCb::MCParticle* mother = vertex.mother(); vertex.mother() ) {
      if ( mother->particleID().hasBottom() )
        rc = 3;
      else if ( mother->particleID().hasCharm() )
        rc = 2;
      else if ( mother->particleID().hasStrange() )
        rc = 1;
      else
        rc = 4;
    }
    return rc;
  }

  //=============================================================================
  // Return the origin vertex type of a particle.
  //=============================================================================
  int mcVertexType( const LHCb::MCParticle& particle ) {
    const LHCb::MCVertex& vertex = findMCOriginVertex( particle );
    return vertex.type();
  }
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPTrackEff::VPTrackEff( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"MCparticleLocation", LHCb::MCParticleLocation::Default},
                KeyValue{"MCHitLocation", LHCb::MCHitLocation::VP}, KeyValue{"DeVPLocation", DeVPLocation::Default}}} {}

//=============================================================================
// Initialization.
//=============================================================================
StatusCode VPTrackEff::initialize() {
  return Consumer::initialize().andThen( [&] { setHistoTopDir( "VP/" ); } );
}

//=============================================================================
// Execute
//=============================================================================
void VPTrackEff::operator()( const LHCb::MCParticles& particles, const LHCb::MCHits& hits, const DeVP& det ) const {

  LinkerTool<LHCb::Track, LHCb::MCParticle>               linker( evtSvc(), m_trackLocation );
  LinkerTool<LHCb::Track, LHCb::MCParticle>::DirectType*  directLinker  = linker.direct();
  LinkerTool<LHCb::Track, LHCb::MCParticle>::InverseType* inverseLinker = linker.inverse();

  // Create a map from all MCParticles to MCHits
  std::map<const LHCb::MCParticle*, std::vector<const LHCb::MCHit*>> mchitmap;
  for ( const LHCb::MCParticle* particle : particles ) { mchitmap[particle] = std::vector<const LHCb::MCHit*>(); }

  // First collect hits
  for ( const LHCb::MCHit* hit : hits ) {
    if ( !hit ) {
      warning() << "MCHit shouldn't be NULL!" << endmsg;
      continue;
    }
    // Skip hits not linked to a particle.
    if ( !hit->mcParticle() ) continue;
    const double z = hit->entry().z();
    if ( z < -300 * Gaudi::Units::mm || z < 800 * Gaudi::Units::mm ) { continue; }
    bool inAA( false );
    det.runOnAllSensors( [&hit, &inAA]( const DeVPSensor& sensor ) {
      if ( inAA ) return;
      if ( !sensor.isInside( hit->midPoint() ) ) return;
      inAA = sensor.isInActiveArea( sensor.globalToLocal( hit->midPoint() ) );
    } );
    if ( inAA ) { mchitmap[hit->mcParticle()].push_back( hit ); }
  }

  Tuple tuple = nTuple( "VPAcceptance", "" );

  for ( const LHCb::MCParticle* particle : particles ) {
    const bool forward = particle->momentum().z() > 0.;
    const int  ntracks = int( inverseLinker->relations( particle ).size() );

    // Determine origin vertex, decay vertex, and primary vertex.
    double          fd = -1.;
    Gaudi::XYZPoint ov( -99999., -99999., -99999. );
    Gaudi::XYZPoint dv( -99999., -99999., -99999. );
    Gaudi::XYZPoint pv( -99999., -99999., -99999. );

    if ( particle->endVertices().size() > 0 ) { dv = particle->endVertices().front()->position(); }
    if ( particle->originVertex() != 0x0 ) {
      ov = particle->originVertex()->position();
      if ( particle->endVertices().size() > 0 ) {
        fd = ( dv - ov ).R();
      } else {
        fd = 99999.;
      }
    }
    if ( particle->primaryVertex() != 0x0 ) { pv = particle->primaryVertex()->position(); }

    Gaudi::XYZPoint firsthit( 99999., 99999., 99999. );
    if ( !forward ) firsthit = Gaudi::XYZPoint( 99999., 99999., -99999. );
    int nmcparts( -999 );
    int nhits( 0 );
    int nhitsleft( 0 );
    int nhitsright( 0 );
    if ( ntracks > 0 ) {
      const LHCb::Track* track = inverseLinker->relations( particle ).begin()->to();
      nmcparts                 = int( directLinker->relations( track ).size() );

      for ( const LHCb::LHCbID& lhcbID : track->lhcbIDs() ) {
        if ( !track->isOnTrack( lhcbID ) ) continue;
        if ( !lhcbID.isVP() ) continue;
        const DeVPSensor& sensor = det.sensor( lhcbID.vpID() );
        ++nhits;
        if ( sensor.isLeft() ) {
          ++nhitsleft;
        } else if ( sensor.isRight() ) {
          ++nhitsright;
        }

        Gaudi::XYZPoint currenthit( sensor.channelToGlobalPoint( lhcbID.vpID() ) );
        if ( forward ) {
          if ( firsthit.z() > currenthit.z() ) firsthit = currenthit;
        } else {
          if ( firsthit.z() < currenthit.z() ) firsthit = currenthit;
        }
      }
    } // ntracks > 0

    tuple->column( "p", particle->p() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pt", particle->pt() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pid", particle->particleID().pid() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "threecharge", particle->particleID().threeCharge() )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvx", pv.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvy", pv.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvz", pv.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovx", ov.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovy", ov.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovz", ov.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvx", dv.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvy", dv.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvz", dv.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhx", firsthit.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhy", firsthit.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhz", firsthit.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhr", firsthit.Rho() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhphi", firsthit.phi() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fd", fd ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "eta", particle->momentum().eta() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "phi", particle->momentum().phi() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "forward", forward ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "frompv", int( particle->originVertex()->isPrimary() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nendvertex", int( particle->endVertices().size() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ntracks", ntracks ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhits", nhits ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhitsleft", nhitsleft ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhitsright", nhitsright ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nmchits", int( mchitmap.find( particle )->second.size() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nmcparts", nmcparts ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "mcmothertype", mcMotherType( *particle ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "mcvertextype", mcVertexType( *particle ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}
