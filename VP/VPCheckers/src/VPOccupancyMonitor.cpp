/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessor.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/VPCluster.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/VPConstants.h"
#include "VPDet/DeVP.h"

/** @class VPOccupancyMonitor VPOccupancyMonitor.h
 *
 *
 */

class VPOccupancyMonitor
    : public Gaudi::Functional::Consumer<void( const LHCb::VPClusters&, const DeVP& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeVP>> {
public:
  /// Standard constructor
  VPOccupancyMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization

  void operator()( const LHCb::VPClusters&, const DeVP& ) const override;

private:
  Gaudi::Property<unsigned int> m_station{this, "Station", 10};
};

DECLARE_COMPONENT( VPOccupancyMonitor )

//=============================================================================
// Standard constructor
//=============================================================================
VPOccupancyMonitor::VPOccupancyMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Default},
                KeyValue{"DeVPLocation", DeVPLocation::Default}}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPOccupancyMonitor::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&] {
    // Set histo dir
    setHistoTopDir( "VP/" );
    // Get detector element
    auto det = getDet<DeVP>( inputLocation<DeVP>() );
    det->runOnAllSensors( [this]( const DeVPSensor& sensor ) {
      if ( sensor.station() != m_station.value() ) return;
      const unsigned int sn = sensor.sensorNumber();
      for ( unsigned int i = 0; i != VP::NChipsPerSensor; ++i ) {
        for ( unsigned int j = 0; j != VP::NColumns; ++j ) {
          for ( unsigned int k = 0; k != VP::NRows; ++k ) {
            LHCb::VPChannelID id( sn, i, j, k );
            Gaudi::XYZPoint   p = sensor.channelToGlobalPoint( id );
            plot( p.rho(), "ChannelsPerBin", 5.1, 40.1, 500 );
          }
        }
      }
    } );
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void VPOccupancyMonitor::operator()( const LHCb::VPClusters& clusters, const DeVP& det ) const {

  // Loop over the clusters.
  for ( const LHCb::VPCluster* cluster : clusters ) {
    // Check if the cluster is on the station we are interested in.
    LHCb::VPChannelID id = cluster->channelID();
    if ( id.station() != m_station.value() ) continue;
    // Fill the cluster occupancy histogram.
    Gaudi::XYZPoint point( cluster->x(), cluster->y(), cluster->z() );
    plot( point.rho(), "Clusters", 5.1, 40.1, 500 );
    // Get the sensor.
    const DeVPSensor& sensor = det.sensor( id );
    // Loop over the pixels in the cluster.
    for ( auto pixel : cluster->pixels() ) {
      point = sensor.channelToGlobalPoint( pixel );
      // Fill the pixel occupancy histogram.
      plot( point.rho(), "Pixels", 5.1, 40.1, 500 );
    }
  }
}
