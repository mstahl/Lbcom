/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include <Associators/Location.h>

/** @class VPClusterLinker VPClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC hits, based on the association tables for
 * individual pixels produced by VPDigitLinker.
 */

class VPCluster2MCHitLinker
    : public Gaudi::Functional::Linker<LHCb::LinksByKey( const LHCb::VPClusters&, const LHCb::LinksByKey& )> {
public:
  // Standard constructor
  VPCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::LinksByKey operator()( const LHCb::VPClusters& clusters,
                               const LHCb::LinksByKey& digitLinks ) const override; // < Algorithm execution
};

DECLARE_COMPONENT( VPCluster2MCHitLinker )

//=============================================================================
// Constructor
//=============================================================================
VPCluster2MCHitLinker::VPCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : Linker( name, pSvcLocator,
              {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Default},
               KeyValue{"VPDigit2MCHitLinksLocation", Links::location( LHCb::VPDigitLocation::Default + "2MCHits" )}},
              KeyValue{"OutputLocation", Links::location( LHCb::VPClusterLocation::Default + "2MCHits" )} ) {}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPCluster2MCHitLinker::operator()( const LHCb::VPClusters& clusters,
                                                    const LHCb::LinksByKey& bareDigitLinks ) const {
  // Get the association table between digits and hits.
  auto digitLinks = inputLinks<LHCb::VPDigit, LHCb::MCHit>( bareDigitLinks );

  // Create an association table between hits and clusters.
  auto output = outputLinks<LHCb::MCHit>();
  // Loop over the clusters.

  for ( const LHCb::VPCluster* cluster : clusters ) {
    std::map<const LHCb::MCHit*, double> hitMap;
    double                               sum = 0.;
    // Loop over all pixels in a given cluster
    for ( const auto& pixel : cluster->pixels() ) {
      // Get the MCHit pointer from the associator
      const auto& range = digitLinks.from( pixel );
      for ( const auto& entry : range ) {
        const double weight = entry.weight();
        hitMap[entry.to()] += weight;
        sum += weight;
      }
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Got " << range.size() << " MCHits from associator." << endmsg; }
    }
    if ( sum < 1.e-2 ) continue;
    for ( auto [hit, weight] : hitMap ) output.link( cluster, hit, weight / sum );
  }
  return output;
}
