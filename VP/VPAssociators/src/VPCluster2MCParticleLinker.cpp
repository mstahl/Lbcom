/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPLightCluster.h"
#include <Associators/Location.h>

/** @class VPClusterLinker VPClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC particles, based on the association tables
 * for individual pixels produced by VPDigitLinker.
 */

struct VPCluster2MCParticleLinker
    : Gaudi::Functional::Linker<LHCb::LinksByKey( const std::vector<LHCb::VPLightCluster>&, const LHCb::LinksByKey& )> {
  VPCluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  LHCb::LinksByKey operator()( const std::vector<LHCb::VPLightCluster>& clusters,
                               const LHCb::LinksByKey& digitLinks ) const override; // < Algorithm execution
};

DECLARE_COMPONENT( VPCluster2MCParticleLinker )

//=============================================================================
// Constructor
//=============================================================================
VPCluster2MCParticleLinker::VPCluster2MCParticleLinker( const std::string& name, ISvcLocator* svcLoc )
    : Linker( name, svcLoc,
              {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light},
               KeyValue{"VPDigit2MCParticleLinksLocation", Links::location( LHCb::VPDigitLocation::Default )}},
              KeyValue{"OutputLocation", Links::location( LHCb::VPClusterLocation::Light )} ) {}

//=============================================================================
// Initialize
//=============================================================================
StatusCode VPCluster2MCParticleLinker::initialize() {
  return Linker::initialize().andThen( [&] {
    warning()
        << "VPLightClusters no longer contain a vector of pixels contributing to the cluster, for performance reasons"
        << endmsg;
    warning() << "Efficiencies will not be 100% correct." << endmsg;
    warning() << "Use VPFullClusters and VPFullCluster2MCParticleLinker to obtain the correct efficiencies." << endmsg;
  } );
}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPCluster2MCParticleLinker::operator()( const std::vector<LHCb::VPLightCluster>& clusters,
                                                         const LHCb::LinksByKey& bareDigitLinks ) const {
  // Get the association table between digits and particles.
  auto digitLinks = inputLinks<LHCb::MCParticle>( bareDigitLinks );

  auto output = OutputLinks<LHCb::VPLightCluster, LHCb::MCParticle>{};
  // Loop over the clusters.
  for ( const LHCb::VPLightCluster& cluster : clusters ) {
    std::map<const LHCb::MCParticle*, double> particleMap;
    // Get the pixels in the cluster, not available anymore in VPLightClusters.
    // const auto& pixels = cluster.pixels();
    // double sum = 0.;
    // for (const auto& pixel : pixels) {
    //    for (const auto& entry : digitLinks.from(pixel)) {
    //       const double weight = entry.weight();
    //       particleMap[entry.to()] += weight;
    //       sum += weight;
    //    }
    // }
    const auto& centralpixel = cluster.channelID();
    double      sum          = 0.;
    for ( const auto& entry : digitLinks.from( centralpixel ) ) {
      const double weight = entry.weight();
      particleMap[entry.to()] += weight;
      sum += weight;
    }
    if ( sum < 1.e-2 ) continue;
    for ( auto [hit, weight] : particleMap ) output.link( cluster.channelID(), hit, weight / sum );
  }
  return output;
}
