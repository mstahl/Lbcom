/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include "GaudiKernel/LinkManager.h"
#include <Associators/Location.h>

/** @class VPFullClusterLinker VPFullClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC particles, based on the association tables
 * for individual pixels produced by VPDigitLinker.
 */

struct VPFullCluster2MCParticleLinker
    : Gaudi::Functional::Linker<LHCb::LinksByKey( const std::vector<LHCb::VPFullCluster>&, const LHCb::MCParticles&,
                                                  const LHCb::LinksByKey& )> {
  VPFullCluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::LinksByKey operator()( const std::vector<LHCb::VPFullCluster>&, const LHCb::MCParticles&,
                               const LHCb::LinksByKey& ) const override; // < Algorithm execution
};

DECLARE_COMPONENT( VPFullCluster2MCParticleLinker )

VPFullCluster2MCParticleLinker::VPFullCluster2MCParticleLinker( const std::string& name, ISvcLocator* svcLoc )
    : Linker( name, svcLoc,
              {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
               KeyValue{"MCParticlesLocation", LHCb::MCParticleLocation::Default},
               KeyValue{"VPDigit2MCParticleLinksLocation", Links::location( LHCb::VPDigitLocation::Default )}},
              KeyValue{"OutputLocation", Links::location( LHCb::VPFullClusterLocation::Default )} ) {}

LHCb::LinksByKey VPFullCluster2MCParticleLinker::operator()( const std::vector<LHCb::VPFullCluster>& clusters,
                                                             const LHCb::MCParticles&                mcParts,
                                                             const LHCb::LinksByKey& bareDigitLinks ) const {
  LHCb::LinksByKey output;
  output.setSourceClassID( LHCb::VPFullCluster::classID() );
  output.setTargetClassID( LHCb::MCParticle::classID() );
  // Loop over the clusters.
  for ( const LHCb::VPFullCluster& cluster : clusters ) {
    std::map<unsigned int, double> particleMap;
    // Get the pixels in the cluster, not available anymore in VPFullClusters
    const auto& pixels = cluster.pixels();
    double      sum    = 0.;
    for ( const auto& pixel : pixels ) {
      bareDigitLinks.applyToLinks( pixel, [&particleMap, &sum]( unsigned int, unsigned int tgtIndex, float weight ) {
        particleMap[tgtIndex] += weight;
        sum += weight;
      } );
    }
    if ( sum < 1.e-2 ) continue;
    for ( const auto [mcPartIndex, partWeight] : particleMap ) {
      output.addReference( cluster.channelID(), -1, mcPartIndex, output.linkID( &mcParts ), partWeight / sum );
    }
  }
  return output;
}
