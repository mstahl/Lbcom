/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include <Associators/Location.h>

class VPFullCluster2MCHitLinker
    : public Gaudi::Functional::Linker<LHCb::LinksByKey( const std::vector<LHCb::VPFullCluster>&,
                                                         const LHCb::MCHits& mcHits, const LHCb::LinksByKey& )> {
public:
  // Standard constructor
  VPFullCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::LinksByKey operator()( const std::vector<LHCb::VPFullCluster>& clusters, const LHCb::MCHits&,
                               const LHCb::LinksByKey& ) const override; // < Algorithm execution
};

DECLARE_COMPONENT( VPFullCluster2MCHitLinker )

//=============================================================================
// Constructor
//=============================================================================
VPFullCluster2MCHitLinker::VPFullCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : Linker( name, pSvcLocator,
              {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
               KeyValue{"MCHitLocation", LHCb::MCHitLocation::VP},
               KeyValue{"VPDigit2MCHitLinksLocation", Links::location( LHCb::VPDigitLocation::Default + "2MCHits" )}},
              KeyValue{"OutputLocation", Links::location( LHCb::VPFullClusterLocation::Default + "2MCHits" )} ) {}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPFullCluster2MCHitLinker::operator()( const std::vector<LHCb::VPFullCluster>& clusters,
                                                        const LHCb::MCHits&                     mcHits,
                                                        const LHCb::LinksByKey&                 bareDigitLinks ) const {
  // Create an association table between hits and clusters.
  LHCb::LinksByKey output;
  output.setSourceClassID( LHCb::VPFullCluster::classID() );
  output.setTargetClassID( LHCb::MCHit::classID() );

  for ( const LHCb::VPFullCluster& cluster : clusters ) {
    std::map<unsigned int, double> hitMap;
    double                         sum    = 0.;
    const auto&                    pixels = cluster.pixels();
    // Loop over all pixels in a given cluster
    for ( const auto& pixel : pixels ) {
      bareDigitLinks.applyToLinks( pixel, [&hitMap, &sum]( unsigned int, unsigned int tgtIndex, float weight ) {
        hitMap[tgtIndex] += weight;
        sum += weight;
      } );
    }
    if ( sum < 1.e-2 ) continue;
    for ( const auto [mcHitIndex, hitWeight] : hitMap ) {
      output.addReference( cluster.channelID(), -1, mcHitIndex, output.linkID( &mcHits ), hitWeight / sum );
    }
  }

  return output;
}
