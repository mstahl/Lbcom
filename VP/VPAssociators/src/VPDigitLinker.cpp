/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Associators/Location.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCVPDigit.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include <map>

/** @class VPDigitLinker VPDigitLinker.h
 *  This algorithm creates association tables between the channel IDs of
 *  the VP digits and the corresponding MC hits and particles.
 *
 */
struct VPDigitLinker : Gaudi::Functional::MultiLinker<std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>(
                           const LHCb::VPDigits&, const LHCb::MCVPDigits& )> {

  VPDigitLinker( const std::string& name, ISvcLocator* pSvcLocator );
  std::tuple<LHCb::LinksByKey, LHCb::LinksByKey> operator()( const LHCb::VPDigits&,
                                                             const LHCb::MCVPDigits& ) const override;
};

DECLARE_COMPONENT( VPDigitLinker )

namespace {
  template <typename Container>
  const LHCb::MCHit* substituteDeltaRays( const LHCb::MCHit* hit, const Container& hits ) {
    auto particle = hit->mcParticle();
    // Check if the hit originates from a delta ray.
    // Only If no other hit from the mother particle is found,
    // add the delta ray hit as a separate entry.
    if ( particle && particle->originVertex() &&
         particle->originVertex()->type() == LHCb::MCVertex::MCVertexType::DeltaRay ) {
      // Check if there is another hit from the mother particle of the delta.
      auto mhit = std::find_if( hits.begin(), hits.end(), [mother = particle->mother()]( const LHCb::MCHit* h ) {
        // some mchits may be nullptr if from spillover
        if ( h == static_cast<LHCb::MCHit*>( nullptr ) ) return false;
        return h->mcParticle() == mother;
      } );
      if ( mhit != hits.end() ) {
        // Add the deposit to the hit of the mother particle.
        return *mhit;
      }
    }
    return hit;
  }
} // namespace

//=============================================================================
// Constructor
//=============================================================================
VPDigitLinker::VPDigitLinker( const std::string& name, ISvcLocator* svcLoc )
    : MultiLinker( name, svcLoc,
                   {KeyValue{"DigitLocation", LHCb::VPDigitLocation::Default},
                    KeyValue{"MCDigitLocation", LHCb::MCVPDigitLocation::Default}},
                   {KeyValue{"HitLinkLocation", Links::location( LHCb::VPDigitLocation::Default + "2MCHits" )},
                    KeyValue{"ParticleLinkLocation", Links::location( LHCb::VPDigitLocation::Default )}} ) {}

//=============================================================================
// Execution
//=============================================================================
std::tuple<LHCb::LinksByKey, LHCb::LinksByKey> VPDigitLinker::operator()( const LHCb::VPDigits&   digits,
                                                                          const LHCb::MCVPDigits& mcdigits ) const {
  // Create association tables.
  auto hitLinks      = outputLinks<LHCb::MCHit>();
  auto particleLinks = outputLinks<LHCb::MCParticle>();

  // Loop over the digits.
  for ( const auto digit : digits ) {
    // Get the MC digit with the same channel ID.
    const auto mcDigit = mcdigits.object( digit->key() );
    if ( !mcDigit ) continue;
    // Get the MC hits associated to the MC digit and their weights.
    const auto&        hits            = mcDigit->mcHits();
    const auto&        depositAndTimes = mcDigit->depositAndTimes();
    const unsigned int nDeposits       = depositAndTimes.size();
    // Make a map of the MC hits/particles and weights.
    std::map<const LHCb::MCHit*, double>      hitMap;
    std::map<const LHCb::MCParticle*, double> particleMap;
    for ( unsigned int i = 0; i < nDeposits; ++i ) {
      const auto hit = hits[i];
      // test if SmartRef<MCHit> points to an MCHit
      if ( hit.data() == static_cast<LHCb::MCHit*>( nullptr ) ) continue; // spillover MCHit contributed to this digit
      const double       weight = depositAndTimes[i].first;
      const LHCb::MCHit* subHit = substituteDeltaRays( hit, hits );
      hitMap[subHit] += weight;
      particleMap[subHit->mcParticle()] += weight;
    }
    // Make the associations
    for ( auto [hit, weight] : hitMap ) hitLinks.link( digit->key(), hit, weight );
    for ( auto [hit, weight] : particleMap ) particleLinks.link( digit->key(), hit, weight );
  }
  return {std::move( hitLinks ), std::move( particleLinks )};
}
