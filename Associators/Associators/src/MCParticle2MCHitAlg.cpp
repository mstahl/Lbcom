/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the implementation of the MCParticle2MCHitAlg interface
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Authors: Rutger van der Eijk
//   Created: 3-7-2002

#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Transformer.h"

class MCParticle2MCHitAlg
    : public Gaudi::Functional::Transformer<LHCb::LinksByKey( LHCb::MCHits const&, LHCb::MCParticles const& )> {

public:
  MCParticle2MCHitAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {
                         KeyValue( "MCHitPath", "" ),
                         KeyValue( "MCParticles", LHCb::MCParticleLocation::Default ),
                     },
                     KeyValue( "OutputData", "" ) ) {}
  LHCb::LinksByKey operator()( LHCb::MCHits const& mc_hits, LHCb::MCParticles const& mc_particles ) const override {

    LHCb::LinksByKey new_links;
    // The old algo created the below LinkerWithKey<Target, Source>
    // LinkerWithKey<MCParticle, MCHit> myLink( eventSvc(), msgSvc(), m_outputData );
    // But then called the link(source, target) function as link(MCHit, MCParticle)
    // Thus I'm setting source as MCParticle as I belive that's the desired functionality.
    new_links.setTargetClassID( LHCb::MCHit::classID() );
    new_links.setSourceClassID( LHCb::MCParticle::classID() );

    for ( auto const* mc_hit : mc_hits ) {
      if ( !mc_hit ) { error() << "Failed to retrieve MCHit" << endmsg; }
      new_links.addReference( mc_hit->mcParticle()->index(), new_links.linkID( &mc_particles ), mc_hit->index(),
                              new_links.linkID( &mc_hits ) );
    }

    return new_links;
  }
};

DECLARE_COMPONENT( MCParticle2MCHitAlg )
