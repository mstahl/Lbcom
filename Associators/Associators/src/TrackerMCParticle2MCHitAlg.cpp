/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the implementation of the MCParticle2MCHitAlg interface
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Authors: Rutger van der Eijk
//   Created: 3-7-2002
/** @class TrackerMCParticle2MCHitAlg TrackerMCParticle2MCHitAlg.h
 *
 *   @author: M. Needham
 *   @date 03-07-2002
 */

#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkerWithKey.h"
#include <string>
#include <vector>

class TrackerMCParticle2MCHitAlg : public GaudiAlgorithm {

public:
  /// Standard constructor
  TrackerMCParticle2MCHitAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_dets{this, "Detectors", {"Velo", "IT", "TT", "OT", "Muon"}};
  Gaudi::Property<std::string>              m_outputData{
      this, "OutputData", "/Event/MC/Particles2MCTrackerHits"}; ///< location of Particles to associate
};

DECLARE_COMPONENT( TrackerMCParticle2MCHitAlg )

using namespace LHCb;

TrackerMCParticle2MCHitAlg::TrackerMCParticle2MCHitAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

StatusCode TrackerMCParticle2MCHitAlg::execute() {
  // get the MCParticles
  MCParticles* particles = get<MCParticles>( MCParticleLocation::Default );

  // save some typing
  using HitLinks = LinkedFrom<LHCb::MCHit, LHCb::MCParticle>;

  // make the output table
  LinkerWithKey<MCParticle, MCHit> myLink( eventSvc(), msgSvc(), m_outputData );

  // get the sub-detector linkers
  std::vector<HitLinks> links;
  links.reserve( m_dets.size() );
  for ( const auto& det : m_dets ) {
    std::string linkPath = MCParticleLocation::Default + "2MC" + det + "Hits";
    links.emplace_back( evtSvc(), msgSvc(), linkPath );
    if ( links.back().notFound() ) return Warning( "Failed to find linker table", StatusCode::FAILURE );
  }

  // loop over MCParticles
  for ( const auto& p : *particles ) {
    for ( auto& l : links ) {
      for ( auto* aHit = l.first( p ); aHit; aHit = l.next() ) { myLink.link( aHit, p ); }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Linker table stored at " << m_outputData << endmsg;

  return StatusCode::SUCCESS;
}
