2017-07-04 LbcomSys v20r5
---
This version uses Gaudi v28r2 and LHCb v42r5 (and LCG_88 with ROOT 6.08.06)

This version is released on `2017-patches` branch. 



## Change to release notes format
**As from this release, the file `LbcomSys/doc/release.notes` is frozen**  
Instead, there will be a file per release in the new `ReleaseNotes` directory. e.g. this file is called `ReleaseNotes/v20r5.md`


## New functionality

**[MR !154] Updates of Herschel analysis code for delay scans**  
- Allow faster handling of delay scans in `HCDelayScan.{cpp,h}`  
- Allow tupling of L0 response (in `HCDigitTuple`) as well as a few bug-fixes that made analysis of delay scans using tupling complicated  
- First commit of script that allows analysis of delay scan output using tuples

**[MR !150, !152] Update to `HC/HCMonitors/scripts` to allow processing of 2017 data**  
- Update channel mapping and data-type defaults.  
- Removed antequated code for delay scan analysis  
- Added code for analysis of pedestal runs - determines pedestal offsets and writes out new recipe

## Bug fixes
**[MR !145] Fix crash in DaVinci track refitting from lite clusters due to missing `VeloDecodeStatus` object**  
See LHCBPS-1717
