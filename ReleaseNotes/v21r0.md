2017-07-11 LbcomSys v21r0
===
This version uses Gaudi v28r2 and LHCb v42r5 (and LCG_88 with ROOT 6.08.06)

This version is released on `master` branch. The previous release on `master` branch  was `Lbcom v20r3`. This release contains all the changes that were made in `Lbcom v20r4` and `Lbcom v20r5` (released on `2017-patches` branch, see corresponding release notes), as well as the additional changes described in this file.

## Change to compiler support
**As from this release, support for gcc49 is dropped**  
This means that C++14 is now fully supported

## New or changed functionality
**[MR !143] Add files needed for Herschel delay scan analysis**  
**[MR !161] Updates to HCDelayScan**

## Code optimisations
**[MR !132] RICH MaPMT Improvements following [LHCb!618]**

## Bug fixes and cleanups
**[MR !148] Ignore some warnings coming from the Boost library**  
**[MR !137] Add missing include exposed by [LHCb!654]**

## Monitoring changes
**[MR !144] Reduce CaloCluster2MC verbosity**
