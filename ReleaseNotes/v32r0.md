2021-03-12 Lbcom v32r0
===

This version uses
LHCb [v52r0](../../../../LHCb/-/tags/v52r0),
Gaudi [v35r2](../../../../Gaudi/-/tags/v35r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Lbcom [v31r3](/../../tags/v31r3), with the following changes:

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding ~VP ~UT | Remove velo and beetle from UT, !504 (@pkoppenb)
- ~UT | Modernize/cleanup UT code (follow LHCb!2892), !538 (@graven)
- ~Monitoring | Minor cleanup to RawBankSizeMonitor, !547 (@jonrob)
- ~Build | Link RichAlgorithms against DAQEventLib explicitly, !544 (@clemenci)
- Follow changes in lhcb/LHCb!2959, !546 (@graven)
