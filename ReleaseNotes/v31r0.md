2020-02-18 Lbcom v31r0
===

This version uses LHCb v51r0, Gaudi v33r0 and LCG_96b with ROOT 6.18.04.

This version is released on `master` branch.
Built relative to Lbcom v30r6, with the following changes:

### Fixes ~"bug fix" ~workaround

- ~Muon ~"MC checking" | [MuonAssociators] Assign MuonTileID in Pad2MCTool, !419 (@mstahl) [lhcb/Lbcom#2]


### Enhancements ~enhancement

- ~Calo ~"MC checking" | Allow relations changes in LHCb, !385 (@graven)
- ~RICH ~Conditions | SmartIDClustering - convert to functional conditions access, !388 (@jonrob)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Prefer declareProperty over declareUTConfigProperty, !398 (@graven)
- ~RICH | RichSmartIDClustering - Adapt to minor change in RichFutureKernel, !386 (@jonrob)
- ~RICH ~Conditions | RICH - Migrate tools to derived condition objects, !389 (@jonrob)
- ~"MC checking" | Modernize MuonAssociators, !414 (@graven)
- ~"MC checking" | First go at removing Run2 specific code, !376 (@sponce)
- ~Monitoring | Modernize DAQMonitors, !403 (@graven)
- ~Conditions | Cleanups and changes in DetDesc to streamline the code and prepare DD4hep introduction, !391 (@sponce)
- ~Build | Switch from deprecated GaudiKernel/Counters.h to Gaudi/Accumulators.h, !392 (@jonrob)
- Remove ST code from master, !407 (@graven)
- Remove L0/L0MuonMonitor and L0/PuVeto from master, !405 (@graven)
