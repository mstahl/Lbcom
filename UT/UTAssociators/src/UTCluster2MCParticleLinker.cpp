/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "Kernel/UTAlgBase.h"
#include "Linker/LinkerTool.h"

using namespace LHCb;

/** @class UTCluster2MCParticleAlg UTCluster2MCParticleLinker.h
 *
 *
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */
class UTCluster2MCParticleLinker : public UT::AlgBase {
public:
  using PartPair    = std::pair<const LHCb::MCParticle*, double>;
  using ParticleMap = std::map<const LHCb::MCParticle*, double>;

  /// Standard constructor
  UTCluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

  /// execute
  StatusCode execute() override;

private:
  void refsToRelate( std::vector<PartPair>& selectedRefs, const ParticleMap& partMap,
                     LHCb::MCParticles* particles ) const;

  StatusCode associateToTruth( const LHCb::UTCluster* aCluster, ParticleMap& partMap );

  // job options
  /// Linker table location path
  Gaudi::Property<std::string> m_asctLocation{this, "ClusterASCTlocation", UTClusterLocation::UTClusters + "2MCHits"};
  DataObjectReadHandle<UTClusters>  m_inputData{this, "InputData",
                                               UTClusterLocation::UTClusters}; ///< Location of the UTClusters
  DataObjectReadHandle<MCParticles> m_mcParticles{this, "MCParticleLocation", MCParticleLocation::Default};
  Gaudi::Property<std::string>      m_outputData{this, "OutputData",
                                            UTClusterLocation::UTClusters}; ///< Location of the UTClusters linker table
  Gaudi::Property<bool> m_addSpillOverHits{this, "AddSpillOverHits", false}; ///< Flag to add spill-over to linker table
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.2}; ///< Minimal charge fraction to link to MCParticle
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false}; ///< Flag to allow only 1 link for each cluster
};

DECLARE_COMPONENT( UTCluster2MCParticleLinker )

UTCluster2MCParticleLinker::UTCluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::AlgBase( name, pSvcLocator ) {}

StatusCode UTCluster2MCParticleLinker::execute() {
  // get UTClusters
  const UTClusters* clusterCont = m_inputData.get();

  // get MCParticles
  auto* mcParts = m_mcParticles.get();

  // create a linker
  LinkerWithKey<MCParticle, UTCluster> aLinker( evtSvc(), msgSvc(), m_outputData );

  // loop and link UTClusters to MC truth

  for ( auto iterClus : *clusterCont ) {

    // find all particles
    ParticleMap partMap;
    StatusCode  sc = associateToTruth( iterClus, partMap );
    if ( sc.isFailure() ) return sc;

    // select references to add to table
    std::vector<PartPair> selectedRefs;
    refsToRelate( selectedRefs, partMap, mcParts );

    if ( !selectedRefs.empty() ) {

      if ( !m_oneRef.value() ) {
        for ( auto& selectedRef : selectedRefs ) { aLinker.link( iterClus, selectedRef.first, selectedRef.second ); }
      } else {
        PartPair bestPair = selectedRefs.back();
        aLinker.link( iterClus, bestPair.first, bestPair.second );
      }
    } // refsToRelate != empty
  }   // loop iterClus

  return StatusCode::SUCCESS;
}

void UTCluster2MCParticleLinker::refsToRelate( std::vector<PartPair>& selRefs, const ParticleMap& partMap,
                                               MCParticles* particles ) const {
  for ( auto iterMap : partMap ) {
    const MCParticle* aParticle = iterMap.first;
    if ( aParticle && iterMap.second > m_minFrac ) {
      if ( m_addSpillOverHits.value() || particles == aParticle->parent() ) {
        selRefs.emplace_back( aParticle, iterMap.second );
      }
    }
  }
}

StatusCode UTCluster2MCParticleLinker::associateToTruth( const UTCluster* aCluster, ParticleMap& partMap ) {
  // make link to truth  to MCHit from cluster
  using AsctTool = LinkerTool<UTCluster, MCHit>;
  using Table    = AsctTool::DirectType;

  // Use the UTCluster to MCHit association
  AsctTool     associator( evtSvc(), m_asctLocation );
  const Table* aTable = associator.direct();
  if ( !aTable ) return Error( "Failed to find table", StatusCode::FAILURE );

  double foundCharge = 0;
  for ( const auto& iterHit : aTable->relations( aCluster ) ) {
    const MCParticle* aParticle = iterHit.to()->mcParticle();
    partMap[aParticle] += iterHit.weight();
    foundCharge += iterHit.weight();
  } // iterHit

  // difference between depEnergy and total cluster charge = noise (due to norm)
  partMap[nullptr] += 1.0 - foundCharge;

  return StatusCode::SUCCESS;
}
