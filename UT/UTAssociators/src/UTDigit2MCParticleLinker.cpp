/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/UTDigit.h"
#include "Kernel/UTAlgBase.h"
#include "Linker/LinkerWithKey.h"
#include "UTTruthTool.h"

using namespace LHCb;

/** @class UTDigit2MCParticleLinker UTDigit2MCParticleLinker.h
 *
 *
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */

class UTDigit2MCParticleLinker : public UT::AlgBase {
public:
  typedef std::pair<const LHCb::MCParticle*, double> PartPair;
  typedef std::map<const LHCb::MCParticle*, double>  ParticleMap;

  /// Standard constructor
  UTDigit2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

  /// execute
  StatusCode execute() override;

private:
  void refsToRelate( std::vector<PartPair>& selectedRefs, const ParticleMap& hitMap, const double totCharge,
                     LHCb::MCParticles* particles ) const;

  // job options
  DataObjectReadHandle<MCParticles> m_mcparts{this, "MCParticleLcoation", MCParticleLocation::Default};
  Gaudi::Property<std::string>      m_outputData{this, "OutputData",
                                            UTDigitLocation::UTDigits}; ///< Location of the UTDigits linker table
  DataObjectReadHandle<UTDigits>    m_inputData{this, "InputData",
                                             UTDigitLocation::UTDigits};  ///< Location of the UTDigits
  Gaudi::Property<bool> m_addSpillOverHits{this, "AddSpillOverHits", false}; ///< Flag to add spill-over to linker table
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.05}; ///< Minimal charge fraction to link to MCParticle
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false};  ///< Flag to allow only 1 link for each digit
};

DECLARE_COMPONENT( UTDigit2MCParticleLinker )
namespace {
  template <typename Container>
  double totalCharge( const Container& partMap ) {
    return std::accumulate( partMap.begin(), partMap.end(), 0.0,
                            []( double charge, const auto& p ) { return charge + std::abs( p.second ); } );
  }
} // namespace

UTDigit2MCParticleLinker::UTDigit2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::AlgBase( name, pSvcLocator ) {}

StatusCode UTDigit2MCParticleLinker::execute() {
  // get UTDigits
  const UTDigits* digitCont = m_inputData.get();

  // get MCParticles
  auto* mcParts = m_mcparts.get();

  // creating a linker
  LinkerWithKey<MCParticle, UTDigit> myLink( evtSvc(), msgSvc(), m_outputData );

  // loop and link UTDigits to MC truth

  for ( auto iterDigit : *digitCont ) {

    // find all particles
    ParticleMap partMap;
    UTTruthTool::associateToTruth( iterDigit, partMap );

    // select references to add to table
    std::vector<PartPair> selectedRefs;
    double                tCharge = totalCharge( partMap );
    refsToRelate( selectedRefs, partMap, tCharge, mcParts );

    if ( !selectedRefs.empty() ) {
      if ( !m_oneRef.value() ) {
        for ( auto& selectedRef : selectedRefs ) { myLink.link( iterDigit, selectedRef.first, selectedRef.second ); }
      } else {
        PartPair bestPair = selectedRefs.back();
        myLink.link( iterDigit, bestPair.first, bestPair.second );
      }
    } // refsToRelate != empty
  }   // loop iterDigit

  return StatusCode::SUCCESS;
}

void UTDigit2MCParticleLinker::refsToRelate( std::vector<PartPair>& selRefs, const ParticleMap& partMap,
                                             const double totCharge, MCParticles* mcparticles ) const {
  // iterate over map
  for ( auto [particle, charge] : partMap ) {
    double frac = ( totCharge > 0.0 ? charge / totCharge : -1.0 );
    if ( particle && frac > m_minFrac ) {
      if ( m_addSpillOverHits.value() || mcparticles == particle->parent() ) { selRefs.emplace_back( particle, frac ); }
    }
  } // iterMap
}
