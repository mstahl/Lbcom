/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCTruth.h"
#include "Event/UTDigit.h"
#include "Kernel/UTAlgBase.h"
#include "Linker/LinkerWithKey.h"
#include "UTTruthTool.h"

using namespace LHCb;

/** @class UTDigit2MCHitLinker UTDigit2MCHitLinker.h
 *
 *
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */
class UTDigit2MCHitLinker : public UT::AlgBase {

public:
  typedef std::pair<const LHCb::MCHit*, double> HitPair;
  typedef std::map<const LHCb::MCHit*, double>  HitMap;

  /// Standard constructor
  UTDigit2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

  /// execute
  StatusCode execute() override;

private:
  void refsToRelate( std::vector<HitPair>& selectedRefs, const HitMap& hitMap, const double& totCharge,
                     LHCb::MCHits* hits ) const;

  DataObjectReadHandle<MCHits> m_hitLocation{this, "hitLocation", MCHitLocation::UT};
  Gaudi::Property<std::string> m_outputData{
      this, "OutputData", UTDigitLocation::UTDigits + "2MCHits"}; ///< Location of the UTDigits linker table
  DataObjectReadHandle<UTDigits> m_inputData{this, "InputData",
                                             UTDigitLocation::UTDigits};     ///< Location of the UTDigits
  Gaudi::Property<bool> m_addSpillOverHits{this, "AddSpillOverHits", false}; ///< Flag to add spill-over to linker table
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.05}; ///< Minimal charge fraction to link to MCParticle
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false};  ///< Flag to allow only 1 link for each digit
};

DECLARE_COMPONENT( UTDigit2MCHitLinker )
namespace {
  template <typename Container>
  double totalCharge( const Container& hitMap ) {
    return std::accumulate( hitMap.begin(), hitMap.end(), 0.,
                            []( double charge, const auto& p ) { return charge + std::fabs( p.second ); } );
  }
} // namespace

UTDigit2MCHitLinker::UTDigit2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::AlgBase( name, pSvcLocator ) {}

StatusCode UTDigit2MCHitLinker::execute() {
  // get UTDigits
  const UTDigits* digitCont = m_inputData.get();

  // get hits
  auto* mcHits = m_hitLocation.get();

  // create an association table
  LinkerWithKey<MCHit, UTDigit> myLink( evtSvc(), msgSvc(), m_outputData );

  // loop and link UTDigits to MC truth

  for ( const auto* iterDigit : *digitCont ) {

    // find all hits
    HitMap hitMap;
    double foundCharge = 0.0;
    UTTruthTool::associateToTruth( iterDigit, hitMap, foundCharge );
    hitMap[nullptr] += ( iterDigit->depositedCharge() - foundCharge );

    // select references to add to table
    std::vector<HitPair> selectedRefs;
    double               tCharge = totalCharge( hitMap );
    refsToRelate( selectedRefs, hitMap, tCharge, mcHits );

    if ( ( !selectedRefs.empty() ) ) {
      if ( !m_oneRef.value() ) {
        for ( auto& selectedRef : selectedRefs ) {
          myLink.link( iterDigit, selectedRef.first, selectedRef.second );
        } // iterPair
      } else {
        HitPair bestPair = selectedRefs.back();
        myLink.link( iterDigit, bestPair.first, bestPair.second );
      }
    } // refsToRelate ! empty
  }   // loop iterDigit

  return StatusCode::SUCCESS;
}

void UTDigit2MCHitLinker::refsToRelate( std::vector<HitPair>& selectedRefs, const HitMap& hitMap,
                                        const double& totCharge, MCHits* mchits ) const {
  // iterate over map
  for ( auto [hit, charge] : hitMap ) {
    double frac = ( totCharge > 0.0 ? std::abs( charge / totCharge ) : -1.0 );
    if ( hit && frac > m_minFrac ) {
      if ( m_addSpillOverHits.value() || mchits == hit->parent() ) { selectedRefs.emplace_back( hit, frac ); }
    }
  }
}
