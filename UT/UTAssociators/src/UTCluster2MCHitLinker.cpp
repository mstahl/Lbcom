/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "Kernel/UTAlgBase.h"
#include "Linker/LinkerTool.h"

using namespace LHCb;

/** @class UTCluster2MCHitAlg UTCluster2MCHitLinker.h
 *
 *
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */

class UTCluster2MCHitLinker : public UT::AlgBase {
public:
  typedef std::pair<const LHCb::MCHit*, double> HitPair;
  typedef std::map<const LHCb::MCHit*, double>  HitMap;

  /// Standard constructor
  UTCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

  /// execute
  StatusCode execute() override;

private:
  std::vector<HitPair> refsToRelate( const HitMap& hitMap, const LHCb::MCHits* hits ) const;

  StatusCode associateToTruth( const LHCb::UTDigits&, const LHCb::UTCluster&, HitMap& );

  DataObjectReadHandle<UTClusters> m_inputData{this, "InputData", UTClusterLocation::UTClusters};
  DataObjectReadHandle<MCHits>     m_hitLocation{this, "hitLocation", "/Event/" + MCHitLocation::UT};
  DataObjectReadHandle<UTDigits>   m_digitLocation{this, "DigitLocation",
                                                 UTDigitLocation::UTDigits}; ///< Location of the UTDigits
  Gaudi::Property<std::string>     m_asctLocation{this, "DigitASCTlocation",
                                              UTDigitLocation::UTDigits + "2MCHits"}; ///< Location of Digit2MCHit table
  Gaudi::Property<std::string>     m_outputData{
      this, "OutputData", UTClusterLocation::UTClusters + "2MCHits"}; ///< Location of the UTClusters linker table
  Gaudi::Property<bool> m_addSpillOverHits{this, "AddSpillOverHits", false}; ///< Flag to add spill-over to linker table
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.2}; ///< Minimal charge fraction to link to MCParticle
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false}; ///< Flag to allow only 1 link for each cluster
};

DECLARE_COMPONENT( UTCluster2MCHitLinker )

UTCluster2MCHitLinker::UTCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::AlgBase( name, pSvcLocator ) {}

StatusCode UTCluster2MCHitLinker::execute() {
  // get UTClusters
  const UTClusters* clusterCont = m_inputData.get();

  // get the digits
  auto* digitCont = m_digitLocation.get();

  // get MCParticles
  auto* mcHits = m_hitLocation.get();

  // create a linker
  LinkerWithKey<MCHit, UTCluster> myLink( evtSvc(), msgSvc(), m_outputData );

  // loop and link UTClusters to MC truth

  for ( auto iterClus : *clusterCont ) {

    // find all hits
    HitMap     hitMap;
    StatusCode sc = associateToTruth( *digitCont, *iterClus, hitMap );
    if ( sc.isFailure() ) return sc; // error message printed in failed method

    // select references to add to table
    auto selectedRefs = refsToRelate( hitMap, mcHits );

    if ( !selectedRefs.empty() ) {
      if ( !m_oneRef.value() ) {
        for ( auto& selectedRef : selectedRefs ) { myLink.link( iterClus, selectedRef.first, selectedRef.second ); }
      } else {
        HitPair bestPair = selectedRefs.back();
        myLink.link( iterClus, bestPair.first, bestPair.second );
      }
    } // refsToRelate ! empty
  }   // loop iterClus

  return StatusCode::SUCCESS;
}

std::vector<UTCluster2MCHitLinker::HitPair> UTCluster2MCHitLinker::refsToRelate( const HitMap& hitMap,
                                                                                 const MCHits* hits ) const {
  std::vector<HitPair> selRefs;
  for ( auto [aHit, second] : hitMap ) {
    if ( aHit && second > m_minFrac ) {
      if ( m_addSpillOverHits.value() || hits == aHit->parent() ) { selRefs.emplace_back( aHit, second ); }
    }
  }
  return selRefs;
}

StatusCode UTCluster2MCHitLinker::associateToTruth( const LHCb::UTDigits& digitCont, const UTCluster& aCluster,
                                                    HitMap& hitMap ) {
  // make link to truth  to MCHit from cluster
  using AsctTool = LinkerTool<UTDigit, MCHit>;
  using Table    = AsctTool::DirectType;

  // Use the UTDigit to MCHit association
  AsctTool     associator( evtSvc(), m_asctLocation );
  const Table* aTable = associator.direct();
  if ( !aTable ) return Error( "Failed to find " + m_asctLocation + " table", StatusCode::FAILURE );

  for ( auto id : aCluster.channels() ) {
    const UTDigit* aDigit = digitCont.object( id );
    if ( aDigit ) {
      double foundCharge = 0.;
      for ( const auto& iterHit : aTable->relations( aDigit ) ) {
        const MCHit* aHit = iterHit.to();
        hitMap[aHit] += iterHit.weight() * aDigit->depositedCharge();
        foundCharge += iterHit.weight() * aDigit->depositedCharge();
      } // iterHit
      hitMap[nullptr] += aDigit->depositedCharge() - foundCharge;
    } // aDigit
  }   // Digit

  // renormalize to 1

  for ( auto& iterMap : hitMap ) { iterMap.second /= aCluster.totalCharge(); }

  return StatusCode::SUCCESS;
}
