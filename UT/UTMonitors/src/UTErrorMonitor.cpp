/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Event/UTTELL1BoardErrorBank.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTHistoAlgBase.h"
#include "TAxis.h"
#include "TH2D.h"

using namespace LHCb;
using namespace UTDAQ;
using namespace UTBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : UTErrorMonitor
//
// 2018-09-04 : Andy Beiter (based on code by Jeroen Van Tilburg)
//-----------------------------------------------------------------------------

/** @class UTErrorMonitor UTErrorMonitor.h
 *
 *  Class to monitor the error banks. The output consists of:
 *  - One overview histograms that counts the number of error banks per Tell1.
 *  - One 2D histogram for each Tell1 with an error bank. It displays the error
 *    type versus the optical link number.
 *  There are three important job options:
 *  - \b DetType: to switch between "UT"
 *  - \b ErrorLocation: to set the error location in case it is different from
 *     the default location.
 *  - \b ExpertHisto: Set this to false to turn of the 2D histograms.
 *
 *  @author Andy Beiter (based on code by Jeroen Van Tilburg)
 *  @date   2018-09-04
 */
class UTErrorMonitor : public Gaudi::Functional::Consumer<void( const UTTELL1BoardErrorBanks& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<UT::HistoAlgBase>> {
public:
  /// Standard constructor
  UTErrorMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const UTTELL1BoardErrorBanks& errors ) const override;

private:
  // The maximum number of Tell1s (used to determine number of bins)
  unsigned int m_maxTell1s;

  // Total number of active ports in detector
  unsigned int m_activePorts;

  // Job options

  Gaudi::Property<bool> m_expertHisto{this, "ExpertHisto", true}; ///< Flag to turn off filling of expert histograms

  // Book histrograms for online monitoring
  AIDA::IHistogram1D* m_1d_evtCounter = nullptr;
  AIDA::IHistogram1D* m_1d_errorBanks = nullptr;
  AIDA::IHistogram2D* m_2d_errorTypes = nullptr;

  /// Map of error histograms booked in initialize
  std::map<unsigned int, AIDA::IHistogram2D*> m_errorHistos;

  /// Label the y-axis with the error types
  void labelHistoErrorTypes( AIDA::IHistogram2D* histo );

  /// Fraction of ports which send error banks
  AIDA::IHistogram1D* m_1d_fracErrors = nullptr;
  /// Fraction of ports which send error banks per TELL1
  AIDA::IProfile1D* m_prof_fracErrors = nullptr;

  unsigned int m_nTELL1s; ///< Number of TELL1 boards expected.
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTErrorMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UTErrorMonitor::UTErrorMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"ErrorLocation", UTTELL1BoardErrorBankLocation::UTErrors}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UTErrorMonitor::initialize() {
  // Initialize UT::HistoAlgBase
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Get the maximum number of Tell1s to determine number of histogram bin
  m_maxTell1s = readoutTool()->SourceIDToTELLNumberMap().size();

  // Book histograms
  m_1d_evtCounter = book1D( "Event counter", 0.5, 1.5, 1 );
  m_1d_errorBanks = book1D( "Error banks per Tell1", 0.5, m_maxTell1s + 0.5, m_maxTell1s );
  m_1d_fracErrors = book1D( "Fraction of ports which sent error banks", 0., 1.0, 100 );
  m_prof_fracErrors =
      bookProfile1D( "Fraction of ports which sent error banks per TELL1", 0.5, m_maxTell1s + 0.5, m_maxTell1s );
  m_2d_errorTypes = book2D( "ErrorTypes", "Error types per TELL1", 0.5, m_maxTell1s + 0.5, m_maxTell1s, 0., 10., 10 );
  labelHistoErrorTypes( m_2d_errorTypes );

  // Get the tell1 mapping from source ID to tell1 number
  m_activePorts = 0;
  for ( auto&& [_, tellID] : readoutTool()->SourceIDToTELLNumberMap() ) {
    // Create a title for the histogram
    std::string strTellID  = std::to_string( tellID );
    HistoID     histoID    = "error-types_$tell" + strTellID;
    std::string histoTitle = "Error types UTTELL" + strTellID;
    m_errorHistos[tellID]  = book2D( histoID, histoTitle, 0., noptlinks, nports * noptlinks, 0., 10., 10 );
    labelHistoErrorTypes( m_errorHistos[tellID] );
    // Work out the total number of links for the detector
    m_activePorts += nports * noptlinks;
  }

  m_activePorts -= 128; //< Very nastily hardcoded for now.

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of active ports in UT is " << m_activePorts << endmsg;

  // Get the tell1 mapping from source ID to tell1 number
  m_nTELL1s = readoutTool()->nBoard();

  return StatusCode::SUCCESS;
}
//==============================================================================
// Set the labels for the error type histograms
//==============================================================================
void UTErrorMonitor::labelHistoErrorTypes( AIDA::IHistogram2D* histo ) {

  TH2D* h = Gaudi::Utils::Aida2ROOT::aida2root( histo );
  if ( h ) {
    // get the axis:
    TAxis* axis = h->GetYaxis();
    if ( axis ) {
      axis->SetBinLabel( 1, "None" );
      axis->SetBinLabel( 2, "CorruptedBank" );
      axis->SetBinLabel( 3, "OptLinkDisabled" );
      axis->SetBinLabel( 4, "TlkLinkLoss" );
      axis->SetBinLabel( 5, "OptLinkNoClock" );
      axis->SetBinLabel( 6, "SyncRAMFull" );
      axis->SetBinLabel( 7, "SyncEvtSize" );
      axis->SetBinLabel( 8, "OptLinkNoEvent" );
      axis->SetBinLabel( 9, "PseudoHeader" );
      axis->SetBinLabel( 10, "WrongPCN" );
    }
  }
}

//=============================================================================
// Main execution
//=============================================================================
void UTErrorMonitor::operator()( const UTTELL1BoardErrorBanks& errors ) const {

  m_1d_evtCounter->fill( 1. );
  // Get the error banks

  // Number of links with error banks
  unsigned int nErrors = 0;
  /// Store number of error banks/TELL1 (48 (42) Tell1s, 1->48 (42) for UT)
  std::vector<unsigned int> nErrorBanksPerTELL1( m_nTELL1s, 0 );

  // Loop over the error banks
  for ( const auto& bank : errors ) {

    // Print out the error bank information
    if ( msgLevel( MSG::DEBUG ) ) debug() << *bank << endmsg;

    // Get the number of the tell1
    unsigned int sourceID = bank->Tell1ID();
    unsigned int tellNum  = readoutTool()->SourceIDToTELLNumber( sourceID );

    // Plot the number of error banks versus sequential tell number
    m_1d_errorBanks->fill( tellNum );

    if ( !m_expertHisto.value() ) continue;

    // Convert the tell1 number to a string
    // std::string strTellNum  = std::to_string(tellNum);

    // Loop over the ErrorInfo objects (one for each FPGA-PP)
    const auto& errorInfo = bank->errorInfo();
    for ( auto errorIt = errorInfo.begin(); errorIt != errorInfo.end(); ++errorIt ) {

      // Get the PP number [0:3]
      unsigned int pp = errorIt - errorInfo.begin();

      // Get the majority vote for this PP
      unsigned int pcn = ( *errorIt )->pcnVote();

      // Loop over the Beetles and ports
      for ( unsigned int beetle = 0u; beetle < nBeetlesPerPPx; ++beetle ) {
        for ( unsigned int port = 0u; port < nports; ++port ) {
          // Fill 2D histogram with the error types versus Beetle port
          unsigned int errorType = ( *errorIt )->linkInfo( beetle, port, pcn );
          double       portBin   = double( port ) / double( nports ) + beetle + pp * nBeetlesPerPPx;
          m_errorHistos.at( tellNum )->fill( portBin, errorType );
          if ( errorType > 0 ) {
            nErrors++;
            nErrorBanksPerTELL1[tellNum - 1] += 1;
            m_2d_errorTypes->fill( tellNum, errorType );
          }
        }
      }
    }
  }

  double fracErrors = (double)nErrors / m_activePorts;
  if ( fracErrors > 1E-5 ) m_1d_fracErrors->fill( fracErrors );
  int iTell = 1;
  for ( const auto& i : nErrorBanksPerTELL1 ) { m_prof_fracErrors->fill( iTell++, i / ( nports * noptlinks ) ); }
}
//=============================================================================
