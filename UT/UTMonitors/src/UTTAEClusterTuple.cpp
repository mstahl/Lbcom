/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTTupleAlgBase.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTTAEClusterTuple
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTTAEClusterTuple UTTAEClusterTuple.h
 *
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 *
 *  Implementation of tupleing algorithm for TAE events.
 *  Dump clusters from each spill to tuple for the different service boxes.
 *
 */
namespace UT {
  class UTTAEClusterTuple : public TupleAlgBase {
  public:
    /// Standard constructor
    UTTAEClusterTuple( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "Number of events"};
    mutable Gaudi::Accumulators::Counter<> m_nBadChannels{this, "Number of bad channels found"};

    /// Book histograms

    std::vector<std::string> m_clusterLocations; ///< Input location for clusters
    unsigned int             m_nSamples;         ///< Number of input locations
    double                   m_maxSample;        ///< define the max bin of samples sample
    std::vector<std::string> m_spills;           ///< Vector of all possible spill location for TAE events

    /// Cut on the bunch ID (distinguish Beam from cosmics)
    std::vector<unsigned int>        m_bunchID;
    DataObjectReadHandle<LHCb::ODIN> m_odin{this, "ODINLocation", LHCb::ODINLocation::Default};
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTTAEClusterTuple )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTTAEClusterTuple::UTTAEClusterTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::TupleAlgBase( name, pSvcLocator ) {
  // Ordered list of cluster locations
  declareProperty( "ClusterLocations",
                   m_clusterLocations = {
                       "Event/Prev7/Raw/UT/Clusters", "Event/Prev6/Raw/UT/Clusters", "Event/Prev5/Raw/UT/Clusters",
                       "Event/Prev4/Raw/UT/Clusters", "Event/Prev3/Raw/UT/Clusters", "Event/Prev2/Raw/UT/Clusters",
                       "Event/Prev1/Raw/UT/Clusters", "Raw/UT/Clusters", "Event/Next1/Raw/UT/Clusters",
                       "Event/Next2/Raw/UT/Clusters", "Event/Next3/Raw/UT/Clusters", "Event/Next4/Raw/UT/Clusters",
                       "Event/Next5/Raw/UT/Clusters", "Event/Next6/Raw/UT/Clusters", "Event/Next7/Raw/UT/Clusters"} );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTTAEClusterTuple::initialize() {

  StatusCode sc = UT::TupleAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );

  m_nSamples = m_clusterLocations.size();
  // Time aligned events (Central +/- TAE)
  if ( m_nSamples % 2 == 0 ) {
    error() << "Expected odd number of Cluster Locations, got " << m_nSamples << endmsg;
    return StatusCode::FAILURE;
  }
  m_maxSample = ( m_nSamples + 1 ) / 2 - 1 + 0.5;

  // Loop over input locations and fill label vector
  m_spills          = {"Prev7", "Prev6", "Prev5", "Prev4", "Prev3", "Prev2", "Prev1", "Default",
              "Next1", "Next2", "Next3", "Next4", "Next5", "Next6", "Next7"};
  unsigned int diff = m_spills.size() - m_clusterLocations.size();
  m_spills.erase( m_spills.begin(), m_spills.begin() + diff / 2 );
  m_spills.resize( m_clusterLocations.size() );

  /* Loop over input locations
     Couldn't find a good way to do this in the base class yet */
  for ( auto itCL = m_clusterLocations.begin(); itCL != m_clusterLocations.end(); ++itCL ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Cluster Locations: " << ( *itCL ) << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode UT::UTTAEClusterTuple::execute() {
  // Select the correct bunch id
  const LHCb::ODIN* odin = m_odin.get();

  debug() << "==> Execute" << endmsg;
  std::vector<Tuple> tuples;

  unsigned int                        index = 0;
  std::map<std::string, unsigned int> tupleIndex;
  for ( const auto& svcBox : readoutTool()->serviceBoxes() ) {
    //    tuples[svcBox] = nTuple("TAEClusters"+svcBox);
    tuples.push_back( nTuple( svcBox ) );
    tupleIndex[svcBox] = index++;
  }

  ++m_nEvents;

  // code goes here

  // Loop over input locations
  unsigned int iSample = 0;
  for ( auto itCL = m_clusterLocations.begin(); itCL != m_clusterLocations.end(); ++itCL, ++iSample ) {
    // Check location exists
    LHCb::UTClusters* clusters = getIfExists<LHCb::UTClusters>( *itCL );
    if ( !clusters ) continue;

    // Loop over clusters
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Number of clusters in " << ( *itCL ) << " is " << clusters->size() << endmsg;
    }
    int sample = static_cast<int>( iSample - ( m_nSamples - 1 ) / 2 );
    for ( const LHCb::UTCluster* cluster : *clusters ) {

      LHCb::UTChannelID chanID = cluster->channelID();
      const DeUTSector* sector = findSector( chanID );
      double            noise  = sector->noise( chanID );
      if ( noise == 0 ) continue;

      // find which ladders are short and thick.. and skip those...
      if ( sector->thickness() > 350. * Gaudi::Units::um && sector->type() == "Short" ) continue;

      // Loop over the strips in this cluster and find the highest ADC
      unsigned int highestADC = 0;
      // int highestStrip = 0;
      // highestStrip = 0;

      bool                       foundBadChannel = false;
      LHCb::UTCluster::ADCVector adcVec          = cluster->stripValues();
      for ( auto iVec = adcVec.begin(); iVec != adcVec.end(); ++iVec ) {
        if ( iVec->second > highestADC ) {
          // highestStrip = iVec->first;
          highestADC = iVec->second;
          // Check for bad channels
          LHCb::UTChannelID iChan = LHCb::UTChannelID( int( chanID ) + iVec->first );
          if ( !findSector( iChan )->isOKStrip( iChan ) ) foundBadChannel = true;
        }
      }
      // If there is any strip in this cluster that is bad: ignore cluster
      if ( foundBadChannel ) {
        ++m_nBadChannels;
        continue;
      }

      // Determine the layer
      int layer = ( cluster->isUT() ) ? cluster->layer() + 2 * ( cluster->station() - 1 ) : cluster->layer();

      unsigned int sourceID = cluster->sourceID();
      unsigned int tell1ID  = readoutTool()->SourceIDToTELLNumber( sourceID );
      std::string  svcBox   = readoutTool()->serviceBox( cluster->channelID() );
      unsigned int index    = tupleIndex[svcBox];
      tuples[index]->column( "sourceID", sourceID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "tell1ID", tell1ID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      tuples[index]
          ->column( "sectorID", chanID.uniqueSector() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "sectorNr", chanID.sector() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      tuples[index]->column( "thickness", sector->thickness() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]
          ->column( "nSensors", (int)( sector->sensors() ).size() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "channelID", chanID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "layer", layer ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      tuples[index]->column( "strip", cluster->strip() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "clusSize", cluster->size() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]
          ->column( "clusCharge", cluster->totalCharge() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]
          ->column( "stn", cluster->totalCharge() / noise )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]
          ->column( "maxCharge", cluster->maxADCValue() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      tuples[index]->column( "spill", sample ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      tuples[index]->column( "step", odin->calibrationStep() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->column( "BXID", odin->bunchId() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      tuples[index]->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    // fill the tuple
  }

  return StatusCode::SUCCESS;
}
