/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "Event/UTTELL1Data.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTFun.h"
#include "Kernel/UTHistoAlgBase.h"
#include "UTDet/DeUTSector.h"
#include <boost/assign/std/vector.hpp>

using namespace LHCb;
using namespace LHCbConstants;
using namespace boost::assign;
using namespace Gaudi::Units;

/** @class UTPulseMonitor UTPulseMonitor.h
 *
 *  Class for monitoring the pulse shape as a function of the spill. It uses
 *  TAE NZS data to get the full shape of the pulse.
 *  - First it loops over all input clusters. This means that you provide the
 *    decoded clusters for a given spill. If you want to keep this part
 *    flexible, you can also decode the clusters in all spills and then merge
 *    them using the UTEventMerge algorithm.
 *  - Then, for each cluster it loops over the spills and determines the charge
 *    in the strips of this cluster.
 *
 *  The following histograms are produced per service box for each spill:
 *  - Histogram with the total charge.
 *  - Histogram with the highest charge in the cluster.
 *  - Four histograms with the charge of 1-,2-,3- and 4-strip clusters.
 *  The same histograms are also produced with signal over noise (S/N).
 *
 *  Main job options:
 *  - <b>DetType</b>: "UT"
 *  - <b>ClusterLocation</b>: Location of the input clusters. The central spill
 *    is used per default. When merging the events use: "Raw/UT/MergedSpills".
 *  - <b>InputData</b>: Location of the NZS data. By default it takes the
 *    common-mode-subtracted data (from the emulation). Alternatively, you can
 *    also take the pedestal subtracted ADCs: "Raw/UT/PedSubADCs".
 *  - <b>Spills</b>: List to the spills to loop over. By default this list is
 *    set to: {"Prev2","Prev1","Central","Next1","Next2"}.
 *  - <b>ChargeCut</b>: Minimum cut on the charge of the cluster. By default
 *    it is set to -1.0 (i.e. not used).
 *  - <b>BunchID</b>: Possibility to select the bunch IDs. This can remove
 *    (cosmic) background events. For October09 TED it should be set to 2686.
 *    By default this list is empty, which means it is not used.
 *  - <b>DirNames</b>: List of strings that determines whether the histograms in
 *    the subdivided within a service box. For instance, when specifying "type",
 *    it will subdivide the histograms per sector type (within a service box).
 *    When specifying "sector", it will subdivide the histograms per sector
 *    (within a service box). By default this list is set to: {"all"}, which
 *    means that there is no subdivision.
 *  - <b>UseNZSdata</b>: Flag to use NZS data. Default is set to true. In that
 *    case the InputData has to be in the NZS format. If this flag is false the
 *    InputData location has to contain UTClusters.
 *
 *  A presentation on this algorithm and the results was giving on 20.10.2009 in
 *  the UT TED run analysis meeting by Helge: http://indico.cern.ch/event/71185
 *
 *  @author A. Beiter (based on code by J. van Tilburg)
 *  @date   2018-09-04
 */

class UTPulseMonitor : public UT::HistoAlgBase {

public:
  /// Standard constructer
  UTPulseMonitor( const std::string& name, ISvcLocator* svcloc );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  mutable Gaudi::Accumulators::Counter<> m_nSelectedEvents{this, "Number of selected events"};

  // internal event counter
  int m_evtNumber;

  // jobOptions:

  /// Location in the Event Data Store with the ADC values
  std::string m_dataLocation;

  /// Location in the Event Data Store with the UTClusters
  std::string m_clusterLocation;

  /// List to the spills to loop over. E.g. "Next1", "Central", "Prev2", etc.
  std::vector<std::string> m_spills;

  /// Cut on the bunch ID (distinguish TED from cosmics)
  std::vector<unsigned int> m_bunchID;

  bool   m_useNZSdata; ///< Flag to use either NZS or ZS data
  double m_chargeCut;  ///< Cut on the total charge of the UTClusters
  /// List of directory names to subidvide the sectors in the service box.
  /// Possible names are "all", "sector", "type".
  std::vector<std::string> m_dirNames;
};

DECLARE_COMPONENT( UTPulseMonitor )

//--------------------------------------------------------------------
//
//  UTPulseMonitor: Extract the pulse shape from the NZS data.
//
//      More details can be found in UTPulseMonitor.h
//
//--------------------------------------------------------------------

UTPulseMonitor::UTPulseMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {
  // constructer
  declareProperty( "ClusterLocation", m_clusterLocation = LHCb::UTClusterLocation::UTClusters );
  declareProperty( "InputData", m_dataLocation = "Raw/UT/LCMSADCs" );
  declareProperty( "Spills", m_spills = {{"Prev2"}, {"Prev1"}, {"Central"}, {"Next1"}, {"Next2"}} );
  declareProperty( "ChargeCut", m_chargeCut = -1.0 ); // ADC counts
  declareProperty( "BunchID", m_bunchID );            // BunchID
  declareProperty( "DirNames", m_dirNames = {{"all"}} );
  declareProperty( "UseNZSdata", m_useNZSdata = true );
}

StatusCode UTPulseMonitor::initialize() {
  // Initialize UT::HistoAlgBase
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // initialize event counter
  m_evtNumber = 0;

  return StatusCode::SUCCESS;
}

StatusCode UTPulseMonitor::execute() {
  m_evtNumber++;

  // Select the correct bunch id
  const ODIN* odin = getIfExists<ODIN>( ODINLocation::Default );
  if ( odin ) {
    if ( !m_bunchID.empty() && std::find( m_bunchID.begin(), m_bunchID.end(), odin->bunchId() ) == m_bunchID.end() )
      return StatusCode::SUCCESS;
  } else
    return Warning( "No ODIN bank found", StatusCode::SUCCESS, 1 );

  ++m_nSelectedEvents;

  // Get the NZS or ZS data for the different spills
  std::vector<UTTELL1Datas*> dataNZS;
  std::vector<UTClusters*>   dataZS;
  for ( unsigned int iSpill = 0; iSpill < m_spills.size(); ++iSpill ) {
    std::string spillName = m_spills[iSpill];
    if ( spillName == "Central" )
      spillName.clear();
    else
      spillName.insert( '/', 0 );
    std::string dataLocation = "/Event" + spillName + "/" + m_dataLocation;
    if ( m_useNZSdata ) {
      if ( !exist<UTTELL1Datas>( dataLocation ) )
        dataNZS.push_back( 0 );
      else
        dataNZS += get<UTTELL1Datas>( dataLocation );
    } else {
      if ( !exist<UTClusters>( dataLocation ) )
        dataZS.push_back( 0 );
      else
        dataZS += get<UTClusters>( dataLocation );
    }
  }

  const LHCb::UTClusters* clusters = getIfExists<LHCb::UTClusters>( m_clusterLocation );

  // Check cluster location exists
  if ( !clusters ) return StatusCode::SUCCESS;

  // Loop over the clusters
  for ( const LHCb::UTCluster* cluster : *clusters ) {

    // Select clusters above a certain threshold

    if ( cluster->totalCharge() < m_chargeCut ) continue;

    // Get the DetectorElement
    const DeUTSector* sector = findSector( cluster->channelID() );

    // find which ladders are short and thick.. and skip those...
    if ( sector->thickness() > 350 * um && sector->type() == "Short" ) continue;

    // Get the sourceID of the Tell1 for this cluster
    UTChannelID chanID   = cluster->channelID();
    int         sourceID = cluster->sourceID();

    // Get the noise corresponding to this cluster
    double noise = sector->noise( chanID );

    // Loop over the strips in this cluster and find the highest ADC
    unsigned int                         highestADC      = 0;
    int                                  highestStrip    = 0;
    bool                                 foundBadChannel = false;
    UTCluster::ADCVector                 adcVec          = cluster->stripValues();
    UTCluster::ADCVector::const_iterator iVec            = adcVec.begin();
    for ( ; iVec < adcVec.end(); ++iVec ) {
      if ( iVec->second > highestADC ) {
        highestStrip = iVec->first;
        highestADC   = iVec->second;
        // Check for bad channels
        UTChannelID iChan = UTChannelID( int( chanID ) + iVec->first );
        if ( !findSector( iChan )->isOKStrip( iChan ) ) foundBadChannel = true;
      }
    }
    // If there is any strip in this cluster that is bad: ignore cluster
    if ( foundBadChannel ) continue;

    // Loop over the spills
    for ( unsigned int iSpill = 0; iSpill < m_spills.size(); ++iSpill ) {
      std::string spillName = m_spills[iSpill];

      double charge     = 0.0;
      double highCharge = 0.0;

      if ( m_useNZSdata ) {
        if ( dataNZS[iSpill] == 0 ) continue;
        UTTELL1Data* boardData = dataNZS[iSpill]->object( sourceID );
        if ( boardData == 0 ) {
          std::string sWarning = "No valid data from source " + std::to_string( sourceID ) + " in " + spillName;
          Warning( sWarning, StatusCode::SUCCESS, 1 ).ignore();
          continue;
        }
        const UTTELL1Data::Data& dataValues = boardData->data();
        // Loop over the strips in this cluster and calculate charge in ADC
        UTCluster::ADCVector::const_iterator iVec = adcVec.begin();

        for ( ; iVec < adcVec.end(); ++iVec ) {
          // Determine Tell1 channel of this strip
          UTChannelID     iChan  = UTChannelID( int( chanID ) + iVec->first );
          UTDAQ::chanPair cPair  = readoutTool()->offlineChanToDAQ( iChan, 0.0 );
          int             iStrip = cPair.second;

          // Add the charges and find the highest charge
          charge += dataValues[iStrip / nStripsInBeetle][iStrip % nStripsInBeetle];
          if ( iVec->first == highestStrip )
            highCharge = dataValues[iStrip / nStripsInBeetle][iStrip % nStripsInBeetle];
        }
      } else { // use the ZS data (i.e. use the clusters)
        if ( dataZS[iSpill] == 0 ) continue;
        UTClusters::iterator iclus = dataZS[iSpill]->begin();
        while ( iclus != dataZS[iSpill]->end() && !( *iclus )->overlaps( cluster ) ) ++iclus;
        if ( iclus != dataZS[iSpill]->end() ) {
          charge     = ( *iclus )->totalCharge();
          highCharge = ( *iclus )->maxADCValue();
        } else {
          continue; // To avoid peak at zero in Landau plot
        }
      }

      std::string sb       = readoutTool()->serviceBox( chanID );
      std::string clusSize = std::to_string( cluster->size() );

      // Loop over the directories (can be used to subdivide the service box)
      std::vector<std::string>::const_iterator iDir = m_dirNames.begin();
      for ( ; iDir != m_dirNames.end(); ++iDir ) {

        // Create the directory name.
        std::string dir = *iDir;
        if ( *iDir == "sector" ) // if it contains "sector", add sector name
          dir += sector->nickname();
        if ( *iDir == "type" ) // if it contains "type", add sector type
          dir += sector->type();

        // Make histograms for the whole detector
        plot1D( charge, dir + "/Charge" + spillName, "Charge " + spillName, -50.5, 150.5, 201 );
        plot1D( charge, dir + "/Charge" + clusSize + spillName, "Charge " + clusSize + "-strip " + spillName, -50.5,
                150.5, 201 );
        plot1D( highCharge, dir + "/HighestCharge" + spillName, "Highest charge " + spillName, -50.5, 150.5, 201 );
        if ( noise > 0.0 ) {
          plot1D( charge / noise, dir + "/SN" + spillName, "Signal over noise " + spillName, -50.5, 150.5, 201 );
          plot1D( charge / noise, dir + "/SN" + clusSize + spillName,
                  "Signal over noise " + clusSize + "-strip " + spillName, -50.5, 150.5, 201 );
          plot1D( highCharge / noise, dir + "/HighestSN" + spillName, "Highest signal over noise " + spillName, -50.5,
                  150.5, 201 );
        }

        // Make histograms per service box
        plot1D( charge, sb + "/" + dir + "/Charge" + spillName, "Charge " + spillName, -50.5, 150.5, 201 );
        plot1D( charge, sb + "/" + dir + "/Charge" + clusSize + spillName, "Charge " + clusSize + "-strip " + spillName,
                -50.5, 150.5, 201 );
        plot1D( highCharge, sb + "/" + dir + "/HighestCharge" + spillName, "Highest charge " + spillName, -50.5, 150.5,
                201 );
        if ( noise > 0.0 ) {
          plot1D( charge / noise, sb + "/" + dir + "/SN" + spillName, "Signal over noise " + spillName, -50.5, 150.5,
                  201 );
          plot1D( charge / noise, sb + "/" + dir + "/SN" + clusSize + spillName,
                  "Signal over noise " + clusSize + "-strip " + spillName, -50.5, 150.5, 201 );
          plot1D( highCharge / noise, sb + "/" + dir + "/HighestSN" + spillName,
                  "Highest signal over noise " + spillName, -50.5, 150.5, 201 );
        }
      }
    }

    plot1D( double( cluster->totalCharge() ), "ChargeOfCluster", "Charge of cluster", -50.5, 150.5, 201 );
    if ( noise > 0.0 ) {
      plot1D( double( cluster->totalCharge() ) / noise, "SNofCluster", "Signal over noise of cluster", -50.5, 150.5,
              201 );
    }
    plot1D( double( cluster->size() ), "SizeOfCluster", "Size of cluster", 0.5, 4.5, 4 );

  } // End of cluster iterator

  plot1D( double( m_evtNumber ), "NumClustersVsEvt", "Number of clusters vs event number", -0.5, 100.5, 101,
          double( clusters->size() ) );

  return StatusCode::SUCCESS;
}
