/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTDQCounters_H
#define UTDQCounters_H 1

/** @class UTDQCounters UTDQCounters.h
 *
 *  Wrapper-Class containing the counters
 *  for UTDQSummaryAlg
 *
 *  @author A.Beiter (based on code by N.Chiapolini)
 *  @date   2018-09-04
 */

#include <algorithm>
#include <cmath>
#include <iterator>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
//#include <boost/accumulators/statistics/density.hpp>

using namespace boost::accumulators;

class UTDQCounters {

public:
  /// constructor
  UTDQCounters();
  UTDQCounters( int minADC, int maxADC );

  int  chargeMPV();
  void addCharge( double );

  /// typedef template for counters
  template <typename T>
  struct Type {
    typedef accumulator_set<T, stats<tag::sum, tag::mean>> Counter;
  };

  /** Counters */
  unsigned int          m_event;       ///< number of events
  std::string           m_RunDate;     ///< date of this run
  Type<int>::Counter    m_sumClusters; ///< total clusters
  Type<int>::Counter    m_sumNoise;    ///< noise clusters
  Type<double>::Counter m_sumEff;      ///< processing efficiency
  Type<int>::Counter    m_nError;      ///< total number of error banks
  Type<int>::Counter    m_nCorrupted;  ///< corrupted banks
  Type<int>::Counter    m_sumMissing;  ///< missing banks

  typedef std::vector<std::string> Strings;
  static Strings                   m_txtColumns; ///< text file column headers

  /// Struct containing the data collected for one run
  struct DataRow {
    int          run; ///< run number
    std::string  runDate;
    unsigned int event;      ///< number of events
    double       clus;       ///< mean number of clusters
    double       noise;      ///< mean number of noise clusters
    double       procEff;    ///< mean processing efficiency
    int          error;      ///< total number of error banks
    int          corrupted;  ///< total number of corrupted banks
    int          sumMissing; ///< total number of missing banks
    int          chargeMPV;  ///< Most probable charge value
  };

private:
  void init( int minADC, int maxADC );

  std::vector<int> m_chargeHist;
  int              m_minADC, m_maxADC;
  bool             m_entries;
};

UTDQCounters::Strings UTDQCounters::m_txtColumns( 0, "" );

void UTDQCounters::init( int minADC, int maxADC ) {
  m_event = 0;

  // prepare m_chargeHist
  m_minADC = minADC;
  m_maxADC = maxADC;
  m_chargeHist.resize( m_maxADC - m_minADC );
  m_entries = false;

  if ( !m_txtColumns.size() ) {
    m_txtColumns.push_back( "RunNr" );
    m_txtColumns.push_back( "RunDate" );
    m_txtColumns.push_back( "Events" );
    m_txtColumns.push_back( "Clusters/evt" );
    m_txtColumns.push_back( "#Noise/event" );
    m_txtColumns.push_back( "Proc InEff" );
    m_txtColumns.push_back( "#ErrorBanks" );
    m_txtColumns.push_back( "#Corrupted" );
    m_txtColumns.push_back( "#Missing" );
    m_txtColumns.push_back( "Charge MPV" );
    m_txtColumns.push_back( "Comments" );
  }
}

UTDQCounters::UTDQCounters() { init( 15, 45 ); }
UTDQCounters::UTDQCounters( int minADC, int maxADC ) { init( minADC, maxADC ); }

void UTDQCounters::addCharge( double charge ) {
  if ( ( charge < m_minADC ) || ( charge >= m_maxADC ) ) { return; }
  m_entries = true;
  m_chargeHist[(int)( floor( charge ) - m_minADC )]++;
}

int UTDQCounters::chargeMPV() {
  if ( !m_entries ) { return 0; }
  return std::distance( m_chargeHist.begin(), std::max_element( m_chargeHist.begin(), m_chargeHist.end() ) ) + m_minADC;
}

#endif
