/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDetectorPlot.h"
#include "Kernel/UTTupleAlgBase.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTDumpClusters
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTDumpClusters UTDumpClusters.h
 *
 * Dump the UTClusters into an ntuples containing:
 *
 * "run"                   : run number
 * "event"                 : event number
 * "nClus"                 : Total number of clusters
 * "tell1[m_Clusters]",    : TELL1 board which produced cluster
 * "tellChan[m_Clusters]", : TELL1 channel of the highest strip (0 to 3071)
 * "channelID[m_Clusters]",: UTChannelID of the highest strip
 * "strip[m_Clusters]",    : strip number of the highest strip (1 to 512)
 * "station[m_Clusters]",  : station number from the UTChannelID for the highest strip
 * "layer[m_Clusters]",    : layer number from the UTChannelID for the highest strip
 * "detRegion[m_Clusters]",: detRegion from the UTChannelID for the highest strip
 * "sector[m_Clusters]",   : sector number from the UTChannelID for the highest strip
 * "fracStrip[m_Clusters]",: interstrip fraction
 * "size[m_Clusters]",     : cluster size
 * "charge[m_Clusters]",   : total charge of the cluster
 * "strip1[m_Clusters]",   : strip number 1
 * "strip2[m_Clusters]",   : strip number 2
 * "strip3[m_Clusters]",   : strip number 3
 * "strip4[m_Clusters]",   : strip number 4
 * "adc1[m_Clusters]",     : ADC value of strip1
 * "adc2[m_Clusters]",     : ADC value of strip2
 * "adc3[m_Clusters]",     : ADC value of strip3
 * "adc4[m_Clusters]",     : ADC value of strip4
 *
 * The size of the array can be changed via the job option MaximumNumberClusters
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */

namespace UT {
  class UTDumpClusters : public Gaudi::Functional::Consumer<void( const LHCb::ODIN&, const LHCb::UTClusters& ),
                                                            Gaudi::Functional::Traits::BaseClass_t<TupleAlgBase>> {
  public:
    /// Standard constructor
    UTDumpClusters( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    void       operator()( const LHCb::ODIN&, const LHCb::UTClusters& ) const override;

  private:
    void dumpClusterMapping( const LHCb::UTCluster* cluster ) const;

    Gaudi::Property<unsigned int> m_nClusters{this, "MaxNumberClusters",
                                              5000}; ///< Defines the maximum number of clusters expected
    Gaudi::Property<bool>         m_dumpMap{this, "DumpMapping", false}; ///< Build mapping to TELL1 and sectors

    mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "Number of events"};

    mutable std::bitset<10000> m_sectorTELL1s; ///<<< a map of cluster sector to TELL1
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTDumpClusters )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTDumpClusters::UTDumpClusters( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"ClusterLocation", LHCb::UTClusterLocation::UTClusters}}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTDumpClusters::initialize() {
  return UT::TupleAlgBase::initialize().andThen( [&] { m_sectorTELL1s.set(); } );
}

//=============================================================================
// Main execution
//=============================================================================
void UT::UTDumpClusters::operator()( const LHCb::ODIN& odin, const LHCb::UTClusters& clusters ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_nEvents;

  const unsigned int nClusters = clusters.size();
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of clusters in " << inputLocation<LHCb::UTClusters>() << " is " << nClusters << endmsg;

  const Gaudi::Time odinTime = odin.eventTime();

  Tuple tuple = nTuple( "Clusters", "Information stored on the cluster" );

  std::vector<double>       clusterCharge( m_nClusters, 0. );
  std::vector<unsigned int> clusterSize( m_nClusters, 0 );
  std::vector<unsigned int> uniqueSector( m_nClusters, 0 );
  std::vector<unsigned int> channelID( m_nClusters, 0 );
  std::vector<unsigned int> strip( m_nClusters, 0 );
  std::vector<unsigned int> station( m_nClusters, 0 );
  std::vector<unsigned int> layer( m_nClusters, 0 );
  std::vector<unsigned int> detRegion( m_nClusters, 0 );
  std::vector<unsigned int> sector( m_nClusters, 0 );
  std::vector<double>       interStripFrac( m_nClusters, 0. );
  std::vector<unsigned int> tell1ID( m_nClusters, 0 );
  std::vector<unsigned int> tell1Channel( m_nClusters, 0 );
  //    std::vector<unsigned int> countCluster(m_nClusters,0);
  std::vector<std::vector<unsigned int>> adcs( 4, std::vector<unsigned int>( m_nClusters, 0 ) );
  std::vector<std::vector<unsigned int>> strips( 4, std::vector<unsigned int>( m_nClusters, 0 ) );
  // Loop over clusters
  unsigned int iClus = 0;
  if ( nClusters > m_nClusters )
    debug() << "Number of clusters bigger than buffer size: n=" << nClusters << ",buf=" << m_nClusters << endmsg;
  for ( const LHCb::UTCluster* cluster : clusters ) {
    // only fill ntuple for clusters less than tuple array size
    if ( iClus < m_nClusters ) {

      clusterSize[iClus]          = cluster->size();
      clusterCharge[iClus]        = cluster->totalCharge();
      uniqueSector[iClus]         = cluster->channelID().uniqueSector();
      channelID[iClus]            = cluster->channelID();
      strip[iClus]                = cluster->strip();
      station[iClus]              = cluster->station();
      layer[iClus]                = cluster->layer();
      detRegion[iClus]            = cluster->detRegion();
      sector[iClus]               = cluster->sector();
      interStripFrac[iClus]       = cluster->interStripFraction();
      const unsigned int sourceID = cluster->sourceID();
      tell1ID[iClus]              = ( this->readoutTool() )->SourceIDToTELLNumber( sourceID );
      tell1Channel[iClus]         = cluster->tell1Channel();
      //      countCluster[iClus] = iClus;
      unsigned int                                              iStrip = 0;
      std::vector<std::pair<int, unsigned int>>::const_iterator iStp   = cluster->stripValues().begin();
      for ( ; iStp != cluster->stripValues().end(); ++iStp, ++iStrip ) {
        strips[iStrip][iClus] = strip[iClus] + ( *iStp ).first;
        adcs[iStrip][iClus]   = ( *iStp ).second;
      }
      ++iClus;
    }
    if ( m_dumpMap.value() ) dumpClusterMapping( cluster );
  } // End of cluster iterator
  tuple->column( "run", odin.runNumber() ).ignore();
  tuple->column( "event", static_cast<unsigned int>( odin.eventNumber() ) ).ignore();
  tuple->column( "nClus", nClusters ).ignore();
  if ( fullDetail() ) {
    tuple->column( "day", odinTime.day( 0 ) ).ignore();
    tuple->column( "month", odinTime.month( 0 ) + 1 ).ignore();
    tuple->column( "year", odinTime.year( 0 ) ).ignore();
    tuple->column( "hour", odinTime.hour( 0 ) ).ignore();
    tuple->column( "minute", odinTime.minute( 0 ) ).ignore();
    tuple->column( "second", odinTime.second( 0 ) ).ignore();
  }
  //    tuple->farray( "cluster",   countCluster,   "n", m_nClusters );
  tuple->farray( "tell1", tell1ID, "n", m_nClusters ).ignore();
  tuple->farray( "tellChan", tell1Channel, "n", m_nClusters ).ignore();
  tuple->farray( "channelID", channelID, "n", m_nClusters ).ignore();
  tuple->farray( "strip", strip, "n", m_nClusters ).ignore();
  tuple->farray( "station", station, "n", m_nClusters ).ignore();
  tuple->farray( "layer", layer, "n", m_nClusters ).ignore();
  tuple->farray( "detRegion", station, "n", m_nClusters ).ignore();
  tuple->farray( "sector", sector, "n", m_nClusters ).ignore();
  tuple->farray( "fracStrip", interStripFrac, "n", m_nClusters ).ignore();
  tuple->farray( "size", clusterSize, "n", m_nClusters ).ignore();
  tuple->farray( "charge", clusterCharge, "n", m_nClusters ).ignore();
  tuple->farray( "strip1", strips[0], "n", m_nClusters ).ignore();
  tuple->farray( "strip2", strips[1], "n", m_nClusters ).ignore();
  tuple->farray( "strip3", strips[2], "n", m_nClusters ).ignore();
  tuple->farray( "strip4", strips[3], "n", m_nClusters ).ignore();
  tuple->farray( "adc1", adcs[0], "n", m_nClusters ).ignore();
  tuple->farray( "adc2", adcs[1], "n", m_nClusters ).ignore();
  tuple->farray( "adc3", adcs[2], "n", m_nClusters ).ignore();
  tuple->farray( "adc4", adcs[3], "n", m_nClusters ).ignore();

  tuple->write().ignore();
}

//==============================================================================
// Return cluster mapping to TELL1, bins for the hitmap plot
//==============================================================================
void UT::UTDumpClusters::dumpClusterMapping( const LHCb::UTCluster* cluster ) const {
  if ( m_sectorTELL1s.test( cluster->channelID().uniqueSector() ) ) {
    const unsigned int sourceID = cluster->sourceID();
    unsigned int       TELL1ID  = ( this->readoutTool() )->SourceIDToTELLNumber( sourceID );
    int                n;
    std::string        idMap = "UT cluster map";
    double             x;
    double             yMin, yMax;
    n                                 = 512;
    const DeUTSector*        aSector  = tracker()->findSector( cluster->channelID() );
    const DeUTSector*        utSector = dynamic_cast<const DeUTSector*>( aSector );
    UT::UTDetectorPlot       hitMap( idMap, idMap );
    UT::UTDetectorPlot::Bins bins = hitMap.toBins( utSector );
    x                             = bins.xBin;
    yMin                          = bins.beginBinY;
    yMax                          = bins.endBinY;
    //      v.push_back(new UTSectorHistoID::UTSectorHistoID(2337, "UTaXRegionCSector1", 7, 2 ,10));
    std::string svcBox = readoutTool()->serviceBox( cluster->channelID() );
    std::cout << ( this->name() ) << " v.push_back(new UTSectorHistoID::UTSectorHistoID("
              << cluster->channelID().uniqueSector() << ", \"" << cluster->sectorName() << "\", " << TELL1ID << ", "
              << cluster->tell1Channel() / n << ", \"" << svcBox << "\", " << x << ", " << yMin << ", " << yMax << "));"
              << std::endl;
    m_sectorTELL1s.reset( cluster->channelID().uniqueSector() );
  }
}
