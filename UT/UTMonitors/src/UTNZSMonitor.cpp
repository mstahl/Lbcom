/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Event/ODIN.h"
#include "Event/UTTELL1Data.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/IUTNoiseCalculationTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTHistoAlgBase.h"
#include "TH2D.h"
#include "TProfile.h"

using namespace LHCb;
using namespace AIDA;
using namespace UTDAQ;
using namespace UTBoardMapping;

/** @class UTNZSMonitor UTNZSMonitor.h
 *
 *  Class for monitoring the noise of the Tell1's. For each Tell1 the noise
 *  versus the strip number is stored in a histogram. The histograms can be
 *  stored using sourceID or tell1 name with the option \b UseSourceID. The
 *  noise is calculated using a IUTNoiseCalculationTool.  The algorithm can be
 *  configured using the following options:
 *  - \b UpdateRate: Rate at which the histograms are updated (in number of
 *    events). Useful in online mode. Set to -1 to update only at the end.
 *  - \b NoiseToolType: Sets the type of the noise calculation tool. (default is UT::UTNoiseCalculationTool)
 *  - \b NoiseToolName: Sets the name of the noise calculation tool. (default is UTNoiseCalculationTool)
 *  - \b UseRawNoise: fills histograms with the RAW noise (Default is CMS)
 *  - \b UsePedSubNoise: fills histograms with the noise after pedestal subtraction (Default is CMS)
 *
 *  @author A. Beiter (based on code by J. van Tilburg, N. Chiapolini, and M. Tobin)
 *  @date   2018-09-04
 *
 */

class UTNZSMonitor : public UT::HistoAlgBase {

public:
  /// Standard constructer
  UTNZSMonitor( const std::string& name, ISvcLocator* svcloc );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Book histograms
  void bookHistograms();

  /// Fill the noise histograms (only called every N events and at finalize)
  void updateNoiseHistogram( unsigned int tell1ID, bool updateTitle = false );

  /// Fill the 1- and 2-d summary plots of noise, pedestals for every strip....
  void updateSumamryPlots();

  int m_evtNumber = 0;

  unsigned int m_nTELL1s; ///< Number of TELL1 boards expected.

  // jobOptions:

  /// Location in the Event Data Store with the ADC values
  std::string m_dataLocation;

  /** When set to true: use the sourceID in the histogram name. Otherwise use
      the tell1 name. **/
  bool m_useSourceID;

  /** Rate at which the histograms are updated (in number of events).
      Set to -1 to update only at the end. **/
  int m_updateRate;

  /// Set rate for update of summary histograms
  int m_summaryUpdateRate;

  /// Update summary plots of noise, pedestals for all strips (1- and 2- histograms of noise/pedestal)
  void updateSummaryPlots();

  /// Dumps noise calculation variables to histograms for debugging
  bool m_checkCalculation;

  /// Plot noise calculation variables (mean, mean squared and rms)
  void dumpNoiseCalculation( unsigned int sourceID );

  std::vector<unsigned int> m_limitToTell;   /// List of TELL1s to look at
  bool                      m_selectedTells; ///< Use only selected TELL1s

  bool        m_useODINTime; ///< ODIN time of first event is added to the histogram title in the finalize method
  std::string m_odinEvent;   ///< String of the time of the first run

  bool m_rawNoise; ///< Fill histograms with the RAW noise

  bool m_pedSubNoise; ///< Fill histograms with the noise after pedestal subtraction

  /// Map of noise histograms booked in initialize
  std::map<int, TProfile*> m_noiseHistos;

  /// Map of pedestal histograms booked in initialize
  std::map<int, TProfile*> m_pedestalHistos;

  TH2D* m_2d_noisePerLinkVsTell1    = nullptr; ///< 2d map of noise vs link
  TH2D* m_2d_pedestalPerLinkVsTell1 = nullptr; ///< 2d map of pedestal vs link

  /// 2d map used for normalisation of noise, pedestal plots as 2d profile histograms are not supported by online
  /// monitoring
  TH2D* m_2d_normalisationPerLinkVsTell1 = nullptr;

  /// Histograms of all strips combined
  AIDA::IHistogram1D* m_1d_noise    = nullptr; ///< Noise of each strip
  AIDA::IHistogram1D* m_1d_pedestal = nullptr; ///< Pedestal of each strip

  UT::IUTNoiseCalculationTool* m_noiseTool = nullptr; ///< Tool to calculate noise
  std::string                  m_noiseToolType;       ///< Tool type (default is UTNoiseCalculationTool)
  std::string                  m_noiseToolName;       ///< Tool name (default is UTNoiseCalculationTool)

  /// Period of the an exponential moving average.(read from the noise tool)
  int m_followingPeriod;

  /// Rate at which the counters for histograms are reset (read from the noise tool)
  int m_resetRate;

  /// Number of events to be skipped (read from noise tool)
  int m_skipEvents;

  DataObjectReadHandle<ODIN> m_odin{this, "ODINLocation", LHCb::ODINLocation::Default};
};

DECLARE_COMPONENT( UTNZSMonitor )

//--------------------------------------------------------------------
//
//  UTNZSMonitor
//
//--------------------------------------------------------------------

UTNZSMonitor::UTNZSMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {
  // constructer
  declareProperty( "UseSourceID", m_useSourceID = true );
  declareProperty( "UpdateRate", m_updateRate = -1 );
  declareProperty( "UpdateSummaryRate", m_summaryUpdateRate = -1 );

  // Debugging
  declareProperty( "CheckNoiseCalculation", m_checkCalculation = false );
  // Limit calculation to vector of tell1s given in terms of TELLID (eg TTTELL1 = 1)
  declareProperty( "LimitToTell", m_limitToTell );

  // Use ODIN time in histograms
  declareProperty( "UseODINTime", m_useODINTime = false );

  /// Noise calculation tool
  declareProperty( "NoiseToolType", m_noiseToolType = "UT::UTNoiseCalculationTool" );
  declareProperty( "NoiseToolName", m_noiseToolName = "UTNoiseCalculationTool" );

  /// Plot RAW noise - default is CMS noise
  declareProperty( "UseRawNoise", m_rawNoise = false );

  /// Plot noise after pedestal subtraction - default is CMS noise
  declareProperty( "UsePedSubNoise", m_pedSubNoise = false );
}

StatusCode UTNZSMonitor::initialize() {
  // Initialize UT::HistoAlgBase
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_evtNumber = 0;

  m_noiseTool = tool<UT::IUTNoiseCalculationTool>( m_noiseToolType, m_noiseToolName );

  // Read following period, reset rate and skip events from configuration of tool
  m_followingPeriod = m_noiseTool->followPeriod();
  m_resetRate       = m_noiseTool->resetRate();
  m_skipEvents      = m_noiseTool->skipEvents();

  // Select small number of TELL1s (useful for debugging) then book histograms
  m_nTELL1s       = readoutTool()->nBoard();
  m_selectedTells = false;
  if ( m_limitToTell.size() > 0 ) {
    m_selectedTells = true;
    sort( m_limitToTell.begin(), m_limitToTell.end() );
  }

  if ( m_rawNoise && m_pedSubNoise ) {
    Warning( "Trying to plot raw noise and ped sub noise, setting m_pedSubNoise to false", StatusCode::SUCCESS, 1 )
        .ignore();
    m_pedSubNoise = false;
  }
  bookHistograms();

  return StatusCode::SUCCESS;
}

void UTNZSMonitor::bookHistograms() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "bookHistograms" << endmsg;
  // Get the tell1 mapping from source ID to tell1 number
  std::map<unsigned int, unsigned int>::const_iterator itT = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
  for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int sourceID = ( *itT ).first;
    // Limit to selected tell1s
    if ( m_selectedTells ) {
      if ( !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                           ( this->readoutTool() )->SourceIDToTELLNumber( sourceID ) ) ) {
        continue;
      }
    }
    unsigned int tellID = m_useSourceID ? sourceID : ( *itT ).second;
    // Create a title for the histogram
    std::string strTellID       = std::to_string( tellID );
    HistoID     noiseHistoID    = "noise_$tell" + strTellID;
    std::string noiseHistoTitle = "Noise for UTTELL" + strTellID;
    m_noiseHistos[sourceID]     = Gaudi::Utils::Aida2ROOT::aida2root(
        bookProfile1D( noiseHistoID, noiseHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );

    HistoID     pedHistoID     = "pedestal_$tell" + strTellID;
    std::string pedHistoTitle  = "Pedestal for UTTELL" + strTellID;
    m_pedestalHistos[sourceID] = Gaudi::Utils::Aida2ROOT::aida2root(
        bookProfile1D( pedHistoID, pedHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );
  }
  m_2d_noisePerLinkVsTell1 = Gaudi::Utils::Aida2ROOT::aida2root(
      book2D( "Noise per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );
  m_2d_pedestalPerLinkVsTell1 = Gaudi::Utils::Aida2ROOT::aida2root(
      book2D( "Pedestal per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );
  m_2d_normalisationPerLinkVsTell1 =
      Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Normalisation", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );

  m_1d_noise = book1D( "Noise (all strips)", 0., 10., 500 );
  if ( m_rawNoise ) {
    m_1d_pedestal = book1D( "Pedestal (all strips)", 100., 160., 500 );
  } else {
    m_1d_pedestal = book1D( "Pedestal (all strips)", -5., 5., 501 );
  }
}

StatusCode UTNZSMonitor::execute() {
  m_evtNumber++;

  // Get the time of the first event and convert to a string for the histogram title.
  if ( m_evtNumber == 1 ) {
    if ( m_useODINTime ) {
      auto              ODIN     = m_odin.get();
      const Gaudi::Time odinTime = ODIN->eventTime();
      m_odinEvent = "(#" + std::to_string( ODIN->runNumber() ) + " on " + std::to_string( odinTime.day( 0 ) );
      +"/" + std::to_string( odinTime.month( 0 ) + 1 );
      +"/" + std::to_string( odinTime.year( 0 ) );
      +" @ " + std::to_string( odinTime.hour( 0 ) );
      +":" + std::to_string( odinTime.minute( 0 ) );
      +":" + std::to_string( odinTime.second( 0 ) ) + ")";
    }
  }

  // Skip first m_skipEvents. Useful when running over CMS data.
  if ( m_evtNumber < m_skipEvents ) { return StatusCode::SUCCESS; }

  // loop over the boards which contained an NZS bank
  std::vector<unsigned int>::const_iterator itT = m_noiseTool->tell1WithNZSBegin();
  for ( ; itT != m_noiseTool->tell1WithNZSEnd(); ++itT ) {
    // Flag to check if histogram needs to be updated
    bool needToUpdate = false;

    unsigned int sourceID = ( *itT );

    // Loop over number of events for FPGA-PP and see if the histograms need to be reset
    std::vector<unsigned int>::const_iterator itEvts = m_noiseTool->cmsNEventsPPBegin( sourceID );
    for ( ; itEvts != m_noiseTool->cmsNEventsPPEnd( sourceID ); ++itEvts ) {

      int nEvt = ( *itEvts );

      // Check if at least one of the PPs requires to update the histogram
      if ( m_updateRate > 0 && nEvt % m_updateRate == 0 && nEvt != 0 ) { needToUpdate = true; }
    } // FPGA-PP
    // Update the noise histogram
    if ( needToUpdate ) updateNoiseHistogram( sourceID );

  } // boards
  if ( m_summaryUpdateRate > 0 && m_evtNumber % m_summaryUpdateRate == 0 ) { updateSummaryPlots(); }

  return StatusCode::SUCCESS;
}

StatusCode UTNZSMonitor::finalize() {
  // printHistos();
  // Update all histograms at the end
  std::map<int, TProfile*>::const_iterator itH = m_noiseHistos.begin();

  for ( ; itH != m_noiseHistos.end(); ++itH ) {
    // Limit to selected tell1s
    if ( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                            ( this->readoutTool() )->SourceIDToTELLNumber( ( *itH ).first ) ) ) {
      continue;
    }
    updateNoiseHistogram( ( *itH ).first, m_useODINTime );
    updateSummaryPlots();
    if ( m_checkCalculation ) dumpNoiseCalculation( ( *itH ).first );
  }

  return UT::HistoAlgBase::finalize(); // must be called after all other actions
}

void UTNZSMonitor::updateNoiseHistogram( unsigned int sourceID, bool updateTitle ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "updateNoiseHistogram: " << m_evtNumber << endmsg;

  // Get the histogram and reset it in case it is already booked.
  if ( m_noiseHistos.find( sourceID ) != m_noiseHistos.end() &&
       m_pedestalHistos.find( sourceID ) != m_pedestalHistos.end() ) {

    TProfile* noiseHist = m_noiseHistos.find( sourceID )->second; //->second.end();
    noiseHist->Reset();

    TProfile* pedestalHist = m_pedestalHistos.find( sourceID )->second; //->second.end();
    pedestalHist->Reset();

    // Loop over strips in tell1
    unsigned int strip = 0;

    std::vector<unsigned int>::const_iterator itNEvt    = m_noiseTool->nEventsBegin( sourceID );
    std::vector<unsigned int>::const_iterator itNEvtEnd = m_noiseTool->nEventsEnd( sourceID );

    std::vector<double>::const_iterator itNoiseBegin;
    std::vector<double>::const_iterator itPedBegin;
    if ( m_rawNoise ) {
      itPedBegin   = m_noiseTool->pedestalBegin( sourceID );
      itNoiseBegin = m_noiseTool->rawNoiseBegin( sourceID );
    } else if ( m_pedSubNoise ) {
      itPedBegin   = m_noiseTool->pedSubMeanBegin( sourceID );
      itNoiseBegin = m_noiseTool->pedSubNoiseBegin( sourceID );
    } else {
      itPedBegin   = m_noiseTool->cmsMeanBegin( sourceID );
      itNoiseBegin = m_noiseTool->cmsNoiseBegin( sourceID );
    }
    std::vector<double>::const_iterator itNoise = itNoiseBegin;
    std::vector<double>::const_iterator itPed   = itPedBegin;
    for ( ; itNEvt != itNEvtEnd; ++itNoise, ++itPed, ++strip, ++itNEvt ) {
      double noise = ( *itNoise );
      double nevts = ( *itNEvt );
      double error = ( nevts > 0 ) ? noise / sqrt( nevts ) : 0;
      // trick to set bin error for profile histograms
      noiseHist->SetBinContent( strip + 1, noise );
      noiseHist->SetBinEntries( strip + 1, 1 );
      noiseHist->SetBinError( strip + 1, sqrt( noise * noise + error * error ) );
      pedestalHist->Fill( strip, ( *itPed ) );
    }
    if ( updateTitle ) {
      std::string title = noiseHist->GetTitle();
      title += " " + m_odinEvent;
      noiseHist->SetTitle( title.c_str() );
    }
  } else {
    unsigned int tellID = m_useSourceID ? sourceID : ( this->readoutTool() )->SourceIDToTELLNumber( sourceID );
    Warning( "No histogram booked for " + std::to_string( tellID ), StatusCode::FAILURE, 1 ).ignore();
  }
}

// Make plots of the noise and pedestal for all strips
// Make 2-d plots of noise and pedestal per link vs tell1.
void UTNZSMonitor::updateSummaryPlots() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "updateSummaryPlots: " << m_evtNumber << endmsg;
  m_1d_noise->reset();
  m_1d_pedestal->reset();
  m_2d_pedestalPerLinkVsTell1->Reset();
  m_2d_noisePerLinkVsTell1->Reset();
  m_2d_normalisationPerLinkVsTell1->Reset();

  std::map<unsigned int, unsigned int>::const_iterator itT = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
  for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int sourceID = ( *itT ).first;
    unsigned int tell1ID  = ( *itT ).second;

    std::vector<double>::const_iterator itNoiseBegin;
    std::vector<double>::const_iterator itPedBegin;
    if ( m_rawNoise ) {
      itPedBegin   = m_noiseTool->pedestalBegin( sourceID );
      itNoiseBegin = m_noiseTool->rawNoiseBegin( sourceID );
    } else if ( m_pedSubNoise ) {
      itPedBegin   = m_noiseTool->pedSubMeanBegin( sourceID );
      itNoiseBegin = m_noiseTool->pedSubNoiseBegin( sourceID );
    } else {
      itPedBegin   = m_noiseTool->cmsMeanBegin( sourceID );
      itNoiseBegin = m_noiseTool->cmsNoiseBegin( sourceID );
    }
    auto itNoise     = itNoiseBegin;
    auto itPed       = itPedBegin;
    auto itStatus    = m_noiseTool->stripStatusBegin( sourceID );
    auto itStatusEnd = m_noiseTool->stripStatusEnd( sourceID );
    int  strip       = 0;
    for ( ; itStatus != itStatusEnd; ++itNoise, ++itPed, ++itStatus, ++strip ) {
      if ( *itStatus ) {
        m_1d_noise->fill( ( *itNoise ) );
        m_1d_pedestal->fill( ( *itPed ) );
        int port = strip / LHCbConstants::nStripsInPort;
        m_2d_pedestalPerLinkVsTell1->Fill( tell1ID, port, ( *itPed ) );
        m_2d_noisePerLinkVsTell1->Fill( tell1ID, port, ( *itNoise ) );
        m_2d_normalisationPerLinkVsTell1->Fill( tell1ID, port, 1. );
      }
    }
  }
}

void UTNZSMonitor::dumpNoiseCalculation( unsigned int sourceID ) {
  const unsigned int        TELL        = ( this->readoutTool() )->SourceIDToTELLNumber( sourceID );
  unsigned int              strip       = 0;
  std::vector<double>       rawMean     = m_noiseTool->rawMean( TELL );
  auto                      rawMeanIt   = rawMean.begin();
  std::vector<double>       rawMeanSq   = m_noiseTool->rawMeanSq( TELL );
  auto                      rawMeanSqIt = rawMeanSq.begin();
  std::vector<double>       rawNoise    = m_noiseTool->rawNoise( TELL );
  auto                      rawNoiseIt  = rawNoise.begin();
  std::vector<unsigned int> rawN        = m_noiseTool->rawN( TELL );
  auto                      rawNIt      = rawN.begin();

  std::vector<double>       cmsMean     = m_noiseTool->cmsMean( TELL );
  auto                      cmsMeanIt   = cmsMean.begin();
  std::vector<double>       cmsMeanSq   = m_noiseTool->cmsMeanSq( TELL );
  auto                      cmsMeanSqIt = cmsMeanSq.begin();
  std::vector<double>       cmsNoise    = m_noiseTool->cmsNoise( TELL );
  auto                      cmsNoiseIt  = cmsNoise.begin();
  std::vector<unsigned int> cmsN        = m_noiseTool->cmsN( TELL );
  auto                      cmsNIt      = cmsN.begin();

  std::string idTELL      = std::to_string( TELL );
  std::string idRawMean   = "Raw mean, TELL " + idTELL;
  std::string idRawMeanSq = "Raw mean squared, TELL " + idTELL;
  std::string idRawNoiseS = "Raw noise stored, TELL " + idTELL;
  std::string idRawNoiseC = "Raw noise calculated, TELL " + idTELL;
  std::string idRawN      = "Raw number events, TELL " + idTELL;

  std::string idCMSMean   = "CMS mean, TELL " + idTELL;
  std::string idCMSMeanSq = "CMS mean squared, TELL " + idTELL;
  std::string idCMSNoiseS = "CMS noise stored, TELL " + idTELL;
  std::string idCMSNoiseC = "CMS noise calculated, TELL " + idTELL;
  std::string idCMSN      = "CMS number events, TELL " + idTELL;
  int         pp          = 0;
  for ( ; rawNIt != rawN.end(); ++rawNIt, ++cmsNIt, ++pp ) {
    profile1D( pp, ( *rawNIt ), idRawN, idRawN, -0.5, 3.5, 4 );
    profile1D( pp, ( *cmsNIt ), idCMSN, idCMSN, -0.5, 3.5, 4 );
  }
  for ( ; rawMeanIt != rawMean.end();
        ++rawMeanIt, ++rawMeanSqIt, ++rawNoiseIt, ++cmsMeanIt, ++cmsMeanSqIt, ++cmsNoiseIt, ++strip ) {
    profile1D( strip, ( *rawMeanIt ), idRawMean, idRawMean, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *rawMeanSqIt ), idRawMeanSq, idRawMeanSq, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *rawNoiseIt ), idRawNoiseS, idRawNoiseS, -0.5, 3071.5, 3072 );
    profile1D( strip, sqrt( *rawMeanSqIt - std::pow( *rawMeanIt, 2 ) ), idRawNoiseC, idRawNoiseC, -0.5, 3071, 3072 );

    profile1D( strip, ( *cmsMeanIt ), idCMSMean, idCMSMean, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *cmsMeanSqIt ), idCMSMeanSq, idCMSMeanSq, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *cmsNoiseIt ), idCMSNoiseS, idCMSNoiseS, -0.5, 3071.5, 3072 );
    profile1D( strip, sqrt( *cmsMeanSqIt - std::pow( *cmsMeanIt, 2 ) ), idCMSNoiseC, idCMSNoiseC, -0.5, 3071.5, 3072 );
  }
}
