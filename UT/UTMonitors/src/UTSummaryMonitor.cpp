/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "Event/UTSummary.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTHistoAlgBase.h"

using namespace LHCb;

/** @class UTSummaryMonitor UTSummaryMonitor.h
 *
 *  Algorithm to monitor the information in the UTSummary class. The UTSummary
 *  information is filled in the ZS decoding. The following histograms are
 *  filled:
 *  - PCN distribution. The democratically-chosen PCN is used (from UTSummary),
 *  - Total data size (kB) per event,
 *  - Error information in the UTSummary: each bin signals a problem in the
 *    event: bin 1, PCN sync is false; bin 2, corrupted banks, bin 3, lost banks
 *    bin 4, recovered banks.
 *  There are three main job options:
 *  - \b DetType: "UT"
 *  - \b InputData: Input location of the UTSummary data. Default is set to
 *    UTSummaryLocation::UTSummary.
 *  - \b SuppressMissing: Flag to suppress the filling of the 3th bin in the
 *    error information histogram (default is false).
 *  - \b SuppressRecovered: Flag to suppress the filling of the 4th bin in the
 *    error information histogram (default is false).
 *  - \b PipeLineSize: Default is set to 187.
 *
 *  @author A. Beiter (based on code by N. Chiapolini)
 *  @date   2018-09-04
 */

class UTSummaryMonitor : public Gaudi::Functional::Consumer<void( const UTSummary& ),
                                                            Gaudi::Functional::Traits::BaseClass_t<UT::HistoAlgBase>> {

public:
  /// constructer
  UTSummaryMonitor( const std::string& name, ISvcLocator* svcloc );

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const UTSummary& summary ) const override;

private:
  static constexpr int    c_nErrorBins{4};
  static constexpr double c_binIDaSynch{0.5};
  static constexpr double c_binIDmissing{2.5};
  static constexpr double c_binIDcorrupted{1.5};
  static constexpr double c_binIDrecovered{3.5};
  // jobOptions

  /// When set to true: do not fill the bin for missing banks
  Gaudi::Property<bool> m_suppressMissing{this, "SuppressMissing", false};

  /// When set to true: do not fill the bin for recovered banks
  Gaudi::Property<bool> m_suppressRecovered{this, "SuppressRecovered", false};

  /// Length of the beetle pipeline
  Gaudi::Property<int> m_pipeLineSize{this, "PipeLineSize", 187};

  // Book histograms for online monitoring
  AIDA::IHistogram1D* m_1d_pcn;
  AIDA::IHistogram1D* m_1d_errors;
  AIDA::IHistogram1D* m_1d_dataSize;
};

DECLARE_COMPONENT( UTSummaryMonitor )

//--------------------------------------------------------------------
//
//  UTSummaryMonitor
//
//--------------------------------------------------------------------

UTSummaryMonitor::UTSummaryMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"InputData", UTSummaryLocation::UTSummary}} {}

StatusCode UTSummaryMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    // Book histograms
    m_1d_pcn      = book1D( "pcn", "PCN distribution", 0, m_pipeLineSize, m_pipeLineSize );
    m_1d_errors   = book1D( "errors", "Error Info in Summary", 0., c_nErrorBins, c_nErrorBins );
    m_1d_dataSize = book1D( "dataSize", "Data size (kB) per event", 0, 500, 250 );
  } );
}

void UTSummaryMonitor::operator()( const UTSummary& summary ) const {

  // debug() << "Found " << data->size() << " boards." << endmsg;

  // Filling the histograms

  // Fill PCN histogram
  m_1d_pcn->fill( summary.pcn() );

  // Fill error summary histogram
  if ( !( summary.pcnSynch() ) ) { m_1d_errors->fill( c_binIDaSynch ); }
  if ( ( summary.corruptedBanks() ).size() > 0 ) { m_1d_errors->fill( c_binIDcorrupted ); }
  if ( !m_suppressMissing.value() && ( summary.missingBanks() ).size() > 0 ) { m_1d_errors->fill( c_binIDmissing ); }
  if ( !m_suppressRecovered.value() && ( summary.recoveredBanks() ).size() > 0 ) {
    m_1d_errors->fill( c_binIDrecovered );
  }

  // Fill data size histogram
  m_1d_dataSize->fill( summary.rawBufferSize() / 1024 );
}
