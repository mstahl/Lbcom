/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTCommonBase.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDetectorPlot.h"
#include "Kernel/UTNames.h"
#include "TH1D.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <algorithm>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <limits>
#include <string>

//#include <boost/accumulators/statistics/mean.hpp>
// Use root histos to get MPV for sectors (in absence of something better)
// Boost accumulator classes to store MPVs of sectors

//-----------------------------------------------------------------------------
// Implementation file for class : UTClusterMonitor
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTClusterMonitor UTClusterMonitor.h
 *
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */

/// Define some typedefs for boost accumulators
namespace UT {

  // Median of distribution
  typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::median(
                                                           boost::accumulators::with_p_square_quantile )>>
      MedianAccumulator;

  // Mean of distribution
  typedef boost::accumulators::accumulator_set<
      double, boost::accumulators::stats<boost::accumulators::tag::mean( boost::accumulators::immediate )>>
      MeanAccumulator;

  class UTClusterMonitor
      : public Gaudi::Functional::Consumer<void( const LHCb::ODIN&, const LHCb::UTClusters& ),
                                           Gaudi::Functional::Traits::BaseClass_t<CommonBase<GaudiHistoAlg>>> {
  public:
    /// Standard constructor
    UTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    void       operator()( const LHCb::ODIN&, const LHCb::UTClusters& ) const override;

  private:
    /// Fill basic histograms
    void fillHistograms( const LHCb::UTCluster* cluster, std::vector<unsigned int>& nClustersPerTELL1 ) const;

    /// Fill detailed histograms
    void fillDetailedHistograms( const LHCb::UTCluster* cluster ) const;

    /// Fill the hitmaps when required.
    void fillClusterMaps( const LHCb::UTCluster* cluster ) const;

    /// Fill the cluster mpv map per sector
    void fillMPVMap( const DeUTSector* sector, double charge ) const;

    /// Toggles to turn plots off/on
    Gaudi::Property<bool> m_plotBySvcBox{this, "ByServiceBox", false, "Plot by Service Box"};
    Gaudi::Property<bool> m_plotByDetRegion{this, "ByDetectorRegion", false, "Plot by unique detector region"};
    Gaudi::Property<bool> m_plotByPort{this, "ByPort", false, "Plot number of clusters/tell1 by vs port"};
    Gaudi::Property<bool> m_hitMaps{this, "HitMaps", false, "True if cluster maps are to be shown"};

    /// Cuts on the data quality
    Gaudi::Property<double>       m_chargeCut{this, "ChargeCut", 0, "Cut on charge"};
    Gaudi::Property<unsigned int> m_minNClusters{this, "MinTotalClusters", 0,
                                                 "Cut on minimum number of clusters in the event"};
    Gaudi::Property<unsigned int> m_maxNClusters{this, "MaxTotalClusters", std::numeric_limits<unsigned int>::max(),
                                                 "Cut on maximum number of clusters in the event"};
    Gaudi::Property<double>       m_minMPVCharge{this, "MinMPVCharge", 0, "Minimum charge for calculation on MPV"};

    Gaudi::Property<unsigned int> m_overFlowLimit{this, "OverflowLimit", 5000,
                                                  "Overflow limit for number of clusters in event"};
    Gaudi::Property<unsigned int> m_resetRate{this, "ResetRate", 1000, "Reset rate for MPV calculation"};

    mutable Gaudi::Accumulators::Counter<> m_eventsCount{this, "Number of events"};

    unsigned int m_nTELL1s; ///< Number of TELL1 boards expected.

    /// Book histograms
    void bookHistograms();

    // filled in monitor clusters
    AIDA::IHistogram1D* m_1d_nClusters          = nullptr; ///< Number of clusters
    AIDA::IHistogram1D* m_1d_nClusters_gt_100   = nullptr; ///< Number of clusters (N > 100)
    AIDA::IHistogram1D* m_1d_nClustersVsTELL1   = nullptr; ///< Number of clusters per TELL1
    AIDA::IProfile1D*   m_prof_nClustersVsTELL1 = nullptr; ///< Number of clusters per TELL1
    AIDA::IHistogram2D* m_2d_nClustersVsTELL1   = nullptr; ///< Number of clusters per TELL1

    AIDA::IHistogram1D* m_1d_nClustersVsTELL1Links   = nullptr; ///< Number of clusters per TELL1 (split into links)
    AIDA::IHistogram1D* m_1d_nClustersVsTELL1Sectors = nullptr; ///< Number of clusters per TELL1 (split into sectors)

    AIDA::IHistogram1D* m_1d_nClusters_overflow = nullptr; ///< Number of clusters where last bin contains overflow info

    // filled in fillHistograms
    AIDA::IHistogram1D* m_1d_ClusterSize            = nullptr; ///< Cluster Size
    AIDA::IHistogram2D* m_2d_ClusterSizeVsTELL1     = nullptr; ///< Cluster Size vs TELL1
    AIDA::IHistogram2D* m_2d_UTNVsTELL1             = nullptr; ///< Signal to Noise vs TELL1
    AIDA::IHistogram2D* m_2d_ChargeVsTELL1          = nullptr; ///< Cluster Charge vs TELL1
    AIDA::IHistogram2D* m_2d_ClustersPerPortVsTELL1 = nullptr; ///< Clusters per port vs TELL1

    AIDA::IHistogram1D* m_1d_totalCharge = nullptr; ///< Total charge in cluster

    /// Plots by service box
    std::map<std::string, AIDA::IHistogram1D*> m_1ds_chargeByServiceBox;
    /// Plots by detector region
    std::map<std::string, AIDA::IHistogram1D*> m_1ds_chargeByDetRegion;

    /// Hitmaps
    AIDA::IHistogram2D* m_2d_hitmap       = nullptr; ///< Cluster hitmap
    AIDA::IProfile1D*   m_prof_sectorMPVs = nullptr; ///< Sector MPV vs arbitrary sector number
    AIDA::IProfile1D*   m_prof_sectorTruncMean1 =
        nullptr; ///< Sector MPV calculated using truncated mean losing first and last 15%
    AIDA::IProfile1D* m_prof_sectorTruncMean2 =
        nullptr; ///< Sector MPV calculated using truncated mean 1st 70% of ADC distribution
    AIDA::IProfile1D*   m_prof_sectorBinnedMPV = nullptr; ///< Sector MPV vs arbitrary sector number
    AIDA::IHistogram2D* m_2d_sectorMPVs        = nullptr; ///< Sector MPV maps
    AIDA::IHistogram2D* m_2d_sectorMPVsNorm =
        nullptr; ///< Use to normalize the MPV distribution in the history mode of the presenter

    unsigned int              m_nSectors;              ///< Number of sectors (ie hybrids)
    unsigned int              m_nBeetlePortsPerSector; ///< Number of beetle ports per sector
    static const unsigned int m_nBeetlePortsPerUTSector =
        16u; ///< Number of bins per UT sector in the hitmap (beetle ports)
    unsigned int m_nSectorsPerTELL1;
    /// Accumulation of statistics for the MPV per sector
    std::map<const unsigned int, TH1D*>                       m_1ds_chargeBySector;
    mutable std::map<const unsigned int, std::vector<double>> m_chargeBySector;

    /// Contains the bin corresponding to a given sectors in the 1d representation of the hitmap
    std::map<const unsigned int, unsigned int> m_sectorBins1D;
    AIDA::IHistogram1D*                        m_1d_nClustersVsSector = nullptr; ///< Number of clusters in each sector
    AIDA::IHistogram1D* m_1d_nClustersVsBeetlePort = nullptr; ///< Number of clusters in each beetle port

    mutable std::mutex m_mutex;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTClusterMonitor )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTClusterMonitor::UTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"ClusterLocation", LHCb::UTClusterLocation::UTClusters}}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTClusterMonitor::initialize() {

  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );

  // Turn all histograms on in expert mode
  if ( fullDetail() ) {
    m_hitMaps         = true;
    m_plotBySvcBox    = true;
    m_plotByDetRegion = true;
    m_plotByPort      = true;
  }

  // Get the tell1 mapping from source ID to tell1 number
  m_nTELL1s = readoutTool()->nBoard();

  m_nBeetlePortsPerSector = m_nBeetlePortsPerUTSector;
  m_nSectorsPerTELL1      = UTDAQ::noptlinks * UTDAQ::nports / m_nBeetlePortsPerSector;

  // Book histograms
  bookHistograms();

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
void UT::UTClusterMonitor::operator()( const LHCb::ODIN& odin, const LHCb::UTClusters& clusters ) const {

  plot1D( odin.bunchId(), "BCID", "BCID", -0.5, 2807.5, 2808 );
  ++m_eventsCount;

  /// This  plots cluster ADCs vs Sampling time
  /// Store number of clusters/TELL1 (48 (42) Tell1s, 1->48
  std::vector<unsigned int> nClustersPerTELL1( m_nTELL1s, 0 );

  const unsigned int nClusters = clusters.size();
  debug() << "Number of clusters in " << inputLocation<LHCb::UTClusters>() << " is " << nClusters << endmsg;

  if ( nClusters < m_minNClusters || nClusters > m_maxNClusters ) return;

  m_1d_nClusters->fill( nClusters );
  if ( 100 < nClusters ) { m_1d_nClusters_gt_100->fill( nClusters ); }
  if ( m_overFlowLimit < nClusters )
    m_1d_nClusters_overflow->fill( m_overFlowLimit );
  else
    m_1d_nClusters_overflow->fill( nClusters );
  // Loop over clusters

  for ( const auto& cluster : clusters ) {
    fillHistograms( cluster, nClustersPerTELL1 );
    if ( fullDetail() ) fillDetailedHistograms( cluster );
  } // End of cluster iterator

  // Fill histogram for number of clusters/TELL1
  unsigned int TELL1 = 1;
  for ( auto itClPerTELL1 = nClustersPerTELL1.begin(); itClPerTELL1 != nClustersPerTELL1.end();
        ++itClPerTELL1, ++TELL1 ) {
    verbose() << "TELL1: " << TELL1 << ",clusters: " << ( *itClPerTELL1 ) << endmsg;
    unsigned int nClusters = ( *itClPerTELL1 );
    if ( 0 < nClusters ) {
      m_2d_nClustersVsTELL1->fill( TELL1, nClusters );
      m_prof_nClustersVsTELL1->fill( TELL1, nClusters );
    }
  }
}

//================================================================================================================================
// Fill the MPV cluster maps
// Three means are calculated:
// 1) Truncated mean using 70% of the distribution
// 2) Truncated mean using only the 1st 70% of the distribution
// 3) Binned mean with 20 bins from 0 to 100 ADC counts
//================================================================================================================================
void UT::UTClusterMonitor::fillMPVMap( const DeUTSector* sector, double charge ) const {
  unsigned int sectorID = sector->elementID().uniqueSector();
  if ( charge > m_minMPVCharge ) {
    auto                 lock          = std::scoped_lock{m_mutex};
    std::vector<double>& sectorCharges = m_chargeBySector.at( sectorID );
    TH1D*                hSec          = m_1ds_chargeBySector.at( sectorID );
    hSec->Fill( charge );
    sectorCharges.push_back( charge );
    if ( sectorCharges.size() > m_resetRate ) {
      std::sort( sectorCharges.begin(), sectorCharges.end() );
      unsigned int fullRange = sectorCharges.end() - sectorCharges.begin();
      unsigned int usedRange = static_cast<unsigned int>( 0.7 * fullRange );
      unsigned int start     = ( fullRange - usedRange ) / 2;
      unsigned int stop      = usedRange + ( fullRange - usedRange ) / 2;
      double       mean1     = std::accumulate( sectorCharges.begin() + start, sectorCharges.begin() + stop, 0. );
      mean1 /= usedRange;
      double mean2 = std::accumulate( sectorCharges.begin(), sectorCharges.begin() + usedRange, 0. );
      mean2 /= usedRange;
      std::vector<double> binnedMPV( 20, 0. );

      for ( const auto& iMPV : sectorCharges ) {
        unsigned int bin = static_cast<int>( iMPV / 5. );
        if ( bin < binnedMPV.size() ) binnedMPV[bin] += 1;
      }
      int    maxBin = std::max_element( binnedMPV.begin(), binnedMPV.end() ) - binnedMPV.begin();
      double mean3  = ( maxBin * 5. ) + 2.5;
      debug() << "Sector " << sectorID << ": truncMean=" << mean1 << ", mean2(70%)=" << mean2
              << ", mean3(binned)=" << mean3
              << ", full=" << std::accumulate( sectorCharges.begin(), sectorCharges.end(), 0. ) / fullRange
              << ", histo=" << ( hSec->GetBinCenter( hSec->GetMaximumBin() ) ) << endmsg;
      unsigned int bin = m_sectorBins1D.at( sectorID );
      m_prof_sectorTruncMean1->fill( bin, mean1 );
      m_prof_sectorTruncMean2->fill( bin, mean2 );
      m_prof_sectorBinnedMPV->fill( bin, mean3 );

      double mpv = mean3;
      m_prof_sectorMPVs->fill( bin, mpv );
      const DeUTSector*        utSector = dynamic_cast<const DeUTSector*>( sector );
      UT::UTDetectorPlot       hitMap( "map", "map" );
      UT::UTDetectorPlot::Bins bins = hitMap.toBins( utSector );
      double                   xBin = bins.xBin;
      for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) {
        m_2d_sectorMPVs->fill( xBin, yBin, mpv );
        m_2d_sectorMPVsNorm->fill( xBin, yBin, 1. );
      }
      sectorCharges.clear();
    }
  }
}

//==============================================================================
// Book histograms
//==============================================================================
void UT::UTClusterMonitor::bookHistograms() {
  // filled in monitor clusters
  m_1d_nClusters          = book1D( "Number of clusters", 0., 20000., 2000 );
  m_1d_nClusters_gt_100   = book1D( "Number of clusters (N > 100)", 0., 20000., 2000 );
  m_1d_nClusters_overflow = book1D( "Number of clusters (no overflow)", 0., m_overFlowLimit + 1, 1000 );
  m_2d_nClustersVsTELL1   = book2D( "Number of clusters per TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 100., 50 );

  // Number of clusters produced by each TELL1
  m_1d_nClustersVsTELL1   = book1D( "Number of clusters vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  m_prof_nClustersVsTELL1 = bookProfile1D( "Mean number of clusters per TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  m_1d_nClustersVsTELL1Links =
      book1D( "Number of clusters vs TELL1 links", 1., m_nTELL1s + 1., m_nTELL1s * UTDAQ::noptlinks );
  m_1d_nClustersVsTELL1Sectors =
      book1D( "Number of clusters vs TELL1 sectors", 1., m_nTELL1s + 1., m_nTELL1s * m_nSectorsPerTELL1 );

  // filled in fillHistograms
  m_1d_ClusterSize        = book1D( "Cluster Size", 0.5, 4.5, 4 );
  m_2d_ClusterSizeVsTELL1 = book2D( "Cluster Size vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0.5, 4.5, 4 );
  m_2d_UTNVsTELL1         = book2D( "Signal to Noise vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 100., 25 );
  m_2d_ChargeVsTELL1      = book2D( "Cluster Charge vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 60., 60 );

  if ( m_plotByPort ) {
    m_2d_ClustersPerPortVsTELL1 = book2D( "Clusters per port vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 );
  }
  m_1d_totalCharge = book1D( "Cluster ADC Values", 0., 200., 200 );
  /// list of service boxes
  for ( const auto& svcBox : readoutTool()->serviceBoxes() ) {
    std::string quadrant = svcBox.substr( 0, 2 );
    if ( m_1ds_chargeByServiceBox[quadrant] == 0 )
      m_1ds_chargeByServiceBox[quadrant] = book1D( "Cluster ADC Values " + quadrant, 0., 200., 200 );
    if ( m_plotBySvcBox ) m_1ds_chargeByServiceBox[svcBox] = book1D( "Cluster ADC Values " + svcBox, 0., 200., 200 );
  } // End of service box loop

  if ( m_plotByDetRegion ) {
    for ( const auto& region : LHCb::UTNames().allDetRegions() ) {
      m_1ds_chargeByDetRegion[region] = book1D( "Cluster ADC Values " + region, 0., 200., 200 );
    };
  }
  // Book histograms for hitmaps
  if ( m_hitMaps ) {
    std::string        idMap    = "UT cluster map";
    std::string        idMPVMap = "UT Sector MPVs";
    UT::UTDetectorPlot hitMap( idMap, idMap, m_nBeetlePortsPerUTSector );
    m_2d_hitmap = book2D( hitMap.name(), hitMap.minBinX(), hitMap.maxBinX(), hitMap.nBinX(), hitMap.minBinY(),
                          hitMap.maxBinY(), hitMap.nBinY() );
    UT::UTDetectorPlot MPVMap( idMPVMap, idMPVMap );
    m_2d_sectorMPVs     = book2D( MPVMap.name(), MPVMap.minBinX(), MPVMap.maxBinX(), MPVMap.nBinX(), MPVMap.minBinY(),
                              MPVMap.maxBinY(), MPVMap.nBinY() );
    m_2d_sectorMPVsNorm = book2D( MPVMap.name() + " Normalisation", MPVMap.minBinX(), MPVMap.maxBinX(), MPVMap.nBinX(),
                                  MPVMap.minBinY(), MPVMap.maxBinY(), MPVMap.nBinY() );
    // Create histogram of with total charge for each sector
    m_sectorBins1D.clear();
    int bin = 1;
    for ( auto Sectors = tracker()->sectors().begin(); Sectors != tracker()->sectors().end(); ++Sectors, ++bin ) {
      std::string idh   = "charge_$sector" + std::to_string( ( *Sectors )->elementID().uniqueSector() );
      std::string title = "Total charge in " + ( *Sectors )->nickname();
      m_1ds_chargeBySector[( *Sectors )->elementID().uniqueSector()] =
          Gaudi::Utils::Aida2ROOT::aida2root( book1D( "BySector/" + idh, title, 0., 100., 50 ) );
      //      std::cout << title+":" << (*Sectors)->elementID().uniqueSector() << "," << bin << std::endl;
      m_sectorBins1D[( *Sectors )->elementID().uniqueSector()] = bin;
    }
    // 1d representation for online monitoring
    m_nSectors             = m_sectorBins1D.size();
    m_1d_nClustersVsSector = book1D( "Number of clusters per sector", 0.5, m_nSectors + 0.5, m_nSectors );
    m_1d_nClustersVsBeetlePort =
        book1D( "Number of clusters per beetle port", 0.5, m_nSectors + 0.5, m_nSectors * m_nBeetlePortsPerSector );
    m_prof_sectorMPVs       = bookProfile1D( "Cluster MPV vs Sector", 0.5, m_nSectors + 0.5, m_nSectors );
    m_prof_sectorTruncMean1 = bookProfile1D( "Cluster trunc mean vs Sector", 0.5, m_nSectors + 0.5, m_nSectors );
    m_prof_sectorTruncMean2 =
        bookProfile1D( "Cluster trunc mean (1st 70%) vs Sector", 0.5, m_nSectors + 0.5, m_nSectors );
    m_prof_sectorBinnedMPV = bookProfile1D( "Cluster binned MPV vs Sector", 0.5, m_nSectors + 0.5, m_nSectors );
  } // end of hitmaps
}

//==============================================================================
// Fill histograms
//==============================================================================
void UT::UTClusterMonitor::fillHistograms( const LHCb::UTCluster*     cluster,
                                           std::vector<unsigned int>& nClustersPerTELL1 ) const {

  const double totalCharge = cluster->totalCharge();
  if ( totalCharge < m_chargeCut ) return;

  // calculate MPVs
  //  unsigned int sectorID=cluster->channelID().uniqueSector();
  const DeUTSector* sector = findSector( cluster->channelID() );
  if ( m_hitMaps ) {
    fillClusterMaps( cluster );
    fillMPVMap( sector, totalCharge );
    unsigned int sectorID = sector->elementID().uniqueSector();
    unsigned int bin      = m_sectorBins1D.at( sectorID );
    m_1d_nClustersVsSector->fill( bin );
    unsigned int port = ( cluster->channelID().strip() - 1 ) / UTDAQ::nstrips;
    m_1d_nClustersVsBeetlePort->fill( ( bin - 0.5 ) + static_cast<double>( port ) / m_nBeetlePortsPerSector );
  }

  const unsigned int clusterSize = cluster->size();

  const unsigned int sourceID = cluster->sourceID();
  unsigned int       TELL1ID  = ( readoutTool() )->SourceIDToTELLNumber( sourceID );
  nClustersPerTELL1[TELL1ID - 1] += 1;
  m_1d_nClustersVsTELL1->fill( TELL1ID );
  unsigned int tell1Link = cluster->tell1Channel() / 128;
  m_1d_nClustersVsTELL1Links->fill( ( TELL1ID ) + ( ( 0.5 + tell1Link ) / UTDAQ::noptlinks ) );
  unsigned int tell1Sector = cluster->tell1Channel() / ( m_nBeetlePortsPerSector * UTDAQ::nstrips );
  m_1d_nClustersVsTELL1Sectors->fill( ( TELL1ID ) + ( ( 0.5 + tell1Sector ) / m_nSectorsPerTELL1 ) );

  m_1d_ClusterSize->fill( clusterSize );
  m_2d_ClusterSizeVsTELL1->fill( TELL1ID, clusterSize );

  double noise = sector->noise( cluster->channelID() );
  if ( noise > 1e-3 ) {
    const double signalToNoise = cluster->totalCharge() / noise;
    m_2d_UTNVsTELL1->fill( TELL1ID, signalToNoise );
  } else
    Warning( "Zero S/N for some clusters", StatusCode::SUCCESS, 1 ).ignore();
  m_2d_ChargeVsTELL1->fill( TELL1ID, totalCharge );
  if ( m_plotByPort ) {
    const unsigned int tell1Channel = cluster->tell1Channel();
    unsigned int       port         = tell1Channel / UTDAQ::nstrips;
    m_2d_ClustersPerPortVsTELL1->fill( TELL1ID, port );
  }
  // Always fill histograms per readout quadrant
  // Get service box and set up histogram IDs
  m_1d_totalCharge->fill( totalCharge );
  std::string svcBox   = readoutTool()->serviceBox( cluster->channelID() );
  std::string quadrant = svcBox.substr( 0, 2 );
  m_1ds_chargeByServiceBox.at( quadrant )->fill( totalCharge );

  if ( m_plotBySvcBox ) m_1ds_chargeByServiceBox.at( svcBox )->fill( totalCharge );
  if ( m_plotByDetRegion ) m_1ds_chargeByDetRegion.at( cluster->detRegionName() )->fill( totalCharge );
}
//==============================================================================
/// Fill more detailed histograms
//==============================================================================
void UT::UTClusterMonitor::fillDetailedHistograms( const LHCb::UTCluster* cluster ) const {
  // high threshold
  plot( cluster->highThreshold(), "High threshold", "High threshold", -0.5, 1.5, 2 );

  // by strip, modulo a few things....
  const unsigned int strip = cluster->strip();
  plot1D( strip % 8, "strip modulo 8", "strip modulo 8", -0.5, 8.5, 9 );
  plot1D( strip % 32, "strip modulo 32", "strip modulo 32", -0.5, 32.5, 33 );
  plot1D( strip % 128, "strip modulo 128", "strip modulo 128", -0.5, 128.5, 129 );

  // histogram by station
  const int station = cluster->station();
  plot1D( station, "Number of clusters per station", "Number of clusters per station", -0.5, 4.5, 5 );

  // by layer
  const int layer = cluster->layer();
  plot( (double)( 10 * station + layer ), "Number of clusters per layer", "Number of clusters per layer", -0.5, 40.5,
        41 );

  const double neighbourSum = cluster->neighbourSum();
  plot1D( cluster->pseudoSize(), "Pseudo size of cluster", "Pseudo size of cluster", -0.5, 10.5, 11 );

  const double totalCharge = cluster->totalCharge();
  plot1D( neighbourSum / totalCharge, "Relative neighbour sum", "Relative neighbour sum", -1.02, 1.02, 51 );

  const unsigned int clusterSize = cluster->size();
  if ( 3 > clusterSize ) {
    plot1D( neighbourSum, "Neighbour sum (1- and 2-strip clusters)", "Neighbour sum (1- and 2-strip clusters)", -16.5,
            26.5, 43 );
    plot1D( neighbourSum / totalCharge, "Relative neighbour sum (1- and 2-strip clusters)",
            "Relative neighbour sum (1- and 2-strip clusters)", -1.02, 1.02, 51 );
  }
  // charge and S/N
  plot1D( totalCharge, cluster->layerName() + "/Charge", "Charge", -0.5, 500.5, 501 );
  const DeUTSector* sector = findSector( cluster->channelID() );
  double            noise  = sector->noise( cluster->channelID() );
  if ( noise > 1e-3 ) {
    const double signalToNoise = cluster->totalCharge() / noise;
    plot1D( signalToNoise, cluster->layerName() + "/Signal to noise", "S/N", 0., 100., 100 );
  } else
    Warning( "Some cluster have zero noise", StatusCode::SUCCESS, 1 ).ignore();
  // Plot cluster ADCs for 1, 2, 3, 4 strip clusters
  std::string svcBox   = readoutTool()->serviceBox( cluster->channelID() );
  std::string idhStrip = " (" + std::to_string( clusterSize ) + " strip)";
  std::string idh;
  idh = "Cluster ADCs in " + svcBox.substr( 0, 2 ) + idhStrip;
  plot1D( totalCharge, idh, idh, 0., 200., 200 );
  idh = "Cluster ADCs in " + svcBox + idhStrip;
  plot1D( totalCharge, idh, idh, 0., 200., 200 );

  // Charge in each sector for 1, 2, 3 and 4 strip clusters
  idh = "BySector/s" + std::to_string( cluster->size() ) + "/charge_$sector" +
        std::to_string( cluster->channelID().uniqueSector() );
  plot1D( totalCharge, idh, "Total charge in " + cluster->sectorName(), 0., 200., 200 );
}
//==============================================================================
/// Fill histograms of cluster maps
//==============================================================================
void UT::UTClusterMonitor::fillClusterMaps( const LHCb::UTCluster* cluster ) const {
  const std::string idMap    = "UT cluster map";
  const DeUTSector* aSector  = tracker()->findSector( cluster->channelID() );
  const DeUTSector* utSector = dynamic_cast<const DeUTSector*>( aSector );

  // make the real hit map
  UT::UTDetectorPlot       hitMap( idMap, idMap, m_nBeetlePortsPerUTSector );
  UT::UTDetectorPlot::Bins bins  = hitMap.toBins( utSector );
  int                      port  = ( cluster->channelID().strip() - 1 ) / UTDAQ::nstrips;
  double                   xBin  = bins.xBin - double( port + 1 ) / m_nBeetlePortsPerUTSector + 0.5;
  int                      nBins = bins.endBinY - bins.beginBinY;

  for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) { m_2d_hitmap->fill( xBin, yBin, 1. / nBins ); }
}
