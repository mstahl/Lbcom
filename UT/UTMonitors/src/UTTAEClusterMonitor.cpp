/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/Trajectory.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTHistoAlgBase.h"
#include "TH2D.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTTAEClusterMonitor
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTTAEClusterMonitor UTTAEClusterMonitor.h
 *
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 *
 *  Implementation of monitoring algorithm for TAE events.
 *  Defaults:
 *  (UT) Plots number of clusters per spill for each readout quadrant
 *
 *  Options:
 *  (UT) Can plot per service box
 */
namespace UT {
  class UTTAEClusterMonitor : public HistoAlgBase {
  public:
    /// Standard constructor
    UTTAEClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    /// Book histograms

    /// This is the algorithm which plots cluster ADCs vs Sampling time
    void monitorClusters();

    std::vector<std::string> m_clusterLocations; ///< Input location for clusters
    unsigned int             m_nSamples  = 0;    ///< Number of input locations
    double                   m_maxSample = 0.;   ///< define the max bin of samples sample
    std::vector<std::string> m_spills;           ///< Vector of all possible spill location for TAE events

    /// Cut on the bunch ID (distinguish Beam from cosmics)
    std::vector<unsigned int> m_bunchID;

    unsigned int m_maxClusterSize;  ///< Cut on number of strips in clusters
    double       m_chargeCut;       ///< Cut on charge
    bool         m_plotBySvcBox;    ///< Plot by Service Box
    bool         m_plotByDetRegion; ///< Plot by unique detector region
    bool         m_useMean;         ///< Use mean instead of highest bin as MPV
    bool         m_debug   = false; ///< true if message service level is debug
    bool         m_verbose = false; ///< true if message service level is verbose

    unsigned int m_updateRateMPV;

  private: // Histogram operations
    mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "Number of events"};

    void                bookHistograms();
    AIDA::IProfile1D*   m_prof_clustersVsSample = nullptr;
    AIDA::IHistogram2D* m_2d_ADCsVsSample       = nullptr;

    typedef std::pair<TH2D*, AIDA::IProfile1D*> HistoPair;
    std::map<std::string, HistoPair>            m_histos_ADCsVsSampleByServiceBox;
    std::map<std::string, HistoPair>            m_histos_ADCsVsSampleByDetRegion;
    void                                        fillHistogram( HistoPair histoPair, int sample, double charge );
    void                                        updateMPVHistograms();
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTTAEClusterMonitor )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTTAEClusterMonitor::UTTAEClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {
  // Ordered list of cluster locations
  declareProperty( "ClusterLocations",
                   m_clusterLocations = {
                       "Event/Prev7/Raw/UT/Clusters", "Event/Prev6/Raw/UT/Clusters", "Event/Prev5/Raw/UT/Clusters",
                       "Event/Prev4/Raw/UT/Clusters", "Event/Prev3/Raw/UT/Clusters", "Event/Prev2/Raw/UT/Clusters",
                       "Event/Prev1/Raw/UT/Clusters", "Raw/UT/Clusters", "Event/Next1/Raw/UT/Clusters",
                       "Event/Next2/Raw/UT/Clusters", "Event/Next3/Raw/UT/Clusters", "Event/Next4/Raw/UT/Clusters",
                       "Event/Next5/Raw/UT/Clusters", "Event/Next6/Raw/UT/Clusters", "Event/Next7/Raw/UT/Clusters"} );

  // Cuts
  declareProperty( "BunchID", m_bunchID ); // BunchID
  declareProperty( "ChargeCut", m_chargeCut = 12 );
  /// Maximum number of strips in clusters
  declareProperty( "MaxClusterSize", m_maxClusterSize = 6 );

  /// Plot by service box
  declareProperty( "ByServiceBox", m_plotBySvcBox = false );

  /// Plot by detector region
  declareProperty( "ByDetectorRegion", m_plotByDetRegion = false );

  /// Use mean as MPV
  declareProperty( "UseMeanAsMPV", m_useMean = false );

  /// Select update rate for filling MPV histograms
  declareProperty( "MPVUpdateRate", m_updateRateMPV = 10 );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTTAEClusterMonitor::initialize() {

  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_debug   = msgLevel( MSG::DEBUG );
  m_verbose = msgLevel( MSG::VERBOSE );
  if ( m_verbose ) m_debug = true;
  if ( m_debug ) debug() << "==> Initialize" << endmsg;
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );

  m_nSamples = m_clusterLocations.size();
  // Time aligned events (Central +/- TAE)
  if ( m_nSamples % 2 == 0 ) {
    error() << "Expected odd number of Cluster Locations, got " << m_nSamples << endmsg;
    return StatusCode::FAILURE;
  }
  m_maxSample = ( m_nSamples + 1 ) / 2 - 1 + 0.5;

  // Loop over input locations and fill label vector
  m_spills = {"Prev7", "Prev6", "Prev5", "Prev4", "Prev3", "Prev2", "Prev1", "Default",
              "Next1", "Next2", "Next3", "Next4", "Next5", "Next6", "Next7"};

  unsigned int diff = m_spills.size() - m_clusterLocations.size();
  m_spills.erase( m_spills.begin(), m_spills.begin() + diff / 2 );
  m_spills.resize( m_clusterLocations.size() );

  /* Loop over input locations
     Couldn't find a good way to do this in the base class yet */
  for ( auto& cl : m_clusterLocations ) {
    if ( m_debug ) debug() << "Cluster Locations: " << cl << endmsg;
  }

  bookHistograms();

  return StatusCode::SUCCESS;
}
//==============================================================================
// Book histograms
//==============================================================================
void UT::UTTAEClusterMonitor::bookHistograms() {
  m_prof_clustersVsSample =
      bookProfile1D( "Number of clusters vs sampling points", -m_maxSample, m_maxSample, m_nSamples );
  m_2d_ADCsVsSample =
      book2D( "Cluster ADC values vs sampling point", -m_maxSample, m_maxSample, m_nSamples, 0., 60., 60 );
  if ( m_plotBySvcBox ) {
    /// list of service boxes
    for ( const auto& svcBox : this->readoutTool()->serviceBoxes() ) {
      std::string quadrant = svcBox.substr( 0, 2 );
      if ( m_histos_ADCsVsSampleByServiceBox.find( quadrant ) == m_histos_ADCsVsSampleByServiceBox.end() ) {
        m_histos_ADCsVsSampleByServiceBox[quadrant] = HistoPair(
            Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Cluster ADC values vs sampling point " + quadrant,
                                                        -m_maxSample, m_maxSample, m_nSamples, 0., 60., 60 ) ),
            bookProfile1D( "ADC MPV vs sampling point" + quadrant, -m_maxSample, m_maxSample, m_nSamples, "", 0.,
                           60. ) );
      }
      if ( m_plotBySvcBox ) {
        m_histos_ADCsVsSampleByServiceBox[svcBox] = HistoPair(
            Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Cluster ADC values vs sampling point " + svcBox, -m_maxSample,
                                                        m_maxSample, m_nSamples, 0., 60., 60 ) ),
            bookProfile1D( "ADC MPV vs sampling point " + svcBox, -m_maxSample, m_maxSample, m_nSamples, "", 0.,
                           60. ) );
      }
    } // End of service box loop
  }   // End of service box condition
  if ( m_plotByDetRegion ) {
    for ( const auto& region : LHCb::UTNames().allDetRegions() ) {
      m_histos_ADCsVsSampleByDetRegion[region] = HistoPair(
          Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Cluster ADC values vs sampling point " + region, -m_maxSample,
                                                      m_maxSample, m_nSamples, 0., 60., 60 ) ),
          bookProfile1D( "ADC MPV vs sampling point " + region, -m_maxSample, m_maxSample, m_nSamples, "", 0., 60. ) );
    };
  }
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode UT::UTTAEClusterMonitor::execute() {

  debug() << "==> Execute" << endmsg;
  // Select the correct bunch id
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
  if ( !odin ) return Warning( "No ODIN bank found", StatusCode::SUCCESS, 1 );
  if ( !m_bunchID.empty() && std::find( m_bunchID.begin(), m_bunchID.end(), odin->bunchId() ) == m_bunchID.end() )
    return StatusCode::SUCCESS;

  ++m_nEvents;

  // code goes here
  monitorClusters();
  if ( !m_useMean && m_nEvents.nEntries() % m_updateRateMPV ) updateMPVHistograms();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Look at the clusters histogram
//=============================================================================
void UT::UTTAEClusterMonitor::monitorClusters() {
  // Loop over input locations
  unsigned int iSample = 0;
  for ( auto itCL = m_clusterLocations.begin(); itCL != m_clusterLocations.end(); ++itCL, ++iSample ) {
    const LHCb::UTClusters* clusters = getIfExists<LHCb::UTClusters>( *itCL );
    // Check location exists
    if ( clusters ) {
      LHCb::UTClusters::const_iterator itClus;
      // Loop over clusters
      if ( m_debug ) { debug() << "Number of clusters in " << ( *itCL ) << " is " << ( clusters->size() ) << endmsg; }
      int sample = static_cast<int>( iSample - ( m_nSamples - 1 ) / 2 );
      m_prof_clustersVsSample->fill( sample, clusters->size() );
      for ( itClus = clusters->begin(); itClus != clusters->end(); ++itClus ) {
        const LHCb::UTCluster* cluster     = ( *itClus );
        const unsigned int     clusterSize = cluster->size();

        // Look at 1, 2, 3 strip cluster separately by changing job options
        if ( clusterSize <= m_maxClusterSize ) {
          // Get service box and set up histogram IDs
          std::string svcBox = ( this->readoutTool() )->serviceBox( cluster->firstChannel() );

          // ADC values vs spill
          const double totalCharge = cluster->totalCharge();
          if ( totalCharge < m_chargeCut ) continue;
          m_2d_ADCsVsSample->fill( sample, totalCharge );
          // Always fill histograms per readout quadrant for UT
          std::string quadrant = svcBox.substr( 0, 2 );
          fillHistogram( m_histos_ADCsVsSampleByServiceBox[quadrant], sample, totalCharge );
          if ( m_plotBySvcBox ) { fillHistogram( m_histos_ADCsVsSampleByServiceBox[svcBox], sample, totalCharge ); }
          if ( m_plotByDetRegion ) {
            std::string region = cluster->detRegionName();
            fillHistogram( m_histos_ADCsVsSampleByServiceBox[region], sample, totalCharge );
          }
        } // End of cluster condition
      }   // End of cluster iterator
    } else
      Warning( "No clusters found at " + ( *itCL ), StatusCode::SUCCESS, 0 ).ignore(); // End of cluster getIfExist
  } // End loop over cluster locations
}

void UT::UTTAEClusterMonitor::fillHistogram( HistoPair histos, int sample, double charge ) {
  histos.first->Fill( sample, charge );
  if ( m_useMean ) histos.second->fill( sample, charge );
}

void UT::UTTAEClusterMonitor::updateMPVHistograms() {
  if ( m_plotBySvcBox ) {
    auto iHP = m_histos_ADCsVsSampleByServiceBox.begin();
    for ( ; iHP != m_histos_ADCsVsSampleByServiceBox.end(); ++iHP ) {
      TH2D*             h2d = ( *iHP ).second.first;
      AIDA::IProfile1D* h1p = ( *iHP ).second.second;
      h1p->reset();
      int nbinsX = h2d->GetNbinsX();
      for ( int i = 1; i <= nbinsX; ++i ) {
        TH1D* hpy = h2d->ProjectionY( "hpy", i, i );
        if ( !( 0.0 < hpy->GetEntries() ) ) { continue; }
        double sample = h2d->GetXaxis()->GetBinCenter( i );
        double mpv    = hpy->GetBinCenter( hpy->GetMaximumBin() );
        h1p->fill( sample, mpv );
      }
    }
  }
}
