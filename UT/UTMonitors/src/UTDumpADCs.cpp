/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTTELL1Data.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTTupleAlgBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTDumpADCs
//
// 2018-09-04 : Andy Beiter (based on code by Mark TOBIN)
//-----------------------------------------------------------------------------

/** @class UTDumpADCs UTDumpADCs.h
 *
 *  Dumps the ADC values for each TELL1 into a tuple
 *  ADC values can be accessed/scanned event by event using:
 *
 *  TTree* adcs=(TTree*)gDirectory->Get("AlgName/ADCs");
 *  adcs->Draw("TELL34:channels","","",1,1)
 *
 *  @author Andy Beiter (based on code by Mark TOBIN)
 *  @date   2018-09-04
 */
namespace UT {
  class UTDumpADCs : public Gaudi::Functional::Consumer<void( LHCb::UTTELL1Datas const& ),
                                                        Gaudi::Functional::Traits::BaseClass_t<TupleAlgBase>> {
  public:
    /// Standard constructor
    UTDumpADCs( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;                                  ///< Algorithm initialization
    void       operator()( const LHCb::UTTELL1Datas& ) const override; ///< Algorithm execution

  private:
    std::vector<double>                    m_channelNumbers;
    mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "Number of events"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTDumpADCs )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  UTDumpADCs::UTDumpADCs( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"InputLocation", LHCb::UTTELL1DataLocation::UTFull}} {}

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode UTDumpADCs::initialize() {
    return Consumer::initialize().andThen( [&] {
      debug() << "==> Initialize" << endmsg;
      debug() << "Reading ADC values from " << inputLocation<LHCb::UTTELL1Datas>() << endmsg;
      for ( unsigned int chan = 0; chan < UTDAQ::nStripsPerBoard; chan++ ) { m_channelNumbers.push_back( chan ); }
    } );
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  void UTDumpADCs::operator()( const LHCb::UTTELL1Datas& data ) const {
    // Skip if there is no Tell1 data
    ++m_nEvents;

    Tuple tuple = nTuple( "ADCs", "ADC values from TELL1s" );
    // Get the data
    // loop over the data
    for ( const auto& board : data ) {
      // get the tell board and the data headers
      int                            sourceID   = board->TELL1ID();
      std::string                    idh        = "Mean ADC for sourceID" + std::to_string( sourceID );
      const LHCb::UTTELL1Data::Data& dataValues = board->data();
      std::vector<double>            ADCValues( 3072, 0. );
      // Loop over the PPs that have sent data
      for ( unsigned int pp : board->sentPPs() ) {
        // Loop over the links (i.e. Beetles)
        for ( unsigned int iBeetle = 0; iBeetle < UTDAQ::nBeetlesPerPPx; ++iBeetle ) {
          unsigned int beetle = iBeetle + pp * UTDAQ::nBeetlesPerPPx;
          // Loop over the strips in this link
          for ( unsigned int iStrip = 0; iStrip < LHCbConstants::nStripsInBeetle; ++iStrip ) {
            // Get the ADC value
            const int value = dataValues[beetle][iStrip];

            // Calculate the pedestal and the pedestal squared
            int strip        = iStrip + beetle * LHCbConstants::nStripsInBeetle;
            ADCValues[strip] = value;
            profile1D( strip, value, idh, -0.5, UTDAQ::nStripsPerBoard - 0.5, UTDAQ::nStripsPerBoard );
          } // strip
        }   // beetle
      }     // Loop on PPs
      std::string tupleName = "TELL" + std::to_string( readoutTool()->SourceIDToTELLNumber( sourceID ) );
      tuple->farray( tupleName, ADCValues, "channel", 3072 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    } // Loop on boards
    tuple->farray( "channels", m_channelNumbers, "channel", 3072 )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    // debug() << "Found " << data.size() << " boards." << endmsg;
  }
} // namespace UT
