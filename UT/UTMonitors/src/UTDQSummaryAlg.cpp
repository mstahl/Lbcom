/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "Event/UTSummary.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTTupleAlgBase.h"
#include "UTDQCounters.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "boost/accumulators/accumulators.hpp"
#include <fstream>
#include <string>
using namespace LHCb;

/** @class UTDQSummaryAlg UTDQSummaryAlg.h
 *
 *  Class for writing UTDQ summary
 *
 *  @author A.Beiter (based on code by M.Needham, N.Chiapolini)
 *  @date   2018-09-04
 */

class UTDQSummaryAlg : public Gaudi::Functional::Consumer<void( const ODIN&, const UTSummary&, const UTClusters& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<UT::TupleAlgBase>> {

public:
  /// constructer
  UTDQSummaryAlg( const std::string& name, ISvcLocator* svcloc );

  /// execute
  void operator()( const ODIN&, const UTSummary&, const UTClusters& ) const override;

  /// finalize
  StatusCode finalize() override;

private:
  /// write run summary into storage vector (m_dataStorage)
  void outputInfo() const;

  /// get data from event and add to Counters
  void processEvent( const LHCb::UTClusters& clusters, const LHCb::UTSummary& summary ) const;

  /// calculate the processed efficiency
  double calculateProcEff( const unsigned int corrupted, const LHCb::UTSummary::RecoveredInfo& recovered ) const;

  /// count events with signal (S/N > m_threshold)
  unsigned int countHigh( const LHCb::UTClusters& clusters ) const;

  /// write content of m_dataStorage to text file
  void writeTxtFile();

  /// write content of m_dataStorage to tuple
  void writeTuple();

  mutable int m_lastRunNumber{-1}; ///< RunNumber of last event

  /// temporary storage vector contains data of all events
  mutable std::vector<UTDQCounters::DataRow> m_dataStorage;

  /// Class with all boost::accumulators.
  mutable std::unique_ptr<UTDQCounters> m_counters;

  mutable std::mutex m_serialize;

  Gaudi::Property<int>         m_minADC{this, "minADC", 15};
  Gaudi::Property<int>         m_maxADC{this, "maxADC", 45};
  Gaudi::Property<bool>        m_writeTxtFile{this, "writeTxtFile", true};
  Gaudi::Property<bool>        m_writeTuple{this, "writeTuple", true};
  Gaudi::Property<std::string> m_outputFileName{this, "outputFile", "UTDQSummary.txt"}; ///< filename for the text file
  Gaudi::Property<std::string> m_separator{this, "separator", "|"}; ///< column separator in text file
  Gaudi::Property<int>    m_fpPrecision{this, "fpPrecision", 2}; ///< precision of floating point numbers in text file
  Gaudi::Property<double> m_threshold{this, "threshold", 6};     // S/N cut///< threshold for signal (S/N)
};

DECLARE_COMPONENT( UTDQSummaryAlg )

//--------------------------------------------------------------------
//
//  UTDQSummaryAlg:Summary class for UTDQ
//
//--------------------------------------------------------------------

UTDQSummaryAlg::UTDQSummaryAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"ODINLocation", ODINLocation::Default},
                KeyValue{"summaryLocation", UTSummaryLocation::UTSummary},
                KeyValue{"clusterLocation", UTClusterLocation::UTClusters}}} {}

void UTDQSummaryAlg::operator()( const ODIN& odin, const UTSummary& summary, const UTClusters& clusters ) const {
  auto      _   = std::scoped_lock{m_serialize};
  const int run = odin.runNumber();
  if ( run != m_lastRunNumber ) {
    /// reset all counters
    if ( m_lastRunNumber != -1 ) outputInfo();
    auto time             = odin.eventTime();
    m_counters            = std::make_unique<UTDQCounters>( m_minADC.value(), m_maxADC.value() );
    m_counters->m_RunDate = std::to_string( time.year( 0 ) ) + "-" + std::to_string( time.month( 0 ) + 1 ) + "-" +
                            std::to_string( time.day( 0 ) );
    m_lastRunNumber = run;
  }
  // summarize this event
  processEvent( clusters, summary );
}

void UTDQSummaryAlg::outputInfo() const {

  // only output if there is something to output !
  if ( m_counters->m_event == 0 ) {
    info() << " Run contains no events , no DQ possible" << endmsg;
    return;
  }

  UTDQCounters::DataRow row;
  row.run        = m_lastRunNumber;
  row.runDate    = m_counters->m_RunDate;
  row.event      = m_counters->m_event;
  row.clus       = mean( m_counters->m_sumClusters );
  row.noise      = mean( m_counters->m_sumNoise );
  row.procEff    = 1 - mean( m_counters->m_sumEff );
  row.error      = sum( m_counters->m_nError );
  row.corrupted  = sum( m_counters->m_nCorrupted );
  row.sumMissing = sum( m_counters->m_sumMissing );
  row.chargeMPV  = m_counters->chargeMPV();
  m_dataStorage.push_back( row );
}

void UTDQSummaryAlg::processEvent( const UTClusters& clusters, const UTSummary& summary ) const {

  const unsigned int              nClus        = clusters.size();                 // all clusters
  const unsigned int              nHigh        = countHigh( clusters );           // remove noise clusters
  const unsigned int              missingBanks = summary.missingBanks().size();   // MIA
  const unsigned int              corrupted    = summary.corruptedBanks().size(); // really rubbish banks
  const UTSummary::RecoveredInfo& recovered    = summary.recoveredBanks();

  // fill the counters
  m_counters->m_event++;
  m_counters->m_sumNoise( nClus - nHigh );
  m_counters->m_sumClusters( nClus );
  m_counters->m_sumEff( calculateProcEff( corrupted, recovered ) );
  m_counters->m_nError( summary.nErrorBanks() );
  m_counters->m_nCorrupted( corrupted );
  m_counters->m_sumMissing( missingBanks );
}

unsigned int UTDQSummaryAlg::countHigh( const UTClusters& clusters ) const {

  unsigned int nh = 0;
  for ( const auto& cluster : clusters ) {
    const DeUTSector* sector        = findSector( cluster->channelID() );
    const double      signalToNoise = ( cluster->totalCharge() / sector->noise( cluster->channelID() ) );
    m_counters->addCharge( cluster->totalCharge() );
    if ( signalToNoise > m_threshold ) ++nh;
  }
  return nh;
}

double UTDQSummaryAlg::calculateProcEff( const unsigned int              corrupted,
                                         const UTSummary::RecoveredInfo& recovered ) const {

  const unsigned int nBanks = readoutTool()->nBoard();
  double             eff    = nBanks - corrupted; // corrupted banks are not processed
  eff                       = std::accumulate( recovered.begin(), recovered.end(), eff,
                         []( double e, const std::pair<const unsigned int, double>& r ) {
                           return e - r.second; // how much was processed
                         } );
  return eff / double( nBanks );
}

void UTDQSummaryAlg::writeTxtFile() {

  std::ofstream oFile( m_outputFileName );
  if ( oFile.fail() ) {
    Warning( "Failed to open output file" ).ignore();
    return;
  }

  auto writeTxtEntry = [&]( std::ofstream& str, auto data ) { str << m_separator << " " << std::fixed << data << " "; };

  oFile << std::setprecision( m_fpPrecision );
  // add a header describing each column in the txt file
  for ( const auto& c : UTDQCounters::m_txtColumns ) writeTxtEntry( oFile, c );
  oFile << m_separator << std::endl;

  // output to a txt file
  for ( const auto& r : m_dataStorage ) {
    writeTxtEntry( oFile, r.run );
    writeTxtEntry( oFile, r.runDate );
    writeTxtEntry( oFile, r.event );
    writeTxtEntry( oFile, r.clus );
    writeTxtEntry( oFile, r.noise );
    writeTxtEntry( oFile, std::abs( r.procEff ) > 1e-20 ? r.procEff : 0 );
    writeTxtEntry( oFile, r.error );
    writeTxtEntry( oFile, r.corrupted );
    writeTxtEntry( oFile, r.sumMissing );
    writeTxtEntry( oFile, r.chargeMPV );
    oFile << m_separator << " " << m_separator << std::endl;
  }
}

void UTDQSummaryAlg::writeTuple() {
  Tuple tuple = nTuple( name() );
  for ( const auto& r : m_dataStorage ) {
    tuple->column( "run", r.run ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    //    tuple->column("runDate",    r.runDate    );
    tuple->column( "event", r.event ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "clus", r.clus ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "noise", r.noise ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "procInEff", r.procEff ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ErrorBanks", r.error ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "corrupted", r.corrupted ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "sumMissing", r.sumMissing ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "chargeMPV", r.chargeMPV ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}

StatusCode UTDQSummaryAlg::finalize() {
  outputInfo();
  if ( m_writeTuple.value() ) writeTuple();
  if ( m_writeTxtFile.value() ) writeTxtFile();
  return Consumer::finalize();
}
