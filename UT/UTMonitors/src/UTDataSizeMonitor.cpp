/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IProfile1D.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTRawBankMap.h"
#include "Kernel/UTTell1ID.h"
#include <string>
using namespace LHCb;
//
// This File contains the definition of the OTSmearer -class
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Author: A. Beiter (based on code by M. Needham)
//   Created: 2018-09-04

/** @class UTDataSizeMonitor UTDataSizeMonitor.h
 *
 *  Class for checking UT RAW buffer
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 *
 *  Move part to UTMonitors to look at data size per tell1
 */

namespace UT {
  class UTDataSizeMonitor
      : public Gaudi::Functional::Consumer<void( const LHCb::RawEvent& ),
                                           Gaudi::Functional::Traits::BaseClass_t<UT::HistoAlgBase>> {

  public:
    /// constructor
    UTDataSizeMonitor( const std::string& name, ISvcLocator* svcloc );

    /// initialize
    StatusCode initialize() override;

    /// execute
    void operator()( const LHCb::RawEvent& ) const override;

  private:
    unsigned int      m_nTELL1s              = 0;
    AIDA::IProfile1D* m_prof_dataSizeVsTELL1 = nullptr; ///< Mean data size vs TELL1
  };

  DECLARE_COMPONENT( UTDataSizeMonitor )
} // namespace UT

//--------------------------------------------------------------------
//
//  Check the data size of the UT raw banks per tell1
//
//--------------------------------------------------------------------

UT::UTDataSizeMonitor::UTDataSizeMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"RawEventLocation", RawEventLocation::Default}} {}

StatusCode UT::UTDataSizeMonitor::initialize() {
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return UT::HistoAlgBase::initialize().andThen( [&] {
    // Get the tell1 mapping from source ID to tell1 number
    m_nTELL1s              = readoutTool()->nBoard();
    m_prof_dataSizeVsTELL1 = bookProfile1D( "Mean Raw Bank Size vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  } );
}

void UT::UTDataSizeMonitor::operator()( const RawEvent& rawEvt ) const {

  // execute once per event

  // init counters
  //   unsigned int maxBoardSize = 0;
  //   UTTell1ID hotBoard(0);
  //   unsigned int eventDataSize = 0;

  // get banks and loop

  // Retrieve the RawEvent:
  for ( const auto& bank : rawEvt.banks( LHCb::RawBank::UT ) ) {
    // board info....
    size_t       bankSize = bank->size() / sizeof( char );
    unsigned int sourceID = bank->sourceID();
    //     UTTell1ID aBoard(sourceID);
    unsigned int TELL1ID = ( this->readoutTool() )->SourceIDToTELLNumber( sourceID );
    m_prof_dataSizeVsTELL1->fill( TELL1ID, bankSize );

    //     // event counters
    //     if (bankSize > maxBoardSize){
    //       maxBoardSize = bankSize;
    //       hotBoard = aBoard;
    //     }
    //     eventDataSize += bankSize;

    //     // histogram per board
    //     plot((double)bankSize, "board data size", 0., 200., 200);

    //     // data size per board
    //     unsigned int id = (aBoard.region()*20) + aBoard.subID();
    //     plot(id, "data size", 0., 100. , 100,(double)bankSize);

  } // iterBank

  //   // data size
  //   plot((double)eventDataSize,1, "event data size",0. , 10000., 500);

  //   // include standard header HARDCODE !!!
  //   unsigned int headerSize = tBanks.size()*2u;
  //   plot((double)(eventDataSize+headerSize),2, "total data size",0.,10000., 500);

  //   plot((double)maxBoardSize,3, "hot board size", 0., 200., 200);
  //   unsigned int id = (hotBoard.region()*20) + hotBoard.subID();
  //   plot((double)id,4,"hot board ID", 0., 100., 100);
}
