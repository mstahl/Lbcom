/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Event/ODIN.h"
#include "Event/UTTELL1Data.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Kernel/IUTNoiseCalculationTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTTell1Board.h"
#include "Kernel/UTTell1ID.h"
#include "TH2D.h"
#include "TProfile.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

using namespace LHCb;
using namespace AIDA;
using namespace UTDAQ;
using namespace UTBoardMapping;
using namespace Gaudi::Utils; //::Aida2ROOT::aida2root

/** @class UTNoiseMonitor UTNoiseMonitor.h
 *
 *  Class for monitoring the noise of the Tell1's. For each Tell1 the noise
 *  versus the strip number is stored in a histogram. The histograms can be
 *  stored using sourceID or tell1 name with the option \b UseSourceID. The
 *  noise is calculated using a IUTNoiseCalculationTool.  The algorithm can be
 *  configured using the following options:
 *  - \b UpdateRate: Rate at which the histograms are updated (in number of
 *    events). Useful in online mode. Set to -1 to update only at the end.
 *  - \b NoiseToolType: Sets the type of the noise calculation tool. (default is UT::UTNoiseCalculationTool)
 *  - \b NoiseToolName: Sets the name of the noise calculation tool. (default is UTNoiseCalculationTool)
 *
 *  Code produces histograms of Noise, Common Mode Subtracted noise and pedestals.
 *
 *  Based on UTNZSMonitor from
 *  @author A. Beiter (based on code by J. van Tilburg, N. Chiapolini)
 *  @date   2018-09-04
 *
 */

namespace UT {

  class UTNoiseMonitor : public UT::HistoAlgBase {

  public:
    /// Standard constructer
    UTNoiseMonitor( const std::string& name, ISvcLocator* svcloc );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    /// Book histograms
    void bookHistograms();

    /// Fill the noise histograms (only called every N events and at finalize)
    void updateNoiseHistogram( unsigned int tell1ID, bool updateTitle = false );

    /// Make some summary plots of things like average noise/link etc...
    void updateSummaryHistograms();

    // const std::string   m_basenameNoiseHisto;
    int m_evtNumber;

    unsigned int m_nTELL1s; ///< Number of TELL1 boards expected.

    // jobOptions:

    /// Location in the Event Data Store with the ADC values
    std::string m_dataLocation;

    /// When set to true: use the sourceID in the histogram name. Otherwise use
    /// the tell1 name.
    bool m_useSourceID;

    /// Rate at which the histograms are updated (in number of events).
    /// Set to -1 to update only at the end.
    int m_updateRate;

    /// Dumps noise calculation variables to histograms for debugging
    bool m_checkCalculation;
    /// Plot noise calculation variables (mean, mean squared and rms)
    void dumpNoiseCalculation( unsigned int sourceID );

    std::vector<unsigned int> m_limitToTell;   /// List of TELL1s to look at
    bool                      m_selectedTells; ///< Use only selected TELL1s

    bool        m_useODINTime; ///< ODIN time of first event is added to the histogram title in the finalize method
    std::string m_odinEvent;   ///< String of the time of the first run

    /// Histogram of number of nzs banks per tell1
    AIDA::IHistogram1D* m_1d_nNZS;

    /// Map of RAW noise histograms booked in initialize
    std::map<int, TProfile*> m_raw_noiseHistos;

    /// Map of pedestal histograms booked in initialize
    std::map<int, TProfile*> m_raw_pedestalHistos;

    /// Map of pedestal subtracted noise histograms booked in initialize
    std::map<int, TProfile*> m_pedsub_noiseHistos;

    /// Map of pedestal histograms after pedestal subtraction booked in initialize
    std::map<int, TProfile*> m_pedsub_pedestalHistos;

    /// Map of CMS noise histograms booked in initialize
    std::map<int, TProfile*> m_cms_noiseHistos;

    /// Map of pedestal histograms after LCMS booked in initialize
    std::map<int, TProfile*> m_cms_pedestalHistos;

    /// Map of CMS noise histograms booked in initialize
    std::map<int, TProfile*> m_cm_noiseHistos;

    //     /// Map of average RAW noise per port histograms booked in initialize
    //     std::map<int, TProfile*> m_average_raw_noiseHistos;

    //     /// Map of average raw pedestal per port histograms booked in initialize
    //     std::map<int, TProfile*> m_average_raw_pedestalHistos;

    //     /// Map of average CMS noise per port histograms booked in initialize
    //     std::map<int, TProfile*> m_average_cms_noiseHistos;

    //     /// Map of average CMS pedestal per port histograms booked in initialize
    //     std::map<int, TProfile*> m_average_cms_pedestalHistos;

    //     /// Map of average CMS per port noise histograms booked in initialize
    //     std::map<int, TProfile*> m_average_cm_noiseHistos;

    /// Histograms of all strips combined
    AIDA::IHistogram1D* m_1d_raw_noise       = nullptr; ///< Raw noise of each strip
    AIDA::IHistogram1D* m_1d_pedsub_noise    = nullptr; ///< Pedestal Subtracted noise of each strip
    AIDA::IHistogram1D* m_1d_cms_noise       = nullptr; ///< CMS noise of each strip
    AIDA::IHistogram1D* m_1d_cm_noise        = nullptr; ///< CM noise of each strip
    AIDA::IHistogram1D* m_1d_raw_pedestal    = nullptr; ///< Raw pedestal of each strip
    AIDA::IHistogram1D* m_1d_pedsub_pedestal = nullptr; ///< Pedestal of each strip after pedestal subtraction
    AIDA::IHistogram1D* m_1d_cms_pedestal    = nullptr; ///< CMS pedestal of each strip

    // Maps of histograms by sector type
    std::map<std::string, AIDA::IHistogram1D*> m_1d_raw_noiseByType;    ///< Raw noise of each strip
    std::map<std::string, AIDA::IHistogram1D*> m_1d_cms_noiseByType;    ///< CMS noise of each strip
    std::map<std::string, AIDA::IHistogram1D*> m_1d_cm_noiseByType;     ///< CM noise of each strip
    std::map<std::string, AIDA::IHistogram1D*> m_1d_raw_pedestalByType; ///< Raw pedestal of each strip
    std::map<std::string, AIDA::IHistogram1D*> m_1d_cms_pedestalByType; ///< CMS pedestal of each strip

    TH2D* m_2d_RawPedestalPerLinkVsTell1 = nullptr; ///< 2d map of raw pedestal vs link
    TH2D* m_2d_RawNoisePerLinkVsTell1    = nullptr; ///< 2d map of raw noise vs link

    TH2D* m_2d_PedSubPedestalPerLinkVsTell1 = nullptr; ///< 2d map of pedestal subtracted ADC values vs link
    TH2D* m_2d_PedSubNoisePerLinkVsTell1    = nullptr; ///< 2d map of pedestal subtracted noise vs link

    TH2D* m_2d_CMSPedestalPerLinkVsTell1 = nullptr; ///< 2d map of lcms pedestal vs link
    TH2D* m_2d_CMSNoisePerLinkVsTell1    = nullptr; ///< 2d map of lcms noise vs link

    /// 2d map used for normalisation of noise, pedestal plots as 2d profile histograms are not supported by online
    /// monitoring
    TH2D* m_2d_NormalisationPerLinkVsTell1 = nullptr;

    UT::IUTNoiseCalculationTool* m_noiseTool = nullptr; ///< Tool to calculate noise
    std::string                  m_noiseToolType;       ///< Tool type (default is UTNoiseCalculationTool)
    std::string                  m_noiseToolName;       ///< Tool name (default is UTNoiseCalculationTool)

    /// Period of the an exponential moving average.(read from the noise tool)
    int m_followingPeriod;

    /// Rate at which the counters for histograms are reset (read from the noise tool)
    int m_resetRate;

    /// Number of events to be skipped (read from noise tool)
    int m_skipEvents;

    /// Make summary plots
    bool m_summaryPlots;

    /// plot by sensor type (long/short IT; 1-4, UT)
    bool m_plotByType;

    unsigned int m_nStripsInSector; ///< number of strips in sector (from UTDAQ::nStripsInXTSector where X== T or I)

    std::map<int, std::vector<const DeUTSector*>> m_sectorMap; ///< Map of DeUTSectors (for use filling / type
                                                               ///< histograms)
    std::map<std::string, std::string> m_types;                ///< keep map of sector types to book / type histograms
    DataObjectReadHandle<ODIN>         m_odin{this, "ODINLocation", LHCb::ODINLocation::Default};
  };

  DECLARE_COMPONENT( UTNoiseMonitor )
} // namespace UT
//--------------------------------------------------------------------
//
//  UTNoiseMonitor
//
//--------------------------------------------------------------------

UT::UTNoiseMonitor::UTNoiseMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {
  // constructer
  declareProperty( "UseSourceID", m_useSourceID = false );
  declareProperty( "UpdateRate", m_updateRate = 1 );

  // Debugging
  declareProperty( "CheckNoiseCalculation", m_checkCalculation = false );
  // Limit calculation to vector of tell1s given in terms of TELLID (eg TTTELL1 = 1)
  declareProperty( "LimitToTell", m_limitToTell );

  // Use ODIN time in histograms
  declareProperty( "UseODINTime", m_useODINTime = false );

  /// Noise calculation tool
  declareProperty( "NoiseToolType", m_noiseToolType = "UT::UTOnlineNoiseCalculationTool" );
  declareProperty( "NoiseToolName", m_noiseToolName = "UTNoiseCalculationTool" );

  // Make summary plots
  declareProperty( "SummaryPlots", m_summaryPlots = true );

  // plot by sensor type
  declareProperty( "PlotBySensorType", m_plotByType = false );
}

StatusCode UT::UTNoiseMonitor::initialize() {
  // Initialize UT::HistoAlgBase
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_nStripsInSector = UTDAQ::nStripsInUTSector;

  m_evtNumber = 0;

  m_noiseTool = tool<UT::IUTNoiseCalculationTool>( m_noiseToolType, m_noiseToolName );

  // Read following period, reset rate and skip events from configuration of tool
  m_followingPeriod = m_noiseTool->followPeriod();
  m_resetRate       = m_noiseTool->resetRate();
  m_skipEvents      = m_noiseTool->skipEvents();

  // Select small number of TELL1s (useful for debugging) then book histograms
  m_nTELL1s       = readoutTool()->nBoard();
  m_selectedTells = false;
  if ( m_limitToTell.size() > 0 ) {
    m_selectedTells = true;
    sort( m_limitToTell.begin(), m_limitToTell.end() );
  }
  // Store DeUTSector for each TELL1 sector:
  // -----------------------------> BIT OF A NAUTY HACK
  for ( const auto& itT : readoutTool()->SourceIDToTELLNumberMap() ) {
    unsigned int        TELL1SourceID = itT.first;
    const UTTell1Board* board         = readoutTool()->findByBoardID( UTTell1ID( TELL1SourceID ) );
    auto&               sectors       = m_sectorMap[TELL1SourceID];
    unsigned int        strip         = 0;
    while ( strip < 3072 ) {
      LHCb::UTChannelID channelID =
          ( board->DAQToOffline( 0, UTDAQ::version::v4, UTDAQ::UTStripRepresentation( strip ) ).first );
      const DeUTSector* sector = tracker()->findSector( channelID );
      sectors.push_back( sector );
      if ( sector ) { m_types[sector->type()] = sector->type(); }
      strip += m_nStripsInSector;
    }
  }
  bookHistograms();

  return sc;
}

void UT::UTNoiseMonitor::bookHistograms() {

  m_1d_nNZS = book1D( "Number of NZS banks", "#  NZS banks / TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );

  // Get the tell1 mapping from source ID to tell1 number
  auto itT = readoutTool()->SourceIDToTELLNumberMap().begin();
  for ( ; itT != readoutTool()->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int sourceID = itT->first;
    // Limit to selected tell1s
    if ( m_selectedTells ) {
      if ( !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                           readoutTool()->SourceIDToTELLNumber( sourceID ) ) ) {
        continue;
      }
    }
    unsigned int tellID = m_useSourceID ? sourceID : itT->second;
    // Create a title for the histogram
    std::string strTellID = std::to_string( tellID );

    //================================================== noise / strip

    // Raw Noise
    HistoID     raw_noiseHistoID    = "raw_noise_$tell" + strTellID;
    std::string raw_noiseHistoTitle = " Raw Noise for UTTELL" + strTellID;
    m_raw_noiseHistos[sourceID]     = Aida2ROOT::aida2root(
        bookProfile1D( raw_noiseHistoID, raw_noiseHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );

    HistoID     raw_pedHistoID     = "raw_pedestal_$tell" + strTellID;
    std::string raw_pedHistoTitle  = "Raw Pedestal for UTTELL" + strTellID;
    m_raw_pedestalHistos[sourceID] = Aida2ROOT::aida2root(
        bookProfile1D( raw_pedHistoID, raw_pedHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );

    // Pedestal subtracted noise
    HistoID     pedsub_noiseHistoID    = "pedsub_noise_$tell" + strTellID;
    std::string pedsub_noiseHistoTitle = "Pedestal Subtracted Noise for UTTELL" + strTellID;
    m_pedsub_noiseHistos[sourceID]     = Aida2ROOT::aida2root(
        bookProfile1D( pedsub_noiseHistoID, pedsub_noiseHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );
    HistoID     pedsub_pedHistoID     = "pedsub_pedestal_$tell" + strTellID;
    std::string pedsub_pedHistoTitle  = "Pedestal after Pedestal Subtraction for UTTELL" + strTellID;
    m_pedsub_pedestalHistos[sourceID] = Aida2ROOT::aida2root(
        bookProfile1D( pedsub_pedHistoID, pedsub_pedHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );

    // Common Mode Subtracted Noise
    HistoID     cms_noiseHistoID    = "cms_noise_$tell" + strTellID;
    std::string cms_noiseHistoTitle = "Common Mode Subtracted Noise for UTTELL" + strTellID;
    m_cms_noiseHistos[sourceID]     = Aida2ROOT::aida2root(
        bookProfile1D( cms_noiseHistoID, cms_noiseHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );
    HistoID     cms_pedHistoID     = "cms_pedestal_$tell" + strTellID;
    std::string cms_pedHistoTitle  = "Pedestal after CMS for UTTELL" + strTellID;
    m_cms_pedestalHistos[sourceID] = Aida2ROOT::aida2root(
        bookProfile1D( cms_pedHistoID, cms_pedHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );

    // Common Mode Noise
    HistoID     cm_noiseHistoID    = "cm_noise_$tell" + strTellID;
    std::string cm_noiseHistoTitle = "Common Mode Noise for UTTELL" + strTellID;
    m_cm_noiseHistos[sourceID]     = Aida2ROOT::aida2root(
        bookProfile1D( cm_noiseHistoID, cm_noiseHistoTitle, -0.5, nStripsPerBoard - 0.5, nStripsPerBoard ) );
  }
  if ( m_summaryPlots ) {

    m_2d_RawPedestalPerLinkVsTell1 = Aida2ROOT::aida2root(
        book2D( "Mean Raw ADCs per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );
    m_2d_RawNoisePerLinkVsTell1 =
        Aida2ROOT::aida2root( book2D( "Raw Noise per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );

    m_2d_PedSubPedestalPerLinkVsTell1 = Aida2ROOT::aida2root(
        book2D( "Mean Ped Subtracted ADCs per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );
    m_2d_PedSubNoisePerLinkVsTell1 = Aida2ROOT::aida2root(
        book2D( "Pedestal Subtracted Noise per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );

    m_2d_CMSPedestalPerLinkVsTell1 = Aida2ROOT::aida2root(
        book2D( "Mean CMS ADCs per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );
    m_2d_CMSNoisePerLinkVsTell1 =
        Aida2ROOT::aida2root( book2D( "CMS Noise per link vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );

    m_2d_NormalisationPerLinkVsTell1 =
        Aida2ROOT::aida2root( book2D( "Normalisation", 0.5, m_nTELL1s + 0.5, m_nTELL1s, 0., 96., 96 ) );

    m_1d_raw_noise       = book1D( "Raw noise", 0., 10., 500 );
    m_1d_pedsub_noise    = book1D( "Pedestal subtracted noise", 0., 10., 500 );
    m_1d_cms_noise       = book1D( "CMS noise", 0., 10., 500 );
    m_1d_cm_noise        = book1D( "Common Mode noise", 0., 10., 500 );
    m_1d_raw_pedestal    = book1D( "Raw pedestal", 100., 160., 500 );
    m_1d_pedsub_pedestal = book1D( "Pedestal subtracted pedestal", -5, 5., 501 );
    m_1d_cms_pedestal    = book1D( "CMS pedestal", -5., 5., 501 );

    if ( m_plotByType ) {

      for ( auto&& [_, type] : m_types ) {
        m_1d_raw_noiseByType[type]    = book1D( "Raw noise " + type, 0., 10., 500 );
        m_1d_cms_noiseByType[type]    = book1D( "CMS noise " + type, 0., 10., 500 );
        m_1d_cm_noiseByType[type]     = book1D( "Common Mode noise " + type, 0., 10., 500 );
        m_1d_raw_pedestalByType[type] = book1D( "Raw pedestal " + type, 100., 160., 500 );
        m_1d_cms_pedestalByType[type] = book1D( "CMS pedestal " + type, -5., 5., 501 );
      }
    }
  }
}

StatusCode UT::UTNoiseMonitor::execute() {
  m_evtNumber++;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "execute:\t" << m_evtNumber << endmsg;
  // Get the time of the first event and convert to a string for the histogram title.
  if ( m_evtNumber == 1 ) {
    if ( m_useODINTime ) {
      auto              ODIN     = m_odin.get();
      const Gaudi::Time odinTime = ODIN->eventTime();
      m_odinEvent = "(#" + std::to_string( ODIN->runNumber() ) + " on " + std::to_string( odinTime.day( 0 ) );
      +"/" + std::to_string( odinTime.month( 0 ) + 1 );
      +"/" + std::to_string( odinTime.year( 0 ) );
      +" @ " + std::to_string( odinTime.hour( 0 ) );
      +":" + std::to_string( odinTime.minute( 0 ) );
      +":" + std::to_string( odinTime.second( 0 ) ) + ")";
    }
  }

  // Skip first m_skipEvents. Useful when running over CMS data.
  if ( m_evtNumber < m_skipEvents ) { return StatusCode::SUCCESS; }

  // loop over the boards which contained an NZS bank

  for ( auto itT = m_noiseTool->tell1WithNZSBegin(); itT != m_noiseTool->tell1WithNZSEnd(); ++itT ) {
    // Flag to check if histogram needs to be updated
    bool needToUpdate = false;

    unsigned int sourceID = ( *itT );
    unsigned int tell1    = readoutTool()->SourceIDToTELLNumber( sourceID );
    m_1d_nNZS->fill( tell1 );

    // Loop over number of events for FPGA-PP and see if the histograms need to be reset

    for ( auto itEvts = m_noiseTool->cmsNEventsPPBegin( sourceID ); itEvts != m_noiseTool->cmsNEventsPPEnd( sourceID );
          ++itEvts ) {

      int nEvt = ( *itEvts );

      // Check if at least one of the PPs requires to update the histogram
      if ( m_updateRate > 0 && nEvt % m_updateRate == 0 && nEvt != 0 ) { needToUpdate = true; }
    } // FPGA-PP
    // Update the noise histogram
    if ( needToUpdate ) updateNoiseHistogram( sourceID );

  } // boards
  if ( m_summaryPlots && m_updateRate > 0 && m_evtNumber % m_updateRate == 0 ) updateSummaryHistograms();

  return StatusCode::SUCCESS;
}

StatusCode UT::UTNoiseMonitor::finalize() {
  // printHistos();
  // Update all histograms at the end
  std::map<int, TProfile*>::const_iterator itH = m_raw_noiseHistos.begin();

  for ( ; itH != m_raw_noiseHistos.end(); ++itH ) {
    // Limit to selected tell1s
    if ( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                            readoutTool()->SourceIDToTELLNumber( itH->first ) ) ) {
      continue;
    }
    updateNoiseHistogram( itH->first, m_useODINTime );
    if ( m_checkCalculation ) dumpNoiseCalculation( itH->first );
  }
  if ( m_summaryPlots ) updateSummaryHistograms();

  return UT::HistoAlgBase::finalize(); // must be called after all other actions
}

void UT::UTNoiseMonitor::updateNoiseHistogram( unsigned int sourceID, bool updateTitle ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "updateNoiseHistograms:\t" << m_evtNumber << endmsg;
  // Get the histogram and reset it in case it is already booked.
  if ( m_raw_noiseHistos.find( sourceID ) != m_raw_noiseHistos.end() &&
       m_raw_pedestalHistos.find( sourceID ) != m_raw_pedestalHistos.end() &&
       m_pedsub_noiseHistos.find( sourceID ) != m_pedsub_noiseHistos.end() &&
       m_pedsub_pedestalHistos.find( sourceID ) != m_pedsub_pedestalHistos.end() &&
       m_cms_noiseHistos.find( sourceID ) != m_cms_noiseHistos.end() &&
       m_cms_pedestalHistos.find( sourceID ) != m_cms_pedestalHistos.end() &&
       m_cm_noiseHistos.find( sourceID ) != m_cm_noiseHistos.end() ) {

    // Plots / strip
    TProfile* raw_noiseHist = m_raw_noiseHistos.find( sourceID )->second;
    raw_noiseHist->Reset();
    TProfile* raw_pedestalHist = m_raw_pedestalHistos.find( sourceID )->second;
    raw_pedestalHist->Reset();

    TProfile* pedsub_noiseHist = m_pedsub_noiseHistos.find( sourceID )->second;
    pedsub_noiseHist->Reset();
    TProfile* pedsub_pedestalHist = m_pedsub_pedestalHistos.find( sourceID )->second;
    pedsub_pedestalHist->Reset();

    TProfile* cms_noiseHist = m_cms_noiseHistos.find( sourceID )->second;
    cms_noiseHist->Reset();
    TProfile* cms_pedestalHist = m_cms_pedestalHistos.find( sourceID )->second;
    cms_pedestalHist->Reset();

    TProfile* cm_noiseHist = m_cm_noiseHistos.find( sourceID )->second;
    cm_noiseHist->Reset();

    // Loop over strips in tell1
    unsigned int                        strip         = 0;
    std::vector<double>::const_iterator itRawPed      = m_noiseTool->pedestalBegin( sourceID );
    std::vector<double>::const_iterator itRawNoise    = m_noiseTool->rawNoiseBegin( sourceID );
    std::vector<double>::const_iterator itRawNoiseEnd = m_noiseTool->rawNoiseEnd( sourceID );

    std::vector<double>::const_iterator itPedSubPed   = m_noiseTool->pedSubMeanBegin( sourceID );
    std::vector<double>::const_iterator itPedSubNoise = m_noiseTool->pedSubNoiseBegin( sourceID );

    std::vector<double>::const_iterator itCMSPed   = m_noiseTool->cmsMeanBegin( sourceID );
    std::vector<double>::const_iterator itCMSNoise = m_noiseTool->cmsNoiseBegin( sourceID );

    std::vector<bool>::const_iterator itStatus = m_noiseTool->stripStatusBegin( sourceID );
    for ( ; itRawNoise != itRawNoiseEnd;
          ++itRawNoise, ++itRawPed, ++itPedSubNoise, ++itPedSubPed, ++itCMSNoise, ++itCMSPed, ++itStatus, ++strip ) {
      if ( *itStatus ) {
        raw_noiseHist->Fill( strip, ( *itRawNoise ) );
        raw_pedestalHist->Fill( strip, ( *itRawPed ) );
        pedsub_noiseHist->Fill( strip, ( *itPedSubNoise ) );
        pedsub_pedestalHist->Fill( strip, ( *itPedSubPed ) );
        cms_noiseHist->Fill( strip, ( *itCMSNoise ) );
        cms_pedestalHist->Fill( strip, ( *itCMSPed ) );
        double commonMode = std::pow( *itRawNoise, 2 ) - std::pow( *itCMSNoise, 2 );
        if ( commonMode > 0 )
          commonMode = sqrt( commonMode );
        else
          commonMode = 0;
        cm_noiseHist->Fill( strip, commonMode );
      }
    }
    if ( updateTitle ) {
      std::string title = raw_noiseHist->GetTitle();
      title += " " + m_odinEvent;
      raw_noiseHist->SetTitle( title.c_str() );
    }
  } else {
    unsigned int tellID = m_useSourceID ? sourceID : readoutTool()->SourceIDToTELLNumber( sourceID );
    Warning( "No histogram booked for " + std::to_string( tellID ), StatusCode::FAILURE, 1 ).ignore();
  }
}

void UT::UTNoiseMonitor::updateSummaryHistograms() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "updateSummaryHistograms:\t" << m_evtNumber << endmsg;
  m_1d_raw_noise->reset();
  m_1d_raw_pedestal->reset();
  m_1d_pedsub_noise->reset();
  m_1d_pedsub_pedestal->reset();
  m_1d_cms_noise->reset();
  m_1d_cms_pedestal->reset();
  m_1d_cm_noise->reset();

  m_2d_RawPedestalPerLinkVsTell1->Reset();
  m_2d_RawNoisePerLinkVsTell1->Reset();
  m_2d_PedSubPedestalPerLinkVsTell1->Reset();
  m_2d_PedSubNoisePerLinkVsTell1->Reset();
  m_2d_CMSPedestalPerLinkVsTell1->Reset();
  m_2d_CMSNoisePerLinkVsTell1->Reset();
  m_2d_NormalisationPerLinkVsTell1->Reset();

  if ( m_plotByType ) {
    for ( auto iType = m_types.begin(); iType != m_types.end(); ++iType ) {
      const auto& type = iType->first;
      m_1d_raw_noiseByType[type]->reset();
      m_1d_cms_noiseByType[type]->reset();
      m_1d_cm_noiseByType[type]->reset();
      m_1d_raw_pedestalByType[type]->reset();
      m_1d_cms_pedestalByType[type]->reset();
    }
  }
  for ( const auto [sourceID, tell1ID] : readoutTool()->SourceIDToTELLNumberMap() ) {
    auto itRawPed      = m_noiseTool->pedestalBegin( sourceID );
    auto itRawNoise    = m_noiseTool->rawNoiseBegin( sourceID );
    auto itRawNoiseEnd = m_noiseTool->rawNoiseEnd( sourceID );

    auto itPedSubPed   = m_noiseTool->pedSubMeanBegin( sourceID );
    auto itPedSubNoise = m_noiseTool->pedSubNoiseBegin( sourceID );

    auto itCMSPed   = m_noiseTool->cmsMeanBegin( sourceID );
    auto itCMSNoise = m_noiseTool->cmsNoiseBegin( sourceID );

    auto  itStatus = m_noiseTool->stripStatusBegin( sourceID );
    auto& sectors  = m_sectorMap[sourceID];
    int   strip    = 0;
    int   sector   = 0;
    for ( ; itRawNoise != itRawNoiseEnd;
          ++itRawNoise, ++itRawPed, ++itPedSubNoise, ++itPedSubPed, ++itCMSNoise, ++itCMSPed, ++itStatus, ++strip ) {
      if ( *itStatus ) {
        m_1d_raw_noise->fill( ( *itRawNoise ) );
        m_1d_raw_pedestal->fill( ( *itRawPed ) );
        m_1d_pedsub_noise->fill( ( *itPedSubNoise ) );
        m_1d_pedsub_pedestal->fill( ( *itPedSubPed ) );
        m_1d_cms_noise->fill( ( *itCMSNoise ) );
        m_1d_cms_pedestal->fill( ( *itCMSPed ) );
        double commonMode = std::pow( *itRawNoise, 2 ) - std::pow( *itCMSNoise, 2 );
        if ( commonMode > 0 )
          commonMode = sqrt( commonMode );
        else
          commonMode = 0;
        m_1d_cm_noise->fill( commonMode );
        int port = strip / LHCbConstants::nStripsInPort;
        m_2d_RawPedestalPerLinkVsTell1->Fill( tell1ID, port, ( *itRawPed ) );
        m_2d_RawNoisePerLinkVsTell1->Fill( tell1ID, port, ( *itRawNoise ) );
        m_2d_PedSubPedestalPerLinkVsTell1->Fill( tell1ID, port, ( *itPedSubPed ) );
        m_2d_PedSubNoisePerLinkVsTell1->Fill( tell1ID, port, ( *itPedSubNoise ) );
        m_2d_CMSPedestalPerLinkVsTell1->Fill( tell1ID, port, ( *itCMSPed ) );
        m_2d_CMSNoisePerLinkVsTell1->Fill( tell1ID, port, ( *itCMSNoise ) );
        m_2d_NormalisationPerLinkVsTell1->Fill( tell1ID, port, 1. );
        if ( m_plotByType ) {
          sector = strip / m_nStripsInSector;
          if ( sectors[sector] != 0 ) {
            std::string type = sectors[sector]->type();
            m_1d_raw_noiseByType[type]->fill( ( *itRawNoise ) );
            m_1d_raw_pedestalByType[type]->fill( ( *itRawPed ) );
            m_1d_cms_noiseByType[type]->fill( ( *itCMSNoise ) );
            m_1d_cms_pedestalByType[type]->fill( ( *itCMSPed ) );
            m_1d_cm_noiseByType[type]->fill( commonMode );
          }
        }
      } // strip status
    }
  }
}

void UT::UTNoiseMonitor::dumpNoiseCalculation( unsigned int sourceID ) {
  const unsigned int                  TELL        = readoutTool()->SourceIDToTELLNumber( sourceID );
  unsigned int                        strip       = 0;
  std::vector<double>                 rawMean     = m_noiseTool->rawMean( TELL );
  std::vector<double>::iterator       rawMeanIt   = rawMean.begin();
  std::vector<double>                 rawMeanSq   = m_noiseTool->rawMeanSq( TELL );
  std::vector<double>::iterator       rawMeanSqIt = rawMeanSq.begin();
  std::vector<double>                 rawNoise    = m_noiseTool->rawNoise( TELL );
  std::vector<double>::iterator       rawNoiseIt  = rawNoise.begin();
  std::vector<unsigned int>           rawN        = m_noiseTool->rawN( TELL );
  std::vector<unsigned int>::iterator rawNIt      = rawN.begin();

  std::vector<double>                 cmsMean     = m_noiseTool->cmsMean( TELL );
  std::vector<double>::iterator       cmsMeanIt   = cmsMean.begin();
  std::vector<double>                 cmsMeanSq   = m_noiseTool->cmsMeanSq( TELL );
  std::vector<double>::iterator       cmsMeanSqIt = cmsMeanSq.begin();
  std::vector<double>                 cmsNoise    = m_noiseTool->cmsNoise( TELL );
  std::vector<double>::iterator       cmsNoiseIt  = cmsNoise.begin();
  std::vector<unsigned int>           cmsN        = m_noiseTool->cmsN( TELL );
  std::vector<unsigned int>::iterator cmsNIt      = cmsN.begin();

  std::string idTELL      = std::to_string( TELL );
  std::string idRawMean   = "Raw mean, TELL " + idTELL;
  std::string idRawMeanSq = "Raw mean squared, TELL " + idTELL;
  std::string idRawNoiseS = "Raw noise stored, TELL " + idTELL;
  std::string idRawNoiseC = "Raw noise calculated, TELL " + idTELL;
  std::string idRawN      = "Raw number events, TELL " + idTELL;

  std::string idCMSMean   = "CMS mean, TELL " + idTELL;
  std::string idCMSMeanSq = "CMS mean squared, TELL " + idTELL;
  std::string idCMSNoiseS = "CMS noise stored, TELL " + idTELL;
  std::string idCMSNoiseC = "CMS noise calculated, TELL " + idTELL;
  std::string idCMSN      = "CMS number events, TELL " + idTELL;
  int         pp          = 0;
  for ( ; rawNIt != rawN.end(); ++rawNIt, ++cmsNIt, ++pp ) {
    profile1D( pp, ( *rawNIt ), idRawN, idRawN, -0.5, 3.5, 4 );
    profile1D( pp, ( *cmsNIt ), idCMSN, idCMSN, -0.5, 3.5, 4 );
  }
  for ( ; rawMeanIt != rawMean.end();
        ++rawMeanIt, ++rawMeanSqIt, ++rawNoiseIt, ++cmsMeanIt, ++cmsMeanSqIt, ++cmsNoiseIt, ++strip ) {
    profile1D( strip, ( *rawMeanIt ), idRawMean, idRawMean, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *rawMeanSqIt ), idRawMeanSq, idRawMeanSq, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *rawNoiseIt ), idRawNoiseS, idRawNoiseS, -0.5, 3071.5, 3072 );
    profile1D( strip, sqrt( *rawMeanSqIt - std::pow( *rawMeanIt, 2 ) ), idRawNoiseC, idRawNoiseC, -0.5, 3071, 3072 );

    profile1D( strip, ( *cmsMeanIt ), idCMSMean, idCMSMean, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *cmsMeanSqIt ), idCMSMeanSq, idCMSMeanSq, -0.5, 3071.5, 3072 );
    profile1D( strip, ( *cmsNoiseIt ), idCMSNoiseS, idCMSNoiseS, -0.5, 3071.5, 3072 );
    profile1D( strip, sqrt( *cmsMeanSqIt - std::pow( *cmsMeanIt, 2 ) ), idCMSNoiseC, idCMSNoiseC, -0.5, 3071.5, 3072 );
  }
}
