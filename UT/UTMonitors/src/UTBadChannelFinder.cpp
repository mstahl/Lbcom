/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "Event/ODIN.h"
#include "Kernel/IUTNoiseCalculationTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBeetleRepresentation.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTTell1Board.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <ostream>

//-----------------------------------------------------------------------------
// Implementation file for class : UTBadChannelFinder
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTBadChannelFinder UTBadChannelFinder.h
 *
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 *
 *  Re-implementation of UTOpenFinder from Viktor/Johan.
 */

namespace UT {
  enum NoiseFlag { OK, Open, LowNoise };

  std::ostream& operator<<( std::ostream& s, NoiseFlag e ) {
    switch ( e ) {
    case NoiseFlag::OK:
      return s << "OK";
    case NoiseFlag::Open:
      return s << "Open";
    case NoiseFlag::LowNoise:
      return s << "LowNoise";
    default:
      return s << "ERROR wrong value for enum UT::NoiseFlag::Status";
    }
  }

  class UTBadChannelFinder : public HistoAlgBase {
  public:
    /// Standard constructor
    UTBadChannelFinder( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "Number of events"};

    unsigned int m_maxEvents; ///< Number of events to use

    IUTNoiseCalculationTool* m_noiseTool = nullptr; ///< Tool to calculate noise
    std::string              m_noiseToolType;       ///< Tool type (default is UTNoiseCalculationTool)
    std::string              m_noiseToolName;       ///< Tool name (default is TTNoiseCalculationTool)

    /// Calculate slope and intercept of line for channels in a beetle port
    void   calcSlope( std::vector<double>::const_iterator noiseBegin, std::vector<double>::const_iterator noiseEnd );
    double m_slope;     ///< Slope of line fitted to channels in beetle port
    double m_intercept; ///< y-intercept of line fitted to channels in beetle port

    double m_rawNoiseOpen; ///< Cut value to find open channels from raw noise
    double m_rawNoiseLow;  ///<  Cut value to find channels with low noise from raw noise

    bool m_plotBySector;  ///< Make 2d plots showing number of bad bonds per sector
    bool m_compareWithDB; ///< Compare bad channels with DB status
    void compareWithDB(); ///< Compare bad channels to DB status

    void findBadChannels();

    unsigned int m_nTELL1s; ///< NUmber of TELL1s = readoutTool()->nBoard();

    // Store number of bad channels per TELL1, 2nd number in pair is flag of bad channel type
    typedef std::pair<unsigned int, NoiseFlag>         ChannelStatus;
    std::map<unsigned int, std::vector<ChannelStatus>> m_badChannelsPerTELL1;

    void                bookHistograms();          ///< Book the histograms
    AIDA::IHistogram1D* m_1d_nBad       = nullptr; ///< Number of channels failing any cuts vs TELL1
    AIDA::IHistogram1D* m_1d_nOpen      = nullptr; ///< Number of open noise channels vs TELL1
    AIDA::IHistogram1D* m_1d_nLowNoise  = nullptr; ///< Number of low noise channels vs TELL1
    AIDA::IHistogram2D* m_2d_nBad       = nullptr; ///< Number of channels failing any cuts in each sector vs TELL1
    AIDA::IHistogram2D* m_2d_nOpen      = nullptr; ///< Number of open strips in each sector vs TELL1
    AIDA::IHistogram2D* m_2d_nLowNoise  = nullptr; ///< Number of low noise strips in each sector vs TELL1
    AIDA::IHistogram1D* m_1d_nDifferent = nullptr; ///< Number of channels with status different from DB
  };

  DECLARE_COMPONENT( UTBadChannelFinder )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTBadChannelFinder::UTBadChannelFinder( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {
  declareProperty( "MaxEvents", m_maxEvents = 4000 );

  /// Noise calculation tool
  declareProperty( "NoiseToolType", m_noiseToolType = "UT::UTNoiseCalculationTool" );
  declareProperty( "NoiseToolName", m_noiseToolName = "UTNoiseCalculationTool" );

  /// Cut value for low noise in ADC counts (Hard cut for open channels)
  declareProperty( "RawNoiseOpen", m_rawNoiseOpen = 1.75 );

  /// Cut value for low noise in ADC counts (Soft cut to fitted line)
  declareProperty( "RawNoiseLow", m_rawNoiseLow = 0.5 );

  /// Make 2d plots showing bad channels per sector
  declareProperty( "PlotBySector", m_plotBySector = false );

  /// Compare channel status with DB
  declareProperty( "CompareDB", m_compareWithDB = false );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTBadChannelFinder::initialize() {

  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) {
    error() << "Failed to initialize base class" << endmsg;
    return sc;
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  if ( fullDetail() ) {
    m_plotBySector  = true;
    m_compareWithDB = true;
  }
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );

  m_noiseTool = tool<UT::IUTNoiseCalculationTool>( m_noiseToolType, m_noiseToolName );

  m_nTELL1s = readoutTool()->nBoard();

  // Book histograms...
  bookHistograms();

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode UT::UTBadChannelFinder::execute() {

  ++m_nEvents;
  //  const LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  //  const Gaudi::Time time = odin->eventTime();
  //  info() << "GaudiTime: " << time.day(0) << "/" << time.month(0)+1 << "/" << time.year(0)
  //  << " @ " << time.hour(0) << ":" << time.minute(0) << ":" << time.second(0) << endmsg;
  if ( m_nEvents.nEntries() % m_maxEvents == 0 ) {
    // code goes here
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Looking for new bad channels" << endmsg;
    m_1d_nBad->reset();
    m_1d_nOpen->reset();
    m_1d_nLowNoise->reset();
    if ( m_plotBySector ) {
      m_2d_nBad->reset();
      m_2d_nOpen->reset();
      m_2d_nLowNoise->reset();
    }
    findBadChannels();
  } // counter
  return StatusCode::SUCCESS;
}

//==============================================================================
// Loop over list of channels and compare status with that in the conditions DB
//==============================================================================
void UT::UTBadChannelFinder::compareWithDB() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "compareWithDB" << endmsg;
  m_1d_nDifferent->reset();
  /// Loop over TELL1s with bad channels
  std::map<unsigned int, std::vector<ChannelStatus>>::iterator itBad;
  for ( itBad = m_badChannelsPerTELL1.begin(); itBad != m_badChannelsPerTELL1.end(); ++itBad ) {

    unsigned int TELL1    = ( *itBad ).first;
    unsigned int sourceID = readoutTool()->TELLNumberToSourceID( TELL1 );

    const UTTell1Board* board = readoutTool()->findByBoardID( UTTell1ID( sourceID ) );

    for ( const auto& chan : itBad->second ) {
      //      unsigned int link = static_cast<unsigned int>(chan.first/128);
      LHCb::UTChannelID channelID =
          ( board->DAQToOffline( 0, UTDAQ::version::v4, UTDAQ::UTStripRepresentation( chan.first ) ).first );
      //      UTDAQ::BeetleRepresentation beetle(link);
      const DeUTSector*  sector      = tracker()->findSector( channelID );
      DeUTSector::Status StripStatus = sector->stripStatus( channelID );
      if ( (int)chan.second != (int)StripStatus ) {
        m_1d_nDifferent->fill( TELL1 );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Algorithm+DB different: TELL=" << TELL1 << ",channel=" << ( chan ).first
                  << ",strip=" << channelID.strip() << ",statusAlg=" << ( chan ).second << ",statusDB=" << StripStatus
                  << endmsg;
        }
      }
    }
  }
}

//==============================================================================
// Loop over all channels and find open/low noise channels
//==============================================================================
void UT::UTBadChannelFinder::findBadChannels() {
  // Get the tell1 mapping from source ID to tell1 number

  // loop over the boards
  for ( auto [sourceID, TELLID] : readoutTool()->SourceIDToTELLNumberMap() ) {
    // Temporary hack as possibility to subtract online histograms does not exist in the histogramdb yet.
    m_1d_nBad->fill( TELLID );
    m_1d_nOpen->fill( TELLID );
    m_1d_nLowNoise->fill( TELLID );
    std::vector<ChannelStatus>* badChannels = &m_badChannelsPerTELL1[TELLID];
    badChannels->clear();
    // Loop over raw noise and calculate bad channels
    auto itNoiseBegin = m_noiseTool->rawNoiseBegin( sourceID );
    for ( unsigned int optLink = 0; optLink < UTDAQ::noptlinks; ++optLink ) {
      for ( unsigned int port = 0; port < UTDAQ::nports; ++port ) {
        unsigned int TELL1Channel = optLink * LHCbConstants::nStripsInBeetle + port * LHCbConstants::nStripsInPort;
        auto         itNoise      = itNoiseBegin + TELL1Channel;
        auto         itNoiseEnd   = itNoise + UTDAQ::nstrips;
        calcSlope( itNoise, itNoiseEnd );
        // loop over strips and make cuts
        unsigned int channel = 0;
        for ( ; itNoise != itNoiseEnd; ++itNoise, ++TELL1Channel, ++channel ) {
          double       noise  = ( *itNoise );
          unsigned int sector = static_cast<unsigned int>( TELL1Channel / 512 );
          // hard cut: opens...
          if ( m_rawNoiseOpen > noise && 0.05 < noise ) {
            m_1d_nBad->fill( TELLID );
            m_1d_nOpen->fill( TELLID );
            if ( m_plotBySector ) {
              m_2d_nBad->fill( TELLID, sector );
              m_2d_nOpen->fill( TELLID, sector );
            }
            if ( msgLevel( MSG::DEBUG ) )
              debug() << "Open channel: " << TELL1Channel << ", TELL=" << sourceID << ", UTTELL" << TELLID << endmsg;
            // Add some checks for status....
            badChannels->push_back( ChannelStatus( TELL1Channel, UT::NoiseFlag::Open ) );
            continue;
          }
          // Soft cut
          double noiseFit = m_slope * channel + m_intercept;
          if ( noise > 0.05 && ( noiseFit - noise ) > m_rawNoiseLow ) {
            if ( msgLevel( MSG::DEBUG ) )
              debug() << "Low noise channel: " << TELL1Channel << ", TELL=" << sourceID << ", UTTELL" << TELLID
                      << endmsg;
            m_1d_nBad->fill( TELLID );
            m_1d_nLowNoise->fill( TELLID );
            if ( m_plotBySector ) {
              m_2d_nBad->fill( TELLID, sector );
              m_2d_nLowNoise->fill( TELLID, sector );
            }
            badChannels->push_back( ChannelStatus( TELL1Channel, UT::NoiseFlag::LowNoise ) );
            continue;
          }
          if ( noise > 0.05 ) { badChannels->push_back( ChannelStatus( TELL1Channel, UT::NoiseFlag::OK ) ); }
        }
      } // ports
    }   // links
  }     // TELL1s
  if ( m_compareWithDB ) compareWithDB();
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode UT::UTBadChannelFinder::finalize() {

  debug() << "==> Finalize" << endmsg;

  findBadChannels();

  return UT::HistoAlgBase::finalize();
}

//======================================================================================================================
// Calculate slope and intercept of line for channels in a beetle port
//======================================================================================================================
void UT::UTBadChannelFinder::calcSlope( std::vector<double>::const_iterator noiseBegin,
                                        std::vector<double>::const_iterator noiseEnd ) {

  std::vector<double>::const_iterator itNoise = noiseBegin;
  double                              channel = 0.;
  double                              meanX, meanY, meanXY, meanX2;
  meanX = meanY = meanXY = meanX2 = 0.;
  unsigned int n                  = 0;
  for ( ; itNoise != noiseEnd; ++itNoise, ++channel, ++n ) {
    meanX += channel;
    meanX2 += channel * channel;
    meanY += ( *itNoise );
    meanXY += channel * ( *itNoise );
  }
  if ( n > 0 ) {
    meanX /= n;
    meanX2 /= n;
    meanY /= n;
    meanXY /= n;

    m_slope     = ( meanXY - meanX * meanY ) / ( meanX2 - meanX * meanX );
    m_intercept = meanY - m_slope * meanX;
  } else {
    m_slope     = 0.;
    m_intercept = 0.;
    Warning( "calcSlope failed due to zero noisy channels" ).ignore();
  }
}

//======================================================================================================================
// Book histograms
//======================================================================================================================
void UT::UTBadChannelFinder::bookHistograms() {
  m_1d_nBad      = book1D( "Number of bad channels vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  m_1d_nOpen     = book1D( "Number of open channels vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  m_1d_nLowNoise = book1D( "Number of low noise channels vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
  if ( m_plotBySector ) {
    int    nSectors = 6;
    double yMax     = nSectors - 0.5;
    m_2d_nBad =
        book2D( "Number of bad channels per sector vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, -0.5, yMax, nSectors );
    m_2d_nOpen =
        book2D( "Number of open channels per sector vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, -0.5, yMax, nSectors );
    m_2d_nLowNoise = book2D( "Number of low noise channels per sector vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s, -0.5,
                             yMax, nSectors );
  }
  if ( m_compareWithDB )
    m_1d_nDifferent = book1D( "Number of status different channels vs TELL1", 0.5, m_nTELL1s + 0.5, m_nTELL1s );
}
