/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IUTNoiseCalculationTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTNoiseCalculation
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTNoiseCalculation UTNoiseCalculation.h
 *
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */
namespace UT {
  class UTNoiseCalculation : public GaudiAlgorithm {
  public:
    using GaudiAlgorithm::GaudiAlgorithm;
    StatusCode execute() override { return m_noiseTool->updateNoise(); }

  private:
    PublicToolHandle<IUTNoiseCalculationTool> m_noiseTool{
        this, "NoiseTool", "UT::UTNoiseCalculationTool/UTNoiseCalculationTool"}; ///< Tool to calculate noise
  };

  DECLARE_COMPONENT( UTNoiseCalculation )
} // namespace UT

//=============================================================================
