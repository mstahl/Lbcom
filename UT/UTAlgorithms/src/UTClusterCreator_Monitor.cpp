/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTCommonBase.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

/** @class UTClusterCreator_Monitor UTClusterCreator_Monitor.h
 *
 *  Class for clustering in the UT tracker
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTClusterCreator_Monitor
    : public Gaudi::Functional::Transformer<LHCb::UTClusters( const LHCb::UTDigits& ),
                                            Gaudi::Functional::Traits::BaseClass_t<UT::CommonBase<GaudiAlgorithm>>> {

public:
  // Constructors and destructor
  UTClusterCreator_Monitor( const std::string& name, ISvcLocator* pSvcLocator );

  // IAlgorithm members
  StatusCode       initialize() override;
  LHCb::UTClusters operator()( const LHCb::UTDigits& digitCont ) const override;

private:
  StatusCode createClusters( const LHCb::UTDigits& digitsCont, LHCb::UTClusters& clustersCont ) const;

  LHCb::UTCluster::ADCVector strips( const SmartRefVector<LHCb::UTDigit>& clusteredDigits,
                                     const LHCb::UTChannelID              closestChan ) const;

  StatusCode loadCutsFromConditions();

  Gaudi::Property<bool>        m_forceOptions{this, "forceOptions", false};
  Gaudi::Property<double>      m_digitSig2NoiseThreshold{this, "DigitSignal2Noise", 3.0};
  Gaudi::Property<double>      m_clusterSig2NoiseThreshold{this, "ClusterSignal2Noise", 4.0};
  Gaudi::Property<double>      m_highThreshold{this, "HighSignal2Noise", 10.0};
  Gaudi::Property<int>         m_outputVersion{this, "OutputVersion", 1};
  Gaudi::Property<int>         m_maxSize{this, "Size", 4};
  Gaudi::Property<bool>        m_byBeetle{this, "ByBeetle", true};
  Gaudi::Property<std::string> m_spillName{this, "Spill", "Central"};

  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation",
                                                   "/dd/Conditions/ReadoutConf/UT/ClusteringThresholds"};

  typedef std::map<const DeUTSector*, unsigned int> CutMap;

  CutMap m_clusterSig2NoiseCut;
  CutMap m_highSig2NoiseCut;

  LHCb::UTCluster::Spill m_spill;

  PublicToolHandle<IUTClusterPosition> m_positionTool{this, "PositionTool", "UTOnlinePosition"};
};

using namespace LHCb;

DECLARE_COMPONENT( UTClusterCreator_Monitor )

UTClusterCreator_Monitor::UTClusterCreator_Monitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", UTDigitLocation::UTTightDigits},
                  {"OutputLocation", UTClusterLocation::UTClusters}} {}

StatusCode UTClusterCreator_Monitor::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  // get the cuts from condition if present
  if ( !this->template existDet<Condition>( m_conditionLocation ) || m_forceOptions ) {
    this->info() << "Default to jobOptions for cuts" << endmsg;
  } else {
    this->registerCondition( m_conditionLocation, &UTClusterCreator_Monitor::loadCutsFromConditions );
    sc = this->runUpdate();
    if ( sc.isFailure() ) return this->Error( "Failed to update conditions", sc );
  }

  // calculate cut values
  for ( const auto& iterS : this->tracker()->sectors() ) {
    const double NoiseInADC      = iterS->sectorNoise();
    m_clusterSig2NoiseCut[iterS] = LHCb::Math::round( m_clusterSig2NoiseThreshold * NoiseInADC );
    m_highSig2NoiseCut[iterS]    = LHCb::Math::round( m_highThreshold * NoiseInADC );
  }

  m_spill = UTCluster::SpillToType( m_spillName );

  return StatusCode::SUCCESS;
}

LHCb::UTClusters UTClusterCreator_Monitor::operator()( const UTDigits& digitCont ) const {
  UTClusters clusterCont;
  clusterCont.setVersion( m_outputVersion );
  createClusters( digitCont, clusterCont ).orThrow( "Failed to create clusters", "UTClusterCreator_Monitor" ).ignore();
  return clusterCont;
}

StatusCode UTClusterCreator_Monitor::createClusters( const UTDigits& digitCont, UTClusters& clusterCont ) const {

  auto iterDigit = digitCont.begin();
  while ( iterDigit != digitCont.end() ) {

    auto jterDigit = iterDigit;
    ++jterDigit;

    // make a cluster !
    SmartRefVector<UTDigit> clusteredDigits;
    clusteredDigits.push_back( *iterDigit );
    double totCharge = ( *iterDigit )->depositedCharge();

    // calculate the interstrip fraction a la Tell1
    IUTClusterPosition::Info measValue = m_positionTool->estimate( clusteredDigits );

    const double nSum = totCharge;

    // make cluster +set things
    const UTLiteCluster clusterLite( measValue.strip, measValue.fractionalPosition, clusteredDigits.size(), true,
                                     true );

    // link to the DAQ
    UTDAQID utdaqID = this->readoutTool()->channelIDToDAQID( static_cast<UTChannelID>( measValue.strip ) );
    if ( utdaqID.id() == UTTell1ID::nullBoard ) {
      // screwed
      this->err() << "failed to link " << this->uniqueSector( measValue.strip ) << endmsg;
      return this->Error( "Failed to find Tell1 board", StatusCode::FAILURE );
    }

    UTCluster* newCluster = new UTCluster( clusterLite, strips( clusteredDigits, measValue.strip ), nSum, utdaqID.id(),
                                           utdaqID.channel(), m_spill );
    // add to container
    clusterCont.insert( newCluster, measValue.strip );

    iterDigit = jterDigit;
  } // iterDigit

  return StatusCode::SUCCESS;
}

UTCluster::ADCVector UTClusterCreator_Monitor::strips( const SmartRefVector<UTDigit>& clusteredDigits,
                                                       const UTChannelID              closestChan ) const {

  // make the vector of ADC values stored in the cluster
  UTCluster::ADCVector tVec;
  for ( const auto& d : clusteredDigits ) {
    const int strip = d->channelID().strip() - closestChan.strip();
    tVec.emplace_back( strip, d->depositedCharge() );
  }
  return tVec;
}

StatusCode UTClusterCreator_Monitor::loadCutsFromConditions() {

  // load conditions
  auto* cInfo = this->template getDet<Condition>( m_conditionLocation );
  this->info() << "Loading cuts tagged as " << cInfo->param<std::string>( "tag" ) << endmsg;

  m_digitSig2NoiseThreshold   = cInfo->param<double>( "HitThreshold" );
  m_clusterSig2NoiseThreshold = cInfo->param<double>( "ConfirmationThreshold" );
  m_highThreshold             = cInfo->param<double>( "SpilloverThreshold" );

  return StatusCode::SUCCESS;
}
