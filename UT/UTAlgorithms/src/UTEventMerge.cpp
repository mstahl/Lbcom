/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTAlgBase.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTDataFunctor.h"
using namespace LHCb;

/** @class UTEventMerge UTEventMerge.h
 *
 *  Class for merging events
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTEventMerge : public UT::AlgBase {

public:
  // Constructors and destructor
  using UT::AlgBase::AlgBase;

  // IAlgorithm members
  StatusCode initialize() override;
  StatusCode execute() override;

private:
  void overlappingClusters( const LHCb::UTClusters* inCont, const LHCb::UTCluster* testClus,
                            std::vector<LHCb::UTCluster*>& outCont ) const;

  unsigned int spillDifference( const LHCb::UTCluster* clus1, const LHCb::UTCluster* clus2 ) const;

  LHCb::UTCluster* selectBestCluster( const std::vector<LHCb::UTCluster*>& clusters,
                                      const LHCb::UTCluster*               testClus ) const;

  Gaudi::Property<std::vector<std::string>> m_spillsVector{this, "spills", {}};
  std::vector<std::string>                  m_spillPath;
  Gaudi::Property<std::string>              m_inputLocation{this, "inputLocation", UTClusterLocation::UTClusters};
  Gaudi::Property<std::string>              m_clusterLocation{this, "clusterLocation", "/Event/Raw/UT/MergedSpills"};
};

DECLARE_COMPONENT( UTEventMerge )

namespace {
  struct Less_by_Channel {
    bool operator()( const LHCb::UTLiteCluster& obj1, const LHCb::UTLiteCluster& obj2 ) const {
      return obj1.channelID() < obj2.channelID();
    }
  };
} // namespace

StatusCode UTEventMerge::initialize() {
  return UT::AlgBase::initialize().andThen( [&] {
    std::transform( m_spillsVector.begin(), m_spillsVector.end(), std::back_inserter( m_spillPath ),
                    [&]( const auto& s ) { return "/Event" + s + m_inputLocation; } );
  } );
}

StatusCode UTEventMerge::execute() {

  auto* fCont = new UTClusters();
  fCont->reserve( 5000 );
  put( fCont, m_clusterLocation );

  for ( const auto& iterS : m_spillPath ) {
    auto* clusterCont = get<UTClusters>( iterS );

    for ( auto& iterC : *clusterCont ) {

      std::vector<UTCluster*> clusters;
      overlappingClusters( fCont, iterC, clusters );

      if ( clusters.empty() ) {
        // not in the container insert
        fCont->insert( iterC->clone(), iterC->channelID() );
      } else {
        // if this is a better cluster take it...
        // first pick the best in the container
        UTCluster* bestCluster = selectBestCluster( clusters, iterC );
        if ( iterC->totalCharge() > bestCluster->totalCharge() ) {
          fCont->remove( bestCluster );
          fCont->insert( iterC->clone(), iterC->channelID() );
        }
      }
    }
  } // spills

  // sort
  std::sort( fCont->begin(), fCont->end(), UTDataFunctor::Less_by_Channel<const UTCluster*>() );

  return StatusCode::SUCCESS;
}

UTCluster* UTEventMerge::selectBestCluster( const std::vector<UTCluster*>& clusters, const UTCluster* testClus ) const {

  if ( testClus->key() == clusters.front()->key() ) return clusters.front();

  UTCluster* bestC      = nullptr;
  double     bestCharge = -100;
  for ( auto cluster : clusters ) {
    if ( cluster->totalCharge() > bestCharge ) {
      bestCharge = cluster->totalCharge();
      bestC      = cluster;
    }
  } // iterC

  return bestC;
}

void UTEventMerge::overlappingClusters( const UTClusters* inCont, const UTCluster* testClus,
                                        std::vector<UTCluster*>& outCont ) const {

  // test if a cluster with the same key exists
  UTCluster* aCluster = inCont->object( testClus->key() );
  if ( aCluster ) { outCont.push_back( aCluster ); }

  // get list of channels to test
  std::vector<UTChannelID> chans = testClus->channels();
  for ( auto iterC = chans.begin(); iterC != chans.end(); ++iterC ) {
    UTCluster* aCluster2 = inCont->object( *iterC );
    if ( aCluster2 ) {
      if ( spillDifference( testClus, aCluster2 ) == 1 ) {
        // overlapping clusters from 'spillover'
        outCont.push_back( aCluster2 );
      }
    }
  } // iterC

  //  std::unique(outCont.begin(), outCont.end());
}

unsigned int UTEventMerge::spillDifference( const UTCluster* clus1, const UTCluster* clus2 ) const {
  return abs( clus1->spill() - clus2->spill() );
}
