/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTLiteCluster.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTAlgBase.h"
#include "Kernel/UTChannelID.h"

using namespace LHCb;

/** @class UTLiteClusterKiller UTLiteClusterKiller.h
 *
 *  Class for killing clusters and random
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTLiteClusterKiller : public UT::AlgBase {

public:
  // Constructor
  using UT::AlgBase::AlgBase;

  // IAlgorithm members
  StatusCode execute() override {
    UTLiteCluster::UTLiteClusters* clusterCont = m_clusters.get();
    std::remove_if( clusterCont->begin(), clusterCont->end(),
                    [&]( const LHCb::UTLiteCluster& c ) { return ( *m_clusterSelector )( c.channelID() ); } );
    return StatusCode::SUCCESS;
  }

private:
  PublicToolHandle<IUTChannelIDSelector> m_clusterSelector{this, "Selector", "UTRndmChannelIDSelector/UTLiteKiller"};
  DataObjectReadHandle<UTLiteCluster::UTLiteClusters> m_clusters{this, "InputLocation",
                                                                 UTLiteClusterLocation::UTClusters};
};

DECLARE_COMPONENT( UTLiteClusterKiller )
