/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTCLUUTERUTODIGITS_H
#define UTCLUUTERUTODIGITS_H 1

/** @class UTClustersToDigits
 *
 *  simple class for stripping liteCluster from clusters
 *  UT specialization
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

// Gaudi
#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Kernel/UTAlgBase.h"

#include "UTSiClusterTraits.h"
#include "UTSiClustersToDigits.h"
using UTClustersToDigits = UTSiClustersToDigits<LHCb::UTCluster>;
#include "UTSiClustersToDigits.icpp"

template <>
UTSiClustersToDigits<LHCb::UTCluster>::UTSiClustersToDigits( const std::string& name, ISvcLocator* pSvcLocator )
    : UTSiClusterTraits<LHCb::UTCluster>::BASEALG( name, pSvcLocator ) {

  declareProperty( "inputLocation", m_inputLocation = LHCb::UTClusterLocation::UTClusters );
  declareProperty( "outputLocation", m_outputLocation = LHCb::UTDigitLocation::UTDigits );

  declareProperty( "addNeighbourInfo", m_addNeighbourInfo = true );
}

template <>
UTSiClusterTraits<LHCb::UTCluster>::DIGIT*
UTSiClustersToDigits<LHCb::UTCluster>::createDigit( const unsigned int adcValue ) {
  return new UTSiClusterTraits<LHCb::UTCluster>::DIGIT( adcValue );
}

#include "UTDet/DeUTDetector.h"

template <>
inline void UTSiClustersToDigits<LHCb::UTCluster>::addNeighbours(
    LHCb::UTCluster* clus, UTSiClusterTraits<LHCb::UTCluster>::DIGIT::Container* digitCont ) {

  // use the neighbour sum to add information on the neighbouring strips..

  // left + right neighbour
  LHCb::UTChannelID leftChan  = tracker()->nextLeft( clus->firstChannel() );
  LHCb::UTChannelID rightChan = tracker()->nextRight( clus->firstChannel() );

  double neighbourSum = clus->neighbourSum();
  if ( leftChan != 0u && rightChan != 0u ) {
    neighbourSum *= 0.5;
    if ( !digitCont->object( leftChan ) ) { digitCont->insert( new LHCb::UTDigit( neighbourSum ), leftChan ); }
    if ( !digitCont->object( rightChan ) ) { digitCont->insert( new LHCb::UTDigit( neighbourSum ), rightChan ); }
  } else if ( leftChan != 0u ) {
    if ( !digitCont->object( leftChan ) ) { digitCont->insert( new LHCb::UTDigit( neighbourSum ), leftChan ); }
  } else if ( rightChan != 0u ) {
    if ( !digitCont->object( rightChan ) ) { digitCont->insert( new LHCb::UTDigit( neighbourSum ), rightChan ); }
  } else {
    // nothing
  }
}

DECLARE_COMPONENT_WITH_ID( UTClustersToDigits, "UTClustersToDigits" )

#endif // UTClustersToDigits
