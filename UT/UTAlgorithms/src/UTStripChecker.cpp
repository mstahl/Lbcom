/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IProfile1D.h"
#include "Event/UTCluster.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBeetleRepresentation.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTTell1Board.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <algorithm>
#include <sstream>

using namespace std;
using namespace LHCb;
using namespace UT;
using namespace AIDA;

/** @class UTStripChecker UTStripChecker.h
 *
 *
 *  @author Andy Beiter (based on code by Johan Luisier)
 *  @date   2018-09-04
 */
class UTStripChecker : public Gaudi::Functional::Consumer<void( const LHCb::UTClusters& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<UT::HistoAlgBase>> {
public:
  /// Standard constructor
  UTStripChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                                ///< Algorithm initialization
  void       operator()( const LHCb::UTClusters& ) const override; ///< Algorithm execution
  StatusCode finalize() override;                                  ///< Algorithm finalization

private:
  /**
   * Initialise couting map
   */
  void initMap();

  /**
   * Prints out the strip
   */
  void printZeros( const std::pair<LHCb::UTChannelID, unsigned int>& p );

  /**
   * Map containing the number of hits above the S/N threshold.
   */
  mutable std::map<LHCb::UTChannelID, unsigned int> m_StripsMap;

  /**
   * Cut applied on the S/N, in order to determine whether the strip is
   * alive or not.
   */
  Gaudi::Property<double> m_signalToNoiseCut{this, "SignalToNoise", 8.};

  std::string m_lastSector; //  Last "printed" sector

  /**
   * Counts how many strips didn't get a hit
   */
  unsigned int m_counter = 0;

  /**
   * Number of wanted hits in order to tag the strip as alive
   */
  Gaudi::Property<unsigned int> m_hitsNumber{this, "HitsNumber", 1u};
};
//-----------------------------------------------------------------------------
// Implementation file for class : UTStripChecker
//
// 2018-09-04 : Andy Beiter (based on code by Johan Luisier)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTStripChecker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UTStripChecker::UTStripChecker( const string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"DataLocation", "Raw/UT/Clusters"}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UTStripChecker::initialize() { return Consumer::initialize().andThen( &UTStripChecker::initMap, this ); }

//=============================================================================
// Main execution
//=============================================================================
void UTStripChecker::operator()( const LHCb::UTClusters& clusterCont ) const {
  for ( const auto& itClust : clusterCont ) {
    const auto& channelIDs = itClust->channels();

    const DeUTSector* sector = tracker()->findSector( channelIDs.front() );
    if ( !sector ) continue;

    auto clusEnd   = channelIDs.end();
    auto clusBegin = channelIDs.begin();
    for ( auto clusIt = clusBegin; clusIt != clusEnd; clusIt++ ) {
      double Noise = sector->noise( *clusIt );

      if ( static_cast<double>( itClust->adcValue( clusIt - clusBegin ) ) > Noise * m_signalToNoiseCut ) {
        if ( auto s = m_StripsMap.find( *clusIt ); s != m_StripsMap.end() ) ++s->second;
      }
    }
  }
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode UTStripChecker::finalize() {
  for ( const auto& elem : m_StripsMap ) printZeros( elem );
  info() << endmsg << "In total, " << m_counter << " strips didn't have a hit" << endmsg;
  for ( const auto& sec : tracker()->sectors() ) {
    int         nbrStrips = sec->nStrip();
    const auto& nick      = sec->nickname();
    for ( int strip = 1; strip < nbrStrips; strip++ ) {
      auto channelID = sec->stripToChan( strip );
      profile1D( strip, m_StripsMap[channelID], nick, "Number of hits in " + nick, .5, nbrStrips + .5, nbrStrips );
    }
  }
  return Consumer::finalize(); // must be called after all other actions
}

void UTStripChecker::initMap() {
  for ( const auto& sec : tracker()->sectors() ) {
    for ( unsigned int strip = 1; strip < sec->nStrip(); strip++ ) {
      auto channelID = sec->stripToChan( strip );
      if ( sec->isOKStrip( channelID ) ) m_StripsMap[channelID] = 0u;
    }
  }
}

void UTStripChecker::printZeros( const pair<UTChannelID, unsigned int>& p ) {
  if ( p.second < m_hitsNumber ) {
    const DeUTSector* sector = tracker()->findSector( p.first );

    if ( m_lastSector != sector->nickname() ) {
      info() << endmsg;
      m_lastSector = sector->nickname();

      info() << "No hits found in  " << sector->nickname() << " strip(s) ";
    }

    info() << p.first.strip() << ' ';
    m_counter++;
  }
}

//=============================================================================
