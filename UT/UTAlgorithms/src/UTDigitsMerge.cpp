/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class UTDigitsMerge
 *
 *  simple class for merging cluster containers
 *  UT specialization
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Kernel/UTAlgBase.h"
#include "UTSiClusterTraits.h"
#include "UTSiDigitsMerge.h"
using UTDigitsMerge = UTSiDigitsMerge<LHCb::UTCluster>;
#include "UTSiDigitsMerge.icpp"

template <>
UTSiDigitsMerge<LHCb::UTCluster>::UTSiDigitsMerge( const std::string& name, ISvcLocator* pSvcLocator )
    : UTSiClusterTraits<LHCb::UTCluster>::BASEALG( name, pSvcLocator ) {

  declareProperty( "nBits", m_nBits = 6u );
  declareProperty( "spills", m_spillsVector );

  declareProperty( "outputLocation", m_outputLocation = "/Event/Raw/UT/MergedDigits" );
  declareProperty( "inputLocation", m_inputLocation = LHCb::UTDigitLocation::UTDigits );
}

template <>
void UTSiDigitsMerge<LHCb::UTCluster>::updateCharge( UTSiClusterTraits<LHCb::UTCluster>::DIGIT* digit1,
                                                     UTSiClusterTraits<LHCb::UTCluster>::DIGIT* digit2 ) {
  const double sumvalue = digit1->depositedCharge() + digit2->depositedCharge();
  const double newvalue = std::max( std::min( m_maxValue, sumvalue ), m_minValue );
  digit1->setDepositedCharge( newvalue );
}

DECLARE_COMPONENT_WITH_ID( UTDigitsMerge, "UTDigitsMerge" )
