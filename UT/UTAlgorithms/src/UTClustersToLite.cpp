/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class UTClustersToLite
 *
 *  simple class for stripping liteCluster from clusters
 *  UT specialization
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Kernel/UTAlgBase.h"
#include "UTSiClusterTraits.h"
#include "UTSiClustersToLite.icpp"

template <>
UTSiClustersToLite<LHCb::UTCluster>::UTSiClustersToLite( const std::string& name, ISvcLocator* pSvcLocator )
    : Gaudi::Functional::Transformer<typename UTSiClusterTraits<LHCb::UTCluster>::LITECONT(
                                         const typename UTSiClusterTraits<LHCb::UTCluster>::CLUSCONT& ),
                                     UTSiClusterBaseClassTraits<LHCb::UTCluster>>(
          name, pSvcLocator, {KeyValue{"inputLocation", LHCb::UTClusterLocation::UTClusters}},
          KeyValue{"outputLocation", LHCb::UTLiteClusterLocation::UTClusters} ) {}

using UTClustersToLite = UTSiClustersToLite<LHCb::UTCluster>;
DECLARE_COMPONENT( UTClustersToLite )
