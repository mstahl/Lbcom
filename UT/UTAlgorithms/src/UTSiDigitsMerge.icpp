/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTSiClusterTraits.h"
#include "UTSiDigitsMerge.h"

using namespace LHCb;

template <class TYPE>
StatusCode UTSiDigitsMerge<TYPE>::initialize() {
  StatusCode sc = UTSiClusterTraits<TYPE>::BASEALG::initialize();
  if ( sc.isFailure() ) return GaudiAlgorithm::Error( "Failed to initialize", sc );

  m_maxValue = 2 << m_nBits;
  m_minValue = -m_maxValue;

  std::vector<std::string>::const_iterator iSpillName = m_spillsVector.begin();
  while ( iSpillName != m_spillsVector.end() ) {
    m_spillsPath.push_back( "/Event" + ( *iSpillName ) + m_inputLocation );
    ++iSpillName;
  } // spillnames

  return StatusCode::SUCCESS;
}

template <class TYPE>
inline StatusCode UTSiDigitsMerge<TYPE>::execute() {

  // output
  typedef typename UTSiClusterTraits<TYPE>::DIGIT::Container DigitCont;
  typedef typename UTSiClusterTraits<TYPE>::DIGIT            DigitType;
  DigitCont*                                                 dCont = new DigitCont();
  dCont->reserve( 10000 );
  GaudiAlgorithm::put( dCont, m_outputLocation );

  // loop over the input spills
  std::vector<std::string>::const_iterator iterS = m_spillsPath.begin();
  for ( ; iterS != m_spillsPath.end(); ++iterS ) {
    // loop over this container
    DigitCont*                   aCont = GaudiAlgorithm::template get<DigitCont>( *iterS );
    typename DigitCont::iterator iterD = aCont->begin();
    for ( ; iterD != aCont->end(); ++iterD ) {
      DigitType* tDigit = dCont->object( ( *iterD )->key() );
      if ( !tDigit ) {
        // if channel not hit, just add it...
        dCont->insert( new UTDigit( **iterD ), ( *iterD )->channelID() );
      } else {
        // we should merge it
        updateCharge( tDigit, *iterD );
      }
    } // digits
  }   // spills

  std::sort( dCont->begin(), dCont->end(),
             []( const DigitType* l, const DigitType* r ) { return l->channelID() < r->channelID(); } );

  return StatusCode::SUCCESS;
}
