/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTCommonBase.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

/** @class UTClusterCreator UTClusterCreator.h
 *
 *  Class for clustering in the UT tracker
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTClusterCreator
    : public Gaudi::Functional::Transformer<LHCb::UTClusters( const LHCb::UTDigits& ),
                                            Gaudi::Functional::Traits::BaseClass_t<UT::CommonBase<GaudiAlgorithm>>> {

public:
  // Constructors and destructor
  UTClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

  // IAlgorithm members
  StatusCode       initialize() override;
  LHCb::UTClusters operator()( const LHCb::UTDigits& digitCont ) const override;

private:
  StatusCode createClusters( const LHCb::UTDigits& digitsCont, LHCb::UTClusters& clustersCont ) const;

  bool keepClustering( const LHCb::UTDigit* firstDigit, const LHCb::UTDigit* secondDigit,
                       const DeUTSector* aSector ) const;

  bool sameBeetle( const LHCb::UTChannelID firstChan, const LHCb::UTChannelID secondChan ) const;

  bool aboveDigitSignalToNoise( const LHCb::UTDigit* aDigit, const DeUTSector* aSector ) const;
  bool aboveClusterSignalToNoise( const double charge, const DeUTSector* sector ) const;
  bool hasHighThreshold( const double charge, const DeUTSector* aSector ) const;

  double neighbourSum( LHCb::UTDigits::const_iterator start, LHCb::UTDigits::const_iterator stop,
                       const LHCb::UTDigits& digits ) const;

  LHCb::UTCluster::ADCVector strips( const SmartRefVector<LHCb::UTDigit>& clusteredDigits,
                                     const LHCb::UTChannelID              closestChan ) const;

  StatusCode loadCutsFromConditions();

  Gaudi::Property<bool>        m_forceOptions{this, "forceOptions", false};
  Gaudi::Property<double>      m_digitSig2NoiseThreshold{this, "DigitSignal2Noise", 3.0};
  Gaudi::Property<double>      m_clusterSig2NoiseThreshold{this, "ClusterSignal2Noise", 4.0};
  Gaudi::Property<double>      m_highThreshold{this, "HighSignal2Noise", 10.0};
  Gaudi::Property<int>         m_outputVersion{this, "OutputVersion", 1};
  Gaudi::Property<int>         m_maxSize{this, "Size", 4};
  Gaudi::Property<bool>        m_byBeetle{this, "ByBeetle", true};
  Gaudi::Property<std::string> m_spillName{this, "Spill", "Central"};

  std::string m_conditionLocation;

  typedef std::map<const DeUTSector*, unsigned int> CutMap;

  mutable CutMap m_clusterSig2NoiseCut;
  mutable CutMap m_highSig2NoiseCut;

  LHCb::UTCluster::Spill m_spill;

  PublicToolHandle<IUTClusterPosition> m_positionTool{this, "PositionTool", "UTOnlinePosition"};
};

using namespace LHCb;

DECLARE_COMPONENT( UTClusterCreator )

UTClusterCreator::UTClusterCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", UTDigitLocation::UTDigits},
                  {"OutputLocation", UTClusterLocation::UTClusters}} {
  this->declareProperty( "conditionLocation",
                         m_conditionLocation = "/dd/Conditions/ReadoutConf/UT/ClusteringThresholds" );
}

StatusCode UTClusterCreator::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  // get the cuts from condition if present
  if ( !this->template existDet<Condition>( m_conditionLocation ) || m_forceOptions ) {
    this->info() << "Default to jobOptions for cuts" << endmsg;
  } else {
    this->registerCondition( m_conditionLocation, &UTClusterCreator::loadCutsFromConditions );
    sc = this->runUpdate();
    if ( sc.isFailure() ) return this->Error( "Failed to update conditions", sc );
  }

  // calculate cut values
  for ( const auto& iterS : this->tracker()->sectors() ) {
    const double NoiseInADC      = iterS->sectorNoise();
    m_clusterSig2NoiseCut[iterS] = LHCb::Math::round( m_clusterSig2NoiseThreshold * NoiseInADC );
    m_highSig2NoiseCut[iterS]    = LHCb::Math::round( m_highThreshold * NoiseInADC );
  }

  m_spill = UTCluster::SpillToType( m_spillName );

  return StatusCode::SUCCESS;
}

LHCb::UTClusters UTClusterCreator::operator()( const UTDigits& digitCont ) const {
  UTClusters clusterCont;
  clusterCont.setVersion( m_outputVersion );
  createClusters( digitCont, clusterCont ).orThrow( "Failed to create clusters", "UTClusterCreator" ).ignore();
  return clusterCont;
}

StatusCode UTClusterCreator::createClusters( const UTDigits& digitCont, UTClusters& clusterCont ) const {

  auto iterDigit = digitCont.begin();
  while ( iterDigit != digitCont.end() ) {

    auto jterDigit = iterDigit;
    ++jterDigit;

    const DeUTSector* aSector = this->tracker()->findSector( ( *iterDigit )->channelID() );
    ;
    if ( aboveDigitSignalToNoise( *iterDigit, aSector ) ) {

      // make a cluster !
      auto                    startCluster = iterDigit;
      SmartRefVector<UTDigit> clusteredDigits;
      clusteredDigits.push_back( *iterDigit );
      double totCharge = ( *iterDigit )->depositedCharge();

      // clustering loop
      while ( ( jterDigit != digitCont.end() ) && ( iterDigit != digitCont.end() ) &&
              ( clusteredDigits.size() < (unsigned int)m_maxSize ) &&
              ( keepClustering( *iterDigit, *jterDigit, aSector ) == true ) ) {
        clusteredDigits.push_back( *jterDigit );
        totCharge += ( *jterDigit )->depositedCharge();
        ++iterDigit;
        ++jterDigit;
      } // clustering loop

      if ( aboveClusterSignalToNoise( totCharge, aSector ) ) {

        // calculate the interstrip fraction a la Tell1
        IUTClusterPosition::Info measValue = m_positionTool->estimate( clusteredDigits );

        const double nSum = neighbourSum( startCluster, iterDigit, digitCont );

        // make cluster +set things
        const UTLiteCluster clusterLite( measValue.strip, measValue.fractionalPosition, clusteredDigits.size(),
                                         hasHighThreshold( totCharge, aSector ), true );

        // link to the tell
        UTDAQ::chanPair aPair = this->readoutTool()->offlineChanToDAQ( measValue.strip, measValue.fractionalPosition );
        if ( aPair.first.id() == UTTell1ID::nullBoard ) {
          // screwed
          this->err() << "failed to link " << this->uniqueSector( measValue.strip ) << endmsg;
          return this->Error( "Failed to find Tell1 board", StatusCode::FAILURE );
        }

        UTCluster* newCluster = new UTCluster( clusterLite, strips( clusteredDigits, measValue.strip ), nSum,
                                               aPair.first.id(), aPair.second, m_spill );
        // add to container
        clusterCont.insert( newCluster, measValue.strip );
      }
    } // if found cluster seed

    iterDigit = jterDigit;
  } // iterDigit

  return StatusCode::SUCCESS;
}

bool UTClusterCreator::aboveDigitSignalToNoise( const UTDigit* aDigit, const DeUTSector* aSector ) const {
  // digit above threshold ?
  const unsigned int threshold = LHCb::Math::round( m_digitSig2NoiseThreshold * aSector->noise( aDigit->channelID() ) );
  return aDigit->depositedCharge() > threshold;
}

bool UTClusterCreator::aboveClusterSignalToNoise( const double charge, const DeUTSector* sector ) const {
  // cluster above threshold ?
  return charge > m_clusterSig2NoiseCut[sector];
}

bool UTClusterCreator::hasHighThreshold( const double charge, const DeUTSector* aSector ) const {
  // cluster above threshold ?
  return charge > m_highSig2NoiseCut[aSector];
}

bool UTClusterCreator::keepClustering( const UTDigit* firstDigit, const UTDigit* secondDigit,
                                       const DeUTSector* aSector ) const {
  // check whether to continue clustering.
  // Digits have to be above threshold and in consecutive channels in same wafer
  bool clusFlag = false;

  if ( aboveDigitSignalToNoise( secondDigit, aSector ) ) {
    const UTChannelID firstChan  = firstDigit->channelID();
    const UTChannelID secondChan = secondDigit->channelID();
    if ( aSector->nextRight( firstChan ) == secondChan ) {
      if ( !m_byBeetle || sameBeetle( firstChan, secondChan ) ) clusFlag = true;
    }
  }
  return clusFlag;
}

bool UTClusterCreator::sameBeetle( const UTChannelID firstChan, const UTChannelID secondChan ) const {
  // check channels are on the sameBeetle
  const unsigned int firstBeetle  = ( firstChan.strip() - 1 ) / LHCbConstants::nStripsInBeetle;
  const unsigned int secondBeetle = ( secondChan.strip() - 1 ) / LHCbConstants::nStripsInBeetle;
  return ( firstBeetle == secondBeetle );
}

double UTClusterCreator::neighbourSum( LHCb::UTDigits::const_iterator start, LHCb::UTDigits::const_iterator stop,
                                       const LHCb::UTDigits& digits ) const {
  double nSum = 0;

  if ( start != digits.begin() ) {
    auto iter = std::prev( start );
    if ( ( *iter )->channelID() == this->tracker()->nextLeft( ( *start )->channelID() ) ) {
      nSum += ( *iter )->depositedCharge();
    }
  }

  if ( stop != digits.end() ) {
    auto iter = std::next( stop );
    if ( iter != digits.end() && ( ( *iter )->channelID() == this->tracker()->nextRight( ( *start )->channelID() ) ) ) {
      nSum += ( *iter )->depositedCharge();
    }
  }
  return nSum;
}

UTCluster::ADCVector UTClusterCreator::strips( const SmartRefVector<UTDigit>& clusteredDigits,
                                               const UTChannelID              closestChan ) const {

  // make the vector of ADC values stored in the cluster
  UTCluster::ADCVector tVec;
  for ( const auto& d : clusteredDigits ) {
    const int strip = d->channelID().strip() - closestChan.strip();
    tVec.emplace_back( strip, d->depositedCharge() );
  }
  return tVec;
}

StatusCode UTClusterCreator::loadCutsFromConditions() {

  // load conditions
  auto* cInfo = this->template getDet<Condition>( m_conditionLocation );
  this->info() << "Loading cuts tagged as " << cInfo->param<std::string>( "tag" ) << endmsg;

  m_digitSig2NoiseThreshold   = cInfo->param<double>( "HitThreshold" );
  m_clusterSig2NoiseThreshold = cInfo->param<double>( "ConfirmationThreshold" );
  m_highThreshold             = cInfo->param<double>( "SpilloverThreshold" );

  return StatusCode::SUCCESS;
}
