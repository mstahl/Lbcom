/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/UTAlgBase.h"
#include "Kernel/UTXMLUtils.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <fstream>

/** @class WriteUTAlignmentConditions WriteUTAlignmentConditions.h
 *
 *  Class for writing UT status conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class WriteUTStatusConditions : public UT::AlgBase {

public:
  /// constructer
  WriteUTStatusConditions( const std::string& name, ISvcLocator* svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

  // finalize
  StatusCode finalize() override;

private:
  std::string m_footer;
  std::string m_startTag;

  std::string footer() const;
  std::string header( const std::string& conString ) const;
  std::string strip( const std::string& conString ) const;

  std::string   m_outputFileName;
  std::ofstream m_outputFile;
  std::string   m_author;
  std::string   m_tag;
  std::string   m_desc;
  bool          m_removeCondb;
  unsigned int  m_precision;
  unsigned int  m_depth;
};

DECLARE_COMPONENT( WriteUTStatusConditions )

//--------------------------------------------------------------------
//
//  WriteUTStatusConditions
//
//--------------------------------------------------------------------

WriteUTStatusConditions::WriteUTStatusConditions( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::AlgBase( name, pSvcLocator ) {
  // constructer
  declareProperty( "footer", m_footer = "</DDDB>" );
  declareProperty( "startTag", m_startTag = "<condition" );
  declareProperty( "outputFile", m_outputFileName = "ReadoutSectors.xml" );
  declareProperty( "depths", m_depth = 3u );
  declareProperty( "precision", m_precision = 16u );
  declareProperty( "removeCondb", m_removeCondb = false );
  declareProperty( "author", m_author = "Joe Bloggs" );
  declareProperty( "tag", m_tag = "None" );
  declareProperty( "description", m_desc = "BlahBlahBlah" );
}

StatusCode WriteUTStatusConditions::execute() {
  // execute once per event
  return StatusCode::SUCCESS;
}

StatusCode WriteUTStatusConditions::initialize() {
  StatusCode sc = UT::AlgBase::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to intialize" ); }

  info() << "detector   = UT" << endmsg;
  info() << "footer     = " << m_footer << endmsg;
  info() << "startTag   = " << m_startTag << endmsg;
  info() << "outputFile = " << m_outputFileName << endmsg;
  info() << "precision  = " << m_precision << endmsg;

  return sc;
}

StatusCode WriteUTStatusConditions::finalize() {
  info() << "Writing status conditions to file" << endmsg;
  // Print out the full tree
  info() << "Trying for top element " << tracker()->name() << endmsg;

  // get the sectors
  const DeUTDetector::Sectors& sectors = tracker()->sectors();

  std::ofstream outputFile( m_outputFileName.c_str() );
  if ( outputFile.fail() ) { return Warning( "Failed to open output file", StatusCode::FAILURE ); }

  // write the xml headers
  outputFile << header( sectors.front()->statusCondition()->toXml( "", true, m_precision ) ) << std::endl;

  for ( auto sector : sectors ) {
    const Condition* aCon = sector->statusCondition();
    std::string      temp = strip( aCon->toXml( "", false, m_precision ) );
    outputFile << temp << "\n" << std::endl;
  } // sectors

  // write the footer
  outputFile << footer() << std::endl;

  info() << "After update Active fraction is: " << tracker()->fractionActive() << endmsg;

  return UT::AlgBase::finalize();
}

std::string WriteUTStatusConditions::footer() const {
  std::string temp = m_footer;
  temp.insert( 0, "</catalog>" );
  return temp;
}

std::string WriteUTStatusConditions::header( const std::string& conString ) const {
  // get the header
  std::string::size_type startpos = conString.find( m_startTag );
  std::string            temp     = conString.substr( 0, startpos );
  temp.insert( startpos, "<catalog name=\"ReadoutSectors\">" );

  // correct the location of the DTD
  if ( m_removeCondb ) {
    UT::XMLUtils::replace( temp, "conddb:", "" );
    std::string location;
    for ( unsigned int i = 0; i < m_depth; ++i ) location += "../";
    std::string::size_type pos = temp.find( "/DTD/" );
    temp.insert( pos, location );
    UT::XMLUtils::replace( temp, "//", "/" );
  }

  return temp;
}

std::string WriteUTStatusConditions::strip( const std::string& conString ) const {
  std::string::size_type startpos = conString.find( m_startTag );
  std::string::size_type endpos   = conString.find( m_footer );
  return conString.substr( startpos, endpos - startpos );
}
