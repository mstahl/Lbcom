/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTSIDIGITSMERGE_H
#define UTSIDIGITSMERGE_H 1

/** @class UTSiDigitsMerge
 *
 *  simple class for adding digits
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "UTSiClusterTraits.h"

template <class TYPE>
class UTSiDigitsMerge : public UTSiClusterTraits<TYPE>::BASEALG {

public:
  /// Constructor
  UTSiDigitsMerge( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialize
  StatusCode initialize() override;

  /// IAlgorithm members
  StatusCode execute() override;

private:
  void updateCharge( typename UTSiClusterTraits<TYPE>::DIGIT* digit1, typename UTSiClusterTraits<TYPE>::DIGIT* digit2 );

  unsigned int m_nBits{};
  double       m_minValue{};
  double       m_maxValue{};

  std::vector<std::string> m_spillsVector;
  std::vector<std::string> m_spillsPath;
  std::string              m_inputLocation;
  std::string              m_outputLocation;
};

#endif // UTSiDigitsMerge
