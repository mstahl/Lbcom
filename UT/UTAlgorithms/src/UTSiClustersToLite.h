/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTSICLUSTERSTOLITE_H
#define UTSICLUSTERSTOLITE_H 1

/** @class UTSiClustersToLite
 *
 *  simple class for stripping liteCluster from clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Transformer.h"
#include "UTSiClusterTraits.h"

template <class TYPE>
using UTSiClusterBaseClassTraits = Gaudi::Functional::Traits::BaseClass_t<typename UTSiClusterTraits<TYPE>::BASEALG>;

template <class TYPE>
struct UTSiClustersToLite : Gaudi::Functional::Transformer<typename UTSiClusterTraits<TYPE>::LITECONT(
                                                               const typename UTSiClusterTraits<TYPE>::CLUSCONT& ),
                                                           UTSiClusterBaseClassTraits<TYPE>> {

  /// Constructor
  UTSiClustersToLite( const std::string& name, ISvcLocator* pSvcLocator );

  /// operator()
  typename UTSiClusterTraits<TYPE>::LITECONT
  operator()( const typename UTSiClusterTraits<TYPE>::CLUSCONT& ) const override;
};

#endif // UTSiClustersToLite
