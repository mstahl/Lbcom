/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTSICLUSTERSTODIGITS_H
#define UTSICLUSTERSTODIGITS_H 1

/** @class UTSiClustersToDigits
 *
 *  simple class for stripping digits from clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "UTSiClusterTraits.h"

template <class TYPE>
class UTSiClustersToDigits : public UTSiClusterTraits<TYPE>::BASEALG {

public:
  /// Constructor
  UTSiClustersToDigits( const std::string& name, ISvcLocator* pSvcLocator );

  /// IAlgorithm members
  StatusCode execute() override;

private:
  typename UTSiClusterTraits<TYPE>::DIGIT* createDigit( const unsigned int value );

  void addNeighbours( TYPE* cluster, typename UTSiClusterTraits<TYPE>::DIGIT::Container* digitCont );

  bool        m_addNeighbourInfo;
  std::string m_inputLocation;
  std::string m_outputLocation;
};

#endif // UTSiClustersToDigits
