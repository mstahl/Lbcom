/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTSICLUUTERTRAITS_H
#define UTSICLUUTERTRAITS_H 1

/** @class UTClusterTraits
 *
 *  traits for light clusters
 *  UT specialization
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

// UT includes
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "Event/UTLiteCluster.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/UTAlgBase.h"

template <typename TYPE>
class UTSiClusterTraits {
public:
  typedef TYPE           CLUSCONT;
  typedef TYPE           CLUUTYPE;
  typedef TYPE           LITECONT;
  typedef TYPE           DIGIT;
  typedef GaudiAlgorithm BASEALG;
};

// specialization for UT
template <>
class UTSiClusterTraits<LHCb::UTCluster> {
public:
  typedef LHCb::UTClusters                    CLUSCONT;
  typedef CLUSCONT::value_type                CLUUTYPE;
  typedef LHCb::UTLiteCluster::UTLiteClusters LITECONT;
  typedef LHCb::UTDigit                       DIGIT;
  typedef UT::AlgBase                         BASEALG;
};

#endif
