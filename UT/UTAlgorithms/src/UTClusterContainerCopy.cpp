/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTAlgBase.h"
#include "Kernel/UTChannelID.h"
#include <string>

/** @class UTClusterContainerCopy UTClusterContainerCopy.h
 *
 *  Class for UTClusterContainerCopy
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTClusterContainerCopy
    : public Gaudi::Functional::Transformer<LHCb::UTClusters( const LHCb::UTClusters& ),
                                            Gaudi::Functional::Traits::BaseClass_t<UT::AlgBase>> {

public:
  // Constructors and destructor
  UTClusterContainerCopy( const std::string& name, ISvcLocator* pSvcLocator );

  // IAlgorithm members
  LHCb::UTClusters operator()( const LHCb::UTClusters& clusterCont ) const override;
  StatusCode       finalize() override;

private:
  PublicToolHandle<IUTClusterSelector> m_clusterSelector{this, "Selector", "UTSelectClustersByChannel/UTSelector"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_selected{this, "fraction of clusters selected"};
};

using namespace LHCb;

DECLARE_COMPONENT( UTClusterContainerCopy )

UTClusterContainerCopy::UTClusterContainerCopy( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", UTClusterLocation::UTClusters},
                  {"OutputLocation", UTClusterLocation::UTClusters + "Selected"}} {}

UTClusters UTClusterContainerCopy::operator()( const UTClusters& clusterCont ) const {

  LHCb::UTClusters outputCont;
  auto             selected = m_selected.buffer();
  for ( const auto& iterC : clusterCont ) {
    const bool select = ( *m_clusterSelector )( iterC );
    selected += select;
    if ( select ) outputCont.insert( iterC->clone(), iterC->channelID() );
  }
  return outputCont;
}

StatusCode UTClusterContainerCopy::finalize() {
  info() << "Fraction of clusters selected " << 100 * m_selected.efficiency() << " %" << endmsg;
  return Transformer::finalize();
}
