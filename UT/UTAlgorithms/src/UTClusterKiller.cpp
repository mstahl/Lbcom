/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTAlgBase.h"
#include "Kernel/UTChannelID.h"

/** @class UTClusterKiller UTClusterKiller.h
 *
 *  Class for killing clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTClusterKiller : public UT::AlgBase {

public:
  // Constructor
  using UT::AlgBase::AlgBase;

  // IAlgorithm members
  StatusCode execute() override;
  StatusCode finalize() override;

private:
  Gaudi::Accumulators::BinomialCounter<> m_counter{this, "fraction of clusters killed"};

  // smart interface to generator
  PublicToolHandle<IUTClusterSelector>   m_clusterSelector{this, "Selector", "UTSelectClustersByChannel/UTKiller"};
  DataObjectReadHandle<LHCb::UTClusters> m_clusters{this, "InputLocation", LHCb::UTClusterLocation::UTClusters};
};

using namespace LHCb;

DECLARE_COMPONENT( UTClusterKiller )

StatusCode UTClusterKiller::execute() {
  UTClusters* clusterCont = m_clusters.get();
  // make list of clusters to remove
  std::vector<UTChannelID> chanList;
  chanList.reserve( 100 );
  auto buffer = m_counter.buffer();
  // select the cluster to remove
  for ( const auto& iterC : *clusterCont ) {
    const bool select = ( *m_clusterSelector )( iterC );
    buffer += select;
    if ( select ) chanList.push_back( iterC->key() );
  }
  // remove from the container
  for ( auto iterVec = chanList.rbegin(); iterVec != chanList.rend(); ++iterVec ) {
    clusterCont->erase( *iterVec );
  } // iterVec
  return StatusCode::SUCCESS;
}

StatusCode UTClusterKiller::finalize() {
  info() << "Fraction of clusters killed " << 100 * m_counter.efficiency() << " %" << endmsg;
  return UT::AlgBase::finalize();
}
