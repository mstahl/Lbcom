/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Linker/LinkerTool.h"
#include "UTDet/DeUTDetector.h"
#include <boost/assign/std/vector.hpp>

using namespace LHCb;

/** @class UTClusterClassification UTClusterClassification.h
 *
 *  This algorithm counts the total number of UTClusters from
 *  - a primary interaction
 *  - a secondary interaction
 *  - noise
 *  - spillover
 *  - unknown sources.
 *  It prints out the statistics in the finalize method.
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTClusterClassification : public UT::HistoAlgBase {

public:
  /// constructer
  using UT::HistoAlgBase::HistoAlgBase;

  /// execute
  StatusCode execute() override;

  /// finalize
  StatusCode finalize() override;

private:
  typedef LinkerTool<LHCb::UTCluster, LHCb::MCHit> AsctTool;
  using Table    = AsctTool::DirectType;
  using Range    = Table::Range;
  using iterator = Table::iterator;

  StatusCode  fillInfo( const LHCb::MCHit* aHit ) const;
  std::string findSpill( const LHCb::MCHit* aHit ) const;

  unsigned int tCount() const;

  std::vector<std::string>                  m_spillNames; // full name of spills
  Gaudi::Property<std::vector<std::string>> m_spillVector{
      this, "SpillVector", {"/", "/Prev/", "/PrevPrev/", "/Next/"}, [=]( auto& ) {
        // construct container names once
        // path in Transient data store
        m_spillNames.clear();
        std::transform( m_spillVector.begin(), m_spillVector.end(), std::back_inserter( m_spillNames ),
                        [&]( const auto& sn ) { return "/Event" + sn + m_hitLocation; } );
      }}; // short names of spills

  int m_nEvent = 0;

  DataObjectReadHandle<UTClusters> m_clusterLocation{this, "inputData", UTClusterLocation::UTClusters};
  Gaudi::Property<std::string>     m_asctLocation{this, "asctLocation", m_clusterLocation.objKey() + "2MCHits"};
  Gaudi::Property<std::string>     m_hitLocation{this, "hitLocation", MCHitLocation::UT};

  mutable std::map<std::string, unsigned int> m_infoMap;
};

DECLARE_COMPONENT( UTClusterClassification )

//--------------------------------------------------------------------
//
//  UTClusterClassification: ...
//
//--------------------------------------------------------------------

StatusCode UTClusterClassification::execute() {
  // retrieve clusters
  const UTClusters* clusterCont = m_clusterLocation.get();
  ++m_nEvent;

  // linker
  AsctTool     associator( evtSvc(), m_asctLocation );
  const Table* aTable = associator.direct();
  if ( !aTable ) return Error( "Failed to find table", StatusCode::FAILURE );

  // histos per digit
  for ( const auto& i : *clusterCont ) {
    Range range = aTable->relations( i );
    if ( range.empty() ) {
      ++m_infoMap["noise"];
    } else {
      this->fillInfo( range.begin()->to() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode UTClusterClassification::finalize() {
  // normalization
  unsigned     total      = tCount();
  unsigned int eventSpill = m_infoMap["primary"] + m_infoMap["secondary"] + m_infoMap["unknown"];
  unsigned int spillover  = total - eventSpill - m_infoMap["noise"];

  info() << "--- Cluster Classification " << endmsg;
  info() << "Event Spill " << eventSpill / double( total ) << endmsg;
  info() << "   |---> Primary " << m_infoMap["primary"] / double( total ) << endmsg;
  info() << "   |---> Secondary " << m_infoMap["secondary"] / double( total ) << endmsg;
  info() << "   |---> Unknown " << m_infoMap["unknown"] / double( total ) << endmsg;
  info() << "Noise " << m_infoMap["noise"] / double( total ) << " # per event " << m_infoMap["noise"] / m_nEvent
         << endmsg;
  info() << "Spillover " << spillover / double( total ) << endmsg;

  for ( const auto& i : m_infoMap ) {
    if ( ( i.first != "noise" ) && ( i.first != "primary" ) && ( i.first != "unknown" ) &&
         ( i.first != "secondary" ) ) {
      info() << i.first + " " << i.second / double( total ) << endmsg;
    }
  }
  info() << "----------------------------------------" << endmsg;

  return UT::HistoAlgBase::finalize();
}

unsigned int UTClusterClassification::tCount() const {
  return std::accumulate( m_infoMap.begin(), m_infoMap.end(), 0u,
                          []( unsigned int total, const auto& p ) { return total + p.second; } );
}

StatusCode UTClusterClassification::fillInfo( const MCHit* aHit ) const {
  // find out which spill came from....
  std::string spill = this->findSpill( aHit );

  if ( spill == "/" ) {
    // event spill...
    if ( const MCParticle* particle = aHit->mcParticle(); particle ) {
      if ( const MCVertex* origin = particle->originVertex(); origin ) {
        if ( ( origin->type() < MCVertex::MCVertexType::HadronicInteraction ) && ( origin->type() != 0 ) ) {
          ++m_infoMap["primary"];
        } else if ( origin->type() == 0 ) {
          ++m_infoMap["unknown"];
        } else {
          ++m_infoMap["secondary"];
        }
      } // vertex
    }   // particle
  } else {
    ++m_infoMap[spill];
  }
  return StatusCode::SUCCESS;
}

std::string UTClusterClassification::findSpill( const MCHit* aHit ) const {
  const IRegistry* reg = aHit->parent()->registry();
  auto             i   = std::find( m_spillNames.begin(), m_spillNames.end(), reg->identifier() );
  return i != m_spillNames.end() ? *i : "Unknown";
}
