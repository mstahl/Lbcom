/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// xml geometry
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "Kernel/LHCbConstants.h"
#include "Kernel/UTLexicalCaster.h"

// local
#include "UTOccupancy.h"

#include "Kernel/UTNames.h"

using namespace LHCb;

//--------------------------------------------------------------------
//
//  UTOccupancy : Monitor occupancies in Trigger Tracker
//
//--------------------------------------------------------------------

template <class PBASE>
UTOccupancy<PBASE>::UTOccupancy( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {}

template <class PBASE>
StatusCode UTOccupancy<PBASE>::initialize() {
  // Set the top directory to IT or UT.
  setHistoDir( m_histoLocation );
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );

  // Initialize GaudiHistoAlg
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_nBins = m_hMax / m_binSize;

  return sc;
}

template <class PBASE>
StatusCode UTOccupancy<PBASE>::execute() {
  // retrieve Digitizations
  const auto* objCont = m_dataLocation.get();

  unsigned int nClus = 0;

  std::map<unsigned int, unsigned int> SectorMap;
  std::map<std::string, unsigned int>  BeetleMap;
  std::map<std::string, unsigned int>  PortMap;

  // histos per digit
  auto iterObj = objCont->begin();
  for ( ; iterObj != objCont->end(); ++iterObj ) {
    const double signalToNoise = SN( *iterObj );
    if ( signalToNoise < m_threshold[( *iterObj )->station() - tracker()->firstStation()] ) continue;

    ++nClus;

    this->fillHistograms( *iterObj );
    SectorMap[( *iterObj )->channelID().uniqueSector()] += weight( *iterObj );
    BeetleMap[uniqueBeetle( ( *iterObj )->channelID() )] += weight( *iterObj );
    PortMap[uniquePort( ( *iterObj )->channelID() )] += weight( *iterObj );
  } // loop iterDigit

  // fill histogram of sector occupancy
  for ( auto iterS = SectorMap.begin(); iterS != SectorMap.end(); ++iterS ) {
    double occ = iterS->second / double( tracker()->sectors().front()->nStrip() );
    plot( occ, "module occupancy", -0.005, 1.005, 101 );
  } // iter

  // fill histogram of beetle occupancy
  for ( auto iterS = BeetleMap.begin(); iterS != BeetleMap.end(); ++iterS ) {
    double occ = iterS->second / double( LHCbConstants::nStripsInBeetle );
    plot( occ, "beetle occupancy", -0.005, 1.005, 101 );
  } // iter

  // fill histogram of port occupancy
  for ( auto iterP = PortMap.begin(); iterP != PortMap.end(); ++iterP ) {
    const double occ = iterP->second / double( LHCbConstants::nStripsInPort );
    plot( occ, "port occupancy", -0.005, 1.005, 101 );
  } // iter

  plot( nClus, "nClus", 0., 2000., 500 );

  return StatusCode::SUCCESS;
}

template <class PBASE>
void UTOccupancy<PBASE>::fillHistograms( const PBASE* obj ) {

  const UTChannelID aChan = obj->channelID();

  const DeUTSector* utSector = tracker()->findSector( aChan );

  const unsigned int nstrips = utSector->nStrip();

  plot( aChan.station(), "station", -0.5, 5.5, 6 );

  int offset;

  if ( aChan.detRegion() == UTNames::detRegion::RegionC ) {
    offset = utSector->column() - 1;
  } else if ( aChan.detRegion() == UTNames::detRegion::RegionB ) {
    offset = utSector->column() - 7;
  } else if ( aChan.detRegion() == UTNames::detRegion::RegionA ) {
    aChan.station() == UTNames::Station::UTa ? offset = utSector->column() - 10 : offset = utSector->column() - 12;
  } else {
    warning() << "Unknown region " << aChan.detRegion() << endmsg;
    return;
  }

  unsigned int row = utSector->row();

  std::string rowString = UT::toString( row );
  plot( (double)aChan.strip() - 1. + ( nstrips * offset ), "N_" + uniqueDetRegion( aChan ) + "_Row_" + rowString, 0.,
        m_hMax, m_nBins );
}

template <class PBASE>
unsigned int UTOccupancy<PBASE>::weight( const PBASE* ) const {
  return 1;
}

template <class PBASE>
double UTOccupancy<PBASE>::SN( const PBASE* obj ) const {
  const DeUTSector* sector = findSector( obj->channelID() );
  return obj->depositedCharge() / sector->noise( obj->channelID() );
}
