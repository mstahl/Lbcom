/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/SiLandauFun.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Linker/LinkerTool.h"
#include "MCInterfaces/IMCParticleSelector.h"
#include "UTDet/DeUTSector.h"

using namespace LHCb;

/** @class UTClusterChecker UTClusterChecker.h
 *
 *  Checking class to plot S/N and charge of UTClusters for each readout sector.
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTClusterChecker : public UT::HistoAlgBase {

public:
  /// constructer
  UTClusterChecker( const std::string& name, ISvcLocator* svcloc );

  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

private:
  typedef LinkerTool<LHCb::UTCluster, LHCb::MCHit> AsctTool;
  using Table    = AsctTool::DirectType;
  using Range    = Table::Range;
  using iterator = Table::iterator;

  void                             fillHistograms( const LHCb::UTCluster& aCluster, const LHCb::MCHit* aHit );
  DataObjectReadHandle<UTClusters> m_clusterLocation{this, "clusterLocation", UTClusterLocation::UTClusters};
  Gaudi::Property<std::string>     m_asctLocation{this, "asctLocation", m_clusterLocation.objKey() + "2MCHits"};

  // selector
  ToolHandle<IMCParticleSelector> m_selector{this, "Selector", "MCParticleSelector"};

  double m_betaMin{};
  double m_gammaMin{};
};

DECLARE_COMPONENT( UTClusterChecker )

//--------------------------------------------------------------------
//
//  UTClusterChecker: Plots S/N and charge of UTClusters for each readout sector
//
//--------------------------------------------------------------------

UTClusterChecker::UTClusterChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {}

StatusCode UTClusterChecker::initialize() {
  // intialize
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  auto                          pid      = LHCb::ParticleID( 211 );
  auto                          pp       = service<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  const LHCb::ParticleProperty* partProp = pp->find( pid );
  const double                  mPion    = partProp->mass();

  const double betaGammaMin = 4.8;
  const double P            = betaGammaMin * mPion;
  const double E            = sqrt( P * P + mPion * mPion );
  m_betaMin                 = P / E;
  m_gammaMin                = E / mPion;

  return StatusCode::SUCCESS;
}

StatusCode UTClusterChecker::execute() {
  // retrieve clusters
  const UTClusters* clusterCont = m_clusterLocation.get();

  // linker
  AsctTool     associator( evtSvc(), m_asctLocation );
  const Table* aTable = associator.direct();
  if ( !aTable ) return Error( "Failed to find table", StatusCode::FAILURE );

  // histos per cluster
  for ( const auto& clus : *clusterCont ) {
    // get MC truth for this cluster
    if ( !clus ) continue;
    Range range = aTable->relations( clus );
    if ( range.empty() ) continue;
    fillHistograms( *clus, range.front().to() );
  } // loop clus
  return StatusCode::SUCCESS;
}

void UTClusterChecker::fillHistograms( const UTCluster& aCluster, const MCHit* aHit ) {
  // fill histograms
  if ( aHit && m_selector->accept( aHit->mcParticle() ) ) {
    // histo cluster size for physics tracks

    std::string dType = detectorType( aCluster.channelID() );
    plot( aCluster.totalCharge(), dType + "/1", "Charge", 0., 200., 200 );
    const DeUTSector* sector        = findSector( aCluster.channelID() );
    const double      signalToNoise = aCluster.totalCharge() / sector->noise( aCluster.channelID() );
    plot( signalToNoise, dType + "/2", "S/N", 0., 100., 100 );

    double mpv  = SiLandauFun::MPV( aHit->mcParticle()->beta(), aHit->mcParticle()->gamma(), aHit->pathLength() );
    double norm = SiLandauFun::MPV( m_betaMin, m_gammaMin, sector->thickness() );
    plot( signalToNoise * norm / mpv, dType + "/3", "S/N (scaled)", 0., 100., 100 );

  } // aHit && accepted
}
