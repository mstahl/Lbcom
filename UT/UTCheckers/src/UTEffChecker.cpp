/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "HistFun.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTCommonBase.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTLexicalCaster.h"
#include "Linker/LinkerTool.h"
#include "MCInterfaces/IMCParticleSelector.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTLayer.h"

using namespace LHCb;

/** @class UTEffChecker UTEffChecker.h
 *
 *  Class for checking UT efficiencies. It produces the following plots:
 *  - x and y distributions of all MCHits in a UT layer.
 *  - x vs y distribution of all MCHits in a UT layer.
 *  - x and y distributions of MCHits which make an UTCluster.
 *  - x vs y distribution of MCHits which make an UTCluster.
 *  By dividing these histograms one gets the efficiency per layer.
 *
 *  In the finalize method, a summary of the efficiency per layer is printed.
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTEffChecker : public UT::CommonBase<GaudiHistoAlg> {

public:
  /// constructor
  UTEffChecker( const std::string& name, ISvcLocator* svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

  /// finalize
  StatusCode finalize() override;

private:
  typedef LinkerTool<LHCb::UTCluster, LHCb::MCHit> AsctTool;

  virtual void initHistograms();

  virtual void layerEff( const LHCb::MCParticle* aParticle );

  bool isInside( const DeUTLayer* aLayer, const LHCb::MCHit* aHit ) const;

  int findHistoId( unsigned int aLayerId );
  int uniqueHistoID( const LHCb::UTChannelID aChannel ) const;

  void unBookHistos();
  void eraseHistos();

  AsctTool::InverseType* m_table{nullptr};

  Gaudi::Property<std::string> m_asctLocation{this, "asctLocation", UTClusterLocation::UTClusters + "2MCHits"};

  // pointer to p to hit associaton
  using HitTable = LinkerTool<LHCb::MCParticle, LHCb::MCHit>;
  const HitTable::DirectType* m_hitTable{nullptr};

  Gaudi::Property<bool>           m_includeGuardRings{this, "IncludeGuardRings", false};
  Gaudi::Property<bool>           m_pEff{this, "PrintEfficiency", true};
  ToolHandle<IMCParticleSelector> m_selector{this, "MCParticleSelector", "MCParticleSelector"};

  Gaudi::Property<std::string> m_hitTableLocation{this, "hitTableLocation",
                                                  LHCb::MCParticleLocation::Default + "2MCUTHits"};

  // mapping
  std::map<unsigned int, int> m_mapping;

  std::vector<AIDA::IHistogram1D*> m_xLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_yLayerHistos;
  std::vector<AIDA::IHistogram2D*> m_xyLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_effXLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_effYLayerHistos;
  std::vector<AIDA::IHistogram2D*> m_effXYLayerHistos;

  DataObjectReadHandle<MCParticles> m_mcParts{MCParticleLocation::Default, this};
};

DECLARE_COMPONENT( UTEffChecker )

//--------------------------------------------------------------------
//
//  UTEffChecker: Plot the UT efficiency
//
//--------------------------------------------------------------------
UTEffChecker::UTEffChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : CommonBase<GaudiHistoAlg>( name, pSvcLocator ) {
  setProperty( "HistoPrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

StatusCode UTEffChecker::initialize() {
  return CommonBase::initialize().andThen( [&] {
    if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
    // init histos
    initHistograms();
  } );
}

void UTEffChecker::initHistograms() {
  std::string tDirPath    = histoPath() + "/";
  int         numInVector = 0;
  int         histID;

  // Intialize histograms
  for ( const auto& layer : tracker()->layers() ) {

    // uniquely id using station and layer
    // add to map
    UTChannelID  aChan    = layer->elementID();
    unsigned int uniqueID = uniqueHistoID( aChan );
    m_mapping[uniqueID]   = numInVector;

    // Create layer name
    std::string layerName = ", layer " + UT::toString( (int)uniqueID );

    // Book x distribution histogram MCHits
    histID                    = (int)uniqueID + 5000;
    IHistogram1D* xLayerHisto = book1D( histID, "x distribution hits" + layerName, -1000., 1000., 400 );
    m_xLayerHistos.push_back( xLayerHisto );

    // Book y distribution histogram MCHits
    histID                    = (int)uniqueID + 6000;
    IHistogram1D* yLayerHisto = book1D( histID, "y distribution hits" + layerName, -1000., 1000., 400 );
    m_yLayerHistos.push_back( yLayerHisto );

    // Book x distribution histogram UTClusters
    histID                       = (int)uniqueID + 7000;
    IHistogram1D* effXLayerHisto = book1D( histID, "x distribution ass. clusters" + layerName, -1000., 1000., 400 );
    m_effXLayerHistos.push_back( effXLayerHisto );

    // Book y distribution histogram UTClusters
    histID                       = (int)uniqueID + 8000;
    IHistogram1D* effYLayerHisto = book1D( histID, "y distribution ass. clusters" + layerName, -1000., 1000., 400 );
    m_effYLayerHistos.push_back( effYLayerHisto );

    // Book x vs y distribution histogram MCHits
    histID = (int)uniqueID + 9000;
    IHistogram2D* xyLayerHisto =
        book2D( histID, "x vs y distribution hits" + layerName, -1000., 1000., 200, -1000., 1000., 200 );
    m_xyLayerHistos.push_back( xyLayerHisto );

    // Book x vs y distribution histogram UTClusters
    histID = (int)uniqueID + 10000;
    IHistogram2D* effXYLayerHisto =
        book2D( histID, "x vs y distribution ass. clusters" + layerName, -1000., 1000., 200, -1000., 1000., 200 );
    m_effXYLayerHistos.push_back( effXYLayerHisto );

    ++numInVector;
  } // iLayer
}

StatusCode UTEffChecker::execute() {
  // Get the MCParticles
  const MCParticles* particles = m_mcParts.get();

  // Get the UTCluster to MCHit associator
  AsctTool associator( evtSvc(), m_asctLocation );
  m_table = associator.inverse();
  if ( !m_table ) return Error( "Failed to find table", StatusCode::FAILURE );

  // Get the MCParticle to MCHit associator
  HitTable hitAsct( evtSvc(), m_hitTableLocation );
  m_hitTable = hitAsct.direct();
  if ( !m_hitTable ) return Error( "Failed to find hit table at " + m_hitTableLocation, StatusCode::FAILURE );

  for ( const auto& particle : *particles ) {
    if ( m_selector->accept( particle ) ) layerEff( particle );
  }

  return StatusCode::SUCCESS;
}

StatusCode UTEffChecker::finalize() {
  // init the message service
  if ( m_pEff ) {
    info() << " -------Efficiency %--------- " << endmsg;
  } else {
    info() << " -------InEfficiency %--------- " << endmsg;
  }

  // print out efficiencys
  for ( const auto& layer : tracker()->layers() ) {

    UTChannelID aChan    = layer->elementID();
    int         iHistoId = findHistoId( uniqueHistoID( aChan ) );

    // eff for this layer
    int    nAcc   = m_xLayerHistos[iHistoId]->allEntries();
    int    nFound = m_effXLayerHistos[iHistoId]->allEntries();
    double eff    = 9999.;
    double err    = 9999.;
    if ( nAcc > 0 ) {
      eff = 100.0 * (double)nFound / (double)nAcc;
      err = sqrt( ( eff * ( 100.0 - eff ) ) / (double)nAcc );
      if ( !m_pEff ) eff = 1 - eff;
    }

    info() << uniqueLayer( aChan ) << " eff " << eff << " +/- " << err << endmsg;

  } // layer

  info() << " -----------------------" << endmsg;

  // hack to prevent crash
  StatusCode sc = CommonBase::finalize();
  if ( sc.isFailure() ) return sc;

  // unbook if not full detail mode: histograms are not saved
  if ( !fullDetail() ) {
    unBookHistos();
    eraseHistos();
  }

  return sc;
}

void UTEffChecker::layerEff( const MCParticle* aParticle ) {
  // find all MC hits for this particle
  auto hits = m_hitTable->relations( aParticle );
  if ( hits.empty() ) return;

  for ( const auto& layer : tracker()->layers() ) {

    // look for MCHit in this layer.....

    std::vector<const MCHit*> layerHits;
    for ( const auto& link : hits ) {
      auto aHit    = link.to();
      auto hitChan = UTChannelID( aHit->sensDetID() );
      if ( uniqueHistoID( hitChan ) == uniqueHistoID( layer->elementID() ) && isInside( layer, aHit ) ) {
        layerHits.push_back( aHit );
      }
    };

    if ( !layerHits.empty() ) {
      bool found = std::any_of( layerHits.begin(), layerHits.end(),
                                [&]( const MCHit* h ) { return !m_table->relations( h ).empty(); } );

      int                   iHistoId = findHistoId( uniqueHistoID( layer->elementID() ) );
      const MCHit*          aHit     = layerHits.front();
      const Gaudi::XYZPoint midPoint = aHit->midPoint();

      // histo vs x
      m_xLayerHistos[iHistoId]->fill( midPoint.x() );
      // histo vs y
      m_yLayerHistos[iHistoId]->fill( midPoint.y() );
      //  xy
      m_xyLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y() );

      if ( found ) {
        if ( m_pEff ) {
          m_effXYLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y() );
          m_effXLayerHistos[iHistoId]->fill( midPoint.x() );
          m_effYLayerHistos[iHistoId]->fill( midPoint.y() );
        }
      } else {
        if ( !m_pEff ) {
          m_effXYLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y() );
          m_effXLayerHistos[iHistoId]->fill( midPoint.x() );
          m_effYLayerHistos[iHistoId]->fill( midPoint.y() );
        }
      }
    } // if
  }   // layer
}

int UTEffChecker::findHistoId( unsigned int aLayerId ) { return m_mapping[aLayerId]; }

int UTEffChecker::uniqueHistoID( const UTChannelID aChan ) const { return aChan.station() * 100 + aChan.layer(); }

bool UTEffChecker::isInside( const DeUTLayer* aLayer, const MCHit* aHit ) const {
  // check if expect hit to make cluster
  if ( !aLayer->isInside( aHit->midPoint() ) ) return false;
  if ( m_includeGuardRings ) return true;
  const DeUTSector* aSector = tracker()->findSector( aHit->midPoint() );
  return aSector && aSector->globalInActive( aHit->midPoint() );
}

void UTEffChecker::unBookHistos() {

  // give ownership back to vector - histos no longer in store
  HistFun::unBookVector( m_xLayerHistos, histoSvc() );
  HistFun::unBookVector( m_yLayerHistos, histoSvc() );
  HistFun::unBookVector( m_xyLayerHistos, histoSvc() );
  HistFun::unBookVector( m_effXLayerHistos, histoSvc() );
  HistFun::unBookVector( m_effYLayerHistos, histoSvc() );
  HistFun::unBookVector( m_effXYLayerHistos, histoSvc() );
}

void UTEffChecker::eraseHistos() {

  // clear everything
  HistFun::eraseVector( m_xLayerHistos );
  HistFun::eraseVector( m_yLayerHistos );
  HistFun::eraseVector( m_xyLayerHistos );
  HistFun::eraseVector( m_effXLayerHistos );
  HistFun::eraseVector( m_effYLayerHistos );
  HistFun::eraseVector( m_effXYLayerHistos );
}
