/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTSummary.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/SiChargeFun.h"
#include "Kernel/UTDetectorPlot.h"
#include "Kernel/UTHistoAlgBase.h"
#include "Kernel/UTTell1ID.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <string>

using namespace LHCb;

/** @class UTPerformanceMonitor UTPerformanceMonitor.h
 *
 *  Class for monitoring UTPerformances
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTPerformanceMonitor
    : public Gaudi::Functional::Consumer<void( const UTSummary&, const UTClusters& ),
                                         Gaudi::Functional::Traits::BaseClass_t<UT::HistoAlgBase>> {

public:
  /// constructer
  UTPerformanceMonitor( const std::string& name, ISvcLocator* svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  void operator()( const UTSummary&, const UTClusters& ) const override;

private:
  void getDeadSectors( std::vector<const DeUTSector*>& deadSectors, const std::vector<unsigned int>& banks ) const;

  mutable std::atomic<unsigned int> m_event{0};
  Gaudi::Property<unsigned int>     m_expectedEvents{this, "ExpectedEvents", 100000};
};

DECLARE_COMPONENT( UTPerformanceMonitor )

//--------------------------------------------------------------------
//
//  UTPerformanceMonitor : Monitor the UTPerformances
//
//--------------------------------------------------------------------

UTPerformanceMonitor::UTPerformanceMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"summaryLocation", UTSummaryLocation::UTSummary},
                KeyValue{"InputData", UTClusterLocation::UTClusters}}} {}

StatusCode UTPerformanceMonitor::initialize() {
  // Set the top directory to UT.
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );

  // Initialize GaudiHistoAlg
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  // get the disabled sectors
  const auto& disabledSec = tracker()->disabledSectors();
  if ( disabledSec.empty() ) {
    info() << "All sectors enabled " << endmsg;
  } else {
    for ( const auto& i : disabledSec ) { info() << "disabled " << i->nickname() << endmsg; } // for
  }

  // get the disabled beetles
  const auto& disabledB = tracker()->disabledBeetles();
  if ( disabledB.empty() ) {
    info() << "All beetles enabled " << endmsg;
  } else {
    for ( const auto& iterB : disabledB ) { info() << "disabled " << uniqueBeetle( iterB ) << endmsg; }
  }

  // active fraction
  info() << "fraction active " << tracker()->fractionActive() << endmsg;

  UT::UTDetectorPlot noiseProp( "noise", "Noise fraction" );
  UT::UTDetectorPlot activeProp( "fracActive", "Active fraction" );
  for ( const auto& iterSector : tracker()->sectors() ) {
    // 2-D plot what is active in the detector element
    const auto&              ttSector = dynamic_cast<const DeUTSector&>( *iterSector );
    UT::UTDetectorPlot::Bins bins     = activeProp.toBins( &ttSector );
    for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) {
      plot2D( bins.xBin, yBin, activeProp.name(), activeProp.title(), activeProp.minBinX(), activeProp.maxBinX(),
              activeProp.minBinY(), activeProp.maxBinY(), activeProp.nBinX(), activeProp.nBinY(),
              iterSector->fractionActive() );
      plot2D( bins.xBin, yBin, noiseProp.name(), noiseProp.title(), noiseProp.minBinX(), noiseProp.maxBinX(),
              noiseProp.minBinY(), noiseProp.maxBinY(), noiseProp.nBinX(), noiseProp.nBinY(),
              iterSector->sectorNoise() );
    } // loop bin Y
  }   // loop sectors

  return StatusCode::SUCCESS;
}

void UTPerformanceMonitor::operator()( const UTSummary& summary, const UTClusters& clusterCont ) const {

  auto event = ++m_event;

  // get list of missing sectors
  std::vector<const DeUTSector*> deadSectors;

  // some are lost because of errors in decoding
  getDeadSectors( deadSectors, summary.corruptedBanks() );

  // some were lost [ie tell1 not readout]
  getDeadSectors( deadSectors, summary.missingBanks() );

  // then all we have to do is loop over the sectors + count
  double      frac    = 0.0;
  const auto& sectors = tracker()->sectors();
  for ( const auto& s : sectors ) {
    if ( std::find( deadSectors.begin(), deadSectors.end(), s ) != deadSectors.end() ) frac += s->fractionActive();
  }
  frac /= sectors.size();
  plot( frac, 1, "active fraction", 0., 1., 200 ); // plot active fraction

  plot2D( event, frac, 11, "active fraction versus time", 0., m_expectedEvents.value(), 0., 1., 50, 200 );

  // get the occupancy
  const double occ = clusterCont.size() / ( tracker()->nStrip() * frac );
  plot( occ, 2, "occupancy", 0., 1., 200 );
  plot2D( event, occ, 12, "occ versus time", 0., m_expectedEvents.value(), 0., 0.1, 50, 200 );

  // get the modal charge
  double shorth = SiChargeFun::shorth( clusterCont.begin(), clusterCont.end() );
  plot( shorth, "shorth", 0., 100., 200 );

  // get the modal charge
  double tm = SiChargeFun::truncatedMean( clusterCont.begin(), clusterCont.end() );
  plot( tm, "tm", 0., 100., 200 );
  plot2D( event, tm, 13, "tms versus time", 0., m_expectedEvents.value(), 0., 100, 200, 200 );

  double lms = SiChargeFun::LMS( clusterCont.begin(), clusterCont.end() );
  plot( lms, "lms", 0., 100., 200 );

  double gm = SiChargeFun::generalizedMean( clusterCont.begin(), clusterCont.end() );
  plot( gm, "gm", 0., 100., 200 );
}

void UTPerformanceMonitor::getDeadSectors( std::vector<const DeUTSector*>&  deadSectors,
                                           const std::vector<unsigned int>& banks ) const {
  for ( const auto& i : banks ) {
    const auto& sectors = readoutTool()->sectors( UTTell1ID( i ) );
    deadSectors.insert( deadSectors.begin(), sectors.begin(), sectors.end() );
  }
}
