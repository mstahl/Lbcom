/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "UTDet/DeUTSector.h"
#include "UTOccupancy.h"
#include "UTOccupancy.icpp"

using UTClusterOccupancy = UTOccupancy<LHCb::UTCluster>;

template <>
const std::string& UTClusterOccupancy::dataLocation() const {
  return LHCb::UTClusterLocation::UTClusters;
}

template <>
const std::string UTClusterOccupancy::histoDirName() const {
  return "UTClusterOccupancy";
}

template <>
double UTClusterOccupancy::defaultThreshold() const {
  return 0.0; // no threshold for clusters
}

template <>
unsigned int UTClusterOccupancy::weight( const LHCb::UTCluster* obj ) const {
  return obj->size();
}

template <>
double UTClusterOccupancy::SN( const LHCb::UTCluster* obj ) const {
  const DeUTSector* sector = findSector( obj->channelID() );
  return obj->totalCharge() / sector->noise( obj->channelID() );
}

// template class UTOccupancy<LHCb::UTCluster>;

DECLARE_COMPONENT_WITH_ID( UTClusterOccupancy, "UTClusterOccupancy" )
