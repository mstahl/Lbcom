/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTOccupancy_H
#define UTOccupancy_H 1

#include "Kernel/UTHistoAlgBase.h"

/** @class UTOccupancy UTOccupancy.h
 *
 *  Class for plotting the occupancy of UTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

template <class PBASE>
class UTOccupancy : public UT::HistoAlgBase {

public:
  /// constructer
  UTOccupancy( const std::string& name, ISvcLocator* svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

private:
  const std::string& dataLocation() const;

  const std::string histoDirName() const;

  double defaultThreshold() const;

  void fillHistograms( const PBASE* obj );

  unsigned int weight( const PBASE* obj ) const;

  double SN( const PBASE* obj ) const;

  int                  m_nBins;
  static constexpr int m_hMax = 512 * 8;

  // job options
  Gaudi::Property<std::vector<double>> m_threshold{
      this, "Threshold", {defaultThreshold(), defaultThreshold()}};               ///< List of threshold values
  Gaudi::Property<int>                            m_binSize{this, "BinSize", 32}; ///< Number of channels in each bin
  DataObjectReadHandle<typename PBASE::Container> m_dataLocation{this, "DataLocation",
                                                                 dataLocation()}; ///< Location of the digits
  Gaudi::Property<std::string>                    m_histoLocation{this, "HistoLocation", histoDirName()};
};

#endif // UTOccupancy_H
