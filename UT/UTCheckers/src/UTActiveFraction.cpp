/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "Kernel/UTHistoAlgBase.h"
#include "TGraph.h"
#include "TH2D.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : UTActiveFraction
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTActiveFraction UTActiveFraction.h
 *
 *  Class to work out the active fraction of the detector from the
 *  conditions database as a function of time.  This makes use of the
 *  FakeEventTime tool in the EventClockSvc.  The start and step time
 *  used in the tool are required inputs for this algorithm.
 *  The input options require a start time and step size given in
 *  ns since the epoch and the total number of steps required
 *
 *  An example python scripts is available in options.
 *  Usage: gaudirun.py $UTCHECKERSROOT/options/runUTActiveTrend.py
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */
namespace UT {
  class UTActiveFraction : public HistoAlgBase {
  public:
    /// Standard constructor
    UTActiveFraction( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    unsigned int                  m_event = 0;
    Gaudi::Property<unsigned int> m_nSteps{this, "Steps", 0};
    Gaudi::Property<long long>    m_startTime{this, "StartTime", 0};
    Gaudi::Property<long long>    m_timeStep{this, "TimeStep", 0};
    std::vector<double>           m_time;
    std::vector<double>           m_active;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTActiveFraction )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTActiveFraction::UTActiveFraction( const std::string& name, ISvcLocator* pSvcLocator )
    : UT::HistoAlgBase( name, pSvcLocator ) {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTActiveFraction::initialize() {

  StatusCode sc = UT::HistoAlgBase::initialize();
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode UT::UTActiveFraction::execute() {

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "==> Execute" << endmsg;
    debug() << m_event << endmsg;

    // get the disabled sectors
    const auto& disabledSec = tracker()->disabledSectors();
    if ( disabledSec.empty() ) {
      debug() << "All sectors enabled " << endmsg;
    } else {
      for ( const auto& ds : disabledSec ) debug() << "disabled " << ds->nickname() << endmsg;
    }

    // get the disabled beetles
    const auto& disabledB = tracker()->disabledBeetles();
    if ( disabledB.empty() ) {
      debug() << "All beetles enabled " << endmsg;
    } else {
      for ( const auto& db : disabledB ) debug() << "disabled " << uniqueBeetle( db ) << endmsg;
    }
  } // MSG::DEBUG

  // active fraction
  info() << m_event << " fraction active " << tracker()->fractionActive() << " @ "
         << ( m_startTime + m_event * m_timeStep ) / 1000000000 << endmsg;
  m_time.push_back( ( m_startTime + m_event * m_timeStep ) / 1000000000 );
  m_active.push_back( tracker()->fractionActive() * 100. );

  ++m_event;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode UT::UTActiveFraction::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;
  TH2D* h = new TH2D( "FrameUT", "", 100, m_time[0], m_time.back(), 10, 95., 100. );
  h->GetXaxis()->SetTimeDisplay( 1 );
  h->GetXaxis()->SetTimeOffset( -m_time[0] );
  h->GetYaxis()->SetTitle( "Fraction active (%)" );
  auto* g = new TGraph( m_time.size(), &m_time[0], &m_active[0] );
  g->SetName( "FractionVsTimeUT" );
  g->Write();
  return UT::HistoAlgBase::finalize(); // must be called after all other actions
}

//=============================================================================
