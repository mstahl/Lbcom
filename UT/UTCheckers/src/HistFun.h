/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HISTFUN_H
#define HISTFUN_H 1

// Include files
#include "GaudiKernel/IHistogramSvc.h"
#include <vector>
// all the fun you can want with histograms !

namespace HistFun {

  template <typename TYPE>
  inline void unBookVector( std::vector<TYPE>& histoVector, IHistogramSvc* hSvc ) {
    // helper for unbooking histos from store
    std::for_each( histoVector.begin(), histoVector.end(),
                   [&]( const TYPE& t ) { hSvc->unregisterObject( t ).ignore(); } );
  }

  template <typename TYPE>
  inline void eraseVector( std::vector<TYPE>& histoVector ) {
    // helper for DELETING histos from vector
    std::for_each( histoVector.begin(), histoVector.end(), []( TYPE& h ) { delete h; } );
    histoVector.clear();
  }

} // namespace HistFun

#endif // HISTFUN_H
