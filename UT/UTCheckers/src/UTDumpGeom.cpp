/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/UTAlgBase.h"
#include "UTDet/DeUTDetector.h"

/** @class UTDumpGeom UTDumpGeom.h
 *
 *  Class for printing out the full geometry tree
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTDumpGeom : public UT::AlgBase {

public:
  /// constructer
  UTDumpGeom( const std::string& name, ISvcLocator* svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

private:
  /// Recursively print out the tree of the child DetectorElements
  void children( std::string indent, const DetectorElement* parent );

  // job options
  Gaudi::Property<bool> m_fullDetail{this, "FullDetail", false, "Print out the details of each DetectorElement"};
};

DECLARE_COMPONENT( UTDumpGeom )

//--------------------------------------------------------------------
//
//  UTDumpGeom : Prints out the full geometry tree
//
//--------------------------------------------------------------------

UTDumpGeom::UTDumpGeom( const std::string& name, ISvcLocator* pSvcLocator ) : UT::AlgBase( name, pSvcLocator ) {}

StatusCode UTDumpGeom::initialize() {
  // initialize
  return UT::AlgBase::initialize().andThen( [&] {
    // Print out the full tree
    info() << "Printing out the full geometry tree of\n" << tracker()->name() << endmsg;
    children( "", tracker() );
  } );
}

StatusCode UTDumpGeom::execute() {
  // execute once per event
  return StatusCode::SUCCESS;
}

void UTDumpGeom::children( std::string indent, const DetectorElement* parent ) {
  if ( m_fullDetail.value() ) {
    // Print the full details of this DetectorElement
    info() << parent << endmsg;
  } else {
    // Print out only the DetectorElement's name
    info() << indent << parent->DataObject::name() << endmsg;
  }

  indent += "  ";
  for ( auto iChild = parent->childBegin(); iChild != parent->childEnd(); ++iChild ) {
    children( indent, dynamic_cast<const DetectorElement*>( *iChild ) );
  }
}
