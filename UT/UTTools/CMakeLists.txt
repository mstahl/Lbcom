###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: UTTools
################################################################################
gaudi_subdir(UTTools v1r0)

gaudi_depends_on_subdirs(Det/UTDet
                         Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiKernel
                         Kernel/LHCbKernel
                         UT/UTKernel
                         UT/UTTELL1Event)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

find_package(AIDA)

gaudi_add_module(UTTools
                 src/*.cpp
                 INCLUDE_DIRS AIDA Event/DigiEvent
                 LINK_LIBRARIES UTDetLib LinkerEvent MCEvent TrackEvent GaudiAlgLib GaudiKernel LHCbKernel UTKernelLib UTTELL1Event)

gaudi_install_python_modules()

