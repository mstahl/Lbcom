/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include <string>

// ============================================================================
/** @file
 *
 *  Implementation file for class : UTChannelIDSelectorAND
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date 2018-09-04
 */
// ============================================================================

/** @class UTChannelIDSelectorAND UTChannelIDSelectorAND.h
 *
 *  Helper concrete tool for selection of stcluster objects
 *  This selector selects the cluster if
 *  all of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needhams)
 *  @date   2018-09-04
 */
class UTChannelIDSelectorAND : public extends<GaudiTool, IUTChannelIDSelector> {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTChannelIDSelector*>;

public:
  /** Standard constructor
   */
  using extends::extends;

  /** "select"/"preselect" method
   *  @see IUTChannelIDSelector
   *  @param  cluster pointer to st cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloUTChannelIDSelector
   *  @param  cluster pointer to st cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Gaudi::Property<Names> m_selectorsTypeNames{this, "SelectorTools", {}};
  Selectors              m_selectors;
};

DECLARE_COMPONENT( UTChannelIDSelectorAND )

// ============================================================================
/** standard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode UTChannelIDSelectorAND::initialize() {
  return extends::initialize().andThen( [&] {
    // locate selectors
    for ( const auto& s : m_selectorsTypeNames ) m_selectors.push_back( tool<IUTChannelIDSelector>( s ) );
  } );
}
// ============================================================================

// ============================================================================
/** "select"/"preselect" method
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTChannelIDSelectorAND::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }
// ============================================================================

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTChannelIDSelectorAND::operator()( const LHCb::UTChannelID& id ) const {
  return !m_selectors.empty() &&
         std::all_of( m_selectors.begin(), m_selectors.end(), [&]( const auto& s ) { return ( *s )( id ); } );
}
// ============================================================================
