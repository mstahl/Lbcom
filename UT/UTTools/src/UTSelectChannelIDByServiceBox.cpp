/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"
#include <algorithm>
#include <vector>

/** @class UTSelectChannelIDByServiceBox UTSelectChannelIDByServiceBox.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByServiceBox : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructor
  using extends::extends;

  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

private:
  Gaudi::Property<std::vector<std::string>> m_serviceBoxes{this, "serviceBoxes", {}};
  std::vector<unsigned int>                 m_sectors;
};

DECLARE_COMPONENT( UTSelectChannelIDByServiceBox )

StatusCode UTSelectChannelIDByServiceBox::initialize() {

  return extends::initialize().andThen( [&] {
    // to save time get a list of all the ids on this box
    m_sectors.reserve( 8 * m_serviceBoxes.size() );
    for ( const auto& box : m_serviceBoxes ) {
      const auto& boxSectors = readoutTool()->sectorIDsOnServiceBox( box );
      for ( const auto& chan : boxSectors ) { m_sectors.push_back( chan.uniqueSector() ); } // each sector
    }                                                                                       // for each board
    std::sort( m_sectors.begin(), m_sectors.end() );
  } );
}

bool UTSelectChannelIDByServiceBox::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectChannelIDByServiceBox::operator()( const LHCb::UTChannelID& id ) const {
  return std::binary_search( m_sectors.begin(), m_sectors.end(), id.uniqueSector() );
}
