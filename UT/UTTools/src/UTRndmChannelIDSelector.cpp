/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"

/** @class UTRndmChannelIDSelector UTRndmChannelIDSelector.h
 *
 *  Tool for selecting clusters at random
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTRndmChannelIDSelector : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructor
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  /**  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

private:
  // smart interface to generator
  SmartIF<IRndmGen> m_uniformDist;

  Gaudi::Property<double> m_fractionToReject{this, "FractionToReject", 0.0};
};

DECLARE_COMPONENT( UTRndmChannelIDSelector )

StatusCode UTRndmChannelIDSelector::initialize() {
  StatusCode sc = UT::ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  /// initialize, flat generator...
  auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
  m_uniformDist    = tRandNumSvc->generator( Rndm::Flat( 0., 1.0 ) );
  if ( !m_uniformDist ) return Error( "Failed to init generator ", sc );
  return StatusCode::SUCCESS;
}

bool UTRndmChannelIDSelector::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTRndmChannelIDSelector::operator()( const LHCb::UTChannelID& ) const {
  return m_uniformDist->shoot() < m_fractionToReject;
}
