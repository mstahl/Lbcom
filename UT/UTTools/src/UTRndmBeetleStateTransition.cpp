/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/StatusMap.h"

/** @class UTRndmBeetleStateTransition
 *
 *  Tool for making Beetles transition from one state to another
 *  Selection is random !
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTRndmBeetleStateTransition : public UT::ToolBase {

public:
  /// constructer
  using UT::ToolBase::ToolBase;
  /// initialize
  StatusCode initialize() override;

private:
  // smart interface to generator
  SmartIF<IRndmGen> m_uniformDist;

  Gaudi::Property<double>      m_fracToChange{this, "fracToChange", 0.02};
  Gaudi::Property<std::string> m_newState{this, "newState", "ReadoutProblems"};
};

DECLARE_COMPONENT( UTRndmBeetleStateTransition )

StatusCode UTRndmBeetleStateTransition::initialize() {

  StatusCode sc = UT::ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // get the random number generator
  auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
  m_uniformDist    = tRandNumSvc->generator( Rndm::Flat( 0., 1.0 ) );
  if ( !m_uniformDist ) return Error( "Failed to init generator ", sc );

  DeUTSector::Status newState = ::Status::toStatus( m_newState );

  // get the list of sectors

  info() << "Active fraction at input " << tracker()->fractionActive() << endmsg;
  unsigned int tcounter = 0;
  for ( const DeUTSector* sector : tracker()->sectors() ) {
    for ( unsigned int i = 1; i <= sector->nBeetle(); ++i ) {
      if ( m_uniformDist->shoot() < m_fracToChange ) {
        const_cast<DeUTSector*>( sector )->setBeetleStatus( i, newState );
        ++tcounter;
        info() << "Changed status of " << sector->nickname() << " Beetle " << i << "to " << m_newState << endmsg;
      }
    } // loop beetles
  }   // loop sectors

  info() << "Killed: " << tcounter << " Beetles " << endmsg;
  info() << "Active fraction now " << tracker()->fractionActive() << endmsg;

  return sc;
}
