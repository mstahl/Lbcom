/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
/** @file
 *
 *  Implementation file for class : UTClusterSelectorOR
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date 2018-09-04
 */
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTClusterSelector.h"
#include <string>

/** @class UTClusterSelectorOR UTClusterSelectorOR.h
 *
 *  Helper concrete tool for selection of UTCluster objects
 *  This selector selects the cluster if
 *  at least one  of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date   2018-09-04
 */
class UTClusterSelectorOR : public extends<GaudiTool, IUTClusterSelector> {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTClusterSelector*>;

public:
  using extends::extends;

  /** "select"/"preselect" method
   *  @see IUTClusterSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see IUTClusterSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTCluster* cluster ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Gaudi::Property<Names> m_selectorsTypeNames{this, "SelectorTools", {}};
  Selectors              m_selectors;
};

DECLARE_COMPONENT( UTClusterSelectorOR )

// ============================================================================
/** stORard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode UTClusterSelectorOR::initialize() {
  if ( m_selectorsTypeNames.empty() ) return Error( "No selectors specified" );
  // initialize the base class
  return GaudiTool::initialize().andThen( [&] {
    std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(), std::back_inserter( m_selectors ),
                    [&]( const auto& s ) {
                      info() << " Adding selector named " << s << endmsg;
                      return tool<IUTClusterSelector>( s );
                    } );
  } );
}
// ============================================================================

/** "select"/"preselect" method
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTClusterSelectorOR::select( const LHCb::UTCluster* cluster ) const { return ( *this )( cluster ); }
// ============================================================================

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTClusterSelectorOR::operator()( const LHCb::UTCluster* cluster ) const {
  ///
  return !m_selectors.empty() &&
         std::any_of( m_selectors.begin(), m_selectors.end(), [&]( const auto& s ) { return ( *s )( cluster ); } );
}
// ============================================================================
// The END
// ============================================================================
