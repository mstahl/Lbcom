/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"

using namespace LHCb;
using namespace Gaudi;

/** @class UTSelectSpilloverCluster UTSelectSpilloverCluster.h
 *
 *  Tool for selecting clusters that are not spillover
 *  Requires you have the previous spill, ie TAE or upgrade !
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDOnTrack : public extends<UT::ToolBase, IUTChannelIDSelector, IIncidentListener> {

public:
  /// constructer
  using extends::extends;

  /// intialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  /// initEvent
  void initEvent() const;

  DataObjectReadHandle<LHCb::Tracks>     m_trackLocation{this, "trackLocation", LHCb::TrackLocation::Default};
  mutable std::vector<LHCb::UTChannelID> m_ids;
  mutable bool                           m_configured = false;
};

DECLARE_COMPONENT( UTSelectChannelIDOnTrack )

StatusCode UTSelectChannelIDOnTrack::initialize() {
  return extends::initialize().andThen( [&] { incSvc()->addListener( this, IncidentType::BeginEvent ); } );
}

void UTSelectChannelIDOnTrack::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) { m_configured = false; }
}

bool UTSelectChannelIDOnTrack::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectChannelIDOnTrack::operator()( const LHCb::UTChannelID& id ) const {

  if ( !m_configured ) {
    // get the spillover clusters
    initEvent();
    m_configured = true;
  }
  return std::binary_search( m_ids.begin(), m_ids.end(), id );
}

void UTSelectChannelIDOnTrack::initEvent() const {
  m_ids.clear();
  for ( const auto& t : *m_trackLocation.get() ) {
    for ( LHCb::LHCbID chan : t->lhcbIDs() ) {
      if ( chan.isUT() ) { m_ids.push_back( chan.utID() ); }
    }
  }
  std::sort( m_ids.begin(), m_ids.end() );
}
