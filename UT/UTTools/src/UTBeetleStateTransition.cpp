/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/StatusMap.h"

/** @class UTBeetleStateTransition
 *
 *  Tool for making Beetles transition from one state to another
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTBeetleStateTransition : public UT::ToolBase {

public:
  /// constructer
  UTBeetleStateTransition( const std::string& type, const std::string& name, const IInterface* parent );

  /// initialize
  StatusCode initialize() override;

private:
  Gaudi::Property<std::string> m_initialState{this, "initalState", "ReadoutProblems"};
  Gaudi::Property<std::string> m_finalState{this, "finalState", "OK"};
};

DECLARE_COMPONENT( UTBeetleStateTransition )

UTBeetleStateTransition::UTBeetleStateTransition( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : UT::ToolBase( type, name, parent ) {}

StatusCode UTBeetleStateTransition::initialize() {

  StatusCode sc = UT::ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  DeUTSector::Status initialState = ::Status::toStatus( m_initialState );
  DeUTSector::Status finalState   = ::Status::toStatus( m_finalState );

  info() << "Active fraction at input " << tracker()->fractionActive() << endmsg;

  // get the list of sectors
  for ( const DeUTSector* sector : tracker()->sectors() ) {

    if ( sector->sectorStatus() == initialState ) continue; // only change a beetle state not sectors !!

    for ( unsigned int i = 1; i <= sector->nBeetle(); ++i ) {
      if ( sector->beetleStatus( i ) == initialState ) {
        info() << "Changing state of " << sector->nickname() << " Beetle: " << i << " from " << m_initialState << " to "
               << m_finalState << endmsg;
        const_cast<DeUTSector*>( sector )->setBeetleStatus( i, finalState );
      }
    } // loop beetles
  }   // loop sectors

  info() << "Active fraction at end of process " << tracker()->fractionActive() << endmsg;

  return StatusCode::SUCCESS;
}
