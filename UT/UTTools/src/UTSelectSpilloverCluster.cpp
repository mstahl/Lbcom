/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTToolBase.h"

/** @class UTSelectSpilloverCluster UTSelectSpilloverCluster.h
 *
 *  Tool for selecting clusters that are not spillover
 *  Requires you have the previous spill, ie TAE or upgrade !
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectSpilloverCluster : public extends<UT::ToolBase, IUTClusterSelector, IIncidentListener> {

public:
  /// constructor
  using extends::extends;

  /// intialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTCluster* cluster ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  bool chargeSelection( const LHCb::UTCluster* spillClus, const LHCb::UTCluster* cluster ) const;

  DataObjectReadHandle<LHCb::UTClusters> m_spilloverLocation{this, "spilloverData",
                                                             LHCb::UTClusterLocation::UTClusters};
  mutable LHCb::UTClusters*              m_spilloverData = nullptr;
  Gaudi::Property<double>                m_spilloverFraction{this, "spilloverFraction", 0.5};
  double                                 m_maxCharge{};
  Gaudi::Property<unsigned int>          m_nBits{this, "nBits", 7u,
                                        [this]( auto& ) { m_maxCharge = double( 2 << m_nBits ) - 1; },
                                        Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
};

DECLARE_COMPONENT( UTSelectSpilloverCluster )

StatusCode UTSelectSpilloverCluster::initialize() {
  return extends::initialize().andThen( [&] { incSvc()->addListener( this, IncidentType::BeginEvent ); } );
}

void UTSelectSpilloverCluster::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) { m_spilloverData = nullptr; }
}

bool UTSelectSpilloverCluster::select( const LHCb::UTCluster* cluster ) const { return ( *this )( cluster ); }

bool UTSelectSpilloverCluster::operator()( const LHCb::UTCluster* cluster ) const {

  if ( !m_spilloverData ) m_spilloverData = m_spilloverLocation.get(); // get the spillover clusters
  const LHCb::UTCluster* spillClus = m_spilloverData->object( cluster->key() );
  return spillClus && chargeSelection( spillClus, cluster );
}

bool UTSelectSpilloverCluster::chargeSelection( const LHCb::UTCluster* spillClus,
                                                const LHCb::UTCluster* cluster ) const {

  const double spillCharge = std::min( spillClus->totalCharge(), m_maxCharge );
  const double totCharge   = std::min( cluster->totalCharge(), m_maxCharge );
  return totCharge < m_spilloverFraction * spillCharge;
}
