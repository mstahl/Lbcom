/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"

/** @class UTSelectChannelIDByElement UTSelectChannelIDByElement.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectLadders17 : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructer
  using extends::extends;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;
};

DECLARE_COMPONENT( UTSelectLadders17 )

bool UTSelectLadders17::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectLadders17::operator()( const LHCb::UTChannelID& id ) const {
  unsigned int sector = id.sector();
  return sector == 1 || sector == 7;
}
