/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTSector.h"
#include "boost/numeric/conversion/bounds.hpp"

/** @class UTSelectBySN UTSelectBySN.h
 *
 *  Tool for selecting clusters by charge
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectBySN : public extends<UT::ToolBase, IUTClusterSelector> {

public:
  /// constructor
  using extends::extends;

  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTCluster* cluster ) const override;

private:
  Gaudi::Property<double> m_minSN{this, "minSN", 0.0};
  Gaudi::Property<double> m_maxSN{this, "maxSN", boost::numeric::bounds<double>::highest()};
};

DECLARE_COMPONENT( UTSelectBySN )

StatusCode UTSelectBySN::initialize() {
  return extends::initialize().andThen(
      [&] { info() << "Min Charge set to " << m_minSN << " / max charge set to " << m_maxSN << endmsg; } );
}

bool UTSelectBySN::select( const LHCb::UTCluster* cluster ) const { return ( *this )( cluster ); }

bool UTSelectBySN::operator()( const LHCb::UTCluster* cluster ) const {
  const DeUTSector* sector = findSector( cluster->channelID() );
  const double      sn     = cluster->totalCharge() / sector->noise( cluster->channelID() );
  return sn > m_minSN && sn < m_maxSN;
}
