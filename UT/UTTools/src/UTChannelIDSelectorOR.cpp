/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include <string>

// ============================================================================
/** @file
 *
 *  Implementation file for class : UTChannelIDSelectorOR
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date 2018-09-04
 */
// ============================================================================

/** @class UTChannelIDSelectorOR UTChannelIDSelectorOR.h
 *
 *  Helper concrete tool for selection of UTChannelID objects
 *  This selector selects the cluster if
 *  at least one  of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date   2018-09-04
 */
class UTChannelIDSelectorOR : public virtual IUTChannelIDSelector, public GaudiTool {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTChannelIDSelector*>;

public:
  /** Standard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @see IAlgTool
   *  @param type   tool type (?)
   *  @param name   tool name
   *  @param parent tool parent
   */
  UTChannelIDSelectorOR( const std::string& type, const std::string& name, const IInterface* parent );

  /** "select"/"preselect" method
   *  @see IUTChannelIDSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see IUTChannelIDSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Names     m_selectorsTypeNames;
  Selectors m_selectors;
};

DECLARE_COMPONENT( UTChannelIDSelectorOR )

// ============================================================================
/** StORard constructor
 *  @see GaudiTool
 *  @see  AlgTool
 *  @see IAlgTool
 *  @param type   tool type (?)
 *  @param name   tool name
 *  @param parent tool parent
 */
// ============================================================================
UTChannelIDSelectorOR::UTChannelIDSelectorOR( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : GaudiTool( type, name, parent ), m_selectorsTypeNames(), m_selectors() {
  declareInterface<IUTChannelIDSelector>( this );
  declareProperty( "SelectorTools", m_selectorsTypeNames );
}
// ============================================================================

// ============================================================================
/** stORard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode UTChannelIDSelectorOR::initialize() {
  // initialize the base class
  return GaudiTool::initialize().andThen( [&] {
    for ( const auto& it : m_selectorsTypeNames ) m_selectors.push_back( tool<IUTChannelIDSelector>( it ) );
  } );
}
// ============================================================================

/** "select"/"preselect" method
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTChannelIDSelectorOR::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }
// ============================================================================

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool UTChannelIDSelectorOR::operator()( const LHCb::UTChannelID& id ) const {
  return std::any_of( m_selectors.begin(), m_selectors.end(),
                      [&]( const auto& selector ) { return ( *selector )( id ); } );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
