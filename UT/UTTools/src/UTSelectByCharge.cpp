/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTToolBase.h"
#include "boost/numeric/conversion/bounds.hpp"

/** @class UTSelectByCharge UTSelectByCharge.h
 *
 *  Tool for selecting clusters by charge
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectByCharge : public extends<UT::ToolBase, IUTClusterSelector> {

public:
  /// constructer
  using extends::extends;

  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTCluster* cluster ) const override;

private:
  Gaudi::Property<double> m_minCharge{this, "minCharge", 0.0};
  Gaudi::Property<double> m_maxCharge{this, "maxCharge", boost::numeric::bounds<double>::highest()};
};

DECLARE_COMPONENT( UTSelectByCharge )

StatusCode UTSelectByCharge::initialize() {

  return extends::initialize().andThen(
      [&] { info() << "Min Charge set to " << m_minCharge << " / max charge set to " << m_maxCharge << endmsg; } );
}

bool UTSelectByCharge::select( const LHCb::UTCluster* cluster ) const { return ( *this )( cluster ); }

bool UTSelectByCharge::operator()( const LHCb::UTCluster* cluster ) const {
  const double charge = cluster->totalCharge();
  return charge > m_minCharge && charge < m_maxCharge;
}
