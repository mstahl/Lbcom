/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTTELL1Data.h"
#include "Kernel/IUTNoiseCalculationTool.h" // Interface
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "UTNoiseToolBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTNoiseCalculationTool
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTNoiseCalculationTool UTNoiseCalculationTool.h
 *
 *  The noise per strip of the TELL1s is calculated for all TELL1s.
 *  Implements abstract methods of UTNoiseToolBase class.  The noise
 *  is calculated from the ADCs given in the InputData location.
 *  In this tool, the CMS noise returned is identical to the raw noise.
 *  There is no outlier rejection performed yet so this tool should only
 *  be used to look at the noise in low occupancy events.
 *
 *  @author A. Beiter (based on code by J. van Tilburg, N. Chiapolini, and M. Tobin)
 *  @date   2018-09-04
 *
 */
namespace UT {
  class UTNoiseCalculationTool : public extends<UTNoiseToolBase, IUTNoiseCalculationTool> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override; ///< Tool initialisation

  private:
    StatusCode calculateNoise() override; ///< Noise calculation

  public:
    std::vector<double>       rawMean( const unsigned int TELL ) const override;
    std::vector<double>       rawMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       rawNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> rawN( const unsigned int TELL ) const override;

    std::vector<double>       cmsMean( const unsigned int TELL ) const override;
    std::vector<double>       cmsMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       cmsNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> cmsN( const unsigned int TELL ) const override;

  public:
    /// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
    std::vector<double>::const_iterator cmsNoiseBegin( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
    std::vector<double>::const_iterator cmsNoiseEnd( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
    std::vector<double>::const_iterator cmsMeanBegin( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
    std::vector<double>::const_iterator cmsMeanEnd( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1
    /// source ID
    std::vector<double>::const_iterator cmsMeanSquaredBegin( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1
    /// source ID
    std::vector<double>::const_iterator cmsMeanSquaredEnd( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the number of events containing data in the first PP for a given TELL1
    /// source ID
    std::vector<unsigned int>::const_iterator cmsNEventsPPBegin( const unsigned int TELL1SourceID ) const override;

    /// Return an iterator corresponding to the number of events containing data in the last PP for a given TELL1 source
    /// ID
    std::vector<unsigned int>::const_iterator cmsNEventsPPEnd( const unsigned int TELL1SourceID ) const override;
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT( UTNoiseCalculationTool )
} // namespace UT

StatusCode UT::UTNoiseCalculationTool::initialize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "initialize" << endmsg;
  StatusCode sc = UT::UTNoiseToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // CMS maps not used in this tool so clear.
  m_cmsMeanMap.clear();
  m_cmsMeanSqMap.clear();
  m_cmsNoiseMap.clear();
  m_cmsNEventsPP.clear();

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode UT::UTNoiseCalculationTool::calculateNoise() {
  m_evtNumber++;

  // Get the data
  const LHCb::UTTELL1Datas* data = getIfExists<LHCb::UTTELL1Datas>( m_dataLocation );
  if ( !data ) {
    // Skip if there is no Tell1 data
    return StatusCode::SUCCESS;
  }
  if ( data->empty() ) return Warning( "Data is empty", StatusCode::SUCCESS, 10 );
  // debug() << "Found " << data->size() << " boards." << endmsg;

  // loop over the data

  for ( const auto& iterBoard : *data ) {

    // get the tell board and the data headers
    unsigned int                   tellID     = iterBoard->TELL1ID();
    const LHCb::UTTELL1Data::Data& dataValues = iterBoard->data();

    // Store tell1s with an NZS bank
    m_tell1WithNZS.push_back( tellID );
    // Local vectors for given TELL1
    std::vector<double>&       pedestalTELL = m_rawPedestalMap[tellID];
    std::vector<double>&       meanTELL     = m_rawMeanMap[tellID];
    std::vector<double>&       meanSqTELL   = m_rawMeanSqMap[tellID];
    std::vector<double>&       noiseTELL    = m_rawNoiseMap[tellID];
    std::vector<unsigned int>& nEventsPP    = m_rawNEventsPP[tellID];

    // Loop over the PPs that have sent data
    for ( unsigned int pp : iterBoard->sentPPs() ) {

      // Count the number of events per PP
      nEventsPP[pp]++;
      if ( m_countRoundRobin ) this->countRoundRobin( tellID, pp );

      // Cumulative average up to m_followingPeriod; after that
      // exponential moving average
      int nEvt = nEventsPP[pp];
      if ( m_followingPeriod > 0 && nEvt > m_followingPeriod ) nEvt = m_followingPeriod;

      // Loop over the links (i.e. Beetles)
      unsigned int iBeetle = 0;
      for ( ; iBeetle < UTDAQ::nBeetlesPerPPx; ++iBeetle ) {
        unsigned int beetle = iBeetle + pp * UTDAQ::nBeetlesPerPPx;

        // Loop over the strips in this link
        unsigned int iStrip = 0;
        for ( ; iStrip < LHCbConstants::nStripsInBeetle; ++iStrip ) {

          // Get the ADC value
          const int value = dataValues[beetle][iStrip];

          // Calculate the pedestal and the pedestal squared
          int strip           = iStrip + beetle * LHCbConstants::nStripsInBeetle;
          meanTELL[strip]     = ( meanTELL[strip] * ( nEvt - 1 ) + value ) / nEvt;
          pedestalTELL[strip] = meanTELL[strip];
          meanSqTELL[strip]   = ( meanSqTELL[strip] * ( nEvt - 1 ) + std::pow( value, 2 ) ) / nEvt;
          noiseTELL[strip]    = sqrt( meanSqTELL[strip] - std::pow( meanTELL[strip], 2 ) );
        } // strip
      }   // beetle

      // Resets the event counter
      if ( m_resetRate > 0 && nEvt % m_resetRate == 0 ) { nEventsPP[pp] = 0; }
    } // FPGA-PP
  }   // boards
  return StatusCode::SUCCESS;
}

/// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
std::vector<double>::const_iterator
UT::UTNoiseCalculationTool::cmsNoiseBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawNoiseMap.find( TELL1SourceID ) == m_rawNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNoiseMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseCalculationTool::cmsNoiseEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawNoiseMap.find( TELL1SourceID ) == m_rawNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNoiseMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseCalculationTool::cmsMeanBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanMap.find( TELL1SourceID ) == m_rawMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseCalculationTool::cmsMeanEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanMap.find( TELL1SourceID ) == m_rawMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator
UT::UTNoiseCalculationTool::cmsMeanSquaredBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanSqMap.find( TELL1SourceID ) == m_rawMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanSqMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator
UT::UTNoiseCalculationTool::cmsMeanSquaredEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanSqMap.find( TELL1SourceID ) == m_rawMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanSqMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the number of events containing data in the first PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseCalculationTool::cmsNEventsPPBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawNEventsPP.find( TELL1SourceID ) == m_rawNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNEventsPP.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the number of events containing data in the last PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseCalculationTool::cmsNEventsPPEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawNEventsPP.find( TELL1SourceID ) == m_rawNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNEventsPP.find( TELL1SourceID )->second.end();
}

std::vector<double> UT::UTNoiseCalculationTool::rawMean( const unsigned int TELL ) const {
  return m_rawMeanMap.find( TELL )->second;
}
std::vector<double> UT::UTNoiseCalculationTool::rawMeanSq( const unsigned int TELL ) const {
  return m_rawMeanSqMap.find( TELL )->second;
}
std::vector<double> UT::UTNoiseCalculationTool::rawNoise( const unsigned int TELL ) const {
  return m_rawNoiseMap.find( TELL )->second;
}
std::vector<unsigned int> UT::UTNoiseCalculationTool::rawN( const unsigned int TELL ) const {
  return m_rawNEventsPP.find( TELL )->second;
}

std::vector<double> UT::UTNoiseCalculationTool::cmsMean( const unsigned int TELL ) const {
  return m_rawMeanMap.find( TELL )->second;
}
std::vector<double> UT::UTNoiseCalculationTool::cmsMeanSq( const unsigned int TELL ) const {
  return m_rawMeanSqMap.find( TELL )->second;
}
std::vector<double> UT::UTNoiseCalculationTool::cmsNoise( const unsigned int TELL ) const {
  return m_rawNoiseMap.find( TELL )->second;
}
std::vector<unsigned int> UT::UTNoiseCalculationTool::cmsN( const unsigned int TELL ) const {
  return m_rawNEventsPP.find( TELL )->second;
}
