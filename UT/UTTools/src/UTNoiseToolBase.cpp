/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// UTTELL1Event
#include "Event/UTTELL1Data.h"
#include "Kernel/UTTell1Board.h"

// LHCbKernel
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"

// Detector description for access to conditions
#include "DetDesc/Condition.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "AIDA/IHistogram2D.h"

#include "boost/algorithm/string.hpp"

// local
#include "UTNoiseToolBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTNoiseToolBase
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// namespace UT {
//   DECLARE_COMPONENT( UTNoiseToolBase );
// }

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTNoiseToolBase::UTNoiseToolBase( const std::string& type, const std::string& name, const IInterface* parent )
    : UT::HistoToolBase( type, name, parent ) {
  // Location of input data used in raw noise calculation
  declareProperty( "InputData", m_dataLocation = LHCb::UTTELL1DataLocation::UTFull );

  /* Calculation of noise is cumulative mean up to following period
  and then exponential moving average after */
  declareProperty( "FollowPeriod", m_followingPeriod = 2000 );

  // Reset rate for counters used in noise calculation
  declareProperty( "ResetRate", m_resetRate = -1 );

  // Skip events to allow time for the pedestals to be calculated
  declareProperty( "SkipEvents", m_skipEvents = -1 );

  // Limit calculation to vector of tell1s given in terms of TELLID (eg TTTELL1 = 1)
  declareProperty( "LimitToTell", m_limitToTell );

  // Count number of round robin events per PP
  declareProperty( "CountRoundRobin", m_countRoundRobin = false );

  // Read pedestals from conditions database
  declareProperty( "PedestalsFromDB", m_readPedestals = false );

  // Read hit thresholds from conditions database
  declareProperty( "ThresholdsFromDB", m_readThresholds = false );

  // Set the path for the conditions database
  declareProperty( "CondPath", m_condPath = "CondDB" );

  // Mask known bad channels
  declareProperty( "MaskBadChannels", m_maskBad = true );

  // Read masked channels from conditions database
  declareProperty( "PedestalMaskFromDB", m_readPedestalMask = true );

  // Use number of events per strip in noise calculations
  declareProperty( "UseEventsPerStrip", m_evtsPerStrip = false );

  // Choose selected steps (for CCEScan)
  declareProperty( "Steps", m_steps );

  // Reset counters after a run change
  declareProperty( "ResetAfterRunChange", m_resetRunChange = true );
}

StatusCode UT::UTNoiseToolBase::initialize() {

  StatusCode sc = UT::HistoToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  m_evtNumber = 0;
  // Select small number of TELL1s
  m_selectedTells = false;
  if ( m_limitToTell.size() > 0 ) {
    m_selectedTells = true;
    std::sort( m_limitToTell.begin(), m_limitToTell.end() );
  }
  m_selectedSteps = false;
  if ( m_steps.size() > 0 ) {
    m_selectedSteps = true;
    std::sort( m_steps.begin(), m_steps.end() );
  }
  // Get the tell1 mapping from source ID to tell1 number
  IUpdateManagerSvc* mgrSvc = svc<IUpdateManagerSvc>( "UpdateManagerSvc", true );
  auto               itT    = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
  for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int TELL1SourceID = ( *itT ).first;
    resetNoiseCounters( TELL1SourceID );
    std::string condPath = m_condPath + "/TELL1Board" + std::to_string( TELL1SourceID );
    mgrSvc->registerCondition( this, condPath, &UT::UTNoiseToolBase::cacheTELL1Parameters );
  }
  StatusCode mgrSvcStatus = mgrSvc->update( this );
  if ( mgrSvcStatus.isFailure() ) { return ( Error( "Failed first UMS update", mgrSvcStatus ) ); }

  m_firstEvent  = true;
  m_eventNumber = 0;
  m_runNumber   = 0;
  m_tell1WithNZS.clear();
  if ( m_countRoundRobin ) {
    unsigned int m_nTELL1s = readoutTool()->nBoard();
    m_2d_nEventsPerPP      = book2D( "Number of NZS banks sent per PP", 0.5, m_nTELL1s + 0.5, m_nTELL1s, -0.5, 3.5, 4 );
  }
  return StatusCode::SUCCESS;
}

// Reset all noise counters (@ initialise or after change in conditions)
void UT::UTNoiseToolBase::resetNoiseCounters( const unsigned int TELL1SourceID ) {

  info() << "Resetting noise counters for " << TELL1SourceID << endmsg;
  m_nEvents[TELL1SourceID].assign( 3072, 0 );
  m_rawPedestalMap[TELL1SourceID].assign( 3072, 0.0 );
  m_rawMeanMap[TELL1SourceID].assign( 3072, 0.0 );
  m_rawMeanSqMap[TELL1SourceID].assign( 3072, 0.0 );
  m_rawNoiseMap[TELL1SourceID].assign( 3072, 0.0 );
  m_rawNEventsPP[TELL1SourceID].assign( 4, 0 );

  m_cmsMeanMap[TELL1SourceID].assign( 3072, 0.0 );
  m_cmsMeanSqMap[TELL1SourceID].assign( 3072, 0.0 );
  m_cmsNoiseMap[TELL1SourceID].assign( 3072, 0.0 );
  m_cmsNEventsPP[TELL1SourceID].assign( 4, 0 );

  m_pedSubMeanMap[TELL1SourceID].assign( 3072, 0.0 );
  m_pedSubMeanSqMap[TELL1SourceID].assign( 3072, 0.0 );
  m_pedSubNoiseMap[TELL1SourceID].assign( 3072, 0.0 );
  m_pedSubNEventsPP[TELL1SourceID].assign( 4, 0 );
}

// Reset all noise counters (after change in conditions)
void UT::UTNoiseToolBase::resetNoiseCounters() {
  info() << "Resetting all noise counters" << endmsg;
  auto itT = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
  for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int TELL1SourceID = ( *itT ).first;
    resetNoiseCounters( TELL1SourceID );
  }
}

//==============================================================================
// Read+cache TELL1 parameters from conditions data base
//==============================================================================
StatusCode UT::UTNoiseToolBase::cacheTELL1Parameters() {
  info() << "==> Caching TELL1 parameters" << endmsg;
  // initialise condition updates
  if ( m_readPedestals | m_readThresholds | m_readPedestalMask ) {
    auto itT = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
    for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
      unsigned int TELL1SourceID = ( *itT ).first;
      resetNoiseCounters( TELL1SourceID );
      readTELL1Parameters( TELL1SourceID );
    }
  }
  return StatusCode::SUCCESS;
}
void UT::UTNoiseToolBase::readTELL1Parameters( const unsigned int TELL1SourceID ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "----------> Reading TELL1 parameters for " << TELL1SourceID << endmsg;
  m_pedestalMaps[TELL1SourceID].resize( 1 );
  std::string condPath = m_condPath + "/TELL1Board" + std::to_string( TELL1SourceID );
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Getting condition: " << condPath << endmsg;
  auto* condition = getDet<Condition>( condPath );
  if ( condition != nullptr ) {
    if ( m_readPedestals ) {
      std::vector<int>                     pedestalValues = condition->param<std::vector<int>>( "pedestal" );
      auto                                 itPed          = pedestalValues.begin();
      std::vector<std::pair<double, int>>* pedestals      = &m_pedestalMaps[TELL1SourceID][0];
      pedestals->clear();
      for ( ; itPed != pedestalValues.end(); ++itPed ) { pedestals->push_back( std::make_pair( ( *itPed ), 1 ) ); }
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "caching pedestals: UTTELL" << ( this->readoutTool() )->SourceIDToTELLNumber( TELL1SourceID )
                  << "\t" << ( *pedestals ) << std::endl;
    }
    if ( m_readThresholds ) {
      std::vector<int>                        hitThresholds = condition->param<std::vector<int>>( "hit_threshold" );
      std::vector<int>                        cmsThresholds = condition->param<std::vector<int>>( "cms_threshold" );
      auto                                    hitIt         = hitThresholds.begin();
      auto                                    cmsIt         = cmsThresholds.begin();
      std::vector<std::pair<double, double>>* thresholds    = &m_thresholdMap[TELL1SourceID];
      thresholds->clear();
      for ( ; hitIt != hitThresholds.end(); ++hitIt, ++cmsIt ) {
        thresholds->push_back( std::make_pair( ( *hitIt ), ( *cmsIt ) ) );
      }
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "caching thresholds: UTTELL" << ( this->readoutTool() )->SourceIDToTELLNumber( TELL1SourceID )
                  << "\t" << ( *thresholds ) << std::endl;
    }
    if ( m_readPedestalMask ) { // status: value is 1 if strip is ok, 0 if strip is not ok
      std::vector<int>   pedestalMasks = condition->param<std::vector<int>>( "pedestal_mask" );
      auto               itMask        = pedestalMasks.begin();
      std::vector<bool>* status        = &m_statusMap[TELL1SourceID];
      //      std::vector<bool>::iterator itStatus = status->begin();
      status->assign( 3072, false );
      auto                itStat  = status->begin();
      unsigned int        channel = 0;
      const UTTell1Board* board   = readoutTool()->findByBoardID( UTTell1ID( TELL1SourceID ) );
      for ( ; itMask != pedestalMasks.end(); ++itMask, ++itStat, ++channel ) { // check channel is valid
        LHCb::UTChannelID channelID =
            ( board->DAQToOffline( 0, UTDAQ::version::v4, UTDAQ::UTStripRepresentation( channel ) ).first );
        const DeUTSector* sector = tracker()->findSector( channelID );
        if ( sector != nullptr ) { *itStat = m_maskBad ? !static_cast<bool>( ( *itMask ) ) : true; }
      }
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "caching pedestal masks: UTTELL" << ( this->readoutTool() )->SourceIDToTELLNumber( TELL1SourceID )
                  << "\t statusMap" << ( *status ) << endmsg;
    }
  } else {
    error() << "No condition: " << condPath << endmsg;
  }
}

//======================================================================================================================
//
// Noise calculation
//
//======================================================================================================================
StatusCode UT::UTNoiseToolBase::updateNoise() {
  // get event number, run number, then test that this is a new event.
  bool               newEvent    = false;
  bool               newRun      = false;
  const LHCb::ODIN*  odin        = m_odin.get();
  const ulonglong    eventNumber = odin->eventNumber();
  const unsigned int runNumber   = odin->runNumber();
  if ( m_firstEvent ) {
    m_firstEvent = false;
    newEvent     = true;
  } else {
    newRun   = ( m_runNumber != runNumber );
    newEvent = ( m_eventNumber != eventNumber );
  }
  if ( newEvent || newRun ) {
    m_eventNumber = eventNumber;
    m_runNumber   = runNumber;
    m_tell1WithNZS.clear();
    if ( m_selectedSteps ) {
      const unsigned int step = odin->calibrationStep();
      if ( !binary_search( m_steps.begin(), m_steps.end(), step ) ) return StatusCode::SUCCESS;
    }
    if ( m_resetRunChange && newRun ) resetNoiseCounters();
    this->calculateNoise().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } else {
    return Warning( "You should only call updateNoise once per event", StatusCode::SUCCESS, 0 );
  }
  return StatusCode::SUCCESS;
}
//======================================================================================================================
//
// Count number of events in Round Robin mode
//
//======================================================================================================================
void UT::UTNoiseToolBase::countRoundRobin( unsigned int TELL1SourceID, unsigned int PP ) {
  m_2d_nEventsPerPP->fill( readoutTool()->SourceIDToTELLNumber( TELL1SourceID ), PP );
}
//======================================================================================================================
//
// Data Accessors
//
//======================================================================================================================
/// Return an iterator corresponding to the pedestal value of the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedestalBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawPedestalMap.find( TELL1SourceID ) == m_rawPedestalMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawPedestalMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the pedestal value of the last channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedestalEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawPedestalMap.find( TELL1SourceID ) == m_rawPedestalMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawPedestalMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the RAW RMS noise on the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawNoiseBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawNoiseMap.find( TELL1SourceID ) == m_rawNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNoiseMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the RAW RMS noise on the last channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawNoiseEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawNoiseMap.find( TELL1SourceID ) == m_rawNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNoiseMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the RAW mean ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawMeanBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanMap.find( TELL1SourceID ) == m_rawMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the RAW mean ADC value for the last channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawMeanEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanMap.find( TELL1SourceID ) == m_rawMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the RAW mean squared ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawMeanSquaredBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanSqMap.find( TELL1SourceID ) == m_rawMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanSqMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the RAW mean squared ADC value for the last channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::rawMeanSquaredEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawMeanSqMap.find( TELL1SourceID ) == m_rawMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawMeanSqMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the number of events containing data in the first PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::rawNEventsPPBegin( const unsigned int TELL1SourceID ) const {
  if ( m_rawNEventsPP.find( TELL1SourceID ) == m_rawNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNEventsPP.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the number of events containing data in the last PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::rawNEventsPPEnd( const unsigned int TELL1SourceID ) const {
  if ( m_rawNEventsPP.find( TELL1SourceID ) == m_rawNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_rawNEventsPP.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsNoiseBegin( const unsigned int TELL1SourceID ) const {
  if ( m_cmsNoiseMap.find( TELL1SourceID ) == m_cmsNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsNoiseMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS RMS noise on the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsNoiseEnd( const unsigned int TELL1SourceID ) const {
  if ( m_cmsNoiseMap.find( TELL1SourceID ) == m_cmsNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsNoiseMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsMeanBegin( const unsigned int TELL1SourceID ) const {
  if ( m_cmsMeanMap.find( TELL1SourceID ) == m_cmsMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsMeanMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS mean ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsMeanEnd( const unsigned int TELL1SourceID ) const {
  if ( m_cmsMeanMap.find( TELL1SourceID ) == m_cmsMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsMeanMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsMeanSquaredBegin( const unsigned int TELL1SourceID ) const {
  if ( m_cmsMeanSqMap.find( TELL1SourceID ) == m_cmsMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsMeanSqMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the CMS mean squared ADC value for the first channel for a given TELL1 source ID
std::vector<double>::const_iterator UT::UTNoiseToolBase::cmsMeanSquaredEnd( const unsigned int TELL1SourceID ) const {
  if ( m_cmsMeanSqMap.find( TELL1SourceID ) == m_cmsMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsMeanSqMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the number of events containing data in the first PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::cmsNEventsPPBegin( const unsigned int TELL1SourceID ) const {
  if ( m_cmsNEventsPP.find( TELL1SourceID ) == m_cmsNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsNEventsPP.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the number of events containing data in the last PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::cmsNEventsPPEnd( const unsigned int TELL1SourceID ) const {
  if ( m_cmsNEventsPP.find( TELL1SourceID ) == m_cmsNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_cmsNEventsPP.find( TELL1SourceID )->second.end();
}

/** Return an iterator corresponding to the RMS noise after pedestal subtraction on the first
    channel for a given TELL1 source ID **/
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedSubNoiseBegin( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubNoiseMap.find( TELL1SourceID ) == m_pedSubNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubNoiseMap.find( TELL1SourceID )->second.begin();
}

/** Return an iterator corresponding to the RMS noise after pedestal subtraction on the last
    channel for a given TELL1 source ID **/
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedSubNoiseEnd( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubNoiseMap.find( TELL1SourceID ) == m_pedSubNoiseMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubNoiseMap.find( TELL1SourceID )->second.end();
}

/** Return an iterator corresponding to the mean ADC value after pedestal subtraction for the
    first channel for a given TELL1 source ID **/
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedSubMeanBegin( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubMeanMap.find( TELL1SourceID ) == m_pedSubMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubMeanMap.find( TELL1SourceID )->second.begin();
}

/** Return an iterator corresponding to the mean ADC value after pedestal subtraction for the last
    channel for a given TELL1 source ID **/
std::vector<double>::const_iterator UT::UTNoiseToolBase::pedSubMeanEnd( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubMeanMap.find( TELL1SourceID ) == m_pedSubMeanMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubMeanMap.find( TELL1SourceID )->second.end();
}

/** Return an iterator corresponding to the mean squared ADC value after pedestal subtraction for the first
    channel for a given TELL1 source ID **/
std::vector<double>::const_iterator
UT::UTNoiseToolBase::pedSubMeanSquaredBegin( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubMeanSqMap.find( TELL1SourceID ) == m_pedSubMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubMeanSqMap.find( TELL1SourceID )->second.begin();
}

/** Return an iterator corresponding to the mean squared ADC value after pedestal subtraction for the last
    channel for a given TELL1 source ID **/
std::vector<double>::const_iterator
UT::UTNoiseToolBase::pedSubMeanSquaredEnd( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubMeanSqMap.find( TELL1SourceID ) == m_pedSubMeanSqMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubMeanSqMap.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the number of events containing data in the first PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::pedSubNEventsPPBegin( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubNEventsPP.find( TELL1SourceID ) == m_pedSubNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubNEventsPP.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the number of events containing data in the last PP for a given TELL1 source ID
std::vector<unsigned int>::const_iterator
UT::UTNoiseToolBase::pedSubNEventsPPEnd( const unsigned int TELL1SourceID ) const {
  if ( m_pedSubNEventsPP.find( TELL1SourceID ) == m_pedSubNEventsPP.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_pedSubNEventsPP.find( TELL1SourceID )->second.end();
}

/** Return an iterator corresponding to the number of events used in the calculation of noise (RAW, CMS, pedSub)
    after outlier removal for the first channel of a given TELL1 source ID **/
std::vector<unsigned int>::const_iterator UT::UTNoiseToolBase::nEventsBegin( const unsigned int TELL1SourceID ) const {
  if ( m_nEvents.find( TELL1SourceID ) == m_nEvents.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_nEvents.find( TELL1SourceID )->second.begin();
}

/** Return an iterator corresponding to the number of events used in the calculation of noise (RAW, CMS, pedSub)
    after outlier removal for the last channel of a given TELL1 source ID **/
std::vector<unsigned int>::const_iterator UT::UTNoiseToolBase::nEventsEnd( const unsigned int TELL1SourceID ) const {
  if ( m_nEvents.find( TELL1SourceID ) == m_nEvents.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_nEvents.find( TELL1SourceID )->second.end();
}

/// Return an iterator corresponding to the source ID of the first TELL1 in the event containing an NZS bank
std::vector<unsigned int>::const_iterator UT::UTNoiseToolBase::tell1WithNZSBegin() const {
  return m_tell1WithNZS.begin();
}

/// Return an iterator corresponding to the source ID of the last TELL1 in the event containing an NZS bank
std::vector<unsigned int>::const_iterator UT::UTNoiseToolBase::tell1WithNZSEnd() const { return m_tell1WithNZS.end(); }

/// Return an iterator corresponding to the status of the first channel for a given TELL1 source ID
std::vector<bool>::const_iterator UT::UTNoiseToolBase::stripStatusBegin( const unsigned int TELL1SourceID ) const {
  if ( m_statusMap.find( TELL1SourceID ) == m_statusMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_statusMap.find( TELL1SourceID )->second.begin();
}

/// Return an iterator corresponding to the status of the last channel for a given TELL1 source ID
std::vector<bool>::const_iterator UT::UTNoiseToolBase::stripStatusEnd( const unsigned int TELL1SourceID ) const {
  if ( m_statusMap.find( TELL1SourceID ) == m_statusMap.end() ) {
    error() << "This should never happen! Did you pass TELLID rather than source ID? " << TELL1SourceID << endmsg;
  }
  return m_statusMap.find( TELL1SourceID )->second.end();
}
