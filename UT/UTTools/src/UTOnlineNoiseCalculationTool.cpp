/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTTELL1Data.h"
#include "Kernel/IUTNoiseCalculationTool.h" // Interface
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "UTNoiseToolBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTOnlineNoiseCalculationTool
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTOnlineNoiseCalculationTool UTOnlineNoiseCalculationTool.h
 *
 *  Calculation of the raw and CMS noise optimised for round robin NZS data
 *  processed using the TELL1 emulator.  The tool calculates the raw and cms noise
 *  using two input locations.
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */
namespace UT {
  class UTOnlineNoiseCalculationTool : public extends<UTNoiseToolBase, IUTNoiseCalculationTool> {

  public:
    /// Standard constructor
    UTOnlineNoiseCalculationTool( const std::string& type, const std::string& name, const IInterface* parent );

  private:
    StatusCode calculateNoise() override; ///< Noise calculation

  public:
    std::vector<double>       rawMean( const unsigned int TELL ) const override;
    std::vector<double>       rawMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       rawNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> rawN( const unsigned int TELL ) const override;

    std::vector<double>       cmsMean( const unsigned int TELL ) const override;
    std::vector<double>       cmsMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       cmsNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> cmsN( const unsigned int TELL ) const override;

  private:
    DataObjectReadHandle<LHCb::UTTELL1Datas> m_LCMS{this, "LCMSInputData", LHCb::UTTELL1DataLocation::UTLCMSADCs,
                                                    "Input location of LCMS ADC values"};
    DataObjectReadHandle<LHCb::UTTELL1Datas> m_pedSub{this, "PedSubInputData", LHCb::UTTELL1DataLocation::UTPedSubADCs,
                                                      "Input location of pedestal subtracted ADC values"};
    Gaudi::Property<bool> m_removeOutliers{this, "RemoveOutliers", true, "Remove outliers from noise calculation"};
  };
  DECLARE_COMPONENT( UTOnlineNoiseCalculationTool )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  UT::UTOnlineNoiseCalculationTool::UTOnlineNoiseCalculationTool( const std::string& type, const std::string& name,
                                                                  const IInterface* parent )
      : extends{type, name, parent} {
    m_evtsPerStrip   = true;
    m_readPedestals  = true;
    m_readThresholds = true;
  }

  //=============================================================================
  StatusCode UTOnlineNoiseCalculationTool::calculateNoise() {
    m_evtNumber++;

    // Get the RAW data
    const LHCb::UTTELL1Datas* data = getIfExists<LHCb::UTTELL1Datas>( m_dataLocation );
    if ( !data ) {
      // Skip if there is no Tell1 data
      return StatusCode::SUCCESS;
    }
    if ( data->empty() ) return Warning( "Data is empty " + m_dataLocation, StatusCode::SUCCESS, 10 );
    // debug() << "Found " << data->size() << " boards." << endmsg;

    // Get the pedestal substracted data
    const LHCb::UTTELL1Datas* pedSub = m_pedSub.get();
    if ( pedSub->empty() ) return Warning( "Data is empty " + m_pedSub.objKey(), StatusCode::SUCCESS, 10 );
    // debug() << "Found " << data->size() << " boards." << endmsg;

    // Get the LCMS data
    const LHCb::UTTELL1Datas* lcms = m_LCMS.getIfExists();
    if ( !lcms ) {
      // Skip if there is no Tell1 data
      return StatusCode::SUCCESS;
    }
    if ( lcms->empty() ) return Warning( "Data is empty " + m_LCMS.objKey(), StatusCode::SUCCESS, 10 );
    // debug() << "Found " << data->size() << " boards." << endmsg;

    if ( data->size() != lcms->size() && data->size() != pedSub->size() && lcms->size() != pedSub->size() ) {
      return Error( "Mis-match in size of data containers", StatusCode::SUCCESS, 10 );
    }
    // loop over the data

    auto iterLCMSData   = lcms->begin();
    auto iterPedSubData = pedSub->begin();
    for ( auto iterFullData = data->begin(); iterFullData != data->end();
          ++iterFullData, ++iterLCMSData, ++iterPedSubData ) {

      // get the tell board and the data headers
      unsigned int tellID = ( *iterFullData )->TELL1ID();
      // Limit to selected tell1s
      if ( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                              ( this->readoutTool() )->SourceIDToTELLNumber( tellID ) ) ) {
        continue;
      }
      const LHCb::UTTELL1Data::Data& dataValues   = ( *iterFullData )->data();
      unsigned int                   tellIDPedSub = ( *iterPedSubData )->TELL1ID();
      const LHCb::UTTELL1Data::Data& pedSubValues = ( *iterPedSubData )->data();
      unsigned int                   tellIDLCMS   = ( *iterLCMSData )->TELL1ID();
      const LHCb::UTTELL1Data::Data& lcmsValues   = ( *iterLCMSData )->data();
      if ( tellID != tellIDLCMS && tellID != tellIDPedSub && tellIDLCMS != tellIDPedSub ) {
        return Error( "Data from different TELL1s", StatusCode::SUCCESS, 10 );
      }
      // Store tell1s with an NZS bank
      m_tell1WithNZS.push_back( tellID );

      // Local vectors for given TELL1
      std::vector<unsigned int>* nEvents = &m_nEvents[tellID];

      std::vector<double>*       rawPedestal  = &m_rawPedestalMap[tellID];
      std::vector<double>*       rawMean      = &m_rawMeanMap[tellID];
      std::vector<double>*       rawMeanSq    = &m_rawMeanSqMap[tellID];
      std::vector<double>*       rawNoise     = &m_rawNoiseMap[tellID];
      std::vector<unsigned int>* rawNEventsPP = &m_rawNEventsPP[tellID];

      std::vector<double>*       cmsMean      = &m_cmsMeanMap[tellID];
      std::vector<double>*       cmsMeanSq    = &m_cmsMeanSqMap[tellID];
      std::vector<double>*       cmsNoise     = &m_cmsNoiseMap[tellID];
      std::vector<unsigned int>* cmsNEventsPP = &m_cmsNEventsPP[tellID];

      std::vector<double>*       pedSubMean      = &m_pedSubMeanMap[tellID];
      std::vector<double>*       pedSubMeanSq    = &m_pedSubMeanSqMap[tellID];
      std::vector<double>*       pedSubNoise     = &m_pedSubNoiseMap[tellID];
      std::vector<unsigned int>* pedSubNEventsPP = &m_pedSubNEventsPP[tellID];

      // Local copy of pedestal and thresholds used on the TELL1
      //    std::vector<std::pair<double, int> >* pedestals = &m_pedestalMaps[tellID][0];
      std::vector<std::pair<double, double>>* thresholds  = &m_thresholdMap[tellID];
      std::vector<bool>*                      stripStatus = &m_statusMap[tellID];
      // Loop over the PPs that have sent data
      std::vector<unsigned int> sentPPs = ( *iterFullData )->sentPPs();

      for ( unsigned int pp : sentPPs ) {
        // Count the number of events per PP
        ( *rawNEventsPP )[pp]++;
        ( *cmsNEventsPP )[pp]++;
        ( *pedSubNEventsPP )[pp]++;
        if ( m_countRoundRobin ) this->countRoundRobin( tellID, pp );

        // Loop over the links (i.e. Beetles)
        unsigned int iBeetle = 0;
        for ( ; iBeetle < UTDAQ::nBeetlesPerPPx; ++iBeetle ) {
          unsigned int beetle = iBeetle + pp * UTDAQ::nBeetlesPerPPx;

          // Loop over the strips in this link
          unsigned int iStrip = 0;
          for ( ; iStrip < LHCbConstants::nStripsInBeetle; ++iStrip ) {

            int strip = iStrip + beetle * LHCbConstants::nStripsInBeetle;
            if ( ( *stripStatus )[strip] ) {
              const int rawValue     = dataValues[beetle][iStrip];
              const int lcmsValue    = lcmsValues[beetle][iStrip];
              const int pedSubValue  = pedSubValues[beetle][iStrip];
              bool      updateRaw    = true;
              bool      updatePedSub = true;
              bool      updateCMS    = true;
              if ( m_removeOutliers.value() ) { // only update the plots if lcms value < cms_threshold
                //            if(fabs(rawValue-(*pedestals)[strip].first) > (*thresholds)[strip].first) updateRaw=false;
                //              if(fabs(static_cast<double>(pedSubValue)) > (*thresholds)[strip].first)
                //              updatePedSub=false;
                if ( fabs( static_cast<double>( lcmsValue ) ) > ( *thresholds )[strip].first ) updateCMS = false;
              }
              // Calculate the pedestal and the pedestal squared
              // Cumulative average up to m_followingPeriod; after that exponential moving average
              if ( updateRaw && updatePedSub && updateCMS ) { // only update the noise if both criteria are fulfilled
                ( *nEvents )[strip] += 1;
                int nEvt = ( *nEvents )[strip];
                if ( m_followingPeriod > 0 && nEvt > m_followingPeriod ) {
                  nEvt                = m_followingPeriod;
                  ( *nEvents )[strip] = m_followingPeriod;
                }
                // raw noise
                ( *rawMean )[strip]     = ( ( *rawMean )[strip] * ( nEvt - 1 ) + rawValue ) / nEvt;
                ( *rawPedestal )[strip] = ( *rawMean )[strip];
                ( *rawMeanSq )[strip]   = ( ( *rawMeanSq )[strip] * ( nEvt - 1 ) + std::pow( rawValue, 2 ) ) / nEvt;
                ( *rawNoise )[strip]    = sqrt( ( *rawMeanSq )[strip] - std::pow( ( *rawMean )[strip], 2 ) );
                // pedestal subtracted noise
                ( *pedSubMean )[strip] = ( ( *pedSubMean )[strip] * ( nEvt - 1 ) + pedSubValue ) / nEvt;
                ( *pedSubMeanSq )[strip] =
                    ( ( *pedSubMeanSq )[strip] * ( nEvt - 1 ) + std::pow( pedSubValue, 2 ) ) / nEvt;
                ( *pedSubNoise )[strip] = sqrt( ( *pedSubMeanSq )[strip] - std::pow( ( *pedSubMean )[strip], 2 ) );
                // cms noise
                ( *cmsMean )[strip]   = ( ( *cmsMean )[strip] * ( nEvt - 1 ) + lcmsValue ) / nEvt;
                ( *cmsMeanSq )[strip] = ( ( *cmsMeanSq )[strip] * ( nEvt - 1 ) + std::pow( lcmsValue, 2 ) ) / nEvt;
                ( *cmsNoise )[strip]  = sqrt( ( *cmsMeanSq )[strip] - std::pow( ( *cmsMean )[strip], 2 ) );
              }
            } // stripStatus
          }   // strip
        }     // beetle

        // Resets the event counter
        //       if( m_resetRate > 0  && nEvt%m_resetRate == 0 ) {
        //         (*nEventsPP)[pp] = 0;
        //       }
      } // FPGA-PP
    }   // boards
    return StatusCode::SUCCESS;
  }

  std::vector<double> UTOnlineNoiseCalculationTool::rawMean( const unsigned int TELL ) const {
    return m_rawMeanMap.find( TELL )->second;
  }
  std::vector<double> UTOnlineNoiseCalculationTool::rawMeanSq( const unsigned int TELL ) const {
    return m_rawMeanSqMap.find( TELL )->second;
  }
  std::vector<double> UTOnlineNoiseCalculationTool::rawNoise( const unsigned int TELL ) const {
    return m_rawNoiseMap.find( TELL )->second;
  }
  std::vector<unsigned int> UTOnlineNoiseCalculationTool::rawN( const unsigned int TELL ) const {
    return m_rawNEventsPP.find( TELL )->second;
  }

  std::vector<double> UTOnlineNoiseCalculationTool::cmsMean( const unsigned int TELL ) const {
    return m_rawMeanMap.find( TELL )->second;
  }
  std::vector<double> UTOnlineNoiseCalculationTool::cmsMeanSq( const unsigned int TELL ) const {
    return m_rawMeanSqMap.find( TELL )->second;
  }
  std::vector<double> UTOnlineNoiseCalculationTool::cmsNoise( const unsigned int TELL ) const {
    return m_rawNoiseMap.find( TELL )->second;
  }
  std::vector<unsigned int> UTOnlineNoiseCalculationTool::cmsN( const unsigned int TELL ) const {
    return m_rawNEventsPP.find( TELL )->second;
  }
} // namespace UT
