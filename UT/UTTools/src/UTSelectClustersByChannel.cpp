/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/IUTClusterSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"

/** @class UTSelectClustersByChannel UTSelectClustersByChannel.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectClustersByChannel : public extends<UT::ToolBase, IUTClusterSelector> {

public:
  /// constructor
  UTSelectClustersByChannel( const std::string& type, const std::string& name, const IInterface* parent );

  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTCluster* cluster ) const override;

private:
  IUTChannelIDSelector* m_selector{};
  std::string           m_selectorType;
  std::string           m_selectorName;
};

DECLARE_COMPONENT( UTSelectClustersByChannel )

UTSelectClustersByChannel::UTSelectClustersByChannel( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : extends( type, name, parent ) {
  declareProperty( "SelectorType", m_selectorType = "UTRndmEffSelector" );
  declareProperty( "SelectorName", m_selectorName = "Selector" );
}

StatusCode UTSelectClustersByChannel::initialize() {

  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  m_selector = tool<IUTChannelIDSelector>( m_selectorType, m_selectorName, this );

  info() << "Adding Tool: type: " << m_selectorType << " / name: " << m_selectorName << endmsg;

  return sc;
}

bool UTSelectClustersByChannel::select( const LHCb::UTCluster* cluster ) const { return ( *this )( cluster ); }

bool UTSelectClustersByChannel::operator()( const LHCb::UTCluster* cluster ) const {
  // just delegate to the channel selector
  return m_selector->select( cluster->channelID() );
}
