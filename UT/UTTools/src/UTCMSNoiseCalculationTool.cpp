/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTTELL1Data.h"
#include "Kernel/IUTNoiseCalculationTool.h" // Interface
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTDAQDefinitions.h"
#include "UTNoiseToolBase.h" // Base class for
#include <bitset>
#include <cmath>

//-----------------------------------------------------------------------------
// Implementation file for class : UTCMSNoiseCalculationTool
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTCMSNoiseCalculationTool UTCMSNoiseCalculationTool.h
 *
 *  This tool can be used to calculate raw noise from the input data.  It then has various
 *  different methods which can be used to perform pedestal and common mode subtraction on
 *  the raw ADC values.  The algorithm should *not* be used with round robin events.  It is
 *  useful to compare CMS algorithms when the full NZS data is available for each TELL1.
 *
 *  The following options can be configured for this algorithm:
 *
 *  @author A. Beiter (based on code by J. van Tilburg, N. Chiapolini, and M. Tobin)
 *  @date   2018-09-04
 *
 */
namespace UT {
  class UTCMSNoiseCalculationTool : public extends<UT::UTNoiseToolBase, UT::IUTNoiseCalculationTool> {

  public:
    /// Standard constructor
    UTCMSNoiseCalculationTool( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override; ///< Tool initialisation

  private:
    StatusCode calculateNoise() override; ///< calculation of noise

  private:
    /// Calculate the different PCN/header configuration for each Beetle port
    void                       calcPCNConfigs( const LHCb::UTTELL1Datas* data );
    std::vector<unsigned long> m_portHeader; /// PCN/Header configuration for each port in this event

    /// Calculation of pedestals
    void sumPedestals( const LHCb::UTTELL1Datas* data );
    /// Something to do with pedestals
    void divPedestals();

    unsigned int m_pedestalBuildup; ///< Number of events used to build pedestals

    bool m_skipCMS{}; ///< Skip calculation of CMS noise

  public:
    std::vector<double>       rawMean( const unsigned int TELL ) const override;
    std::vector<double>       rawMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       rawNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> rawN( const unsigned int TELL ) const override;

    std::vector<double>       cmsMean( const unsigned int TELL ) const override;
    std::vector<double>       cmsMeanSq( const unsigned int TELL ) const override;
    std::vector<double>       cmsNoise( const unsigned int TELL ) const override;
    std::vector<unsigned int> cmsN( const unsigned int TELL ) const override;

  private:
    /// Calculate the pedestal subtracted ADC values for one beetle
    void subtractPedestals( const std::vector<signed int>& BeetleADCs, std::vector<double>::const_iterator rawMeanIt,
                            std::vector<double>::const_iterator rawNoiseIt, const signed int beetle,
                            std::vector<std::pair<double, bool>>& pedSubADC, std::vector<double>& BeetleMeans );

    /// Remove PCN/Header corrected pedestals from raw adcs
    void subtractPCNPedestals( const std::vector<signed int>&                          RawADCs,
                               const std::vector<std::vector<std::pair<double, int>>>* pedestals,
                               const signed int beetle, std::vector<signed int>& PedSubADCs );

    /// Calculate the CM suppressed values for the strips given
    void cmsSimple( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans );

    /// Explain what this does? TO DO
    void tell1CMSIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans,
                            int iPort );

    /// Calculate the CM suppressed values for the strips given
    void cmsUT( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans );

    /// Explain what this does? TO DO
    void stIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans,
                      int iPort );

    /// Different routines to calculate the CMS noise in integer form (a la TELL1)
    void cmsUTInt( std::vector<std::pair<double, bool>>& BeetleTmpADCs );
    void stTell1Code( std::vector<std::pair<double, bool>>& BeetleTmpADCs, int iPort );

    /// Noise calculation in the style of the VELO
    void   cmsVelo( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans );
    void   veloHitDetection1( std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::vector<double>& BeetleMeans,
                              int iPort );
    void   veloHitDetection2( std::vector<std::pair<double, bool>>& BeetleTmpADCs, int iPort );
    double calcThreshold( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs, double nSigma );

    /// Noise calculation using the Achim method
    void cmsAchim( std::vector<std::pair<double, bool>>& BeetleTmpADCs );
    void achimIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs, int iPort );

    bool         m_dump{};        ///< Dump ADC values at various stages of the processing
    unsigned int m_printCMSSteps; ///< Number of CMS steps to print
    bool         m_printADCs;     ///< Print the ADCs at various stages of the processing

    /// Methods to dump ADCs at various stages of the processing.
    void dumpBeetleADCs( const std::vector<std::pair<double, bool>>& BeetleTmpADCs, std::string s );
    void dumpBeetleADCs( const std::vector<signed int>& BeetleADCs, std::string s );
    void dumpPortADCs( const std::vector<std::pair<double, bool>>& BeetleTmpADCs, int port, std::string s );
    void dumpPortADCs( const std::vector<double>& adc, int port, std::string s );

    /** Choose the CMS algorithm to use
        Supported algorithms are:
        Simple
        UT
        UTInt
        VELO
        Achim
    **/
    std::string m_cmsAlg;

    int  m_useAlgo{}; ///< Internal representation of algorithm being used
    void whichCMSAlg();
    int  m_useInts; ///< Use integers for the calculation of pedestals/cms noise

    bool m_printAlgError{}; ///<

    /// Threshold value for raw outlier rejection. value  in standard deviations.
    int  m_skipOutliers;   ///< Number of events to skip before starting outlier removal
    bool m_rawOutliers;    // Remove outliers in RAW noise calculation
    bool m_rawOutliersCMS; // Remove outliers in CMS noise calculation
    int  m_rawThreshold;   ///< Default value is 4

    double m_cmsThreshold;   ///< Threshold for removal of hits from CMS noise
    bool   m_fixedThreshold; ///< Use fixed threshold in outlier removal for CMS noise
    bool   m_zeroNeighbours; ///<

    bool m_cmsHitDetection; ///< Use the CMS Hit detection

    /// Set adc value to zero when
    void zeroHitsUseRawNoise( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs );
    void zeroHitsNormal( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs, double threshold );
    void zeroHits( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs, double threshold );

    void plotPedestals();
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT( UTCMSNoiseCalculationTool )
} // namespace UT

namespace {
  void saturate_for_8bits( LHCb::span<int> data ) {
    for ( auto& d : data ) d = std::clamp( d, -128, 127 );
  }
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTCMSNoiseCalculationTool::UTCMSNoiseCalculationTool( const std::string& type, const std::string& name,
                                                          const IInterface* parent )
    : extends{type, name, parent} {

  /// Which CMS algorithm should be used
  declareProperty( "CMSAlg", m_cmsAlg = "Simple" );

  /// Print ADC values during processing
  declareProperty( "PrintADCs", m_printADCs = false );
  declareProperty( "PrintCMSSteps", m_printCMSSteps = 1000 );

  // Number of events used to build the pedestals
  declareProperty( "PedestalBuildup", m_pedestalBuildup = 300 );

  // Read pedestal values from conditions database (change base class default)
  m_readPedestals = true;

  // Use integer algebra in calculations
  declareProperty( "UseIntegers", m_useInts = false );

  // Remove outliers
  declareProperty( "SkipOutlierRemoval", m_skipOutliers = 1000 );
  declareProperty( "RawThreshold", m_rawThreshold = 4 );
  declareProperty( "RawOutliers", m_rawOutliers = true );
  declareProperty( "RawOutliersCMS", m_rawOutliersCMS = false );

  declareProperty( "CMSHitDetection", m_cmsHitDetection = false );
  declareProperty( "CMUThreshold", m_cmsThreshold = 13.0 );
  declareProperty( "FixedThreshold", m_fixedThreshold = false );

  // Set neighbour strips to zero in CMS calculation
  declareProperty( "ZeroNeighbours", m_zeroNeighbours = true );
}

//==============================================================================
// initialize tool
//==============================================================================
StatusCode UT::UTCMSNoiseCalculationTool::initialize() {

  StatusCode sc = UT::UTNoiseToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // 4 ports per Beetle
  m_portHeader.resize( 4, 0 );

  m_printAlgError = true;

  m_skipCMS = true;

  // Choose CMS algorithm
  whichCMSAlg();

  // Resize maps/vectors for the CMS noise calculation
  auto itT = ( this->readoutTool() )->SourceIDToTELLNumberMap().begin();
  for ( ; itT != ( this->readoutTool() )->SourceIDToTELLNumberMap().end(); ++itT ) {
    unsigned int TELL1SourceID = ( *itT ).first;
    // 8 pedestal configs for each TELL1
    if ( !m_readPedestals ) {
      m_pedestalMaps[TELL1SourceID].resize( 8 );
      for ( int i = 0; i < 8; i++ ) { m_pedestalMaps[TELL1SourceID][i].resize( 3072, std::make_pair( 0.0, 0 ) ); }
    } else {
      //      m_pedestalBuildup = 0;
    }
  }
  return StatusCode::SUCCESS;
}

void UT::UTCMSNoiseCalculationTool::whichCMSAlg() {
  if ( m_cmsAlg == "Simple" ) {
    info() << "Simple CMS algorithm selected" << endmsg;
    m_useAlgo = 1;
  } else if ( m_cmsAlg == "UT" ) {
    info() << "UT CMS algorithm selected" << endmsg;
    m_useAlgo = 3;
  } else if ( m_cmsAlg == "UTInt" ) {
    info() << "Integer UT CMS algorithm selected" << endmsg;
    m_useAlgo = 4;
  } else if ( m_cmsAlg == "VELO" ) {
    info() << "Integer UT CMS algorithm selected" << endmsg;
    m_useAlgo = 5;
  } else if ( m_cmsAlg == "Achim" ) {
    info() << "Integer UT CMS algorithm selected" << endmsg;
    m_useAlgo = 6;
  } else {
    warning() << "No CMS subtraction algorithm is specified." << endmsg;
  }
  if ( m_useInts ) info() << "Using integers for pedestal/noise calculations" << endmsg;
}
//========================================================================================================================
//
// Data accessors
//
//========================================================================================================================

std::vector<double> UT::UTCMSNoiseCalculationTool::rawMean( const unsigned int TELL ) const {
  return m_rawMeanMap.find( TELL )->second;
}
std::vector<double> UT::UTCMSNoiseCalculationTool::rawMeanSq( const unsigned int TELL ) const {
  return m_rawMeanSqMap.find( TELL )->second;
}
std::vector<double> UT::UTCMSNoiseCalculationTool::rawNoise( const unsigned int TELL ) const {
  return m_rawNoiseMap.find( TELL )->second;
}
std::vector<unsigned int> UT::UTCMSNoiseCalculationTool::rawN( const unsigned int TELL ) const {
  return m_rawNEventsPP.find( TELL )->second;
}

std::vector<double> UT::UTCMSNoiseCalculationTool::cmsMean( const unsigned int TELL ) const {
  return m_cmsMeanMap.find( TELL )->second;
}
std::vector<double> UT::UTCMSNoiseCalculationTool::cmsMeanSq( const unsigned int TELL ) const {
  return m_cmsMeanSqMap.find( TELL )->second;
}
std::vector<double> UT::UTCMSNoiseCalculationTool::cmsNoise( const unsigned int TELL ) const {
  return m_cmsNoiseMap.find( TELL )->second;
}
std::vector<unsigned int> UT::UTCMSNoiseCalculationTool::cmsN( const unsigned int TELL ) const {
  return m_cmsNEventsPP.find( TELL )->second;
}

//=====================================================================================================================
//
// Here is where the noise is calculated
//
//=====================================================================================================================
StatusCode UT::UTCMSNoiseCalculationTool::calculateNoise() {

  // Get the data
  const LHCb::UTTELL1Datas* data = getIfExists<LHCb::UTTELL1Datas>( m_dataLocation );
  if ( !data ) {
    // Skip if there is no Tell1 data
    return StatusCode::SUCCESS;
  }
  if ( data->empty() ) return Warning( "Data is empty", StatusCode::SUCCESS, 10 );

  // Calculate the PCN/Header combinations for each Beetle port
  calcPCNConfigs( data );
  if ( m_pedestalBuildup > 0 ) {
    if ( !m_readPedestals ) {
      sumPedestals( data );
      if ( m_pedestalBuildup == 1 ) {
        divPedestals();
        info() << "Pedestals ready, starting noise calc." << endmsg;
      }
    }
    m_pedestalBuildup--;
    return StatusCode::SUCCESS;
  }

  m_evtNumber++;

  if ( m_evtNumber <= m_skipEvents ) {

    // return StatusCode::SUCCESS;
    m_skipCMS = true;

  } else {
    m_skipCMS = false;
  }
  // loop over the data
  auto iterBoard = data->begin();
  for ( ; iterBoard != data->end(); ++iterBoard ) {

    // get the tell board and the data headers
    unsigned int tellID = ( *iterBoard )->TELL1ID();
    // Limit to selected tell1s
    if ( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                            ( this->readoutTool() )->SourceIDToTELLNumber( tellID ) ) ) {
      continue;
    }
    const LHCb::UTTELL1Data::Data& dataValues = ( *iterBoard )->data();

    // Store TELL1s with an NZS bank
    m_tell1WithNZS.push_back( tellID );
    // Local vectors for given TELL1: output is CMS noise
    std::vector<double>*       cmsMean    = &m_cmsMeanMap[tellID];
    std::vector<double>*       cmsMeanSq  = &m_cmsMeanSqMap[tellID];
    std::vector<double>*       cmsNoise   = &m_cmsNoiseMap[tellID];
    std::vector<unsigned int>* cmsNEvents = &m_cmsNEventsPP[tellID];

    // Need to store raw noise: needed for outlier removal in pedestal sub.
    std::vector<double>*       rawMean    = &m_rawMeanMap[tellID];
    std::vector<double>*       rawMeanSq  = &m_rawMeanSqMap[tellID];
    std::vector<double>*       rawNoise   = &m_rawNoiseMap[tellID];
    std::vector<unsigned int>* rawNEvents = &m_rawNEventsPP[tellID];

    std::vector<double>* rawPedestal = &m_rawPedestalMap[tellID];

    std::vector<std::vector<std::pair<double, int>>>* pedestals = &m_pedestalMaps[tellID];

    // Loop over the PPs that have sent data
    std::vector<unsigned int> sentPPs = ( *iterBoard )->sentPPs();
    auto                      iPP     = sentPPs.begin();
    for ( ; iPP != sentPPs.end(); ++iPP ) {
      unsigned int pp = *iPP;

      // Count the number of events per PP
      ( *rawNEvents )[pp]++;
      if ( m_countRoundRobin ) this->countRoundRobin( tellID, pp );

      // Cumulative average up to m_followingPeriod; after that
      // exponential moving average
      int nCMS = 0;
      int nEvt = ( *rawNEvents )[pp];
      if ( m_followingPeriod > 0 && nEvt > m_followingPeriod ) nEvt = m_followingPeriod;

      if ( !m_skipCMS ) {
        ( *cmsNEvents )[pp]++;
        nCMS = ( *cmsNEvents )[pp];
        if ( m_followingPeriod > 0 && nCMS > m_followingPeriod ) { nCMS = m_followingPeriod; }
      }

      // Loop over the links (i.e. Beetles)
      unsigned int iBeetle = 0;
      for ( ; iBeetle < UTDAQ::nBeetlesPerPPx; ++iBeetle ) {
        unsigned int beetle = iBeetle + pp * UTDAQ::nBeetlesPerPPx;

        m_dump = false;

        // const BeetleADC& rawADC = dataValues[beetle];
        // Raw ADC values for Beetle
        std::vector<signed int>              rawADC( LHCbConstants::nStripsInBeetle, 0 );
        std::vector<std::pair<double, bool>> tmpADC( LHCbConstants::nStripsInBeetle, std::make_pair( 0.0, false ) );
        std::vector<double>                  meanPerPort( 4, 0.0 );

        dumpBeetleADCs( dataValues[beetle], "raw: " );

        copy( dataValues[beetle].begin(), dataValues[beetle].end(), rawADC.begin() );

        std::vector<signed int> pedSubADCs;
        if ( m_pedestalBuildup == 0 ) { subtractPCNPedestals( rawADC, pedestals, beetle, pedSubADCs ); }

        if ( !m_skipCMS ) {
          subtractPedestals( pedSubADCs, rawMean->begin(), rawNoise->begin(), beetle, tmpADC, meanPerPort );

          if ( beetle == m_printCMSSteps ) { m_dump = true; }
          switch ( m_useAlgo ) {
          case 1:
            cmsSimple( tmpADC, meanPerPort );
            break;
          case 3:
            cmsUT( tmpADC, meanPerPort );
            break;
          case 4:
            cmsUTInt( tmpADC );
            break;
          case 5:
            cmsVelo( tmpADC, meanPerPort );
            break;
          case 6:
            cmsAchim( tmpADC );
            break;
          default:
            if ( m_printAlgError ) {
              error() << "no CMS Algorithm selected" << endmsg;
              m_printAlgError = false;
            }
          }
        } // End of CMS calculation
        // Loop over the strips in this link
        unsigned int iStrip         = 0;
        auto         rawADCStrip    = rawADC.begin();
        auto         dataStrip      = pedSubADCs.begin();
        auto         tmpStrip       = tmpADC.begin();
        const int    strip_offset   = beetle * LHCbConstants::nStripsInBeetle;
        auto         rawPedStrip    = rawPedestal->begin() + strip_offset;
        auto         rawMeanStrip   = rawMean->begin() + strip_offset;
        auto         rawMeanSqStrip = rawMeanSq->begin() + strip_offset;
        auto         rawNoiseStrip  = rawNoise->begin() + strip_offset;
        auto         cmsMeanStrip   = cmsMean->begin() + strip_offset;
        auto         cmsMeanSqStrip = cmsMeanSq->begin() + strip_offset;
        auto         cmsNoiseStrip  = cmsNoise->begin() + strip_offset;
        for ( ; iStrip < LHCbConstants::nStripsInBeetle; ++iStrip ) {
          // Get the ADC value
          double valueRaw = *dataStrip;
          double valueCMS = tmpStrip->first;
          double valuePed = *rawADCStrip;
          if ( m_rawOutliers & tmpStrip->second & ( m_evtNumber > m_skipOutliers ) ) {
            valueRaw = *rawMeanStrip;
            if ( m_rawOutliersCMS ) { valueCMS = *cmsMeanStrip; }
            // continue;
          }

          // Calculate the pedestal and the pedestal squared
          *rawPedStrip    = ( *rawPedStrip * ( nEvt - 1 ) + valuePed ) / nEvt;
          *rawMeanStrip   = ( *rawMeanStrip * ( nEvt - 1 ) + valueRaw ) / nEvt;
          *rawMeanSqStrip = ( *rawMeanSqStrip * ( nEvt - 1 ) + std::pow( valueRaw, 2 ) ) / nEvt;
          *rawNoiseStrip  = sqrt( *rawMeanSqStrip - std::pow( *rawMeanStrip, 2 ) );

          // Calculate the mean(adc) and mean(adc^2) on cms values
          if ( !m_skipCMS ) {
            *cmsMeanStrip   = ( *cmsMeanStrip * ( nCMS - 1 ) + valueCMS ) / nCMS;
            *cmsMeanSqStrip = ( *cmsMeanSqStrip * ( nCMS - 1 ) + std::pow( valueCMS, 2 ) ) / nCMS;
            *cmsNoiseStrip  = sqrt( *cmsMeanSqStrip - std::pow( *cmsMeanStrip, 2 ) );
          }
          // move to next strip
          ++dataStrip;
          ++tmpStrip;
          ++rawADCStrip;
          ++rawPedStrip;
          ++rawMeanStrip;
          ++rawMeanSqStrip;
          ++rawNoiseStrip;
          ++cmsMeanStrip;
          ++cmsMeanSqStrip;
          ++cmsNoiseStrip;

        } // strip
      }   // beetle

      // Resets the event counter
      if ( m_resetRate > 0 && nEvt % m_resetRate == 0 ) { ( *rawNEvents )[pp] = 0; }

    } // FPGA-PP
  }   // boards
  return StatusCode::SUCCESS;
}

//==============================================================================
// Header bit from beetle [0][1][2] where 2 is LSB
// Header bit 0 is always low (0)
// PCN odd/even given by LSB of port 0
//
// Use header bit 0 to store PCN parity:
// Store eight different configurations
// 000, 001, 010, 011, 100, 101, 110, 111
//==============================================================================
void UT::UTCMSNoiseCalculationTool::calcPCNConfigs( const LHCb::UTTELL1Datas* data ) {
  const LHCb::UTTELL1Data::Data& header = ( *( data->begin() ) )->header();
  for ( int iPort = 0; iPort < 4; iPort++ ) {
    std::bitset<3> binary;
    if ( !m_readPedestals ) {
      // Header configs
      binary.set( 0, ( header[0][iPort * 3 + 2] >= 129 ) );
      binary.set( 1, ( header[0][iPort * 3 + 1] >= 129 ) );
      binary.set( 2, ( header[0][2] >= 129 ) );
      m_portHeader[iPort] = binary.to_ulong();
    } else {
      binary.set( 0, false );
      binary.set( 1, false );
      binary.set( 2, false );
      m_portHeader[iPort] = binary.to_ulong();
    }
  }
}
//==============================================================================
// Calculate sum of pedestals
// 1st of pair is sum of ADC values
// 2nd of pair is number of events used to build pedestals
//==============================================================================
void UT::UTCMSNoiseCalculationTool::sumPedestals( const LHCb::UTTELL1Datas* data ) {

  for ( auto& iterBoard : *data ) {
    unsigned int                   tellID     = iterBoard->TELL1ID();
    const LHCb::UTTELL1Data::Data& dataValues = iterBoard->data();

    // Limit to selected tell1s
    if ( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                            ( this->readoutTool() )->SourceIDToTELLNumber( tellID ) ) ) {
      continue;
    };
    auto& pedestalValues = m_pedestalMaps[tellID];

    // Loop over the PPs that have sent data
    for ( unsigned int pp : iterBoard->sentPPs() ) {
      for ( unsigned int iBeetle = 0; iBeetle < UTDAQ::nBeetlesPerPPx; ++iBeetle ) {
        unsigned int beetle    = iBeetle + pp * UTDAQ::nBeetlesPerPPx;
        auto         dataStrip = dataValues[beetle].begin();
        for ( int iPort = 0; iPort < 4; iPort++ ) {
          unsigned long header = m_portHeader[iPort];
          for ( unsigned int iStrip = 0; iStrip < LHCbConstants::nStripsInPort; ++iStrip ) {
            int strip = iStrip + iPort * LHCbConstants::nStripsInPort + beetle * LHCbConstants::nStripsInBeetle;
            pedestalValues[header][strip].first += *dataStrip;
            pedestalValues[header][strip].second++;
            dataStrip++;
          } // strip
        }   // port
      }     // beetle
    }       // FPGA-PP
  }         // boards
}

//==============================================================================
// Calculate Pedestal values
// Loops over all strips and calculates pedestal values.
//==============================================================================
void UT::UTCMSNoiseCalculationTool::divPedestals() {
  for ( auto& map : m_pedestalMaps ) { // TELL1s
    for ( auto& value : map.second ) { // Header configs
      for ( auto& i : value ) {        // channels
        if ( i.second > 0 ) {
          i.first /= i.second;
        } else {
          i.first = 0;
        }
      }
    }
  }
  plotPedestals();
}

void UT::UTCMSNoiseCalculationTool::plotPedestals() {

  for ( auto& map : m_pedestalMaps ) {
    int TELL = map.first;
    if ( !( m_selectedTells && !binary_search( m_limitToTell.begin(), m_limitToTell.end(),
                                               ( this->readoutTool() )->SourceIDToTELLNumber( TELL ) ) ) ) {
      int header = 0;
      for ( auto& value : map.second ) {
        std::string idh = "Pedestal, TELL " + std::to_string( ( this->readoutTool() )->SourceIDToTELLNumber( TELL ) ) +
                          ", header " + std::to_string( header++ );
        int strip = 0;
        for ( auto& itStrip : value ) { plot1D( strip++, idh, idh, -0.5, 3071.5, 3072, itStrip.first ); }
      }
    }
  }
}

// Subtract pedetals from ADC values..
/// Calculate the pedestal subtracted ADC values for one beetle
void UT::UTCMSNoiseCalculationTool::subtractPedestals( const std::vector<signed int>&        BeetleADCs,
                                                       std::vector<double>::const_iterator   rawMeansIt,
                                                       std::vector<double>::const_iterator   rawNoiseIt,
                                                       const signed int                      beetle,
                                                       std::vector<std::pair<double, bool>>& pedSubADC,
                                                       std::vector<double>&                  BeetleMeans ) {

  int  stripOffset = beetle * LHCbConstants::nStripsInBeetle;
  auto meanMapIt   = rawMeansIt + stripOffset;
  auto rmsMapIt    = rawNoiseIt + stripOffset;

  auto adcIt    = BeetleADCs.begin();
  auto pedSubIt = pedSubADC.begin();

  for ( int iPort = 0; iPort < 4; iPort++ ) {
    double mean = 0;

    for ( unsigned int iStrip = 0; iStrip < LHCbConstants::nStripsInPort; iStrip++ ) {
      // subtract pedestal
      if ( m_useInts ) {
        pedSubIt->first = *adcIt - floor( *meanMapIt + 0.5 );
      } else {
        pedSubIt->first = *adcIt - *meanMapIt;
      }

      // add ADC value to pedestal subtracted mean
      mean += pedSubIt->first;

      // flag outliers
      if ( ( *rmsMapIt > 0.01 ) && ( fabs( pedSubIt->first ) > m_rawThreshold * ( *rmsMapIt ) ) ) {
        pedSubIt->second = true;
      }

      // advance to next channel
      ++adcIt;
      ++pedSubIt;
      ++meanMapIt;
      ++rmsMapIt;
    }
    BeetleMeans[iPort] = mean / LHCbConstants::nStripsInPort;
  }
}

//==============================================================================
// Subtract the PCN dependent pedestals
//==============================================================================
void UT::UTCMSNoiseCalculationTool::subtractPCNPedestals(
    const std::vector<signed int>& RawADCs, const std::vector<std::vector<std::pair<double, int>>>* pedestals,
    const signed int beetle, std::vector<signed int>& PedSubADCs ) {

  int stripOffset = beetle * LHCbConstants::nStripsInBeetle;

  auto        adcIt  = RawADCs.begin();
  const auto* pedMap = pedestals;
  for ( int iPort = 0; iPort < 4; iPort++ ) {
    auto pedIt = ( *pedMap )[m_portHeader[iPort]].begin() + stripOffset;

    for ( unsigned int iStrip = 0; iStrip < LHCbConstants::nStripsInPort; iStrip++ ) {

      PedSubADCs.push_back( *adcIt - (int)floor( pedIt->first + 0.5 ) );

      ++adcIt;
      ++pedIt;
    }

    stripOffset += LHCbConstants::nStripsInPort;
  }
}

//==============================================================================
// CMS calculations
//==============================================================================
//
// Simple: TO DO: explain this.
void UT::UTCMSNoiseCalculationTool::cmsSimple( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                               std::vector<double>&                  BeetleMeans ) {

  for ( int iPort = 0; iPort < 4; ++iPort ) { tell1CMSIteration( BeetleTmpADCs, BeetleMeans, iPort ); }
}

//==============================================================================
// tell1CMSIteration: TO DO : explain what this does
//==============================================================================
void UT::UTCMSNoiseCalculationTool::tell1CMSIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                                       std::vector<double>& BeetleMeans, int iPort ) {

  const int    c_strips = LHCbConstants::nStripsInPort;
  const double c_divBy  = 2736.0; //(2<<12)/3.0;
  const double c_iMin   = -15.5;  //-16;

  const int c_stripOffset = iPort * c_strips;

  const auto adcBegin = BeetleTmpADCs.begin() + c_stripOffset;
  auto       adcIt    = adcBegin;

  // calculate slope
  double mean  = BeetleMeans[iPort];
  double slope = 0;

  double iStrip = c_iMin;
  for ( int i = 0; i < c_strips; i++ ) {
    adcIt->first -= mean;
    slope += iStrip * adcIt->first;

    // advance to next strip
    adcIt++;
    iStrip += 1.0;
  }
  slope /= c_divBy;

  // calculate common mode subtracted ADCs
  // and prepare mean for next round
  iStrip = c_iMin;
  adcIt  = adcBegin;
  for ( int i = 0; i < c_strips; i++ ) {
    adcIt->first -= slope * iStrip;

    // advance to next strip
    adcIt++;
    iStrip += 1.0;
  }
}

// UT: TO DO: explain this
void UT::UTCMSNoiseCalculationTool::cmsUT( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                           std::vector<double>&                  BeetleMeans ) {

  for ( int iPort = 0; iPort < 4; iPort++ ) { stIteration( BeetleTmpADCs, BeetleMeans, iPort ); }
}

// stIteration
/* ******************* *
 *  UT TEll1 Emulation *
 * ******************* */
void UT::UTCMSNoiseCalculationTool::stIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                                 std::vector<double>& BeetleMeans, int iPort ) {
  const int    c_strips = LHCbConstants::nStripsInPort;
  const double c_divBy  = 2736.0; //(2<<12)/3.0;
  const double c_iMin   = -15.5;  //-16;

  const int c_stripOffset = iPort * c_strips;

  const auto adcBegin = BeetleTmpADCs.begin() + c_stripOffset;
  auto       adcIt    = adcBegin;

  std::vector<std::pair<double, bool>> noHitADCs( c_strips, std::make_pair( 0.0, false ) );
  auto                                 noHitIt = noHitADCs.begin();

  double mean    = BeetleMeans[iPort];
  double thValue = m_cmsThreshold;
  if ( !m_fixedThreshold ) { thValue = -m_cmsThreshold; }

  if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "stIteration: s0: " );

  adcIt = adcBegin;
  for ( int i = 0; i < c_strips; i++ ) {
    adcIt->first -= mean;
    noHitIt->first = adcIt->first;

    // advance to next strip
    adcIt++;
    noHitIt++;
  }

  if ( m_dump ) dumpPortADCs( noHitADCs, 0, "stIteration: s1: " );

  noHitIt = noHitADCs.begin();
  zeroHits( noHitIt, thValue );

  if ( m_dump ) dumpPortADCs( noHitADCs, 0, "stIteration: s2: " );

  //-------------------------------second iteration
  //-------------------------------average

  mean    = 0;
  noHitIt = noHitADCs.begin();
  for ( int i = 0; i < c_strips; i++ ) {
    mean += noHitIt->first;
    noHitIt++;
  }
  mean /= c_strips;

  //-------------------------------subtract mean
  //-------------------------------and calc slope

  double slope = 0;

  noHitIt       = noHitADCs.begin();
  double iStrip = c_iMin;
  for ( int i = 0; i < c_strips; i++ ) {
    noHitIt->first -= mean;
    slope += iStrip * ( noHitIt->first );

    // advance to next strip
    noHitIt++;
    iStrip += 1.0;
  }
  slope /= c_divBy;

  if ( m_dump ) dumpPortADCs( noHitADCs, 0, "stIteration: s3: " );

  // calculate common mode subtracted ADCs
  iStrip = c_iMin;
  adcIt  = adcBegin;
  for ( int i = 0; i < c_strips; i++ ) {
    adcIt->first -= slope * iStrip + mean;
    // adcIt->first -= slope * iStrip;

    // advance to next strip
    adcIt++;
    iStrip += 1.0;
  }

  if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "stIteration: s4: " );
}

/// ZERO hits
void UT::UTCMSNoiseCalculationTool::zeroHitsUseRawNoise(
    std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs ) {
  if ( m_zeroNeighbours ) {

    if ( itBeetleTmpADCs->second ) {
      itBeetleTmpADCs->first         = 0;
      ( itBeetleTmpADCs + 1 )->first = 0;
    }
    itBeetleTmpADCs++;
    for ( unsigned i = 1; i < LHCbConstants::nStripsInPort - 1; i++ ) {
      if ( itBeetleTmpADCs->second ) {
        ( itBeetleTmpADCs - 1 )->first = 0;
        itBeetleTmpADCs->first         = 0;
        ( itBeetleTmpADCs + 1 )->first = 0;
      }
      itBeetleTmpADCs++;
    }
    if ( itBeetleTmpADCs->second ) {
      ( itBeetleTmpADCs - 1 )->first = 0;
      itBeetleTmpADCs->first         = 0;
    }
  } else {
    for ( unsigned i = 0; i < LHCbConstants::nStripsInPort; i++ ) {
      if ( itBeetleTmpADCs->second ) { itBeetleTmpADCs->first = 0; }
      itBeetleTmpADCs++;
    }
  }
}

void UT::UTCMSNoiseCalculationTool::zeroHitsNormal( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs,
                                                    double                                          threshold ) {
  if ( m_zeroNeighbours ) {
    double val = ( itBeetleTmpADCs + 1 )->first;

    if ( fabs( itBeetleTmpADCs->first ) > threshold ) {
      itBeetleTmpADCs->first         = 0;
      ( itBeetleTmpADCs + 1 )->first = 0;
    }
    itBeetleTmpADCs++;
    for ( unsigned i = 1; i < LHCbConstants::nStripsInPort - 1; i++ ) {
      if ( fabs( val ) > threshold ) {
        val                            = ( itBeetleTmpADCs + 1 )->first;
        ( itBeetleTmpADCs - 1 )->first = 0;
        itBeetleTmpADCs->first         = 0;
        ( itBeetleTmpADCs + 1 )->first = 0;
      } else {
        val = ( itBeetleTmpADCs + 1 )->first;
      }
      itBeetleTmpADCs++;
    }
    if ( fabs( val ) > threshold ) {
      ( itBeetleTmpADCs - 1 )->first = 0;
      itBeetleTmpADCs->first         = 0;
    }
    itBeetleTmpADCs++;
  } else {
    for ( unsigned i = 0; i < LHCbConstants::nStripsInPort; i++ ) {
      if ( fabs( itBeetleTmpADCs->first ) > threshold ) {
        // itBeetleTmpADCs->second = true;
        itBeetleTmpADCs->first = 0;
      }
      itBeetleTmpADCs++;
    }
  }
}

void UT::UTCMSNoiseCalculationTool::zeroHits( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs,
                                              double                                          threshold ) {
  if ( threshold < 0 ) {
    zeroHitsUseRawNoise( itBeetleTmpADCs );
  } else {
    zeroHitsNormal( itBeetleTmpADCs, threshold );
  }
}

//======================================================================================================================
//
// cmsUTInt: calculation of cms noise in integer representations
//
//======================================================================================================================
void UT::UTCMSNoiseCalculationTool::cmsUTInt( std::vector<std::pair<double, bool>>& BeetleTmpADCs ) {

  for ( int iPort = 0; iPort < 4; iPort++ ) { stTell1Code( BeetleTmpADCs, iPort ); }
}

void UT::UTCMSNoiseCalculationTool::stTell1Code( std::vector<std::pair<double, bool>>& BeetleTmpADCs, int iPort ) {
  int cms_threshold = (int)floor( m_cmsThreshold + 0.5 );

  long int mean_format;
  int      mean_temp;
  long int slope_format;
  int      slope_temp;
  int      data_without_hit[32], data_with_hit[32];

  const auto adcBegin = BeetleTmpADCs.begin() + iPort * LHCbConstants::nStripsInPort;
  auto       adcIt    = adcBegin;

  for ( int& strip : data_with_hit ) {
    strip = (int)floor( adcIt->first + 0.5 );
    adcIt++;
  }

  //--------------------------------------first iteration

  mean_format = 0x10;

  for ( int strip : data_with_hit ) {
    mean_format += strip; // sum_1
  }
  mean_temp = mean_format >> 5; // average

  for ( int strip = 0; strip < 32; strip++ ) {
    data_without_hit[strip] = data_with_hit[strip] - mean_temp;
    data_with_hit[strip]    = data_with_hit[strip] - mean_temp;
  }
  saturate_for_8bits( data_without_hit ); // saturate to -128~127
  saturate_for_8bits( data_with_hit );    // saturate to -128~127

  if ( abs( data_with_hit[0] ) > cms_threshold ) {
    data_without_hit[0] = 0;
    data_without_hit[1] = 0;
  }
  for ( int strip = 1; strip < 31; strip++ ) {
    if ( abs( data_with_hit[strip] ) > cms_threshold ) {
      data_without_hit[strip - 1] = 0;
      data_without_hit[strip]     = 0;
      data_without_hit[strip + 1] = 0;
    }
  }
  if ( abs( data_with_hit[31] ) > cms_threshold ) {
    data_without_hit[30] = 0;
    data_without_hit[31] = 0;
  }

  //--------------------------------------second iteration
  //------------------------------------- average

  mean_format = 0x10; // sum_1
  for ( int strip : data_without_hit ) {
    mean_format += strip; // sum_1
  }
  mean_temp = mean_format >> 5; // average

  for ( int strip = 0; strip < 32; strip++ ) {
    data_with_hit[strip] -= mean_temp;
    data_without_hit[strip] -= mean_temp;
  }
  saturate_for_8bits( data_with_hit );    // saturate to -128~127
  saturate_for_8bits( data_without_hit ); // saturate to -128~127

  //------------------------------------- slope

  slope_format = 0x20;
  for ( int strip = 0; strip < 32; strip++ ) { slope_format += ( strip - 16 ) * data_without_hit[strip]; }

  slope_temp = slope_format >> 6;

  if ( slope_temp > 127 )
    slope_temp = 127;
  else if ( slope_temp < -128 )
    slope_temp = -128;

  for ( int strip = 0; strip < 32; strip++ ) {
    data_with_hit[strip] -= ( ( slope_temp * ( strip - 16 ) * 3 ) + 64 ) >> 7;
  }
  saturate_for_8bits( data_with_hit ); // saturate to -128~127

  //------------------------------------------over
  adcIt = adcBegin;
  for ( int strip : data_with_hit ) {
    adcIt->first = strip;
    adcIt++;
  }
}

//======================================================================================================================
//
// VELO CMS calculation
//
//======================================================================================================================
void UT::UTCMSNoiseCalculationTool::cmsVelo( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                             std::vector<double>&                  BeetleMeans ) {
  int iPort = 0;
  for ( ; iPort < 4; iPort++ ) {

    if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "cmsVelo: s0: " );
    // first iteration
    tell1CMSIteration( BeetleTmpADCs, BeetleMeans, iPort );

    if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "cmsVelo: s1: " );
    // first hit detection
    veloHitDetection1( BeetleTmpADCs, BeetleMeans, iPort );

    if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "cmsVelo: s2: " );
    // second iteration
    tell1CMSIteration( BeetleTmpADCs, BeetleMeans, iPort );

    if ( m_dump ) dumpPortADCs( BeetleTmpADCs, iPort, "cmsVelo: s3: " );

    // final hit detection
    //   implemented with const treshold value
    // veloHitDetection2( BeetleTmpADCs, iPort);
    //
    // if (m_dump) {
    //  info() << "s4: ";
    //  dumpPortADCs(BeetleTmpADCs, iPort, "cmsVelo: s0: ");
    //}
  }
}

void UT::UTCMSNoiseCalculationTool::veloHitDetection1( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                                       std::vector<double>& BeetleMeans, int iPort ) {
  const int c_strips      = LHCbConstants::nStripsInPort;
  const int c_stripOffset = iPort * c_strips;

  int i;

  auto adcIt = BeetleTmpADCs.begin() + c_stripOffset;

  double thValue = m_cmsThreshold;
  if ( !m_fixedThreshold ) {
    thValue = calcThreshold( adcIt, m_cmsThreshold );
    adcIt -= c_strips;
  }

  // find outliers and set adc to 0
  zeroHits( adcIt, thValue );

  // calculate mean
  adcIt -= c_strips;
  double mean = 0;
  for ( i = 0; i < c_strips; i++ ) {
    mean += adcIt->first;
    adcIt++;
  }
  BeetleMeans[iPort] = mean / c_strips;
}

void UT::UTCMSNoiseCalculationTool::veloHitDetection2( std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                                       int                                   iPort ) {
  const int c_strips      = LHCbConstants::nStripsInPort;
  const int c_stripOffset = iPort * c_strips;

  auto adcIt = BeetleTmpADCs.begin() + c_stripOffset;

  int i;
  for ( i = 0; i < c_strips; i++ ) {
    if ( adcIt->first > m_cmsThreshold ) { adcIt->first = 0; }
    adcIt++;
  }
}

double UT::UTCMSNoiseCalculationTool::calcThreshold( std::vector<std::pair<double, bool>>::iterator& itBeetleTmpADCs,
                                                     double                                          nSigma ) {
  double threshold = 0;
  for ( unsigned int i = 0; i < LHCbConstants::nStripsInPort; ++i ) {
    threshold += std::pow( itBeetleTmpADCs->first, 2 );
    itBeetleTmpADCs++;
  }

  return nSigma * sqrt( threshold / 32.0 );
}

//======================================================================================================================
//
// CMS calculation using Achim method
//
//======================================================================================================================
void UT::UTCMSNoiseCalculationTool::cmsAchim( std::vector<std::pair<double, bool>>& BeetleTmpADCs ) {
  int iPort = 0;
  for ( ; iPort < 4; iPort++ ) { achimIteration( BeetleTmpADCs, iPort ); }
}

/**
 * Achims proposed algorithm.
 *   if called with m_useInts = true, the input and output
 *   ADCs get rounded to integer values but doubles are
 *   used for mean, slope and shift, as these values
 *   should be calculated with a higher precision
 *   internally anyway.
 */
void UT::UTCMSNoiseCalculationTool::achimIteration( std::vector<std::pair<double, bool>>& BeetleTmpADCs, int iPort ) {
  const int c_strips      = LHCbConstants::nStripsInPort;
  const int c_stripOffset = iPort * c_strips;

  auto adcBegin = BeetleTmpADCs.begin() + c_stripOffset;
  auto adcIt    = adcBegin;

  // calculate slope
  double meanY[] = {0, 0};
  double meanX[] = {0, 0};
  double N[]     = {0, 0};

  int i, j;
  for ( j = 0; j < 2; j++ ) {
    for ( i = 0; i < c_strips / 2; i++ ) {
      if ( m_useInts ) { adcIt->first = floor( adcIt->first + 0.5 ); }

      // only add the channel if
      //   m_cmsHitDetection is true and the channel is not flagged
      // or if
      //   m_cmsHitDetection is false
      if ( !m_cmsHitDetection | !adcIt->second ) {
        meanY[j] += adcIt->first;
        meanX[j] += i;
        N[j] += 1.0;
      }

      // advance to next strip
      adcIt++;
    }
    meanY[j] /= N[j];
    meanX[j] /= N[j];
  }
  meanX[1] += c_strips / 2;

  double slope = ( meanY[1] - meanY[0] ) / ( meanX[1] - meanX[0] );
  double shift = meanY[0] - slope * meanX[0];

  // info() << "slope/shift: " << slope << " / " << shift << endmsg;

  // calculate common mode subtracted ADCs
  // and prepare mean for next round
  adcIt = adcBegin;
  if ( m_useInts ) {
    for ( i = 0; i < c_strips; i++ ) {
      adcIt->first -= floor( slope * i + shift + 0.5 );
      adcIt++;
    }
  } else {
    for ( i = 0; i < c_strips; i++ ) {
      adcIt->first -= slope * i + shift;
      adcIt++;
    }
  }
}

/**
 * DUMP Functions
 */

void UT::UTCMSNoiseCalculationTool::dumpBeetleADCs( const std::vector<std::pair<double, bool>>& BeetleTmpADCs,
                                                    std::string                                 s ) {
  if ( !m_printADCs | m_skipCMS ) { return; }

  auto adcIt = BeetleTmpADCs.begin();

  info() << s;
  unsigned int i;
  for ( i = 0; i < LHCbConstants::nStripsInBeetle; i++ ) {
    info() << adcIt->first << ", ";
    adcIt++;
  }
  info() << endmsg;
}

void UT::UTCMSNoiseCalculationTool::dumpBeetleADCs( const std::vector<signed int>& BeetleADCs, std::string s ) {
  if ( !m_printADCs | m_skipCMS ) { return; }

  auto adcIt = BeetleADCs.begin();

  info() << s;
  unsigned int i;
  for ( i = 0; i < LHCbConstants::nStripsInBeetle; i++ ) {
    info() << *adcIt << ", ";
    adcIt++;
  }
  info() << endmsg;
}

void UT::UTCMSNoiseCalculationTool::dumpPortADCs( const std::vector<std::pair<double, bool>>& BeetleTmpADCs, int port,
                                                  std::string s ) {
  auto adcIt = BeetleTmpADCs.begin() + LHCbConstants::nStripsInPort * port;

  info() << s;

  for ( unsigned int i = 0; i < LHCbConstants::nStripsInPort; i++ ) {
    info() << adcIt->first << ", ";
    adcIt++;
  }
  info() << endmsg;
}

void UT::UTCMSNoiseCalculationTool::dumpPortADCs( const std::vector<double>& adc, int port, std::string s ) {
  auto adcIt = adc.begin() + LHCbConstants::nStripsInPort * port;

  info() << s;
  unsigned int i;
  for ( i = 0; i < LHCbConstants::nStripsInPort; i++ ) {
    info() << *adcIt << ", ";
    adcIt++;
  }
  info() << endmsg;
}
