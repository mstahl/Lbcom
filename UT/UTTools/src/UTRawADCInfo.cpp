/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTTELL1Data.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/IUTRawADCInfo.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBeetleRepresentation.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTStripRepresentation.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

/** @class UTSelectChannelIDByElement UTSelectChannelIDByElement.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTRawADCInfo : public extends<UT::ToolBase, IUTRawADCInfo, IIncidentListener> {

public:
  /// constructer
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  StatusCode link( const LHCb::UTChannelID& chan, unsigned int& value ) const override;

  StatusCode link( const std::vector<LHCb::UTChannelID>& chans, std::vector<unsigned int>& values ) const override;

  StatusCode link( const LHCb::UTChannelID& chan, const unsigned int window,
                   std::vector<unsigned int>& values ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to nform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  DataObjectReadHandle<LHCb::UTTELL1Datas> m_dataLocation{this, "dataLocation", LHCb::UTTELL1DataLocation::UTFull};
  mutable LHCb::UTTELL1Datas*              m_data = nullptr;
};

DECLARE_COMPONENT( UTRawADCInfo )

StatusCode UTRawADCInfo::initialize() {
  return extends::initialize().andThen( [&] {
    // Add incident at begin of each event
    incSvc()->addListener( this, IncidentType::BeginEvent );
  } );
}

void UTRawADCInfo::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) m_data = nullptr;
}

StatusCode UTRawADCInfo::link( const LHCb::UTChannelID& chan, unsigned int& value ) const {

  // initialize this event
  if ( !m_data ) m_data = m_dataLocation.get();

  StatusCode sc = StatusCode::SUCCESS;

  // get the Tell1 this channel corresponds to
  UTDAQ::chanPair    tPair     = readoutTool()->offlineChanToDAQ( chan, 0 );
  LHCb::UTTELL1Data* tell1Data = m_data->object( tPair.first.id() );
  if ( tell1Data ) {
    // we have NZS data for this board, convert strip to beetle rep
    UTDAQ::UTBeetleRepresentation beetleRep =
        UTDAQ::UTBeetleRepresentation( UTDAQ::UTStripRepresentation( tPair.second ) );
    unsigned int beetle, strip;
    beetleRep.decompose( beetle, strip );

    // get the data
    value = tell1Data->data()[beetle][strip];

  } else {
    // too bad
    sc = StatusCode::FAILURE;
  }
  return sc;
}

StatusCode UTRawADCInfo::link( const std::vector<LHCb::UTChannelID>& chans, std::vector<unsigned int>& values ) const {

  unsigned int val;
  StatusCode   sc( StatusCode::SUCCESS );
  for ( LHCb::UTChannelID aChan : chans ) {
    if ( link( aChan, val ).isSuccess() ) {
      values.push_back( val );
    } else {
      values.push_back( 999 );
      sc = StatusCode::FAILURE;
    }
  }
  return sc;
}

StatusCode UTRawADCInfo::link( const LHCb::UTChannelID& chan, const unsigned int window,
                               std::vector<unsigned int>& values ) const {

  StatusCode         sc         = StatusCode::SUCCESS;
  const DeUTSector*  sector     = findSector( chan );
  const unsigned int firststrip = chan.strip() - window;
  const unsigned int laststrip  = chan.strip() + window;

  for ( unsigned int i = firststrip; i <= laststrip; ++i ) {
    if ( sector->isStrip( i ) ) {
      unsigned int      val;
      LHCb::UTChannelID chanStrip = sector->stripToChan( i );
      if ( link( chanStrip, val ).isSuccess() ) {
        values.push_back( val );
      } else {
        values.push_back( 999 );
        sc = StatusCode::FAILURE;
      }
    } // isStrip
  }   // i
  return sc;
}
