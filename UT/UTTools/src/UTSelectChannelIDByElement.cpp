/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"

/** @class UTSelectChannelIDByElement UTSelectChannelIDByElement.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByElement : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructer
  using extends::extends;

  /// intialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

private:
  std::vector<const DeUTBaseElement*>       m_detElements;
  Gaudi::Property<std::vector<std::string>> m_elementNames{this, "elementNames", {}};
};

DECLARE_COMPONENT( UTSelectChannelIDByElement )

StatusCode UTSelectChannelIDByElement::initialize() {

  StatusCode sc = UT::ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  for ( const auto& name : m_elementNames.value() ) {
    const DeUTBaseElement* detElement = tracker()->findTopLevelElement( name );
    if ( !detElement ) { return Error( "Failed to find detector element", StatusCode::FAILURE ); }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Adding " << name << " to element list" << endmsg;
    m_detElements.push_back( detElement );
  } // for each

  return sc;
}

bool UTSelectChannelIDByElement::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectChannelIDByElement::operator()( const LHCb::UTChannelID& id ) const {
  auto iterElem = std::find_if( m_detElements.begin(), m_detElements.end(),
                                [&]( const DeUTBaseElement* elem ) { return elem->contains( id ); } );
  return iterElem != m_detElements.end();
}
