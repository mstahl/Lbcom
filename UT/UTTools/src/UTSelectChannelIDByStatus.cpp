/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/StatusMap.h"
#include <string>
#include <vector>

/** @class UTSelectChannelIDByStatus UTSelectChannelIDByStatus.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByStatus : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructer
  using extends::extends;

  /// intialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

private:
  Gaudi::Property<std::vector<std::string>> m_statusNames{this, "allowedStatus", {"OK"}};
  std::vector<DeUTSector::Status>           m_statusList;
};

DECLARE_COMPONENT( UTSelectChannelIDByStatus )

StatusCode UTSelectChannelIDByStatus::initialize() {

  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  // to save time convert the strings to a list of status
  const auto& statusMap = ::Status::statusDescription();
  for ( const auto& statusString : m_statusNames ) {
    info() << "Adding status " << statusString << " to list" << endmsg;
    // try to find the status in the map
    auto iterMap =
        std::find_if( statusMap.begin(), statusMap.end(), [&]( const auto& s ) { return s.second == statusString; } );
    if ( iterMap == statusMap.end() ) {
      return Error( "Failed to find status " + statusString + " in map", StatusCode::FAILURE );
    }
    m_statusList.push_back( iterMap->first );
  } // for each in string vector

  return sc;
}

bool UTSelectChannelIDByStatus::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectChannelIDByStatus::operator()( const LHCb::UTChannelID& id ) const {
  const DeUTSector::Status theStatus = findSector( id )->stripStatus( id );
  return std::find( m_statusList.begin(), m_statusList.end(), theStatus ) != m_statusList.end();
}
