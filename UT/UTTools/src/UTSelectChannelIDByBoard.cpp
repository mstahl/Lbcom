/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTToolBase.h"
#include <algorithm>
#include <vector>

/** @class UTSelectChannelIDByBoard UTSelectChannelIDByBoard.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByBoard : public extends<UT::ToolBase, IUTChannelIDSelector> {

public:
  /// constructer
  using extends::extends;
  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::UTChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::UTChannelID& id ) const override;

private:
  Gaudi::Property<std::vector<unsigned int>> m_boards{this, "Boards", {}};
  std::vector<unsigned int>                  m_sectors;
};

DECLARE_COMPONENT( UTSelectChannelIDByBoard )

StatusCode UTSelectChannelIDByBoard::initialize() {

  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  // to save time get a list of all the ids on this box
  m_sectors.reserve( 8 * m_boards.size() );
  for ( unsigned int id : m_boards ) {
    const auto& boardSectors = readoutTool()->sectorIDs( UTTell1ID( id ) );
    std::transform( boardSectors.begin(), boardSectors.end(), std::back_inserter( m_sectors ),
                    []( const LHCb::UTChannelID& chan ) { return chan.uniqueSector(); } );
  } // for each board
  std::sort( m_sectors.begin(), m_sectors.end() );

  return sc;
}

bool UTSelectChannelIDByBoard::select( const LHCb::UTChannelID& id ) const { return ( *this )( id ); }

bool UTSelectChannelIDByBoard::operator()( const LHCb::UTChannelID& id ) const {
  return std::binary_search( m_sectors.begin(), m_sectors.end(), id.uniqueSector() );
}
