###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichAlgorithms
################################################################################
gaudi_subdir(RichAlgorithms v1r9)

gaudi_depends_on_subdirs(Det/RichDet
                         Rich/RichKernel)

find_package(Boost)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} 
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(RichAlgorithms
                 src/*.cpp
                 LINK_LIBRARIES RichDetLib RichKernelLib DAQEventLib)

