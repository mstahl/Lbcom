/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichLoadRawEvent.cpp
 *
 *  Implementation file for RICH DAQ algorithm : RichLoadRawEvent
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-09
 */
//-----------------------------------------------------------------------------

// local
#include "RichLoadRawEvent.h"

// RICH DAQ
using namespace Rich::DAQ;

//-----------------------------------------------------------------------------

DECLARE_COMPONENT( LoadRawEvent )

// Standard constructor, initializes variables
LoadRawEvent::LoadRawEvent( const std::string& name, ISvcLocator* pSvcLocator ) : Rich::AlgBase( name, pSvcLocator ) {
  declareProperty( "RawEventLocation", m_rawEventLoc = LHCb::RawEventLocation::Rich );
}

// Main execution
StatusCode LoadRawEvent::execute() {
  // load raw event data object
  getIfExists<LHCb::RawEvent>( m_rawEventLoc );

  // return OK
  return StatusCode::SUCCESS;
}
