/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichHighOccHPDSuppressionTool.cpp
 *
 * Implementation file for class : RichHighOccHPDSuppressionTool
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 21/03/2006
 */
//-----------------------------------------------------------------------------

// local
#include "RichHighOccHPDSuppressionTool.h"

// RICH DAQ
using namespace Rich::Future;
using namespace Rich::Future::DAQ;

DECLARE_COMPONENT( HighOccHPDSuppressionTool )

// Standard constructor
HighOccHPDSuppressionTool::HighOccHPDSuppressionTool( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : ToolBase( type, name, parent ) {
  // Define interface
  declareInterface<IPixelSuppressionTool>( this );
}

bool HighOccHPDSuppressionTool::applyPixelSuppression( const LHCb::RichSmartID    hpdID,
                                                       LHCb::RichSmartID::Vector& smartIDs ) const {
  // default is no suppression
  bool suppress = false;

  // HPD ID OK ?
  if ( hpdID.isValid() ) {

    // Occupancy for this HPD in current event
    const auto occ = smartIDs.size();

    // is this HPD suppressed
    suppress = ( occ > m_overallMax );
    if ( suppress ) {
      // Print message
      if ( m_sumPrint ) {
        std::ostringstream hpd;
        hpd << hpdID;
        Info( "Fully suppressed     " + hpd.str(), StatusCode::SUCCESS, 0 ).ignore();
      }
      // clear vector (i.e. actually do the suppression)
      smartIDs.clear();
    }
  }

  // return status
  return suppress;
}
