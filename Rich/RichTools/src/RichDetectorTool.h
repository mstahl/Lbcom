/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichDetectorTool.h
 *
 *  Header file for tool : Rich::DetectorTool
 *
 *  @author Antonis Papanestis
 *  @date   2012-10-26
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <vector>

// Base class and interface
#include "RichInterfaces/IRichDetectorTool.h"
#include "RichKernel/RichToolBase.h"

// RichDet
#include "RichDet/DeRich.h"

namespace Rich {
  //---------------------------------------------------------------------------
  /** @class DetectorTool RichDetectorTool.h
   *
   *  A tool to preform the manipulation of RichDetector channel identifiers
   *
   *  @author Antonis Papanestis
   *  @date   2012-11-01
   *
   */
  //---------------------------------------------------------------------------

  class DetectorTool final : public Rich::ToolBase, virtual public IDetectorTool {

  public: // Methods for Gaudi Framework
    /// Standard constructor
    DetectorTool( const std::string& type, const std::string& name, const IInterface* parent );

  public: // methods (and doxygen comments) inherited from interface
    // method to return pointer to existing Rich detectors
    std::vector<DeRich*> deRichDetectors() const override;
  };

} // namespace Rich
