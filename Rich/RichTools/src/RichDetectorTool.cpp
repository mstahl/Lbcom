/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichDetectorTool.cpp
 *
 * Implementation file for class : Rich::DetectorTool
 *
 * @author Antonis Papanestis
 * @date 2012-10-26
 */
//-----------------------------------------------------------------------------

// local
#include "RichDetectorTool.h"

DECLARE_COMPONENT( Rich::DetectorTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Rich::DetectorTool::DetectorTool( const std::string& type, const std::string& name, const IInterface* parent )
    : Rich::ToolBase( type, name, parent ) {
  // Interface
  declareInterface<IDetectorTool>( this );
}

//=========================================================================
//  deRichDetectors
//=========================================================================
std::vector<DeRich*> Rich::DetectorTool::deRichDetectors() const {
  // find the DeRich objects
  std::vector<std::string> locations;

  DetectorElement* afterMagnet = getDet<DetectorElement>( "/dd/Structure/LHCb/AfterMagnetRegion" );
  if ( afterMagnet->exists( "RichDetectorLocations" ) ) {
    locations = afterMagnet->paramVect<std::string>( "RichDetectorLocations" );
  } else {
    locations = {DeRichLocations::Rich1, DeRichLocations::Rich2};
  }

  std::vector<DeRich*> deRichDets;
  deRichDets.reserve( locations.size() );

  for ( const auto& loc : locations ) { deRichDets.push_back( getDet<DeRich>( loc ) ); }

  return deRichDets;
}
