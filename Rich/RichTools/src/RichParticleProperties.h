/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichParticleProperties.h
 *
 *  Header file for tool : Rich::ParticleProperties
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// STL
#include <array>
#include <cmath>
#include <vector>

// Utils
#include "RichUtils/RichTrackSegment.h"

// base class
#include "RichKernel/RichToolBase.h"

// interfaces
#include "RichInterfaces/IRichParticleProperties.h"

// boost
#include "boost/limits.hpp"
#include "boost/numeric/conversion/bounds.hpp"

namespace Rich {

  //-----------------------------------------------------------------------------
  /** @class ParticleProperties RichParticleProperties.h
   *
   *  Tool to calculate various physical properties
   *  for the different mass hypotheses.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   */
  //-----------------------------------------------------------------------------

  class ParticleProperties final : public Rich::ToolBase, virtual public IParticleProperties {

  public:
    // Methods for Gaudi Framework

    /// Standard constructor
    ParticleProperties( const std::string& type, const std::string& name, const IInterface* parent );

    // Initialize method
    StatusCode initialize() override;

  public:
    // methods (and doxygen comments) inherited from public interface

    // Returns 'beta' for given particle hypothesis
    ScType beta( const ScType ptot, const Rich::ParticleIDType id ) const override;

    /// Access the array of all particle masses
    const Rich::ParticleArray<ScType>& masses() const override { return m_particleMass; }

    // Returns the nominal mass for a given particle type
    ScType mass( const Rich::ParticleIDType id ) const override;

    // Returns the nominal mass squared for a given particle type
    ScType massSq( const Rich::ParticleIDType id ) const override;

    // Vector of the mass hypotheses to be considered
    const Rich::Particles& particleTypes() const override;

    // Access the minimum cherenkov photon energies (Aero/R1Gas/R2Gas)
    const RadiatorArray<float>& minPhotonEnergy() const override { return m_minPhotEn.value(); }

    // Access the minimum cherenkov photon energies (Aero/R1Gas/R2Gas)
    const RadiatorArray<float>& maxPhotonEnergy() const override { return m_maxPhotEn.value(); }

    // Access the minimum cherenkov photon energy for given radiator
    float minPhotonEnergy( const Rich::RadiatorType rad ) const override { return m_minPhotEn[rad]; }

    // Access the minimum cherenkov photon energy for given radiator
    float maxPhotonEnergy( const Rich::RadiatorType rad ) const override { return m_maxPhotEn[rad]; }

    // Access the mean cherenkov photon energies (Aero/R1Gas/R2Gas)
    float meanPhotonEnergy( const Rich::RadiatorType rad ) const override {
      return 0.5f * ( m_maxPhotEn[rad] + m_minPhotEn[rad] );
    }

  private:
    // Private data

    /// The minimum photon energies
    Gaudi::Property<RadiatorArray<float>> m_minPhotEn{this, "MinPhotonEnergy", {1.75, 1.75, 1.75}};

    /// The maximum photon energies
    Gaudi::Property<RadiatorArray<float>> m_maxPhotEn{this, "MaxPhotonEnergy", {4.0, 7.0, 7.0}};

    /// Array containing particle masses
    Rich::ParticleArray<ScType> m_particleMass = {{}};

    /// Array containing square of particle masses
    Rich::ParticleArray<ScType> m_particleMassSq = {{}};

    /// Particle ID types to consider in the likelihood minimisation (JO)
    std::vector<std::string> m_pidTypesJO;

    /// Particle ID types to consider in the likelihood minimisation
    Rich::Particles m_pidTypes{Rich::particles()};
  };

} // namespace Rich
