/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichParticleProperties.cpp
 *
 *  Implementation file for tool : Rich::ParticleProperties
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichParticleProperties.h"

// From LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

//-----------------------------------------------------------------------------

DECLARE_COMPONENT( Rich::ParticleProperties )

// Standard constructor
Rich::ParticleProperties::ParticleProperties( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : Rich::ToolBase( type, name, parent ) {
  // initialise
  m_particleMass.fill( 0 );
  m_particleMassSq.fill( 0 );
  // declare interface
  declareInterface<IParticleProperties>( this );
  // PID types
  declareProperty( "ParticleTypes", m_pidTypesJO );
  // setProperty( "OutputLevel", 2 );
}

StatusCode Rich::ParticleProperties::initialize() {
  // Sets up various tools and services
  StatusCode sc = Rich::ToolBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Retrieve particle property service
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  // Retrieve particle masses
  m_particleMass[Rich::Electron]       = ppSvc->find( "e+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Muon]           = ppSvc->find( "mu+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Pion]           = ppSvc->find( "pi+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Kaon]           = ppSvc->find( "K+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Proton]         = ppSvc->find( "p+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Deuteron]       = ppSvc->find( "deuteron" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::BelowThreshold] = boost::numeric::bounds<ScType>::highest();

  // cache squares of masses
  m_particleMassSq[Rich::Electron]       = std::pow( m_particleMass[Rich::Electron], 2 );
  m_particleMassSq[Rich::Muon]           = std::pow( m_particleMass[Rich::Muon], 2 );
  m_particleMassSq[Rich::Pion]           = std::pow( m_particleMass[Rich::Pion], 2 );
  m_particleMassSq[Rich::Kaon]           = std::pow( m_particleMass[Rich::Kaon], 2 );
  m_particleMassSq[Rich::Proton]         = std::pow( m_particleMass[Rich::Proton], 2 );
  m_particleMassSq[Rich::Deuteron]       = std::pow( m_particleMass[Rich::Deuteron], 2 );
  m_particleMassSq[Rich::BelowThreshold] = boost::numeric::bounds<ScType>::highest();

  // release service
  sc = release( ppSvc );

  // PID Types
  if ( !m_pidTypesJO.empty() ) {
    m_pidTypes.clear();
    for ( const auto& S : m_pidTypesJO ) {
      if ( "electron" == S ) {
        m_pidTypes.push_back( Rich::Electron );
      } else if ( "muon" == S ) {
        m_pidTypes.push_back( Rich::Muon );
      } else if ( "pion" == S ) {
        m_pidTypes.push_back( Rich::Pion );
      } else if ( "kaon" == S ) {
        m_pidTypes.push_back( Rich::Kaon );
      } else if ( "proton" == S ) {
        m_pidTypes.push_back( Rich::Proton );
      } else if ( "deuteron" == S ) {
        m_pidTypes.push_back( Rich::Deuteron );
      } else if ( "belowThreshold" == S ) {
        m_pidTypes.push_back( Rich::BelowThreshold );
      } else {
        return Error( "Unknown particle type from options " + S );
      }
    }
  }
  // sort the list to ensure strict (increasing) mass ordering.
  std::sort( m_pidTypes.begin(), m_pidTypes.end() );
  // is pion in the list ?
  const bool hasPion = std::find( m_pidTypes.begin(), m_pidTypes.end(), Rich::Pion ) != m_pidTypes.end();
  info() << "Particle types considered = " << m_pidTypes << endmsg;
  if ( m_pidTypes.empty() ) return Error( "No particle types specified" );
  if ( !hasPion ) return Error( "Pion hypothesis must be enabled" );

  return sc;
}

Rich::IParticleProperties::ScType Rich::ParticleProperties::beta( const ScType               ptot,
                                                                  const Rich::ParticleIDType id ) const {
  const ScType Esquare = ptot * ptot + m_particleMassSq[id];
  return ( Rich::BelowThreshold != id && Esquare > 0 ? ptot / std::sqrt( Esquare ) : 0 );
}

Rich::IParticleProperties::ScType Rich::ParticleProperties::mass( const Rich::ParticleIDType id ) const {
  return m_particleMass[id];
}

Rich::IParticleProperties::ScType Rich::ParticleProperties::massSq( const Rich::ParticleIDType id ) const {
  return m_particleMassSq[id];
}

const Rich::Particles& Rich::ParticleProperties::particleTypes() const { return m_pidTypes; }
