/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichMCCKPhotonPropAlg.h
 *
 * Header file for monitor algorithm Rich::MC::MCCKPhotonPropAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-03
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"

// base class
#include "RichKernel/RichHistoAlgBase.h"

// MCEvent
#include "Event/MCRichOpticalPhoton.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichStatDivFunctor.h"

// tool Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RichInterfaces/IRichRefractiveIndex.h"

// boost
#include "boost/assign/list_of.hpp"

// GSL
#include "gsl/gsl_math.h"

namespace Rich::MC {

  //-----------------------------------------------------------------------------
  /** @class MCCKPhotonPropAlg RichMCCKPhotonPropAlg.h
   *
   *  Monitor algorithm to study the properties of the RICH MC CK photons
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   2006-11-03
   */
  //-----------------------------------------------------------------------------

  class MCCKPhotonPropAlg final : public Rich::HistoAlgBase {

  public:
    /// Standard constructor
    MCCKPhotonPropAlg( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:                                        // data
    const IRefractiveIndex* m_refIndex = nullptr; ///< Refractive index tool
  };

} // namespace Rich::MC
