/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SiDepositedChargeBase.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/LHCbConstants.h"

using namespace LHCb;
using namespace Gaudi::Units;

StatusCode SiDepositedChargeBase::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  /// initialize generators .
  auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
  m_GaussDist      = tRandNumSvc->generator( Rndm::Gauss( 0., 1.0 ) );
  if ( !m_GaussDist ) return Error( "Failed to init generator ", sc );

  return sc;
}
