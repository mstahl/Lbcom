/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiUtils/Aida2ROOT.h"
#include <TH1.h>

//-----------------------------------------------------------------------------
// Implementation file for class : TriggerTypeCounter
//
// 2009-10-07 : Olivier Deschamps
//-----------------------------------------------------------------------------

/** @class TriggerTypeCounter TriggerTypeCounter.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-11-18
 */
class TriggerTypeCounter : public Gaudi::Functional::Consumer<void( const LHCb::ODIN& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /// Standard constructor
  TriggerTypeCounter( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                          ///< Algorithm initialization
  void       operator()( const LHCb::ODIN& ) const override; ///< Algorithm execution

private:
  void hbook( const std::string& name, const std::vector<std::string>& names );
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TriggerTypeCounter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TriggerTypeCounter::TriggerTypeCounter( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"ODINLocation", LHCb::ODINLocation::Default}} {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode TriggerTypeCounter::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;             // error printed already by GaudiAlgorithm

  std::vector<std::string> names;

  // Trigger Type
  int it = 0;
  while ( it >= 0 ) {
    std::stringstream type;
    type << (LHCb::ODIN::TriggerType)it;
    if ( type.str().find( "ERROR" ) != std::string::npos ) it = -1;
    if ( it >= 0 ) {
      names.push_back( type.str() );
      it++;
    }
  }
  hbook( "TriggerTypesCounter", names );

  // BXTypes
  names.clear();
  it = 0;
  while ( it >= 0 ) {
    std::stringstream type;
    type << (LHCb::ODIN::BXTypes)it;
    if ( type.str().find( "ERROR" ) != std::string::npos ) it = -1;
    if ( it >= 0 ) {
      names.push_back( type.str() );
      it++;
    }
  }
  hbook( "BXTypesCounter", names );

  // CalibrationTypes
  names.clear();
  it = 0;
  while ( it >= 0 ) {
    std::stringstream type;
    type << (LHCb::ODIN::CalibrationTypes)it;
    if ( type.str().find( "ERROR" ) != std::string::npos ) it = -1;
    if ( it >= 0 ) {
      names.push_back( type.str() );
      it++;
    }
  }
  hbook( "CalibrationTypesCounter", names );

  return StatusCode::SUCCESS;
}

void TriggerTypeCounter::hbook( const std::string& name, const std::vector<std::string>& names ) {
  int siz = names.size();
  if ( 0 == siz ) return;
  AIDA::IHistogram1D* h  = book1D( name + "/1", name, 0., (double)siz, siz );
  TH1D*               th = Gaudi::Utils::Aida2ROOT::aida2root( h );
  int                 i  = 1;
  for ( const auto& bin : names ) th->GetXaxis()->SetBinLabel( i++, bin.c_str() );
}

//=============================================================================
// Main execution
//=============================================================================
void TriggerTypeCounter::operator()( const LHCb::ODIN& odin ) const {

  int tt = (int)odin.triggerType();
  fill( histo1D( HistoID( "TriggerTypesCounter/1" ) ), tt, 1. );
  counter( "TriggerType " + Gaudi::Utils::toString( odin.triggerType() ) ) += 1;

  int bt = (int)odin.bunchCrossingType();
  fill( histo1D( HistoID( "BXTypesCounter/1" ) ), bt, 1. );
  counter( "BXType " + Gaudi::Utils::toString( odin.bunchCrossingType() ) ) += 1;

  if ( odin.triggerType() == LHCb::ODIN::TriggerType::CalibrationTrigger ) {
    int ct = (int)odin.calibrationType();
    fill( histo1D( HistoID( "CalibrationTypesCounter/1" ) ), ct, 1. );
    counter( "CalibrationType " + Gaudi::Utils::toString( odin.calibrationType() ) ) += 1;
  }
}
//=============================================================================
