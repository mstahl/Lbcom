/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Kernel/CaloCellID.h"
#include "Relations/RelationWeighted1D.h"
// ============================================================================
/** @class CaloHypoEfficiency CaloHypoEfficiency.cpp
 *
 *  Class to check calo hypo efficiency
 *  Start from MC photons and electrons that are matched to a reconstructed cluster
 *  and check how many are correctly classified as photon or electron hypos.
 *  Minimum PT and parent ID requirements are also applied and are configurable.
 *
 *  @author Carla Marin carla.marin@cern.ch based on CaloClusterEfficiency.cpp
 *
 *  @date   2020-09-03
 */

using Cell2MCTable = LHCb::CaloFuture2MC::ClusterTable;
using MC2CellTable = LHCb::CaloFuture2MC::MC2ClusterTable;

namespace LHCb::Calo::Algorithms {
  using namespace LHCb::Event::Calo;
  class HypoEfficiency
      : public Gaudi::Functional::Consumer<void( const v2::Clusters&, const Hypotheses&, const Hypotheses&,
                                                 const LHCb::MCParticles&, const Cell2MCTable& ),
                                           Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
  public:
    using base_type = Gaudi::Functional::Consumer<void( const v2::Clusters&, const Hypotheses&, const Hypotheses&,
                                                        const LHCb::MCParticles&, const Cell2MCTable& ),
                                                  Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>;
    using KeyValue  = typename base_type::KeyValue;

    // ==========================================================================
    /// standard constructor
    HypoEfficiency( const std::string& name, ISvcLocator* pSvc )
        : base_type{name,
                    pSvc,
                    {
                        KeyValue{"clusters", CaloClusterLocation::Ecal},
                        KeyValue{"photonHypos", HypothesesLocation::Photons},
                        KeyValue{"electrHypos", HypothesesLocation::Electrons},
                        KeyValue{"MCParticles", LHCb::MCParticleLocation::Default},
                        KeyValue{"Relations", "Relations/" + LHCb::CaloClusterLocation::Default},
                    }} {}

    // ============================================================================
    // algorithm execution
    // ============================================================================
    void operator()( const v2::Clusters& clusters, const Hypotheses& photons, const Hypotheses& electrons,
                     const LHCb::MCParticles& mcparts, const Cell2MCTable& table ) const override {

      // get index for fast look-up
      const auto clus_index = clusters.index();
      if ( !clus_index ) {
        ++m_canNotIndexClusters;
        return;
      }

      // get counter buffers
      unsigned int nclusphotons = 0;
      unsigned int ncluselectrs = 0;

      // create MCParticle -> CaloCluster relations table from inverse one
      // LHCb::RelationWeighted1D(table, inverse_tag) creates inverse table
      auto mc2cellTable = MC2CellTable( table, MC2CellTable::inverse_tag{} );

      // loop over all mc particles
      for ( const auto& mcp : mcparts ) {
        if ( !mcp ) continue;
        // filter by true id
        auto trueid = abs( mcp->particleID().pid() );
        auto fid    = std::find( m_PDGID.begin(), m_PDGID.end(), trueid );
        if ( m_PDGID.end() == fid ) continue;
        // filter by parent id
        if ( !mcp->mother() ) continue;
        auto parentid = abs( mcp->mother()->particleID().pid() );
        auto fpid     = std::find( m_PDGIDParent.begin(), m_PDGIDParent.end(), parentid );
        if ( m_PDGIDParent.end() == fpid ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle with required ID and parent ID found" << endmsg;
        // apply min ET cut
        if ( mcp->pt() < m_minET ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle passes pt cut" << endmsg;

        // find reco'ed cluster with largest E deposited by this mc particle
        // use inverse MCParticle -> CellID table for this
        const auto& [cell_id, weight] = getClusLargestWeight( mc2cellTable.relations( mcp ) );
        if ( !cell_id ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Found associated reconstructed cluster, weight: " << weight << endmsg;

        // check cluster is a good match: f_mcp x f_cl > 0.9
        // f_mcp: fraction of MCParticle E deposited in cluster
        // f_cl: fraction of cluster E from this MCParticle
        float f_mcp = weight / mcp->momentum().e();
        // find cluster from cell_id and compute f_cl
        auto cl = clus_index.find( cell_id );
        if ( cl == clus_index.end() ) continue; // should not happen, since cell_id is in Cluster2MC table!
        float f_cl = ( cl->e() > 0. ) ? weight / cl->e() : 0.;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "f_mcp=" << f_mcp << endmsg;
          debug() << "f_cl =" << f_cl << endmsg;
        }

        // apply matching
        if ( f_mcp * f_cl < m_minMatchFraction ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Cluster fulfills matching requirement" << endmsg;

        // check if calohypo with correct hypo is reconstructed
        if ( trueid == 22 ) { // photons
          auto [clusreco, hyporeco] = isReco( mcp, cell_id, photons );
          if ( clusreco ) {
            nclusphotons++;
            m_photoneff += hyporeco;
          }
        } else { // electrons
          auto [clusreco, hyporeco] = isReco( mcp, cell_id, electrons );
          if ( clusreco ) {
            ncluselectrs++;
            m_electneff += hyporeco;
          }
        }
      }
      // update counters
      m_clusphotons += nclusphotons;
      m_cluselectrs += ncluselectrs;

      // Part 2: backgrounds
      auto phtuple = checkBkg( photons, clusters, table, true );
      auto eltuple = checkBkg( electrons, clusters, table, false );
    }

  private:
    // properties
    Gaudi::Property<std::vector<int>> m_PDGID{this, "PDGID", {22, 11}};
    Gaudi::Property<std::vector<int>> m_PDGIDParent{this, "PDGIDParent", {511, 521, 531, 541, 5122, 5132, 5232, 5332}};
    Gaudi::Property<float>            m_minMatchFraction{this, "minMatchFraction", 0.9};
    Gaudi::Property<float>            m_minET{this, "minET", 50.};
    Gaudi::Property<float>            m_minEndVtxZ{this, "minEndVtxZ", 7000.};
    Gaudi::Property<std::vector<int>> m_photonBkgs{this, "PhotonBkgs", {11, 13, 211, 321, 2212}}; // e, mu, pi, K, p
    Gaudi::Property<std::vector<int>> m_electronBkgs{this, "ElectronBkgs", {22, 111, 221, 130, 2112}}; // g, pi0, eta,
                                                                                                       // KL, n

    // counters
    mutable Gaudi::Accumulators::StatCounter<int>              m_clusphotons{this, "# Photons with reco cluster"};
    mutable Gaudi::Accumulators::StatCounter<int>              m_cluselectrs{this, "# Electrons with reco cluster"};
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_photoneff{this, "Photon hypo efficiency"};
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_electneff{this, "Electron hypo efficiency"};
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_phBkg{this, "Bkg fraction in Photon hypos"};
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_elBkg{this, "Bkg fraction in Electron hypos"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>        m_canNotIndexClusters{this,
                                                                              "Failed to index cluster container"};

    // get cluster (cellID) with largest weight from given MCParticle
    using CellWi = std::pair<LHCb::CaloCellID, float>;
    template <typename Range>
    CellWi getClusLargestWeight( const Range& range ) const {
      return std::accumulate( range.begin(), range.end(), CellWi{{}, 0.}, [&]( CellWi current, const auto& relation ) {
        const auto cellid = relation.to();
        const auto weight = relation.weight(); // "weight" of the relation
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " -- Cell ID: " << cellid << endmsg;
          debug() << " -- weight: " << weight << endmsg;
        }
        return weight <= current.second ? current : CellWi{cellid, weight};
      } );
    }

    // get MCParticle with largest contribution (weight) to given cluster (cellID)
    // require that MCParticle endVertex is after m_minEndVtxZ
    // if various MCParticles have same weight, return one with largest E fraction (f_mcp) deposited in cluster
    using MCPartWi = std::pair<LHCb::MCParticle*, float>;
    template <typename Range>
    MCPartWi getMCPartLargestWeight( const Range& range ) const {
      return std::accumulate( range.begin(), range.end(), MCPartWi{{}, 0.},
                              [&]( MCPartWi current, const auto& relation ) {
                                const auto mcpart = relation.to();
                                const auto weight = relation.weight(); // "weight" of the relation
                                if ( msgLevel( MSG::DEBUG ) ) {
                                  debug() << " -- MCPart: " << mcpart << endmsg;
                                  debug() << " -- weight: " << weight << endmsg;
                                }
                                // check decay vertex to ensure it reached ecal
                                auto endVertices = mcpart->endVertices();
                                if ( endVertices.empty() ) return current;
                                if ( endVertices.back()->position().z() < m_minEndVtxZ ) return current;
                                // check fraction of MCPart E deposited if equal cluster contribution
                                if ( weight == current.second ) {
                                  auto f_mcp      = weight / mcpart->momentum().e();
                                  auto f_mcp_curr = current.second / current.first->momentum().e();
                                  return f_mcp <= f_mcp_curr ? current : MCPartWi{mcpart, weight};
                                }
                                return weight < current.second ? current : MCPartWi{mcpart, weight};
                              } );
    }

    // check efficiency
    std::pair<bool, bool> isReco( const MCParticle* mcp, const LHCb::CaloCellID& cell_id,
                                  const Hypotheses& hypos ) const {
      auto clusreco = false;
      auto hyporeco = false;
      if ( mcp->particleID().pid() == 22 ) {
        // check endVtx for photons
        auto endVertices = mcp->endVertices();
        if ( endVertices.empty() )
          return std::pair{clusreco, hyporeco}; // should not happen, we have found a matching cluster!
        // get last endVertex, the decay vertex
        auto endVertex = endVertices.back();
        // only photons with conversion after m_minEndVtxZ are reconstructed as photons
        if ( endVertex->position().z() < m_minEndVtxZ ) return std::pair{clusreco, hyporeco};
        clusreco = true;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is a photon decaying after " << m_minEndVtxZ << endmsg;
      } else
        clusreco = true; // don't require anything else to electrons

      // look for hypo for this cluster
      for ( auto h : hypos )
        if ( h.cellID() == cell_id ) hyporeco = true;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "hypo reconstructed: " << hyporeco << endmsg;
      return std::pair{clusreco, hyporeco};
    }

    // check backgrounds in reconstructed hypos
    Tuple checkBkg( const Hypotheses& hypos, const v2::Clusters& clusters, const Cell2MCTable& table,
                    const bool photons = true ) const {
      // get clusters index
      // FIXME: pass directly clus_index to this method, need to know it's type
      const auto clus_index = clusters.index();

      // declare tuple
      auto tname = photons ? "PhotonHypos" : "ElectronHypos";
      auto tuple = this->nTuple( tname );

      // loop over hypos
      for ( const auto h : hypos ) {
        // get MCParticle with largest energy deposited in the cluster of this hypo
        auto h_cell_id            = h.cellID();
        const auto& [mcp, weight] = getMCPartLargestWeight( table.relations( h_cell_id ) ); // look-up by cellID
        if ( !mcp ) continue;
        // check it's a match: f_cl >= m_minMatchFraction
        float f_mcp = weight / mcp->momentum().e();
        auto  cl    = clus_index.find( h_cell_id ); // get cluster with same cell_id
        if ( cl == clus_index.end() ) continue;     // should not happen, since cell_id is in Cluster2MC table!
        float f_cl = ( cl->e() > 0. ) ? weight / cl->e() : 0.;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "f_mcp=" << f_mcp << endmsg;
          debug() << "f_cl =" << f_cl << endmsg;
        }
        if ( f_cl < m_minMatchFraction ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle fulfills matching requirement" << endmsg;
        // count background particles in hypo container
        auto trueid  = mcp->particleID().pid();
        auto bkgpids = photons ? m_photonBkgs : m_electronBkgs;
        auto fpid    = std::find( bkgpids.begin(), bkgpids.end(), abs( trueid ) );
        if ( photons )
          m_phBkg += ( fpid != bkgpids.end() );
        else
          m_elBkg += ( fpid != bkgpids.end() );

        // fill and write tuple
        auto parent   = mcp->mother();
        auto parentid = parent ? parent->particleID().pid() : 0;
        auto tripch   = mcp->particleID().threeCharge();
        auto endVtces = mcp->endVertices();
        auto endVtxZ  = endVtces.empty() ? -10000 : endVtces.back()->position().z();
        auto sc       = tuple->column( "hypo_trueID", trueid );
        sc &= tuple->column( "hypo_3charge", tripch );
        sc &= tuple->column( "hypo_parentID", parentid );
        sc &= tuple->column( "hypo_endVtxZ", endVtxZ );
        sc &= tuple->column( "hypo_fcl", f_cl );
        sc &= tuple->column( "hypo_fmcp", f_mcp );
        sc &= tuple->write();
      }
      return tuple;
    }

    // ==========================================================================
  };
  DECLARE_COMPONENT_WITH_ID( HypoEfficiency, "CaloHypoEfficiency" )
} // namespace LHCb::Calo::Algorithms

// ============================================================================
// The END
// ============================================================================
