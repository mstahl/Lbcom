/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloMCTools.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "Relations/RelationWeighted1D.h"
// ============================================================================
/** @class CaloClusterMCTruth CaloClusterMCTruth.cpp
 *
 *  Simple class to create CaloCluster->MCParticle
 *  relation table basing on information from
 *  CaloDigit->MCParticle relations table
 *
 *  The important  properties of algorithm:
 *
 *    - "Input"
 *      The default value is "Relations/" + LHCb::CaloDigitLocation::Default
 *      The name of relation table CaloDigit->MCParticle
 *
 *    - "Output"
 *      The default value "Relations/" + LHCb::CaloClusterLocation::Default
 *      The name of (output) relation table CaloCluster->MCParticle
 *
 *    - "Clusters"
 *      The default value is CaloClusterLocation :: Ecal  + EcalSplit
 *      list of addresses fro CaloClusters containers
 *
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
 *  @date   2005-05-03
 */
class CaloClusterMCTruth : public Gaudi::Functional::Transformer<LHCb::CaloFuture2MC::ClusterTable(
                               const LHCb::Event::Calo::Clusters&, const LHCb::CaloFuture2MC::DigitTable& )> {
public:
  // ==========================================================================
  /// standard constructor
  CaloClusterMCTruth( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     {KeyValue{"Clusters", LHCb::CaloClusterLocation::Ecal},
                      KeyValue{"Input", "Relations/" + LHCb::CaloDigitLocation::Default}},
                     KeyValue{"Output", "Relations/" + LHCb::CaloClusterLocation::Default} ) {}

  LHCb::CaloFuture2MC::ClusterTable operator()( const LHCb::Event::Calo::Clusters&,
                                                const LHCb::CaloFuture2MC::DigitTable& ) const override;

private:
  // ==========================================================================
  Gaudi::Property<int> m_sFilter{this, "StatusFilter", static_cast<int>( LHCb::CaloDigitStatus::Mask::UseForEnergy )};
  Gaudi::Property<double> m_wFilter{this, "WeightFilter", 0};
  // ==========================================================================
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cc2mcLinkscounter{this, "#CC2MC links"};
  mutable Gaudi::Accumulators::BinomialCounter<int>           m_no_mc_for_cluster{this,
                                                                        "No MC information for the cluster is found"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_empty_relations_table{this, "The relations table is empty"};
};
// ============================================================================
DECLARE_COMPONENT( CaloClusterMCTruth )
// ============================================================================
// algorithm execution
// ============================================================================
LHCb::CaloFuture2MC::ClusterTable CaloClusterMCTruth::
                                  operator()( const LHCb::Event::Calo::Clusters& clusters, const LHCb::CaloFuture2MC::DigitTable& digTable ) const {
  using namespace LHCb::CaloDataFunctor;
  using namespace CaloMCTools;

  /// the actual type of relation table
  typedef LHCb::CaloFuture2MC::ClusterTable Table;

  // create and register the relation table
  Table table( 1000 );

  int  iCluster     = 0;
  auto buffer_no_mc = m_no_mc_for_cluster.buffer();
  for ( const auto& cluster : clusters ) {

    // auxillary container to collect all links from individual digits
    CaloMCMap mcMap;

    // loop over all digits in the cluster and collect
    // the links from individual  digits
    for ( const auto& entry : cluster.entries() ) {

      if ( m_sFilter >= 0 && entry.status().noneOf( LHCb::CaloDigitStatus::Status( m_sFilter ) ) ) continue;

      // get all MC relations from this digit
      // loop over all MC entries and collect the enegy
      for ( const auto& [_, particle, energy] : digTable.relations( entry.cellID() ) ) {
        // accumulate the energy from the same particle
        mcMap[particle] += energy;
      }

    } // end of loop over all entries in Cluster

    buffer_no_mc += mcMap.empty();

    // loop over auxillary container of merged depositions
    // and fill relation table entries for given cluster
    // MC particle & its cumulative energy deposition to the cluster
    ++iCluster;
    int iParticle = 0;
    for ( const auto& [particle, energy] : mcMap ) {
      if ( !particle ) continue;
      if ( cluster.e() > 0 && energy < m_wFilter * cluster.e() ) continue;

      // fill the relation table:         ATTENTION "i_push" is used!
      table.i_push( cluster.seed(), particle, energy ); // NB: "i_push"
      ++iParticle;
    }
  } // end of loop over clusters

  // mandatory after "i_push" ;
  table.i_sort(); // NB: "i_sort"

  // count number of links
  m_cc2mcLinkscounter += table.relations().size();
  if ( table.relations().empty() ) ++m_empty_relations_table;

  return table;
}
// ============================================================================
// The END
// ============================================================================
