/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloMCTools.h"
#include "CaloUtils/Calo2MC.h"
#include "Event/CaloDigit.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Transformer.h"
#include "Relations/RelationWeighted1D.h"

/**
 *  Helper algorithm to "decode" linker object
 *  into the relation table for Monte Carlo associatiations
 *  from CaloDigits to MCParticle
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
 *  @date 2004-05-03
 */
class CaloDigit2MCLinks2Table : public Gaudi::Functional::Transformer<LHCb::Calo2MC::DigitTable(
                                    const LHCb::CaloDigits&, const LHCb::MCParticles&, const LHCb::LinksByKey& )> {

public:
  /// algorithm execution
  LHCb::Calo2MC::DigitTable operator()( const LHCb::CaloDigits&, const LHCb::MCParticles&,
                                        const LHCb::LinksByKey& ) const override;

  /** standard constructor
   *  @param name algorithm instance name
   *  @param pSvc pointer to service locator
   */
  CaloDigit2MCLinks2Table( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     {KeyValue{"CaloDigits", LHCb::CaloDigitLocation::Ecal},
                      KeyValue{"MCParticles", LHCb::MCParticleLocation::Default},
                      KeyValue{"Link", "Link/" + LHCb::CaloDigitLocation::Ecal}},
                     KeyValue{"Output", "Relations/" + LHCb::CaloDigitLocation::Default} ) {}

private:
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cd2mcLinkscounter{this, "#CD2MC links"};
};

DECLARE_COMPONENT( CaloDigit2MCLinks2Table )

LHCb::Calo2MC::DigitTable CaloDigit2MCLinks2Table::operator()( const LHCb::CaloDigits&  digits,
                                                               const LHCb::MCParticles& mcParticles,
                                                               const LHCb::LinksByKey&  links ) const {
  // create the relation table
  LHCb::Calo2MC::DigitTable table{3000};

  // loop over all digits
  for ( const auto* digit : digits ) {
    if ( !digit ) continue;

    // use the auxillary container to be immune agains
    // potential bugs (if any..), like duplicated entries, etc
    // in the Linkers
    CaloMCTools::CaloMCMap mcMap;

    links.applyToLinks( digit->index(), [&mcMap, &mcParticles]( unsigned int, unsigned int tgtIndex, float weight ) {
      // use Linker
      const LHCb::MCParticle* particle =
          static_cast<const LHCb::MCParticle*>( mcParticles.containedObject( tgtIndex ) );
      if ( particle != nullptr ) {
        // update the auxillary container
        mcMap[particle] += weight;
      }
    } );

    // convert the auxillary container into Relation Table
    for ( auto [particle, energy] : mcMap ) {
      // use fast i_push method!
      table.i_push( digit, particle, energy ); // NB !!
    }

  } // end of the loop over digits in the container

  // mandatory operation after "i_push"!
  table.i_sort(); // NB !!

  if ( table.relations().empty() ) { Warning( "Empty Relation table" ).ignore(); }

  m_cd2mcLinkscounter += table.relations().size();

  return table;
}
