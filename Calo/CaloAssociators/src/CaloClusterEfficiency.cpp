/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "Event/CaloClusters_v2.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Kernel/CaloCellID.h"
#include "Relations/RelationWeighted1D.h"
// ============================================================================
/** @class CaloClusterEfficiency CaloClusterEfficiency.cpp
 *
 *  Class to check cluster reconstruction efficiency
 *  Check how many MCParticles of a certain ID, certain MCParent ID,
 *  that deposit energy in ECAL and have a minimum PT are matched to
 *  reconstructed clusters.
 *  It uses the relation table produced by CaloClusterMCTruth
 *  to match MCParticles and reconstructed CaloClusters
 *
 *  Matching criteria:
 *  - reconstructibe: at least m_minMCFraction of the MCParticle energy is deposited
 *  in the CaloCluster. Default 90%
 *  - reconstructed: at least m_minMatchFraction of the CaloCluster energy is coming
 *  from the MCParticle. Default 90%
 *
 *  @author Carla Marin carla.marin@cern.ch based on
 *  CaloClusterResolution.cpp by Sasha Zenaiev oleksandr.zenaiev@cern.ch
 *
 *  @date   2020-04-24
 */

using Cell2MCTable = LHCb::CaloFuture2MC::ClusterTable;
using MC2CellTable = LHCb::CaloFuture2MC::MC2ClusterTable;

namespace LHCb::Calo::Algorithms {

  class ClusterEfficiency : public Gaudi::Functional::Consumer<void( const Cell2MCTable&, const LHCb::MCParticles&,
                                                                     const LHCb::Event::Calo::v2::Clusters& ),
                                                               Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
  public:
    using base_type = Gaudi::Functional::Consumer<void( const Cell2MCTable&, const LHCb::MCParticles&,
                                                        const LHCb::Event::Calo::v2::Clusters& ),
                                                  Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>;
    using KeyValue  = typename base_type::KeyValue;

    // ==========================================================================
    /// standard constructor
    ClusterEfficiency( const std::string& name, ISvcLocator* pSvc )
        : base_type{name,
                    pSvc,
                    {
                        KeyValue{"inputRelations", "Relations/" + LHCb::CaloClusterLocation::Default},
                        KeyValue{"inputMCParticles", LHCb::MCParticleLocation::Default},
                        KeyValue{"recoClusters", LHCb::Event::Calo::ClusterLocation::Default},
                    }} {}

    // ============================================================================
    // algorithm execution
    // ============================================================================
    void operator()( const Cell2MCTable& table, const LHCb::MCParticles& mcparts,
                     const LHCb::Event::Calo::v2::Clusters& clusters ) const override {

      const auto cluster_index = clusters.index();
      if ( !cluster_index ) {
        ++m_canNotIndexClusters;
        return;
      }

      // tuple
      auto tuple = this->nTuple( "clusEff" );

      // initialise counters
      unsigned int nsignal          = 0;
      unsigned int nreconstructible = 0;
      unsigned int nreconstructed   = 0;
      bool         is_reconstructed;

      // create MCParticle -> CaloCluster relations table from inverse one
      // LHCb::RelationWeighted1D(table, inverse_tag) creates inverse table
      auto mc2cellTable = MC2CellTable( table, MC2CellTable::inverse_tag{} );

      // loop over all mc particles
      for ( const auto& mcp : mcparts ) {
        is_reconstructed = false;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Next MCParticle" << endmsg;
        // select signal as defined by m_PDGID and m_PDGIDParent
        if ( !mcp ) continue;
        if ( mcp->particleID().pid() != m_PDGID ) continue;
        if ( !mcp->mother() || abs( mcp->mother()->particleID().pid() ) != m_PDGIDParent ) continue;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "MC particle and parent ID are: " << m_PDGID << ", " << m_PDGIDParent << endmsg;

        // apply min ET cut
        if ( mcp->pt() < m_minET ) continue;
        nsignal++;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MC particle has PT larger than: " << m_minET << endmsg;

        // check endVertex, also fills info into ntuple
        if ( not check_endVtx( mcp, tuple ) ) continue;

        // find largest E deposited by mc particle in an ECAL cluster ("weight")
        // use inverse MCParticle -> CellID table for this
        // TODO: find E deposited in ECAL, not in a given cluster, to have a
        // definition of reconstructible independent of the reconstruction itself!
        const auto& [cell_id, weight] = largestWeight( mc2cellTable.relations( mcp ) );
        if ( !cell_id ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle has desposited E in ECAL" << endmsg;

        // reco'ble: fraction of MCParticle E deposited in cluster > m_minMCFraction
        float f_mcp = weight / mcp->momentum().e();
        if ( f_mcp < m_minMCfraction ) continue;
        nreconstructible++;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ble" << endmsg;

        // fill tuple
        auto sc = tuple->column( "max_fMCP", f_mcp );

        // find cluster matching MCParticle from cell_id
        auto it = cluster_index.find( cell_id ); // returns Iterator (see CaloClusters_v2)
        if ( it == cluster_index.end() ) {
          ++m_clNotFound;
          m_eff += is_reconstructed;
          continue;
        }
        auto cl_pos = it->position();
        auto cl_e   = it->e();

        // fraction of cluster energy from this MCParticle
        float f_cl = ( cl_e > 0. ) ? weight / cl_e : 0.;

        // fill tuple
        sc &= tuple->column( "f_cl", f_cl );
        sc &= tuple->column( "cl_e", cl_e );
        sc &= tuple->column( "cl_x", cl_pos.x() );
        sc &= tuple->column( "cl_y", cl_pos.y() );
        sc &= tuple->column( "cl_z", cl_pos.z() );

        // write tuple
        sc.andThen( [&] { return tuple->write(); } ).ignore();

        // apply reco'ed threshold m_minMatchFraction
        if ( f_cl >= m_minMatchFraction ) {
          nreconstructed++;
          is_reconstructed = true;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ed" << endmsg;
        }
        m_eff += is_reconstructed;
      }

      // update counters
      m_signal += nsignal;
      m_reconstructible += nreconstructible;
      m_reconstructed += nreconstructed;
    }

  private:
    // properties
    Gaudi::Property<int>   m_PDGID{this, "PDGID", 22};
    Gaudi::Property<int>   m_PDGIDParent{this, "PDGIDParent", 511};
    Gaudi::Property<float> m_minMCfraction{this, "minMCfraction", 0.9};
    Gaudi::Property<float> m_minMatchFraction{this, "minMatchFraction", 0.9};
    Gaudi::Property<float> m_minET{this, "minET", 50.};
    Gaudi::Property<float> m_minEndVtxZ{this, "minEndVtxZ", 7000.};

    // counters
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_signal{this, "# signal"};
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_reconstructible{this, "# reconstructible"};
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_reconstructed{this, "# reconstructed"};
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_eff{this, "reco efficiency"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::INFO>    m_multVtx{this, "More than 1 endVertex found for signal"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_clNotFound{
        this, "CellID from relations table not found in clusters"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_canNotIndexClusters{this,
                                                                              "Failed to index cluster container"};

    bool check_endVtx( LHCb::MCParticle* mcp, Tuple tuple ) const {
      // get endVertices
      auto endVertices = mcp->endVertices();
      if ( endVertices.empty() ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle has empty endVertices" << endmsg;
        // no endVertices means no interaction before 50m -> not reconstructible
        return false;
      } else {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle has endVertices" << endmsg;
        if ( endVertices.size() > 1 ) ++m_multVtx;

        // get last endVertex, as it's the decay vertex
        auto endVertex = endVertices.back();
        auto pos       = endVertex->position();

        // only MCParticles with conversion after m_minEndVtxZ are reconstructible
        if ( pos.z() < m_minEndVtxZ ) return false;

        // fill endVtx info
        auto sc = tuple->column( "endVtx_X", pos.x() );
        sc &= tuple->column( "endVtx_Y", pos.y() );
        sc &= tuple->column( "endVtx_Z", pos.z() );
        sc &= tuple->column( "endVtx_type", endVertex->type() );

        return true;
      }
    }

    using Result = std::pair<LHCb::CaloCellID, float>;

    template <typename Range>
    Result largestWeight( const Range& range ) const {
      return std::accumulate( range.begin(), range.end(), Result{{}, 0.}, [&]( Result current, const auto& relation ) {
        const LHCb::CaloCellID cellid = relation.to();
        const auto             weight = relation.weight(); // "weight" of the relation
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " -- Cell ID: " << cellid << endmsg;
          debug() << " -- weight: " << weight << endmsg;
        }
        return weight <= current.second ? current : Result{cellid, weight};
      } );
    }

    // ==========================================================================
  };
  DECLARE_COMPONENT_WITH_ID( ClusterEfficiency, "CaloClusterEfficiency" )
} // namespace LHCb::Calo::Algorithms

// ============================================================================
// The END
// ============================================================================
