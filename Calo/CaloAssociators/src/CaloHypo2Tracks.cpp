/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureUtils/CaloFuture2Track.h"

#include "Relations/RelationWeighted2D.h"

#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/Track.h"

#include "GaudiAlg/Transformer.h"

#include <string>
#include <tuple>

/**
 * simple algorithm extracting tracks associated to a set of CaloHypos
 * The only purpose is to be able to use MC checking via the PrCheker
 * on these tracks
 */

namespace LHCb::Calo::Asssociators {

  using TrackMatchTable = RelationWeighted2D<CellID, Track, float>;

  struct Hypo2Tracks
      : Gaudi::Functional::Transformer<Tracks( const Event::Calo::Hypotheses&, const TrackMatchTable& )> {

    Gaudi::Property<float> m_electrChi2Cut{this, "ElectrMaxChi2", 0.,
                                           "Threshold on maximum electron cluster track match chi2"};

    Hypo2Tracks( const std::string& name, ISvcLocator* pSvc )
        : Transformer( name, pSvc,
                       {KeyValue( "InputHypos", "" ), KeyValue( "InputTable", CaloFutureIdLocation::ClusterMatch )},
                       {KeyValue( "OutputTracks", "" )} ) {}

    Tracks operator()( const Event::Calo::Hypotheses& hypos, const TrackMatchTable& table ) const override {
      Tracks tracks;
      for ( const auto& hypo : hypos ) {
        for ( const auto& cluster : hypo.clusters() ) {
          for ( auto& entry : table.relations( cluster.cellID(), m_electrChi2Cut, false ) ) {
            if ( !tracks.object( entry.to()->key() ) ) tracks.insert( entry.to() );
          }
        }
      }
      return tracks;
    }
  };
  DECLARE_COMPONENT( Hypo2Tracks )
} // namespace LHCb::Calo::Asssociators
