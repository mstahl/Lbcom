/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloMCTools.h"
#include "CaloUtils/Calo2MC.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Relations/RelationWeighted1D.h"
// ============================================================================
/** @class CaloClusterResolution CaloClusterResolution.cpp
 *
 *  Class to monitor energy and position resolution of clusters and hypos by
 *  comparing their parameters with the corresponding parameters of MC particles
 *  matched to them. It uses the relation table produced by CaloClusterMCTruth
 *  and works for relation tables with either CaloCluster or CaloHypo.
 *
 *
 *
 *  @author Sasha Zenaiev oleksandr.zenaiev@cern.ch
 *  @date   2020-01-29
 */
using ICaloTable = LHCb::CaloFuture2MC::ClusterTable;
namespace {

  std::optional<float> checkConversion( const LHCb::MCParticle* matchParticle, const LHCb::CaloCellID currentCluster,
                                        const ICaloTable& table ) {
    auto allParticles = table.relations( currentCluster );

    switch ( matchParticle->particleID().pid() ) {
    case 111:
      // pi0: if it decays to photons, check conversion for each daughter photon
      // (if many are converted, minimum zConv is chosen)
      return std::accumulate( allParticles.begin(), allParticles.end(), std::optional<float>{},
                              [&]( std::optional<float> zConv, const auto& match ) {
                                auto part = match.to();
                                if ( part->mother() && part->mother() == matchParticle &&
                                     part->particleID().pid() == 22 ) {
                                  auto zConvThis = checkConversion( part, currentCluster, table );
                                  if ( zConvThis && ( !zConv || *zConv > *zConvThis ) ) return zConvThis;
                                }
                                return zConv;
                              } );
    case 22: {
      // photons
      bool                  flagEle  = false;
      bool                  flagPos  = false;
      bool                  flagOth  = false;
      const LHCb::MCVertex* convVert = nullptr;
      for ( auto it_range : allParticles ) {
        auto part = it_range.to();
        if ( part->originVertex()->position().Z() > 12000.0 ) continue;
        if ( part->mother() && part->mother() == matchParticle ) {
          if ( part->particleID().pid() == 11 ) {
            flagEle  = !flagEle;
            convVert = part->originVertex();
          } else if ( part->particleID().pid() == -11 ) {
            flagPos  = !flagPos;
            convVert = part->originVertex();
          } else {
            flagOth = true;
            break;
          }
        }
      }
      if ( ( flagEle || flagPos ) && ( !flagOth ) ) return convVert->position().Z();
      return std::nullopt;
    }
    default:
      return std::nullopt;
    }
  }

  template <typename CaloObject>
  std::optional<double> RetrieveZPosition( const CaloObject* in ) {
    return in->position().z();
  }

  std::optional<double> RetrieveZPosition( LHCb::Event::Calo::Clusters::const_reference in ) {
    return in.position().z();
  }

  std::optional<double> RetrieveZPosition( LHCb::Event::Calo::Hypotheses::const_reference in ) {
    return RetrieveZPosition( in.clusters().front() );
  }

  std::optional<double> RetrieveZPosition( const LHCb::CaloHypo* in ) {
    const auto* pos = in->position();
    // pi0->gamma gamma special treatment: return hypo of first gamma
    // in order to have correct z of the cluster
    if ( !pos && in->hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged ) pos = in->hypos()[0]->position();
    if ( pos ) return pos->z();
    return std::nullopt;
  }

  template <typename CaloObject>
  LHCb::CaloPosition::Parameters RetrieveParameters( const CaloObject* in ) {
    return in->position().parameters();
  }

  LHCb::CaloPosition::Parameters RetrieveParameters( LHCb::Event::Calo::Clusters::const_reference in ) {
    return {in.position().x(), in.position().y(), in.e()};
  }

  LHCb::CaloPosition::Parameters RetrieveParameters( LHCb::Event::Calo::Hypotheses::const_reference in ) {
    return RetrieveParameters( in.clusters().front() );
  }

  LHCb::CaloPosition::Parameters RetrieveParameters( const LHCb::CaloHypo* in ) {
    const auto* pos = in->position();
    if ( !pos ) {
      // pi0 special treatment: sum up gammas (x and y are energy weighted)
      if ( in->hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
        auto pars = LHCb::CaloPosition::Parameters();
        for ( auto& hypo : in->hypos() ) {
          pars[2] += hypo->position()->parameters()[2];
          pars[0] += hypo->position()->parameters()[2] * hypo->position()->parameters()[0];
          pars[1] += hypo->position()->parameters()[2] * hypo->position()->parameters()[1];
        }
        pars[0] /= pars[2];
        pars[1] /= pars[2];
        return pars;
      }
    }
    return pos->parameters();
  }
} // namespace

namespace LHCb::Calo {
  template <typename CaloTYPE>
  class ClusterResolution : public Gaudi::Functional::Consumer<void( const CaloTYPE&, const ICaloTable& ),
                                                               Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
  public:
    using base_type = Gaudi::Functional::Consumer<void( const CaloTYPE&, const ICaloTable& ),
                                                  Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>;
    using KeyValue  = typename base_type::KeyValue;

    // ==========================================================================
    // standard constructor
    // ==========================================================================
    ClusterResolution( const std::string& name, ISvcLocator* pSvc )
        : base_type{name,
                    pSvc,
                    {KeyValue{"input", CaloClusterLocation::Default},
                     KeyValue{"inputRelations", "Relations/" + CaloClusterLocation::Default}}} {}

    // ============================================================================
    // algorithm execution
    // ============================================================================
    void operator()( const CaloTYPE& input, const ICaloTable& table ) const override {
      auto bufferMatched     = m_counterMatched.buffer();
      auto bufferDeltaEOverE = m_counterDeltaEOverE.buffer();
      auto buffetDeltaX      = m_counterDeltaX.buffer();
      auto bufferDeltaY      = m_counterDeltaY.buffer();

      auto tuple = this->nTuple( m_tuplePrefix );

      for ( const auto& cluster : input ) {
        auto currentSeed = Functor::cellID( cluster );

        const auto& [matchParticle, matchFraction] = bestMatch( cluster, table.relations( currentSeed ) );
        bufferMatched += ( matchParticle != nullptr );
        if ( !matchParticle ) continue;

        auto currentZPos = RetrieveZPosition( cluster );
        auto currentPars = RetrieveParameters( cluster );
        // check photon conversion
        auto zConv   = checkConversion( matchParticle, currentSeed, table );
        auto vertex  = matchParticle->originVertex();
        auto deltaZ  = currentZPos ? ( *currentZPos - vertex->position().Z() ) : 0.0;
        auto mom     = matchParticle->momentum();
        auto xExtrap = vertex->position().X() + deltaZ * ( mom.Px() / mom.Pz() );
        auto yExtrap = vertex->position().Y() + deltaZ * ( mom.Py() / mom.Pz() );

        auto  energy        = matchParticle->momentum().E();
        auto  energyCluster = currentPars[2];
        float deltaEoverE   = ( energyCluster - energy ) / energy;

        bufferDeltaEOverE += deltaEoverE;
        buffetDeltaX += xExtrap - currentPars[0];
        bufferDeltaY += yExtrap - currentPars[1];

        auto sc = tuple->column( "maxMatchFraction", matchFraction );
        sc &= tuple->column( "energy", energy );
        sc &= tuple->column( "energyCluster", energyCluster );
        sc &= tuple->column( "xCluster", currentPars[0] );
        sc &= tuple->column( "yCluster", currentPars[1] );
        sc &= tuple->column( "zCluster", currentZPos.value_or( 0.0 ) );
        sc &= tuple->column( "xExtrap", xExtrap );
        sc &= tuple->column( "yExtrap", yExtrap );
        sc &= tuple->column( "flagConv", zConv.has_value() );
        sc &= tuple->column( "zConv", zConv.value_or( 0.0 ) );
        sc.andThen( [&] { return tuple->write(); } ).orElse( [&] { ++m_writefailed; } ).ignore();
      }
    }

  private:
    template <typename Cluster, typename Range>
    auto bestMatch( const Cluster& cluster, const Range& range ) const {
      using Result = std::pair<const MCParticle*, float>;
      return std::accumulate(
          range.begin(), range.end(), Result{nullptr, m_minMatchFraction}, [&]( Result current, const auto& match ) {
            const MCParticle* particle = match.to();
            if ( particle->momentum().e() < m_minEnergy ) return current;
            if ( !m_PDGID.empty() &&
                 find( m_PDGID.begin(), m_PDGID.end(), particle->particleID().pid() ) == m_PDGID.end() )
              return current;
            if ( !m_PDGIDMother.empty() &&
                 ( !particle->mother() || find( m_PDGIDMother.begin(), m_PDGIDMother.end(),
                                                particle->mother()->particleID().pid() ) == m_PDGIDMother.end() ) )
              return current;
            auto  weight        = match.weight(); // "weight" of the relation
            auto  pars          = RetrieveParameters( cluster );
            float matchFraction = weight / particle->momentum().e() * weight / pars[2];
            return matchFraction <= current.second ? current : Result{particle, matchFraction};
          } );
    }

    // properties
    Gaudi::Property<std::vector<int>> m_PDGID{this, "PDGID", {}};
    Gaudi::Property<std::vector<int>> m_PDGIDMother{this, "PDGIDMother", {}};
    Gaudi::Property<float>            m_minMatchFraction{this, "minMatchFraction", 0.0};
    Gaudi::Property<float>            m_minEnergy{this, "minEnergy", 0.0};
    Gaudi::Property<std::string>      m_tuplePrefix{this, "tuplePrefix", "matchedClusters"};

    // counters
    mutable Gaudi::Accumulators::BinomialCounter<int>     m_counterMatched{this, "Matched"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaEOverE{this, "DeltaEOverE"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaX{this, "DeltaX"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaY{this, "DeltaY"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_writefailed{this, "Cannot write tuple"};
    // ==========================================================================
  };

  DECLARE_COMPONENT_WITH_ID( ClusterResolution<CaloClusters>, "CaloClusterResolution" )
  DECLARE_COMPONENT_WITH_ID( ClusterResolution<Event::Calo::Clusters>, "CaloFutureClusterResolution" )
  DECLARE_COMPONENT_WITH_ID( ClusterResolution<CaloHypos>, "CaloHypoResolution" )
  DECLARE_COMPONENT_WITH_ID( ClusterResolution<Event::Calo::Hypotheses>, "CaloFutureHypoResolution" )
} // namespace LHCb::Calo
// ============================================================================
// The END
// ============================================================================
