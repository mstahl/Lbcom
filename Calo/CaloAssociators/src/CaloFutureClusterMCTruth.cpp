/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloMCTools.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigit.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "Relations/RelationWeighted1D.h"
// ============================================================================
/** @class Cluster2MCParticle Cluster2MCParticle.cpp
 *
 *  Simple class to create CaloCluster->MCParticle
 *  relation table basing on information from
 *  CaloDigit->MCParticle relations table
 *
 *  The important  properties of algorithm:
 *
 *    - "Input"
 *      The default value is "Relations/" + LHCb::CaloDigitLocation::Default
 *      The name of relation table CaloDigit->MCParticle
 *
 *    - "Output"
 *      The default value "Relations/" + LHCb::CaloClusterLocation::Default
 *      The name of (output) relation table CaloCluster->MCParticle
 *
 *    - "Clusters"
 *      The default value is CaloClusterLocation :: Ecal  + EcalSplit
 *      list of addresses fro CaloClusters containers
 *
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
 *  @date   2005-05-03
 */
namespace LHCb::Calo::Associators {
  class Cluster2MCParticle : public Gaudi::Functional::Transformer<LHCb::CaloFuture2MC::ClusterTable(
                                 const LHCb::CaloFuture2MC::DigitTable&, const LHCb::Event::Calo::Clusters& )> {
    // ==========================================================================
    // set the appropriate default values for input data
    Gaudi::Property<int> m_sFilter{this, "StatusFilter", static_cast<int>( LHCb::CaloDigitStatus::Mask::UseForEnergy )};
    Gaudi::Property<double> m_wFilter{this, "WeightFilter", 0};
    // ==========================================================================
    mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cc2mcLinkscounter{this, "#CC2MC links"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>       m_emptyTable{this, "Empty Relation table"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>       m_no_mc_for_cluster{
        this, "No MC information for the cluster is found", 1};

  public:
    // ==========================================================================
    /** standard constructor
     *  @param name algorithm instance name
     *  @param pSvc pointer to service locator
     */
    Cluster2MCParticle( const std::string& name, ISvcLocator* pSvc )
        : Transformer{name,
                      pSvc,
                      {KeyValue{"Input", "Relations/" + LHCb::CaloDigitLocation::Default},
                       KeyValue{"Clusters", LHCb::Event::Calo::ClusterLocation::EcalOverlap}},
                      KeyValue{"Output", "Relations/" + LHCb::CaloClusterLocation::Default}} {}

    /// algorithm execution
    LHCb::CaloFuture2MC::ClusterTable operator()( const LHCb::CaloFuture2MC::DigitTable& digTable,
                                                  const LHCb::Event::Calo::Clusters&     clusters ) const override {
      using namespace LHCb::CaloDataFunctor;
      using namespace CaloMCTools;

      // create and register the relation table
      LHCb::CaloFuture2MC::ClusterTable table{1000};

      // loop over all clusters in the container
      for ( const auto& cluster : clusters ) {

        // auxillary container to collect all links from individual digits
        std::map<const LHCb::MCParticle*, double> mcMap;

        // loop over all digits in the cluster and collect
        // the links from individual  digits
        for ( const auto& entry : cluster.entries() ) {

          if ( m_sFilter >= 0 && entry.status().noneOf( LHCb::CaloDigitStatus::Status( m_sFilter ) ) ) continue;

          // get all MC relations from this entry / digit
          // loop over all MC entries and collect the enegy
          for ( const auto& [_, particle, energy] : digTable.relations( entry.cellID() ) ) {
            // accumulate the energy from the same particle
            auto [it, ok] = mcMap.try_emplace( particle, energy );
            if ( !ok ) it->second += energy;
          }

        } // end of loop over all entries in Cluster

        if ( mcMap.empty() ) ++m_no_mc_for_cluster;

        // loop over auxillary container of merged depositions
        // and fill relation table entries for given cluster
        // MC particle & its cumulative energy deposition to the cluster
        for ( const auto& [particle, energy] : mcMap ) {
          if ( !particle ) { continue; }
          if ( cluster.e() > 0 && energy < m_wFilter * cluster.e() ) { continue; }

          // fill the relation table:         ATTENTION "i_push" is used!
          table.i_push( cluster.cellID(), particle, energy ); // NB: "i_push"
        }
      } // end of loop over clusters

      // mandatory after "i_push" ;
      table.i_sort(); // NB: "i_sort"

      m_emptyTable += table.relations().empty();

      // count number of links
      m_cc2mcLinkscounter += table.relations().size();

      return table;
    }
  };
  // ============================================================================
  DECLARE_COMPONENT_WITH_ID( Cluster2MCParticle, "CaloFutureClusterMCTruth" )
  // ============================================================================
  // algorithm execution
  // ============================================================================
} // namespace LHCb::Calo::Associators
// ============================================================================
// The END
// ============================================================================
