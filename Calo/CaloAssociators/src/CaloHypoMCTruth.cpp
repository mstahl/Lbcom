/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloUtils/Calo2MC.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/Transformer.h"
#include "Linker/LinkerWithKey.h"
#include "Relations/RelationWeighted1D.h"

/** @class CaloHypoMCTruth CaloHypoMCTruth.h
 *
 *  Simple algorithm to build Linkers for CaloHypo -> MCParticle relations
 *
 *  @author Olivier Deschamps
 *  @date   2012-06-24
 */

class CaloHypoMCTruth
    : public Gaudi::Functional::MultiTransformer<std::tuple<LHCb::Calo2MC::HypoTable, LHCb::LinksByKey>(
          const LHCb::CaloHypos&, const LHCb::Calo2MC::ClusterTable& )> {
public:
  /// Standard constructor
  CaloHypoMCTruth( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          {KeyValue{"CaloHypos", LHCb::CaloHypoLocation::Electrons},
                           KeyValue{"Input", "Relations/" + LHCb::CaloClusterLocation::Default}},
                          {KeyValue{"Output", "Relations/" + LHCb::CaloHypoLocation::Default},
                           KeyValue{"OutputLink", "/Event/Link/" + LHCb::CaloHypoLocation::Electrons}} ) {}

  std::tuple<LHCb::Calo2MC::HypoTable, LHCb::LinksByKey>
  operator()( const LHCb::CaloHypos&, const LHCb::Calo2MC::ClusterTable& ) const override;
};
// ============================================================================

// ============================================================================
// declare algorithm factory
// ============================================================================
DECLARE_COMPONENT( CaloHypoMCTruth )
// ============================================================================

// ============================================================================
/// algorithm execution
// ============================================================================
std::tuple<LHCb::Calo2MC::HypoTable, LHCb::LinksByKey> CaloHypoMCTruth::
                                                       operator()( const LHCb::CaloHypos& hypos, const LHCb::Calo2MC::ClusterTable& cluster2MC ) const {
  LHCb::Calo2MC::HypoTable table( 1000 );
  int                      links     = 0;
  std::string              container = hypos.name();
  LHCb::LinksByKey         linksByKey;
  linksByKey.setSourceClassID( LHCb::CaloHypo::classID() );
  linksByKey.setTargetClassID( LHCb::MCParticle::classID() );
  for ( const auto hypo : hypos ) {
    int hlinks = 0;
    for ( const auto& ir : cluster2MC.relations( LHCb::CaloAlgUtils::ClusterFromHypo( hypo ) ) ) {
      if ( hypo && ir.to() ) {
        int srcIndex   = hypo->index();
        int srcLinkID  = linksByKey.linkID( hypo->parent() );
        int destIndex  = ir.to()->index();
        int destLinkID = linksByKey.linkID( ir.to()->parent() );
        linksByKey.addReference( srcIndex, srcLinkID, destIndex, destLinkID, ir.weight() );
      }
      hlinks++;
      links++;
      table.i_push( hypo, ir.to(), ir.weight() );
    }
    counter( "#links/hypo " + container ) += hlinks;
  }
  table.i_sort(); // NB: "i_sort"
  counter( "#links " + container ) += links;
  return {table, linksByKey};
}
// ============================================================================
