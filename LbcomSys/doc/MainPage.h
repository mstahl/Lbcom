/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor lbcomdoxygenmain
 *
 * This is the code reference manual for %LHCb component libraries that are
 * shared by several applications. See:
 * <a href="requirements-source.html"><b>Lbcom constituent packages</b></a>
 * <p><b>See also:</b>
 * \li \ref lhcbdoxygenmain  "LHCbSys documentation (LHCb core packages)"
 * \li \ref gaudidoxygenmain "Gaudi documentation (Framework packages)"
 * \li \ref externaldocs     "Related external libraries"
 * <hr>
 * \htmlinclude new_release.notes
 * <hr>
 *
 * <b>Additional information:</b>
 * \li <a href="../release.notes"><b>Release notes history</b></a>
 * \li <a href=" http://cern.ch/lhcb-release-area/DOC/lbcom/"><b>Lbcom project Web pages</b></a><p>

 */
