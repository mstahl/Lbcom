/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "MuonDAQ/IMuonRawBuffer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonCoord2MCParticleLink
//
// 2005-12-29 : Alessia Satta
//-----------------------------------------------------------------------------

/** @class MuonCoord2MCParticleLink MuonCoord2MCParticleLink.h
 *
 *
 *  @author Alessia Satta
 *  @date   2005-12-30
 */
class MuonCoord2MCParticleLink : public GaudiAlgorithm {
public:
  /// Standard constructor
  MuonCoord2MCParticleLink( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode execute() override; ///< Algorithm execution
private:
  std::map<const LHCb::MCParticle*, double> associateToTruth( LinkedTo<LHCb::MCParticle, LHCb::MuonDigit>&,
                                                              const LHCb::MuonCoord& coord );
  std::vector<LHCb::MuonTileID>             list_digit;
  ToolHandle<IMuonRawBuffer>                m_muonBuffer{this, "MuonRawBuffer", "MuonRawBuffer"};
  DataObjectReadHandle<LHCb::MuonCoords>    m_coords{this, "MuonCoordLocation", LHCb::MuonCoordLocation::MuonCoords};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonCoord2MCParticleLink )

using namespace LHCb;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonCoord2MCParticleLink::MuonCoord2MCParticleLink( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonCoord2MCParticleLink::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  LinkerWithKey<LHCb::MCParticle, LHCb::MuonCoord> myLink( eventSvc(), msgSvc(),
                                                           rootInTES() + LHCb::MuonCoordLocation::MuonCoords );

  // get the digit link
  LinkedTo<LHCb::MCParticle, LHCb::MuonDigit> digitLink( evtSvc(), msgSvc(), LHCb::MuonDigitLocation::MuonDigit );
  if ( digitLink.notFound() ) info() << " my link not found " << endmsg;
  StatusCode sc = m_muonBuffer->getTile( list_digit );
  if ( sc.isFailure() ) return sc;
  // get the list of coords
  auto* coords = m_coords.get();

  // loop and link MuonDigits to MC truth
  for ( auto& coord : *coords ) {
    for ( auto [particle, weight] : associateToTruth( digitLink, *coord ) ) myLink.link( coord, particle, weight );
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
std::map<const LHCb::MCParticle*, double>
MuonCoord2MCParticleLink::associateToTruth( LinkedTo<LHCb::MCParticle, LHCb::MuonDigit>& digitLink,
                                            const MuonCoord&                             coord ) {

  std::map<const LHCb::MCParticle*, double> partMap;
  MuonTileID                                tile    = coord.key();
  int                                       station = tile.station();
  int                                       region  = tile.region();
  int                                       typeLay = -1;
  switch ( station ) {
  case 0:
    typeLay = 0;
    break;
  case 1:
    typeLay = 1;
    break;
  case 2:
    typeLay = 1;
    break;
  case 3:
    typeLay = ( region == 0 ? 0 : 1 );
    break;
  case 4:
    typeLay = ( region == 0 ? 0 : 1 );
    break;
  default:
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Invalid layer/region" << endmsg;
    return partMap;
  }
  if ( typeLay == 0 ) {
    // correspondance one to one digit coords
    for ( MuonTileID digitile : list_digit ) {
      if ( ( digitile.intercept( tile ) ).isValid() ) {
        if ( const MCParticle* pplink = digitLink.first( digitile ); pplink ) partMap[pplink] = 1.0;
        break;
      }
    }
  } else if ( coord.uncrossed() ) {
    for ( MuonTileID digitile : list_digit ) {
      if ( ( digitile.intercept( tile ) ).isValid() ) {
        if ( const MCParticle* pplink = digitLink.first( digitile ); pplink ) partMap[pplink] = 1.0;
        break;
      }
    }
  } else {
    // check if one or two
    for ( MuonTileID digitile : list_digit ) {
      if ( ( digitile.intercept( tile ) ).isValid() ) {
        if ( const MCParticle* pplink = digitLink.first( digitile ); pplink ) partMap[pplink] += 0.5;
      }
    }
  }
  return partMap;
}
