/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/IntLink.h"
#include "Event/MCMuonDigit.h"
#include "Event/MuonDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonTileDigitInfo
//
// 2006-01-22 : Alessio Sarti
//-----------------------------------------------------------------------------

/** @class MuonTileDigitInfo MuonTileDigitInfo.h
 *
 *
 *  @author Alessia Satta
 *  @date   2005-12-30
 */
class MuonTileDigitInfo : public GaudiAlgorithm {
public:
  /// Standard constructor
  MuonTileDigitInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution
private:
  DataObjectReadHandle<LHCb::MuonDigits>   m_digits{this, "MuonDigitLocation", LHCb::MuonDigitLocation::MuonDigit};
  DataObjectReadHandle<LHCb::MCMuonDigits> m_mcDigits{this, "MCMuonDigitLocation",
                                                      LHCb::MCMuonDigitLocation::MCMuonDigit};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonTileDigitInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonTileDigitInfo::MuonTileDigitInfo( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonTileDigitInfo::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  auto*              myIntLink = new LHCb::IntLink();
  std::map<int, int> mylink; ///< list of linked ints

  auto* digits   = m_digits.get();
  auto* mcDigits = m_mcDigits.get();

  // loop and link MuonDigits to MC truth
  LHCb::MuonDigits::const_iterator iDigit;
  for ( iDigit = digits->begin(); iDigit != digits->end(); iDigit++ ) {

    // match the MCMuonDigit to the MuonDigit via the Key
    LHCb::MCMuonDigit* mcDigit = mcDigits->object( ( *iDigit )->key() );
    if ( !mcDigit ) {
      error() << "Could not find the match for " << ( *iDigit )->key() << " in "
              << LHCb::MCMuonDigitLocation::MCMuonDigit << endmsg;
      return StatusCode::FAILURE;
    }

    unsigned int digitinfo = mcDigit->DigitInfo().DigitInfo();
    long int     tile      = ( *iDigit )->key();

    mylink.insert( std::make_pair( tile, digitinfo ) );
  }
  const std::map<int, int>& myList = mylink;
  myIntLink->setLink( myList );
  std::string path = LHCb::MCMuonDigitLocation::MCMuonDigit + "Info";

  put( myIntLink, path.data() );
  return StatusCode::SUCCESS;
}
