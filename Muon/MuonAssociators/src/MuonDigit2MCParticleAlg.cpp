/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCMuonDigit.h"
#include "Event/MCParticle.h"
#include "Event/MuonDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Linker/LinkerWithKey.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonDigit2MCParticleAlg
//
// 2005-12-29 : Alessia Satta
//-----------------------------------------------------------------------------

/** @class MuonDigit2MCParticleAlg MuonDigit2MCParticleAlg.h
 *
 *
 *  @author Alessia Satta
 *  @date   2005-12-30
 */
class MuonDigit2MCParticleAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  MuonDigit2MCParticleAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  const LHCb::MCParticle*                  associateToTruth( const LHCb::MCParticles& mcParticles,
                                                             const LHCb::MCMuonDigit& mcDigit ) const;
  Gaudi::Property<bool>                    m_associateAll{this, "AssociateAll", true};
  DataObjectReadHandle<LHCb::MuonDigits>   m_digits{this, "MuonDigitLocation", LHCb::MuonDigitLocation::MuonDigit};
  DataObjectReadHandle<LHCb::MCParticles>  m_mcParts{this, "MCParticleLocation", LHCb::MCParticleLocation::Default};
  DataObjectReadHandle<LHCb::MCMuonDigits> m_mcDigits{this, "MCMuonDigitLocation",
                                                      LHCb::MCMuonDigitLocation::MCMuonDigit};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonDigit2MCParticleAlg )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonDigit2MCParticleAlg::MuonDigit2MCParticleAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonDigit2MCParticleAlg::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  auto* digits      = m_digits.get();
  auto* mcParticles = m_mcParts.get();
  auto* mcDigits    = m_mcDigits.get();

  LinkerWithKey<LHCb::MCParticle, LHCb::MuonDigit> myLink( eventSvc(), msgSvc(),
                                                           rootInTES() + LHCb::MuonDigitLocation::MuonDigit );

  // loop and link MuonDigits to MC truth
  for ( auto& digit : *digits ) {
    // match the MCMuonDigit to the MuonDigit via the Key
    const LHCb::MCMuonDigit* mcDigit = mcDigits->object( digit->key() );
    if ( !mcDigit ) {
      error() << "Could not find the match for " << digit->key() << " in " << LHCb::MCMuonDigitLocation::MCMuonDigit
              << endmsg;
      continue;
    }
    if ( auto mcpart = associateToTruth( *mcParticles, *mcDigit ); mcpart ) { myLink.link( digit, mcpart ); }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
const LHCb::MCParticle* MuonDigit2MCParticleAlg::associateToTruth( const LHCb::MCParticles& mcParticles,
                                                                   const LHCb::MCMuonDigit& mcDigit ) const {

  // debug()<<" qui "<<endmsg;
  for ( const LHCb::MCHit* mcHit : mcDigit.mcHits() ) {
    // check the MCMuonHit is still available
    if ( !mcHit ) continue;
    const LHCb::MCParticle* mcPart = mcHit->mcParticle();
    // check found mcParticle
    if ( !mcPart ) continue;
    // check in the current event container
    if ( mcPart->parent() != &mcParticles ) continue;
    // check if muon
    if ( mcPart->particleID().abspid() != 13 ) continue;
    // if muon then ok and exit
    return mcPart;
  }
  // no muon then check the origin of the digit geant hit in current event?
  LHCb::MCMuonDigitInfo digitinfo = mcDigit.DigitInfo();
  if ( digitinfo.isGeantHit() ) {
    if ( digitinfo.doesFiringHitBelongToCurrentEvent() || m_associateAll.value() ) {
      auto itHistory = mcDigit.HitsHistory().begin();
      for ( const LHCb::MCHit* mcHit : mcDigit.mcHits() ) {
        if ( mcHit && itHistory->hasFired() ) {
          if ( const LHCb::MCParticle* mcPart = mcHit->mcParticle(); mcPart ) return mcPart;
        }
        ++itHistory;
      }
    }
  }
  return nullptr;
}
