/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/IntLink.h"
#include "Event/MCMuonDigit.h"
#include "Event/MuonDigit.h"
#include "GaudiAlg/GaudiTool.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/IMuonPad2MCTool.h" // Interface
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonDAQHelper.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonPad2MCTool
//
// 2006-12-06 : Alessia Satta
//-----------------------------------------------------------------------------

/** @class MuonPad2MCTool MuonPad2MCTool.h
 *
 *
 *  @author Alessia Satta
 *  @date   2006-12-06
 */
class MuonPad2MCTool : public extends<GaudiTool, IMuonPad2MCTool> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode        initialize() override;
  LHCb::MCParticle* Pad2MC( LHCb::MuonTileID value ) const override;

  bool              isXTalk( LHCb::MuonTileID value, LHCb::MCParticle*& pp ) const override;
  LHCb::MCParticle* PadNoXtalk2MC( LHCb::MuonTileID value ) const override;

private:
  void XtalkStrip( LHCb::MuonTileID tile, LHCb::MCParticle*& pp ) const;
  void XtalkPad( LHCb::MuonTileID tile, LHCb::MCParticle*& pp ) const;

  DeMuonDetector*                     m_muonDetector = nullptr;
  DataObjectReadHandle<LHCb::IntLink> m_links{this, "IntLinkLocation", LHCb::MCMuonDigitLocation::MCMuonDigit + "Info"};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonPad2MCTool )

using namespace LHCb;

//=============================================================================

StatusCode MuonPad2MCTool::initialize() {
  return extends::initialize().andThen(
      [&] { m_muonDetector = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" ); } );
}

MCParticle* MuonPad2MCTool::PadNoXtalk2MC( LHCb::MuonTileID tile ) const {
  MCParticle*                                 pp      = nullptr;
  MCParticle*                                 pplink  = nullptr;
  MCParticle*                                 ppfirst = nullptr;
  LinkedTo<LHCb::MCParticle, LHCb::MuonDigit> myLink( evtSvc(), msgSvc(), LHCb::MuonDigitLocation::MuonDigit );
  if ( myLink.notFound() ) info() << " my link not found " << endmsg;

  LHCb::MuonTileID strips[2] = {LHCb::MuonTileID{}, LHCb::MuonTileID{}};
  StatusCode       sc        = m_muonDetector->getDAQInfo()->findStrips( tile, strips );
  if ( sc.isFailure() ) return pp;

  bool first  = true;
  bool second = false;
  if ( strips[0].isValid() ) {
    pplink = myLink.first( strips[0] );
    if ( first ) {
      ppfirst = pplink;
      first   = false;
    }
  }
  if ( strips[1].isValid() ) {
    pplink = myLink.first( strips[1] );
    if ( first ) {
      if ( pplink ) return pplink;
      ppfirst = pplink;
      first   = false;
    } else {
      second = true;
      if ( pplink && pplink == ppfirst ) return pplink;
    }
  }
  return ( !second && ppfirst ) ? ppfirst : pp;
}

MCParticle* MuonPad2MCTool::Pad2MC( LHCb::MuonTileID tile ) const {

  LinkedTo<LHCb::MCParticle, LHCb::MuonDigit> myLink( evtSvc(), msgSvc(), LHCb::MuonDigitLocation::MuonDigit );
  if ( myLink.notFound() ) info() << " my link not found " << endmsg;

  LHCb::MuonTileID strips[2] = {LHCb::MuonTileID{}, LHCb::MuonTileID{}};
  StatusCode       sc        = m_muonDetector->getDAQInfo()->findStrips( tile, strips );
  if ( sc.isFailure() ) return nullptr;

  MCParticle* pp = nullptr;
  if ( strips[0].isValid() ) {
    MCParticle* pplink = myLink.first( strips[0] );
    if ( pplink ) {
      if ( pplink->particleID().abspid() == 13 ) return pplink;
    }
    if ( pplink ) pp = pplink;
  }

  return pp;
}

bool MuonPad2MCTool::isXTalk( LHCb::MuonTileID tile, MCParticle*& pp ) const {

  bool xt = false;
  if ( tile.station() == 0 ) {
    XtalkPad( tile, pp );
    if ( pp ) xt = true;
  } else if ( tile.station() == 3 && tile.region() == 0 ) {
    XtalkPad( tile, pp );
    // if(pp!=NULL)info()<<"found a muon "<<pp->particleID().pid()<<endmsg;
    if ( pp ) xt = true;

  } else if ( tile.station() == 4 && tile.region() == 0 ) {
    XtalkPad( tile, pp );
    if ( pp ) xt = true;

  } else {
    XtalkStrip( tile, pp );
    if ( pp ) xt = true;
  }
  return xt;
}

void MuonPad2MCTool::XtalkPad( MuonTileID tile, MCParticle*& pp ) const {
  auto*           link   = m_links.get();
  int             stored = link->link( tile );
  MCMuonDigitInfo mc;
  mc.setDigitInfo( stored );
  if ( mc.isXTalkHit() ) {
    MuonTileID top    = tile.neighbourID( 0, 1 );
    MuonTileID bottom = tile.neighbourID( 0, -1 );
    MuonTileID left   = tile.neighbourID( -1, 0 );
    MuonTileID right  = tile.neighbourID( 1, 0 );
    if ( top.isValid() ) pp = Pad2MC( top );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( bottom.isValid() ) pp = Pad2MC( bottom );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( left.isValid() ) pp = Pad2MC( left );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( right.isValid() ) pp = Pad2MC( right );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
  }
}

void MuonPad2MCTool::XtalkStrip( MuonTileID tile, MCParticle*& pp ) const {
  MuonLayout layoutone( m_muonDetector->getLayoutX( 0, tile.station(), tile.region() ),
                        m_muonDetector->getLayoutY( 0, tile.station(), tile.region() ) );
  MuonLayout layoutdue( m_muonDetector->getLayoutX( 1, tile.station(), tile.region() ),
                        m_muonDetector->getLayoutY( 1, tile.station(), tile.region() ) );
  MuonTileID uno  = tile.containerID( layoutone );
  MuonTileID due  = tile.containerID( layoutdue );
  auto*      link = m_links.get();
  //    info()<<" link found"<<endmsg;
  int storedone = link->link( uno );
  //    info()<<storedone<<endmsg;
  MCMuonDigitInfo mcone;
  mcone.setDigitInfo( storedone );
  if ( mcone.isXTalkHit() ) {
    //      info()<<" cross talk hits !!!"<<endmsg;
    MuonTileID top    = uno.neighbourID( 0, 1 );
    MuonTileID bottom = uno.neighbourID( 0, -1 );
    MuonTileID left   = uno.neighbourID( -1, 0 );
    MuonTileID right  = uno.neighbourID( 1, 0 );
    if ( top.isValid() ) pp = Pad2MC( top );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( bottom.isValid() ) pp = Pad2MC( bottom );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( left.isValid() ) pp = Pad2MC( left );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( right.isValid() ) pp = Pad2MC( right );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
  }
  int             storeddue = link->link( due );
  MCMuonDigitInfo mcdue;
  mcdue.setDigitInfo( storeddue );
  if ( mcdue.isXTalkHit() ) {
    //      info()<<" cross talk hits !!!"<<endmsg;
    MuonTileID top    = due.neighbourID( 0, 1 );
    MuonTileID bottom = due.neighbourID( 0, -1 );
    MuonTileID left   = due.neighbourID( -1, 0 );
    MuonTileID right  = due.neighbourID( 1, 0 );
    if ( top.isValid() ) pp = Pad2MC( top );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( bottom.isValid() ) pp = Pad2MC( bottom );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( left.isValid() ) pp = Pad2MC( left );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
    if ( right.isValid() ) pp = Pad2MC( right );
    if ( pp && std::abs( pp->particleID().pid() ) == 13 ) return;
  }
}
